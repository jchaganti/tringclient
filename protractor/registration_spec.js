describe('Testing the Registration tab', function() {
    it('should be able to click on the Enter tab', function() {
		element(by.model('device.mobileNo')).sendKeys(9850820340);
		element(by.id('register')).click().then(function() {
		
		var EC = protractor.ExpectedConditions;
		var verifyPin1 = element(by.id('verifyPin'));
		browser.wait(EC.elementToBeClickable(verifyPin1), 5000);
		
        expect(browser.getLocationAbsUrl()).toMatch('/pin');
		});
    });

    it('Enter the PIN', function() {	  
	
	  var EC = protractor.ExpectedConditions;
	  var devicePin = element(by.model('device.pin'));
	  browser.wait(EC.elementToBeClickable(devicePin), 5000);
	  
      element(by.model('device.pin')).sendKeys(1729);
	  element(by.model('device.pin')).sendKeys("\uE004");
	  
      element(by.id('verifyPin')).click();     
    });
});
