describe('ManageSubGroupTests', function () {
  var chatId;
  var parentGroup;
  var chatService;
  var constsService;
  var $scope;
  var dbService;
  var mqttBrokerService;
  var $q;
  var httpBackend;
  var syncId = "1a2b3c4d5e6f";
  beforeEach(module('ngCordovaMocks'));
  beforeEach(module('tring.chatService'));
  beforeEach(module('tring.sharedDataService'));
  beforeEach(module('tring.appServices'));
  beforeEach(module('tring.chatService'));
  beforeEach(module('tring.mqttBrokerService'));
  beforeEach(module('TestUtils'));

  beforeEach(inject(function (AuthService) {
    device = {
      did: "abcdef123456",
      host: "43.242.215.204",
      port: 8000,
      p2p: '/p2p/1/testuser'
    };
    AuthService.setDevice(device);
  }));

  beforeEach(inject(function (_$rootScope_, _$q_, ChatService, ConstsService, DBService, $httpBackend, AuthService, MqttBrokerService) {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    chatService = ChatService;
    dbService = DBService;
    constsService = ConstsService;
    $scope = _$rootScope_.$new();
    $q = _$q_;
    httpBackend = $httpBackend;

    mqttBrokerService = MqttBrokerService
    var deleteSyncMsgUrl = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL + '/' + syncId;

    httpBackend.whenDELETE(deleteSyncMsgUrl).respond({
      success: true
    });

    var deleteSyncAdminUrl = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_ADMIN_URL + '/' + syncId;

    httpBackend.whenDELETE(deleteSyncAdminUrl).respond({
      success: true
    });
  }));

  beforeEach(function (done) {
    var testIsDone = {
      value: false
    };
    dbService.initMbrDB(true).then(function (res) {
      console.log("dbService.initMbrDB successful");
      dbService.initMsgDB(true).then(function (_res) {
        console.log("dbService.initMsgDB successful");
        testIsDone.value = true;
        done();
      });
    }).catch(function (err) {
      console.log("Error Recreated DB..");
    });
    TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
  });

  var gid = "parent_grp";
  var parentGroupId = ConstsService.GROUPS_START_KEY + gid;
  parentGroup = {
    _id: parentGroupId,
    name: "Parent Group",
    membershipType: ConstsService.OWNER_ROLE,
    handleName: "parentGroupHandleName",
    topic: "/group/1/" + parentGroupId
  }
  var subGroupId = "subgroup_01";

  var syncMsg = {
    _id: syncId,
    memberType: ConstsService.MODERATOR_ROLE,
    subGroupId: subGroupId,
    pgid: gid,
    name: "Sub Group 01",
    topic: "/group/1/" + subGroupId,
    type: 2,
    appMsgType: 256
  }

  var syncAdminMsg = {
    _id: syncId,
    gid: gid,
    type: 2,
    appMsgType: 128
  }

  describe("Manage SubGroups", function () {

    // 1. Save parent group
    // 2. Save parent group chat
    // 3. Assert parent group exists
    // 4. Create SubGroup sync message (ADD as moderator) and call onMessageArrived
    // 5. Assert sub group exists
    // 6. Test the membershipType of sub group
    // 7. Test the membershipType of parent group

    function createParentGroup() {
      var p0 = function () {
        var mbrDb = dbService.mbrDb();
        return mbrDb.put(parentGroup);
      }
      var chatId = constsService.CHAT_PREFIX + parentGroupId;

      var p1 = function () {
        return chatService.saveGroupChat(parentGroup, chatId);
      }

      var p2 = function (chat) {
        console.log("The response from saveGroupChat: " + chat.sid);
        return chatService.findById(chat.sid);
      }


      return {p0: p0, p1: p1, p2: p2};
    }

    function testHandleAdmin(testIsDone) {
      var functions = [];

      var __ret = createParentGroup();
      functions.push(__ret.p0);
      functions.push(__ret.p1);
      functions.push(__ret.p2);

      var p3 = function (_parentGroup) {
        console.log("The response from chatService.findById: " + JSON.stringify(_parentGroup));
        expect(_parentGroup._id).toBe(parentGroup._id);
        var isSyncAdminMsg = Tring.isSyncUpAdminRoleMessage(syncAdminMsg);
        console.log("isSyncAdminMsg: " + isSyncAdminMsg);
        expect(isSyncAdminMsg).toBe(true);
        return mqttBrokerService.handleAdminRoleSyncUpMessage(syncAdminMsg);
      }
      functions.push(p3);

      var p4 = function (success) {
        console.log("The handleAdminRoleSyncUpMessage: " + success)
        if (success) {
          var promise = mqttBrokerService.deleteSyncId(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_ADMIN_URL, syncMsg._id).then(function (res) {
            console.log("Delete syncId response: " + JSON.stringify(res.data));
            return $q.when(res.data.success);
          });
          httpBackend.flush();
          return promise;
        }
        else {
          return $q.when(false);
        }
      }
      functions.push(p4);

      var p5 = function (success) {
        return chatService.findById(parentGroupId).then(function (_parentGroup) {
          if (syncAdminMsg.action === 'add') {
            expect(_parentGroup.membershipType).toEqual(constsService.ADMIN_ROLE);
          }
          else {
            expect(_parentGroup.membershipType).toEqual(constsService.MEMBER_ROLE);
          }
          return $q.when(true);
        });
      }

      $q.serial(functions).then(function (res) {
        console.log("testIsDone: " + res);
        testIsDone.value = res;
      }).catch(function (err) {
        console.log("Error found!!: " + JSON.stringify(err))
      });

    }

    function testSubGroupCreation(testIsDone, editedRole, isParentGroupMember, errorMessage) {
      var functions = [];
      if (syncMsg.action === 'CREATE' || (syncMsg.action === 'EDIT' &&
        editedRole)) {
        var __ret = createParentGroup();
        functions.push(__ret.p0);
        functions.push(__ret.p1);
        functions.push(__ret.p2);
      }
      else {
        var _p1 = function () {
          console.log("Edit Mode, finding parent group");
          return chatService.findById(parentGroup._id);
        }
        functions.push(_p1);
      }

      var p3 = function (_parentGroup) {
        console.log("The response from chatService.findById: " + JSON.stringify(_parentGroup));
        expect(_parentGroup._id).toBe(parentGroup._id);
        var isSyncMsg = Tring.isSyncUpSubgroupMessage(syncMsg);
        console.log("isSyncMsg: " + isSyncMsg);
        expect(isSyncMsg).toBe(true);
        return mqttBrokerService.handleSubgroupSyncUpMessage(syncMsg);
      }
      functions.push(p3);

      var p4 = function (success) {
        console.log("The handleSubgroupSyncUpMessage: " + success)
        if (success) {
          var promise = mqttBrokerService.deleteSyncId(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL, syncMsg._id).then(function (res) {
            console.log("Delete syncId response: " + JSON.stringify(res.data));
            return $q.when(res.data.success);
          });
          httpBackend.flush();
          return promise;
        }
        else {
          return $q.when(false);
        }
      }
      functions.push(p4);

      var p5 = function (success) {
        expect(success).toBe(true);
        return chatService.findById(parentGroupId);
      }
      functions.push(p5);

      var p6 = function (_parentGroup) {
        if (!editedRole) {
          console.log("_parentGroup: " + JSON.stringify(_parentGroup));
          console.log("isParentGroupMember: " + isParentGroupMember)
        }
        expect(_parentGroup).not.toBeNull();

        console.log("ACTION: " + syncMsg.action + " _parentGroup.membershipType: " + _parentGroup.membershipType + "  parentGroup.membershipType: " + parentGroup.membershipType);

        if (isParentGroupMember) {
          expect(_parentGroup.membershipType).toEqual(constsService.NON_MEMBER_ROLE);
        }
        else {
          expect(_parentGroup.membershipType).toBe(parentGroup.membershipType);
        }
        return chatService.findById(ConstsService.GROUPS_START_KEY + subGroupId);
      }
      functions.push(p6);

      var p7 = function (subGroup) {
        console.log("The subgroup: " + JSON.stringify(subGroup));
        expect(subGroup).toBeTruthy();
        expect(subGroup.membershipType).toBe(syncMsg.memberType);
        expect(subGroup.parentGroupId).toBe(parentGroupId);
        expect(subGroup.handleName).toBe(parentGroup.handleName);
        expect(subGroup.name).toBe(syncMsg.name);
        return $q.when(subGroup != null);
      }
      functions.push(p7);


      $q.serial(functions).then(function (res) {
        console.log("testIsDone: " + res);
        if (syncMsg.action === 'CREATE' && editedRole) {
          syncMsg.action = 'EDIT';
          chatService.findById(parentGroupId).then(function (_parentGroup) {
            console.log("_parentGroup.membershipType: " + _parentGroup.membershipType + " editedRole: " + editedRole);
            var _isParentGroupMember = (_parentGroup.membershipType === constsService.MEMBER_ROLE || _parentGroup.membershipType === constsService.NON_MEMBER_ROLE) &&
              (editedRole === constsService.NON_MEMBER_ROLE || editedRole === constsService.MEMBER_ROLE);
            console.log("_isParentGroupMember: " + _isParentGroupMember);
            syncMsg.memberType = editedRole;
            testSubGroupCreation(testIsDone, null, _isParentGroupMember);
          });

        }
        else {
          testIsDone.value = res;
        }
      }).catch(function (err) {
        var _msg = JSON.stringify(err);
        expect(err).toBe(errorMessage);
        testIsDone.value = true;
        console.log("Error found!!: " + JSON.stringify(err))
      });
    }

    it("should create sub group for owner with role as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.OWNER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should create sub group for admin with role as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.ADMIN_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should create sub group for member with role as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone)
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should create sub group for member with role as member", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MEMBER_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, null, true);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should create sub group for admin with role as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.ADMIN_ROLE;
      syncMsg.memberType = ConstsService.MEMBER_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, null, false, "Owner/Admin's role can only Moderator!!");
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for owner and change the role from moderator to nonmember", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.OWNER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, ConstsService.NON_MEMBER_ROLE);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for admin and change the role from moderator to nonmember", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.ADMIN_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, ConstsService.NON_MEMBER_ROLE);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for member and change the role from member to moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MEMBER_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, ConstsService.MODERATOR_ROLE, true);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for member and change the role from moderator to member", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, ConstsService.MEMBER_ROLE, false);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for member and change the role from moderator to nonmember", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'CREATE';
      testSubGroupCreation(testIsDone, ConstsService.NON_MEMBER_ROLE);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });


    it("should edit sub group for owner and add as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.OWNER;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'EDIT';
      testSubGroupCreation(testIsDone, ConstsService.MODERATOR_ROLE, false);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for admin and add as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.ADMIN_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'EDIT';
      testSubGroupCreation(testIsDone, ConstsService.MODERATOR_ROLE, false);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for member and and add as moderator", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MODERATOR_ROLE;
      syncMsg.action = 'EDIT';
      testSubGroupCreation(testIsDone, ConstsService.MODERATOR_ROLE, false);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should edit sub group for member and and add as member", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncMsg.memberType = ConstsService.MEMBER_ROLE;
      syncMsg.action = 'EDIT';
      testSubGroupCreation(testIsDone, ConstsService.MEMBER_ROLE, true);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should add member as admin", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.MEMBER_ROLE;
      syncAdminMsg.action = 'add';
      testHandleAdmin(testIsDone);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });

    it("should remove admin", function (done) {
      var testIsDone = {
        value: false
      };
      parentGroup.membershipType = ConstsService.ADMIN_ROLE;
      syncAdminMsg.action = 'remove';
      testHandleAdmin(testIsDone);
      TestUtils.runDigestHack(testIsDone, $scope, done, 30, true);
    });
  });

});
