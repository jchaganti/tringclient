//'tring.groupInvite.controller'
describe('Group Membership tests-', function () {
    var scope;
    var httpBackEnd;
    var cService, dbService, groupService;
    var window;
    var q;
    var rootScope;
    var controller;
    var handleName = "aHandle";
    var anotherHandleName = "bHandle";

    var msg = {};
    var url;
    // load the controller's module
    beforeEach(module('ngCordovaMocks'));
    beforeEach(module('tring.constService'));
    beforeEach(module('tring.dbService'));
    beforeEach(module('tring.appServices'));
    beforeEach(module('TestUtils'));
    beforeEach(module('Tring'));

    beforeEach(inject(function (AuthService) {
      device = {
        did: "abcdef123456",
        host: "43.242.215.204",
        port: 8000,
        p2p: '/p2p/1/testuser'
      };
      AuthService.setDevice(device);
    }));


  beforeEach(module('tring.groupInvite.controller'));
    beforeEach(inject(function ($window, $injector, $q, $rootScope, $controller, DBService, GroupService) {
        // Set up the mock http service responses
        httpBackEnd = $injector.get('$httpBackend');
        cService = ConstsService;
        window = $window;
        q = $q;
        rootScope = $rootScope;
        controller = $controller;
        dbService = DBService;
        groupService = GroupService;
    }));
    describe('Group membership acceptance/rejection post-processing - ', function () {
        var msgDataAccept = {
            gid: 'abcdefg',
            handleName: 'sachin',
            syncId: 'QWERTY',
            rejected: false,
            groupName: 'bigGroup',
            groupTopic: '/groups/1/group_abcdefg',
            groupO2M: false
        };
        var messageAccept = new Paho.MQTT.Message(JSON.stringify(msgDataAccept));
        var msgDataReject = angular.extend(angular.copy(msgDataAccept), {
            rejected: true
        });
        var messageReject = new Paho.MQTT.Message(JSON.stringify(msgDataReject));

        beforeEach(function (done) {
            var testIsDone = {
                value: false
            };
            q.when(dbService.initMbrDB(true)).finally(function () {
                testIsDone.value = true;
            });
            TestUtils.runDigestHack(testIsDone, rootScope, done);
        });

        //No point in configuring an expect url request for the deleteSyncId() since it is called async in
        //handleMembershipAcceptRejectMessage. Till we know how to do this, we have to live with it.
        // But this also means that it will be an unfulfilled
        //request hance done the finally block below will never be entered. But the 'done()' func
        //will still be called after a few turns in runDigestHack.
        //But, we can't have any expects, and have to stay contented with the console.log outputs
        //on stdout after running the tests
        it('accept membership message should go through', function (done) {
            var testIsDone = {
                value: false
            };
            //            url = cService.MEMBERSHIP_SERVER_URL + cService.MEMBERSHIP_ACCEPT_REJECT_SYNC_URL + msgDataAccept.syncId;
            //
            //            httpBackEnd.expectDELETE(url).response({
            //                200, {
            //                    success: true
            //                }
            //            });
            groupService.handleMembershipAcceptRejectMessage(msgDataAccept)
                .then(function () {})
                .catch(function (err) {})
                .finally(function () {
                    testIsDone.value = true;
                });
            //window.setTimer(httpBackEnd.flush.bind(httpBackEnd), 100);
            TestUtils.runDigestHack(testIsDone, rootScope, done);
        });

        it('reject membership should go through', function (done) {
            var testIsDone = {
                value: false
            };
            groupService.handleMembershipAcceptRejectMessage(msgDataReject)
                .then(function () {})
                .catch(function (err) {})
                .finally(function () {
                    testIsDone.value = true;
                });
            //httpBackEnd.flush();
            TestUtils.runDigestHack(testIsDone, rootScope, done);
        });
    });

    describe('GroupInviteController tests', function () {
        beforeEach(function (done) {
            var testIsDone = {
                value: false
            };
            msg.groupName = "Agroup";
            msg.gid = cService.GROUPS_START_KEY + "Agid";
            msg.groupTopic = "AgroupTopic";
            msg.groupO2M = false;
            msg.groupClosed = true;
            msg.groupDescription = "A groupDescription";
            msg.groupRules = "A groupRules";
            msg.appMsgType = cService.APP_MSG_TYPE_GROUP_INVITE;
            q.when(dbService.initMbrDB(true)).finally(function () {
                testIsDone.value = true;
            });
            TestUtils.runDigestHack(testIsDone, rootScope, done);

        });
        beforeEach(function () {
            scope = rootScope.$new();
            console.log('2. dbService is ' + dbService);
            controller('GroupInviteController', {
                $scope: scope
            });
            var testIsDone = {
                value: false
            };
            //mock the popup function
            scope.groupInviteDeciderPopup = function () {
                return q.when(handleName);
            };

            url = cService.CREATE_GROUP_MEMBER_URL.replace('@0', Tring.clientToServerGroupId(msg.gid));
            url = cService.MEMBERSHIP_SERVER_URL + url;
        });

        it("group membership and group chat records should be created with the required data", function (done) {
            var mbrDb = dbService.mbrDb();
            var testIsDone = {
                value: false
            };
            httpBackEnd.expectPOST(url, {
                handleName: handleName
            }).respond(200, {
                success: true,
                hid: "memberHid"
            });
            scope.$broadcast('groupInviteClicked', msg);
            httpBackEnd.flush();
            scope.resultPromise
                .then(function () {
                    console.log('GroupInviteController initialized and group membership accepted');
                    return q.when(mbrDb.get(msg.gid));
                })
                .then(function (grp) {
                    console.log('found grp ' + JSON.stringify(grp));
                    expect(grp.name).toEqual(msg.groupName);
                    expect(grp.type).toEqual(cService.MEMBER_ROLE);
                    expect(grp.topic).toEqual(msg.groupTopic);
                    expect(grp.handleName).toEqual(handleName);

                }).catch(function (err) {
                    expect(err).toBeUndefined();
                }).finally(function () {
                    testIsDone.value = true;
                });
            TestUtils.runDigestHack(testIsDone, scope, done);

        });
        describe('partial completion cases-', function () {
            beforeEach(function (done) {
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };
                q.when(mbrDb.put({
                    name: msg.groupName,
                    _id: msg.gid,
                    type: cService.MEMBER_ROLE,
                    topic: msg.groupTopic,
                    handleName: handleName
                })).then(function (grp) {
                    console.log("put a group");
                }).finally(function () {
                    testIsDone.value = true;
                });
                TestUtils.runDigestHack(testIsDone, scope, done);

            });
            it("group record present from previous attempt, so creation fails, but rest should go through", function (done) {
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };
                httpBackEnd.expectPOST(url, {
                    handleName: handleName
                }).respond(200, {
                    success: true,
                    hid: "memberHid"
                });
                scope.$broadcast('groupInviteClicked', msg);
                httpBackEnd.flush();
                scope.resultPromise
                    .then(function () {
                        console.log('GroupInviteController initialized and group membership accepted');
                        return q.when(mbrDb.get(msg.gid));
                    })
                    .then(function (grp) {
                        console.log('found grp ' + JSON.stringify(grp));
                        expect(grp.name).toEqual(msg.groupName);
                        expect(grp.type).toEqual(cService.MEMBER_ROLE);
                        expect(grp.topic).toEqual(msg.groupTopic);
                        expect(grp.handleName).toEqual(handleName);

                    }).catch(function (err) {
                        expect(err).toBeUndefined();
                    }).finally(function () {
                        testIsDone.value = true;
                    });
                TestUtils.runDigestHack(testIsDone, scope, done);

            });
            describe('both group and group chat present in local db from a previous attempt-', function () {
                beforeEach(function (done) {
                    var mbrDb = dbService.mbrDb();
                    var testIsDone = {
                        value: false
                    };
                    q.when(mbrDb.put({
                            _id: cService.CHAT_PREFIX + msg.gid,
                            sendertype: 2,
                            senderName: msg.groupName,
                            name: msg.groupName,
                            sid: msg.gid,
                            localSid: msg.gid
                        }))
                        .finally(function () {
                            testIsDone.value = true;
                        });
                    TestUtils.runDigestHack(testIsDone, scope, done);

                });
                it("even if group  and chat record creation fails rest should go through", function (done) {
                    var mbrDb = dbService.mbrDb();
                    var testIsDone = {
                        value: false
                    };
                    httpBackEnd.expectPOST(url, {
                        handleName: handleName
                    }).respond(200, {
                        success: true,
                        hid: "memberHid"
                    });
                    scope.$broadcast('groupInviteClicked', msg);
                    httpBackEnd.flush();
                    scope.resultPromise
                        .then(function () {
                            console.log('GroupInviteController initialized and group membership accepted');
                            return q.when(mbrDb.get(cService.CHAT_PREFIX + msg.gid));
                        })
                        .then(function (chat) {
                            console.log('found grp ' + JSON.stringify(chat));
                            expect(chat.name).toEqual(msg.groupName);
                            expect(chat.senderName).toEqual(msg.groupName);
                            expect(chat.sid).toEqual(msg.gid);
                            expect(chat.localSid).toEqual(msg.gid);

                        }).catch(function (err) {
                            console.log("errors caught- err is " + JSON.stringify(err));
                            expect(err).toBeUndefined();
                        }).finally(function () {
                            testIsDone.value = true;
                        });
                    TestUtils.runDigestHack(testIsDone, scope, done);

                });

            });
        });
    });
});
