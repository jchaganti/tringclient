describe('GroupChatTests', function () {
  beforeEach(module('ngCordovaMocks'));
  beforeEach(module('tring.message.controller'));
  beforeEach(module('TestUtils'));

  var msgController;
  var rootScope;
  var scope;
  var dbService;
  var constsService;
  var $q;
  var chatService;
  var gid = "parent_grp";
  var chat;
  var parentGroupId = ConstsService.GROUPS_START_KEY + gid;
  var $interval;
  var toHandleName = "mbrHandleName";
  var httpBackend;
  var parentGroup = {
    _id: parentGroupId,
    name: "Parent Group",
    membershipType: ConstsService.OWNER_ROLE,
    handleName: "parentGroupHandleName",
    topic: "/group/1/" + parentGroupId
  }

  beforeEach(inject(function (AuthService) {
    device = {
      did: "abcdef123456",
      host: "43.242.215.204",
      port: 8000,
      p2p: '/p2p/1/testuser'
    };
    AuthService.setDevice(device);
  }));

  beforeEach(inject(function ($rootScope, _$q_, DBService, ConstsService, ChatService, _$interval_, $httpBackend) {
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 20000;
    rootScope = $rootScope;
    //scope = rootScope.$new();
    $q = _$q_;
    dbService = DBService;
    constsService = ConstsService;
    chatService = ChatService;
    $interval = _$interval_;
    httpBackend = $httpBackend;

  }));

  function _createParentGroup(closed, o2m, role) {
    var p0 = function () {
      console.log("createParentGroup entered")
      var mbrDb = dbService.mbrDb();
      parentGroup.closed = closed;
      parentGroup.o2m = o2m;
      parentGroup.membershipType = role;
      return mbrDb.put(parentGroup);
    }
    var chatId = constsService.CHAT_PREFIX + parentGroupId;

    var p1 = function () {
      console.log("Added parentGroup");
      return chatService.saveGroupChat(parentGroup, chatId);
    }

    var p2 = function (_chat) {
      console.log("The response from saveGroupChat: " + _chat.sid);
      chat = _chat;
      return chatService.findById(chat.sid);
    }

    var p3 = function (_parentGroup) {
      console.log("The response from chatService.findById: " + JSON.stringify(_parentGroup));
      expect(_parentGroup._id).toBe(parentGroup._id);
      return $q.when(true);
    }
    return {p0: p0, p1: p1, p2: p2, p3: p3};
  }

  function createParentGroup(closed, o2m, role) {
    var functions = [];
    var ret = _createParentGroup(closed, o2m, role);
    functions.push(ret.p0);
    functions.push(ret.p1);
    functions.push(ret.p2);
    functions.push(ret.p3);
    return $q.serial(functions);
  }


  function initDBs(done) {
    var testIsDone = {
      value: false
    };
    dbService.initMbrDB(true).then(function (res) {
      console.log("dbService.initMbrDB successful");
      dbService.initMsgDB(true).then(function (_res) {
        console.log("dbService.initMsgDB successful");
        testIsDone.value = true;
        done();
      });

    }).catch(function (err) {
      console.log("Error Recreated DB..");
    });
    TestUtils.runDigestHack(testIsDone, scope, done, 30, true);
  }

  describe("M2MGroup Tests", function () {
    var closed = true;
    var open = !closed;
    var o2m = false;

    beforeEach(function (done) {
      scope = rootScope.$new();
      var getHandleDataUrl = ConstsService.MEMBERSHIP_SERVER_URL + "/group/" + gid + "/member/" + toHandleName;
      console.log("getHandleDataUrl: " + getHandleDataUrl);

      httpBackend.whenGET(getHandleDataUrl).respond({
        p2p: "/p2p/1/" + toHandleName
      });

      var reputationUrl = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL;
      httpBackend.whenPOST(reputationUrl).respond({
        success: true
      });
      initDBs(done);
    });

    function testOpenM2M(closed, role, done, setMessageTypeCB, scopeVarCheck) {
      inject(function ($controller) {
        var testIsDone = {
          value: false
        };
        createParentGroup(closed, o2m, role).then(function (res) {
          console.log("createParentGroup done")
          msgController = $controller('MessagesCtrl', {
            $scope: scope,
            $stateParams: {
              senderId: chat.sid,
              localSenderId: chat.sid
            }
          });
          expect(msgController).toBeDefined();
          scope.inputMessage = "Test Message!!!";
          scope.chat = chat;
          rootScope.networkStatus = true;
          setMessageTypeCB();
          TestUtils.trackTestStageCompletionStatus(function () {
            testIsDone.value = scopeVarCheck();
            return testIsDone.value;
          }, null, null, 20, 50);

        }, function (err) {
          expect(err).not.toBeDefined();
        });
        TestUtils.runDigestHack(testIsDone, scope, done, 30, true);
      });

    }

    it("should send private message to member as owner in open and m2m group", function (done) {
      testOpenM2M(open, constsService.OWNER_ROLE, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
        return scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to member as member in open and m2m group", function (done) {
      testOpenM2M(open, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
        return scope.messages && scope.messages.length == 1 && scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to moderator as member in open and m2m group", function (done) {
      testOpenM2M(open, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToModerator();
        scope.sendMessage();
      }, function () {
        return scope.messages && scope.messages.length == 1 && scope.pvtMsgSentToModerator;
      });
    });


    it("should report abuse to moderator as member in open and m2m group", function (done) {
      testOpenM2M(open, constsService.MEMBER_ROLE, done, function () {
        var message = {handleName: toHandleName, data:"You are a dumb ass!!"}
        scope.setReportAbuseMessage(message);
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      }, function () {
        return scope.messages && scope.messages.length >= 1 && scope.pvtMsgSentToModerator  && scope.reportAbuseDone;
      });
    });



    it("should send private message to member as owner in closed and m2m group", function (done) {
      testOpenM2M(closed, constsService.OWNER_ROLE, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
        return scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to member as member in closed and m2m group", function (done) {
      testOpenM2M(closed, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
        return scope.messages && scope.messages.length == 1 && scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to moderator as member in closed and m2m group", function (done) {
      testOpenM2M(closed, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToModerator();
        scope.sendMessage();
      }, function () {
        return scope.messages && scope.messages.length == 1 && scope.pvtMsgSentToModerator;
      });
    });


    it("should report abuse to moderator as member in closed and m2m group", function (done) {
      testOpenM2M(closed, constsService.MEMBER_ROLE, done, function () {
        var message = {handleName: toHandleName, data:"You are a dumb ass!!"}
        scope.setReportAbuseMessage(message);
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      }, function () {
        return scope.messages && scope.messages.length >= 1 && scope.pvtMsgSentToModerator  && scope.reportAbuseDone;
      });
    });
  });

  describe("O2MGroup Tests", function () {
    var closed = true;
    var open = !closed;
    var o2m = true;

    beforeEach(function (done) {
      scope = rootScope.$new();
      var getHandleDataUrl = ConstsService.MEMBERSHIP_SERVER_URL + "/group/" + gid + "/member/" + toHandleName;
      console.log("getHandleDataUrl: " + getHandleDataUrl);

      httpBackend.whenGET(getHandleDataUrl).respond({
        p2p: "/p2p/1/" + toHandleName
      });
      initDBs(done);
    });

    function testOpenO2M(closed, role, done, setMessageTypeCB, scopeVarCheck) {
      inject(function ($controller) {
        var testIsDone = {
          value: false
        };
        createParentGroup(closed, o2m, role).then(function (res) {
          msgController = $controller('MessagesCtrl', {
            $scope: scope,
            $stateParams: {
              senderId: chat.sid,
              localSenderId: chat.sid
            }
          });
          expect(msgController).toBeDefined();
          scope.inputMessage = "Test Message!!!";
          scope.chat = chat;
          rootScope.networkStatus = true;
          setMessageTypeCB();
          TestUtils.trackTestStageCompletionStatus(function () {

            testIsDone.value = scope.messages && scope.messages.length == 1 && scopeVarCheck();
            return testIsDone.value;
          }, null, null, 20, 50);
        }, function (err) {
          expect(err).not.toBeDefined();
        });
        TestUtils.runDigestHack(testIsDone, scope, done, 30);
      });
    }

    it("should send private message to member as owner in open and o2m group", function (done) {
      testOpenO2M(open, constsService.OWNER, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
          return scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to moderator as member in open and o2m group", function (done) {
      testOpenO2M(open, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToModerator();
        scope.sendMessage();
      },function () {
          return scope.pvtMsgSentToModerator
      });
    });

    it("should send private message to member as owner in closed and o2m group", function (done) {
      testOpenO2M(open, constsService.OWNER, done, function () {
        scope.setPrivateMessageToMember({handleName: toHandleName});
        scope.sendMessage();
        TestUtils.httpRequestTracker().track(httpBackend);
      },function () {
        return scope.pvtMessageToMemberSent;
      });
    });

    it("should send private message to moderator as member in closed and o2m group", function (done) {
      testOpenO2M(open, constsService.MEMBER_ROLE, done, function () {
        scope.setPrivateMessageToModerator();
        scope.sendMessage();
      },function () {
        return scope.pvtMsgSentToModerator
      });
    });
  });
});
