describe('UserReputationReportController', function () {
    var scope;
    var getReputationHandler;
    var url, httpBackend;
    // load the controller's module
    beforeEach(module('ngCordovaMocks'));
    beforeEach(module('tring.authService'));
    beforeEach(module('tring.constService'));
    beforeEach(inject(function (AuthService) {
      device = {
        did: "abcdef123456",
        host: "43.242.215.204",
        port: 8000,
        p2p: '/p2p/1/testuser'
      };
      AuthService.setDevice(device);
    }));

  describe('from Profile Screen', function () {
        beforeEach(module('tring.userReputationReport.controller'));
        beforeEach(inject(function ($rootScope, $controller, $injector) {
            scope = $rootScope.$new();
            scope.readOnly = false;
            scope.inProfileScreen = true;
            // Set up the mock http service responses
            httpBackend = $injector.get('$httpBackend');

            // backend definition common for all tests
            url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL;
            getReputationHandler = httpBackend.when('GET', url)
                .respond({
                    data: {
                        warnings: 3,
                        evictions: 1,
                        abusesReported: 5
                    }
                });
            httpBackend.expectGET(url);
            $controller('UserReputationReportController', {
                $scope: scope,
            });
            httpBackend.flush();
            console.log('Injected successfully');
        }));

        // tests start here
        it('should initialize properly', function () {
            console.log(Object.keys(scope));
            console.log('scope.abusesReported ' + scope.abusesReported);
            expect(scope.fromSelfProfileScreen).toEqual(true);
            expect(scope.showReputation).toEqual(true);
            expect(scope.warnings).toEqual(3);
            expect(scope.evictions).toEqual(1);
            expect(scope.abusesReported).toEqual(5);

        });
    });
    describe('processing Group Membership Request', function () {
        beforeEach(module('tring.userReputationReport.controller'));
        beforeEach(inject(function ($rootScope, $controller, $injector) {
            scope = $rootScope.$new();
            scope.processingGroupMembershipRequest = true;
            scope.data = {
                gid: 123,
                handleName: "myhandle"
            };
            // Set up the mock http service responses
            httpBackend = $injector.get('$httpBackend');
            // backend definition common for all tests
            //url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL;
            url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL.replace('@0', scope.data.gid).replace('@1', scope.data.handleName);
            getReputationHandler = httpBackend.when('GET', url)
                .respond({
                    data: {
                        warnings: 3,
                        evictions: 1,
                        abusesReported: 5
                    }
                });
            httpBackend.expectGET(url);
            $controller('UserReputationReportController', {
                $scope: scope,
            });
            httpBackend.flush();
            console.log('Injected successfully');
        }));

        // tests start here
        it('should initialize properly', function () {
            console.log(Object.keys(scope));
            console.log('scope.abusesReported ' + scope.abusesReported);
            expect(scope.processingGroupMembershipRequest).toEqual(true);
            expect(scope.showReputation).toEqual(true);
            expect(scope.warnings).toEqual(3);
            expect(scope.evictions).toEqual(1);
            expect(scope.abusesReported).toEqual(5);
        });
    });
});
