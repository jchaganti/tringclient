describe('NewGroupController', function () {
    var scope;
    var httpBackEnd;
    var cService;
    var window;
    var q;
    var rootScope;
    var groupData = {
        groupInfo: {
            name: "myGroup",
            categories: ['music'],
            rules: "Some rules",
            description: "Some description",
            o2m: false,
            closed: false,
            country: "INDIA",
            state: "Maharashtra",
            city: "Pune",
            contentId: "",
            thumbnailData: ""
        },
        members: [],
        ownerHandle: "myOwnerHandle",
        category: "",
        invitee: {
            number: "",
            type: ConstsService.MEMBER_ROLE
        },
        nameAvailable: null
    };
    var ownerHandle = "myOwnerHandle";
    jasmine.DEFAULT_TIMEOUT_INTERVAL = 5000;

    // load the controller's module
    beforeEach(module('ngCordovaMocks'));
    beforeEach(module('tring.constService'));
    beforeEach(module('tring.dbService'));
    beforeEach(module('tring.appServices'));
    beforeEach(module('TestUtils'));

    beforeEach(module('tring.newGroup.controller'));
    beforeEach(inject(function (AuthService) {
      device = {
        did: "abcdef123456",
        host: "43.242.215.204",
        port: 8000,
        p2p: '/p2p/1/testuser'
      };
      AuthService.setDevice(device);
    }));

  beforeEach(inject(function ($window, $injector, $q, $rootScope) {

        // Set up the mock http service responses
        httpBackEnd = $injector.get('$httpBackend');
        cService = ConstsService;
        window = $window;
        q = $q;
        rootScope = $rootScope;
    }));

    // tests start here
    it('should initialize properly', inject(function ($controller) {
        scope = rootScope.$new();
        $controller('NewGroupController', {
            $scope: scope,
            $stateParams: {
                isCreateNew: true
            }
        });
        scope.$broadcast('$ionicView.enter');
        console.log('Injected successfully');
        console.log(Object.keys(scope));
        console.log('scope.groupInput ' + JSON.stringify(scope.groupInput));
        expect(scope.groupInput).toBeDefined();
    }));

    it('just testing async and promises -', function (done) {
        pending();
        var timedFunc = window.setTimeout(function () {
            return 20;
        }, 0);
        scope = rootScope.$new();

        console.log("timedFunc is " + JSON.stringify(timedFunc));
        q.when(420)
            .then(function (a) {
                console.log("number is " + a);
                return q.resolve();
            })
            //.then($window.clearInterval.bind(null, interval))
            .catch(function () {
                console.log("Test is DONE with error");
            })
            .finally(function () {
                done();
            });
        rootScope.$digest();
    });

    describe('create-update-read-leave group tests - ', function () {
        var dbService, controller, timeout;
        var storedGroup;

        beforeEach(inject(function ($controller, $q, $timeout, DBService) {
            controller = $controller;
            q = $q;
            timeout = $timeout;
            dbService = DBService;
            console.log("done beforeeach");
        }));

        //This guy will create a group at the beginning of every test in this suite.
        beforeEach(function (done) {
            var testIsDone = {
                value: false
            };

            scope = rootScope.$new();
            dbService.initMbrDB(true).then(function (done) {
                console.log("Creating NEW Membership Database");
                var url = cService.MEMBERSHIP_SERVER_URL + cService.CREATE_GROUP_URL;
                httpBackEnd.expectPOST(url, {
                    groupData: groupData.groupInfo,
                    ownerHandleName: ownerHandle
                }).respond({
                    success: true,
                    topic: '/groups/1/as34234',
                    gid: 'as34234'
                });
                controller('NewGroupController', {
                    $scope: scope,
                    $stateParams: {
                        isCreateNew: true
                    }
                });
                scope.$broadcast('$ionicView.enter');
                scope.groupInput = angular.copy(groupData);
                //But in real-life the country will actually be the country code that is then converted
                // to the country name before requesting the url. Hence...
                scope.groupInput.groupInfo.country = 'IN';
                console.log('scope.groupInput ' + JSON.stringify(scope.groupInput));

                scope.createNewGroup()
                    .then(function (a) {
                        console.log("response is " + JSON.stringify(a));
                        return q.resolve();
                    }).then(function () {
                        var mbrDb = dbService.mbrDb();
                        return q.when(mbrDb.get("group_" + "as34234")).then(function (doc) {
                            storedGroup = doc;
                            return doc;
                        });
                    })
                    .catch(function () {
                        console.log("Test is DONE with error");
                    })
                    .finally(function () {
                        testIsDone.value = true;
                    });

                httpBackEnd.flush();
            });
            TestUtils.runDigestHack(testIsDone, scope, done);
        });

        it('experimental test', function (done) {
            pending();
            var url = cService.MEMBERSHIP_SERVER_URL + cService.CREATE_GROUP_URL;
            scope = rootScope.$new();
            var timedFunc = window.setTimeout(function () {
                return 20;
            }, 0);
            q.when(timedFunc)
                .then(function (a) {
                    console.log("number is " + a);
                    return q.resolve();
                })
                //.then($window.clearInterval.bind(null, interval))
                .catch(function () {
                    console.log("Test is DONE with error");
                })
                .finally(done);
            scope.$digest();
        });

        it('should create a group properly', function (done) {
            var mbrDb = dbService.mbrDb();
            var testIsDone = {
                value: false
            };
            q.when(mbrDb.get("group_" + "as34234")).then(function (doc) {
                testIsDone.value = true;
                expect(doc._rev).toBeDefined();
                expect(doc.topic).toBeDefined();
                expect(doc.gid).toEqual("as34234");
                expect(doc.handleName).toEqual(ownerHandle);
                console.log("should create a group properly - succeeded");
            }).catch(function (err) {
                console.log("error !!! err is " + JSON.stringify(err));
                expect(err).toBeUndefined();
            }).finally(function () {
                testIsDone.value = true;
            });
            TestUtils.runDigestHack(testIsDone, scope, done);
        });

        it('should show group details properly', function (done) {
            var mbrDb = dbService.mbrDb();
            var testIsDone = {
                value: false
            };
            scope = rootScope.$new();
            var url = cService.MEMBERSHIP_SERVER_URL + cService.MY_GROUP_INFO_URL.replace('@0', storedGroup.name);

            httpBackEnd.expectGET(url).respond(angular.extend(angular.copy(groupData.groupInfo), {
                success: true,
                topic: '/groups/1/as34234',
                handleName: ownerHandle,
                type: cService.OWNER_ROLE
            }));
            controller('NewGroupController', {
                $scope: scope,
                $stateParams: {
                    isCreateNew: false,
                    readOnly: true,
                    group: storedGroup
                },
            });
            scope.$broadcast('$ionicView.enter');
            console.log("group is " + JSON.stringify(scope.group));
            //httpBackEnd.verifyNoOutstandingRequest();
            console.log("groupsFromServerPromiseForUnitTests is " + JSON.stringify(scope.groupsFromServerPromiseForUnitTests));
            httpBackEnd.flush();
            scope.groupsFromServerPromiseForUnitTests.then(function (success) {
                    console.log("From 'should show group details properly', groupInfo is " + JSON.stringify(scope.groupInput));
                    return success;
                })
                .then(function (success) {
                    console.log("success is " + success);
                    expect(scope.seeGroupDetails).toBe(true);
                    expect(scope.group).toBeDefined();
                    expect(scope.groupInput).toBeDefined();
                    expect(scope.groupInput.groupInfo).toBeDefined();
                    expect(scope.groupInput.groupInfo.topic).toEqual("/groups/1/group_as34234");
                    expect(scope.groupInput.groupInfo._id).toEqual("group_as34234");
                })
                .catch(function (err) {
                    console.log("error !!! err is " + JSON.stringify(err));
                    expect(err).toBeUndefined();
                })
                .finally(function () {
                    testIsDone.value = true;
                });
            TestUtils.runDigestHack(testIsDone, rootScope, done);
        });

        describe('leave group -', function () {

            beforeEach(function (done) {
                console.log("Starting beforeEach for 'leave group' - starting the right controller");
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };
                scope = rootScope.$new();

                var url = cService.MEMBERSHIP_SERVER_URL + cService.MY_GROUP_INFO_URL.replace('@0', storedGroup.name);

                httpBackEnd.expectGET(url).respond(angular.extend(angular.copy(groupData.groupInfo), {
                    success: true,
                    topic: '/groups/1/as34234',
                    handleName: ownerHandle,
                    type: cService.OWNER_ROLE
                }));
                controller('NewGroupController', {
                    $scope: scope,
                    $stateParams: {
                        isCreateNew: false,
                        readOnly: true,
                        group: storedGroup
                    },
                });
                scope.$broadcast('$ionicView.enter');
                httpBackEnd.flush();
                console.log("group is " + JSON.stringify(scope.group));
                console.log("groupsFromServerPromiseForUnitTests is " + JSON.stringify(scope.groupsFromServerPromiseForUnitTests));


                scope.groupsFromServerPromiseForUnitTests
                    .then(function (success) {
                        expect(scope.seeGroupDetails).toBe(true);
                        console.log("success is " + success);
                        return success;
                    })
                    .catch(function (err) {
                        console.log("error !!! err is " + JSON.stringify(err));
                        expect(err).toBeUndefined();
                    })
                    .finally(function () {
                        testIsDone.value = true;
                    });
                TestUtils.runDigestHack(testIsDone, scope, done);
            });

            it('leave group should be prevented for owner', function (done) {
                var testIsDone = {
                    value: false
                };
                expect(scope.seeGroupDetails).toBe(true);
                expect(scope.group).toBeDefined();
                console.log('leave group should be prevented for owner- group is ' + JSON.stringify(scope.group));

                scope.leave(scope.group).then(function () {
                    //Shouldn't have come here - problem
                    console.log('leave(scope.group).then');
                    throw "error";
                }).then(null, function (success) {
                    console.log("leave group should be prevented for owner-passed !");
                    return true;
                }).catch(function (err) {
                    console.log("leave group should be prevented for owner - error !!! err is " + JSON.stringify(err));
                    expect(err).toBeUndefined();
                }).finally(function () {
                    testIsDone.value = true;
                });
                TestUtils.runDigestHack(testIsDone, scope, done);

            });
        });
        describe('update group-', function () {
            //We load the controller in beforeEach, and then call the scope methods in the specs (it funcs)
            //This is to work around the problem of not being able to insert httpbackend calls in the then
            //functions. Hence the split is required.
            beforeEach(function (done) {
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };
                scope = rootScope.$new();
                var url = cService.MEMBERSHIP_SERVER_URL + cService.MY_GROUP_INFO_URL.replace('@0', storedGroup.name);

                httpBackEnd.expectGET(url).respond(angular.extend(angular.copy(groupData.groupInfo), {
                    success: true,
                    topic: '/groups/1/as34234',
                    handleName: ownerHandle,
                    type: cService.OWNER_ROLE
                }));

                controller('NewGroupController', {
                    $scope: scope,
                    $stateParams: {
                        isCreateNew: false,
                        readOnly: false,
                        group: storedGroup
                    },
                });
                scope.$broadcast('$ionicView.enter');
                console.log("group is " + JSON.stringify(scope.group));
                //httpBackEnd.verifyNoOutstandingRequest();
                console.log("groupsFromServerPromiseForUnitTests is " + JSON.stringify(scope.groupsFromServerPromiseForUnitTests));
                httpBackEnd.flush();


                scope.groupsFromServerPromiseForUnitTests
                    .then(function (success) {
                        expect(scope.editGroupDetails).toBe(true);

                        console.log("success is " + success);
                        return success;
                    })
                    .catch(function (err) {
                        console.log("error !!! err is " + JSON.stringify(err));
                        expect(err).toBeUndefined();
                    })
                    .finally(function () {
                        testIsDone.value = true;
                    });
                TestUtils.runDigestHack(testIsDone, scope, done);
            });

            it('update details should work', function (done) {
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };
                var updateUrl = cService.MEMBERSHIP_SERVER_URL + cService.GROUP_DETAILS_UPDATE_URL;
                scope.groupEdited.groupInfo.description = "updated description";
                scope.groupEdited.groupInfo.categories.push('music');
                scope.groupEdited.groupInfo.categories.push('opera');
                scope.groupEdited.groupInfo.rules = "rule 1, rule 2";
                httpBackEnd.expectPOST(updateUrl, {
                    groupData: scope.groupEdited.groupInfo
                }).respond({
                    success: true
                });
                var res = scope.doneEditHandler();
                httpBackEnd.flush();
                res.then(function () {
                        console.log("then clause of doneEditHandler- all well !");
                    }).catch(function (err) {
                        console.log("doneEditHandler error !!! err is " + JSON.stringify(err));
                        expect(err).toBeUndefined();
                    })
                    .finally(function () {
                        testIsDone.value = true;
                    });
                TestUtils.runDigestHack(testIsDone, scope, done);
            });

            it('empty updates should work', function (done) {
                var mbrDb = dbService.mbrDb();
                var testIsDone = {
                    value: false
                };

                var res = scope.doneEditHandler();

                res.then(function () {
                        console.log("then clause of doneEditHandler for empty edits - all well !");
                        httpBackEnd.verifyNoOutstandingRequest();
                        //httpBackEnd.verifyNoOutstandingExpectation();
                    }).catch(function (err) {
                        console.log("doneEditHandler error for empty edits!!! err is " + JSON.stringify(err));
                        expect(err).toBeUndefined();
                    })
                    .finally(function () {
                        testIsDone.value = true;
                    });
                TestUtils.runDigestHack(testIsDone, scope, done);
            });

            it('profile picture updating should change the edited info', function () {
                //groupProfilePictureSet
                var profileInfo = {
                    profileThumbnailData: "data:garbagexyz12356177171",
                    contentId: "abcd1234"
                };
                scope.$broadcast('groupProfilePictureSet', profileInfo);
                var editedGroupInfo = scope.groupEdited.groupInfo;

                expect(editedGroupInfo.thumbnailData).toEqual(profileInfo.profileThumbnailData);
                expect(editedGroupInfo.contentId).toEqual(profileInfo.contentId);
                console.log("profile picture updating should change the edited info - all well !");
            });
        });
    });
});
