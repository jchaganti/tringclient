var testutilsmod = angular.module('TestUtils', []);
var TestUtils = {};

//This hack function is required to be called because jasmine does not work very well with angular promises
//similarly pouchdb promises also have a problem with angular in jasmine test mode.
//The first arg. - testDone is a structure initialized to {value: false} and will be set by
//the calling function (e.g. it() or beforeEach() in our code) whenever it thinks it is done.
TestUtils.runDigestHack = function (testDone, scope, doneFunc, _count, ignoreCheckCount) {
  var interval = window.setInterval((function () {
    var maxCount = _count ? _count : 4;//4 is arbitrary, it seems to be enough
    var count = 0;
    return function () {
      if (testDone.value || (!ignoreCheckCount && count === maxCount)) {
        console.log('testIsDone is ' + testDone.value + " count: " + count + " maxCount: " + maxCount);
        window.clearInterval(interval);
        console.log("Calling done finally !");
        doneFunc();
      } else {
        count++;
        try {
          scope.$digest();
        } catch (err) {
          console.log("From runDigestHack - err is " + JSON.stringify(err));
        }
      }
    };
  })(), 30);
};

TestUtils.trackTestStageCompletionStatus = function (testValCB, testRepeatCB, stageCompleteCB,  maxCount, delay) {
  var count = 0;
  var tid = window.setInterval(function () {
    var testVal = testValCB();
    if (testVal) {
      console.log('testVal is ' + testVal + " count: " + count + " maxCount: " + maxCount);
      window.clearInterval(tid);
      tid = null;
      if(stageCompleteCB && angular.isFunction(stageCompleteCB)) {
        stageCompleteCB();
      }
    }
    else {
      count++;
      if(testRepeatCB && angular.isFunction(testRepeatCB)) {
        testRepeatCB();
      }
    }
  }, delay);
}

TestUtils.httpRequestTracker =  function() {
  var __httpBackend;
  var flushState = 0;
  var testValCB = function () {
    return flushState === 1;
  }

  var testRepeatCB = function () {
    try {
      __httpBackend.flush();
      flushState = 1;
    }
    catch (e) {
      flushState = -1;
      console.log("flush Error: " + JSON.stringify(e.message));
    }
  }

  var track = function (httpBackend) {
    __httpBackend = httpBackend;
    TestUtils.trackTestStageCompletionStatus(testValCB, testRepeatCB, null, 20, 50);
  }
  return {
    track: track
  }
}

testutilsmod.value('TestUtils', TestUtils);
