// Ionic Tring App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'tring' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'tring.controllers' is found in controllers.js
angular.module('tring', ['ngMessages', 'ionic', 'ngCordova', 'tring.intro.controller', 'tring.controllers',
    'tring.capture.controller', 'tring.chat.controller', 'tring.contacts.controller', 'tring.download.controller',
    'tring.fileBrowse.controller', 'tring.group.controller', 'tring.groupInvite.controller', 'tring.message.controller',
    'tring.navigation.controller', 'tring.newGroup.controller', 'tring.profile.controller', 'tring.user.controller',
    'tring.groupMembershipRequest.controller', 'tring.authService', 'angularMoment', 'cwill747.phonenumber',
    'tring.navigationService', 'ngSanitize', 'tring.manageGroup.controller', 'tring.userReputationReport.controller',
    'tring.notification.controller', 'ngTagsInput'])
    .config(function ($stateProvider, $urlRouterProvider, AuthServiceProvider, $ionicConfigProvider, $logProvider) {
        $logProvider.debugEnabled(false);
        //$log.debug("Configuration tring module");
        $ionicConfigProvider.views.transition('none');
        $ionicConfigProvider.tabs.position('bottom');
        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "templates/rubyonic/login.html"
            })
            .state('pin', {
                url: "/pin",
                templateUrl: "templates/rubyonic/pin.html"
            })
            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/rubyonic/tringMenu.html"
            })
            .state('app.discover', {
                url: "/startdiscover",
                cache: false,
                views: {
                    "menuContent@app": {
                        templateUrl: 'templates/rubyonic/groupsDiscoverStart.html',
                        controller: 'searchViewCtr'
                    }
                }
            })
            .state('app.chats', {
                url: "/chats",
                params: {
                    messageId: null
                },
                views: {
                    "app-chats": {
                        templateUrl: 'templates/rubyonic/chats.html'
                    }
                }
            })
            .state('app.contacts', {
                url: "/contacts",
                views: {
                    "menuContent": {
                        templateUrl: 'templates/rubyonic/contacts.html'
                    }
                }
            })
            .state('app.profile', {
                url: "/profile",
                cache: false,
                params: {
                    readOnly: false,
                    contact: null,
                    group: null,
                    fromUrl: "",
                    pendingCaptureResults: null
                },
                views: {
                    "app-settings": {
                        templateUrl: 'templates/rubyonic/profile.html'
                    }
                }
            })
            .state('app.profile-ro', {
                url: "/profile",
                cache: false,
                params: {
                    readOnly: true,
                    contact: null,
                    group: null,
                    fromUrl: ""
                },
                views: {
                    "app-settings": {
                        templateUrl: 'templates/rubyonic/profile.html'
                    }
                }
            })
            .state('app.intro', {
                url: "/intro/:justStarting",
                cache: false,

                views: {
                    "menuContent": {
                        templateUrl: 'templates/rubyonic/introScreen.html'
                    },
                    params: {
                        justStarting: null
                    }
                }
            })
            .state('app.newIntro', {
                url: "/intro/:justStarting",
                cache: false,

                views: {
                    "menuContent": {
                        templateUrl: 'templates/rubyonic/introScreenNew.html'
                    },
                    params: {
                        justStarting: null
                    }
                }
            })
            .state('app.chat-ui', {
                url: "/chat-ui",
                params: {
                    senderId: null,
                    localSenderId: null,
                    fwdedMsgId: null,
                    toHandleName: null,
                    pendingCaptureResults: null
                },
                views: {
                    "app-chats": {
                        templateUrl: 'templates/rubyonic/chat-ui.html',
                        controller: 'MessagesCtrl'
                    }
                }
            })
            .state('app.chat-ui.fileBrowse', {
                url: "/file-browse",
                views: {
                    "app-chats@app": {
                        templateUrl: 'templates/rubyonic/fileBrowser.html',
                    }
                }
            })
            .state('app.chat-ui.processMembershipRequest', {
                url: "/process-membership-request/{message:json}",
                cache: false,
                views: {
                    "menuContent@app": {
                        templateUrl: 'templates/rubyonic/groupMembershipRequestAcceptReject.html',
                        controller: 'GroupMembershipRequestController'
                    }
                }
            })
            //            .state('app.groups', {
            //                url: "/groups",
            //                views: {
            //                    "menuContent": {
            //                        templateUrl: 'templates/rubyonic/groups.html'
            //                    }
            //                }
            //            })
            .state('app.groups', {
                url: "/new-discover",
                cache: true,
                views: {
                    "app-groups": {
                        templateUrl: 'templates/rubyonic/groupDiscover.html',
                        controller: 'searchViewCtr'
                    }
                }
            })
          .state('app.groups.join', {
            url: "/join",
            cache: true,
            params: {
              group: null
            },
            views: {
              "app-groups@app": {
                templateUrl: 'templates/rubyonic/groupJoin.html',
                controller: 'GroupInviteController'
              }
            }
          })
            .state('app.groups.memberships', {
                url: "/memberships",
                cache: false,
                views: {
                    "app-groups@app": {
                        templateUrl: 'templates/rubyonic/groupMemberships.html',
                        controller: 'GroupController'
                    }
                }
            })
          .state('app.groups.memberadmin', {
            url: "/memberadmin",
            cache: false,
            params: {
              group: null
            },
            views: {
              "app-groups@app": {
                templateUrl: 'templates/rubyonic/groupMemberAdmin.html',
                controller: ['$scope','$stateParams','$rootScope',function($scope,$stateParams,$rootScope){
                  $scope.$on('$ionicView.enter', function(){
                    if ($stateParams.group) {
                      $scope.group = $stateParams.group;
                    }
                  })
                }]
              }
            }
          })
            .state('app.seeGroupDetails', {
                url: "/seeGroupDetails/:readOnly/{group:json}",
                cache: false,
                views: {
                    "menuContent": {
                        templateUrl: 'templates/rubyonic/groupDetails.html',
                        controller: 'NewGroupController'
                    },
                    params: {
                        readOnly: true,
                        group: null
                    }
                }
            })
            .state('app.groups.newDiscover', {
                url: "/new-discover",
                cache: true,
                views: {
                    "app-groups@app": {
                        templateUrl: 'templates/rubyonic/groupDiscover.html',
                        controller: 'searchViewCtr'
                    }
                }
            })
            .state('app.groups.seeGroupDetails', {
                url: "/seeGroupDetails/:readOnly/{group:json}",
                views: {
                    "tab-memberships": {
                        templateUrl: 'templates/rubyonic/manageNetwork/parentGroupDetails.html',
                        controller: 'NewGroupController'
                    },
                    params: {
                        readOnly: true,
                        group: null
                    }
                }
            })
            .state('app.groups.createNew', {
                url: "/createNew/:isCreateNew",
                views: {
                    "app-groups@app": {
                        templateUrl: 'templates/rubyonic/groupCreateNew.html',
                        controller: 'NewGroupController'
                    }
                },
                "params": {
                    'isCreateNew': true,
                    'readOnly': false,
                    'group': null,
                    'pendingCaptureResults': null
                }
            })
            .state('app.groups.inviteContacts', {
                url: "/inviteMembers/{group:json}/:inviterIsOwnerOrAdmin",
                cache: false,
                views: {
                    "tab-create": {
                        templateUrl: 'templates/rubyonic/inviteContacts.html',
                        controller: 'GroupInviteController'

                    },
                    "params": {
                        'group': null,
                        'inviterIsOwnerOrAdmin': false
                    }
                }
            })
            .state('app.groups.inviteNonMemberContactsForMembership', {
                url: "/inviteMembers/{group:json}/:inviterIsOwnerOrAdmin",
                cache: false,
                views: {
                    "tab-memberships": {
                        templateUrl: 'templates/rubyonic/inviteContacts.html',
                        controller: 'GroupInviteController'

                    },
                    "params": {
                        'group': null,
                        'inviterIsOwnerOrAdmin': false
                    }
                }
            })
            .state('app.groups.inviteNonMemberContactsFromManageGroups', {
                url: "/inviteMembers/{group:json}/:inviterIsOwnerOrAdmin",
                cache: false,
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/inviteContacts.html',
                        controller: 'GroupInviteController'

                    },
                    "params": {
                        'group': null,
                        'inviterIsOwnerOrAdmin': true
                    }
                },
            })
            .state('app.manage', {
                url: "/manage",
                views: {
                    "app-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/parentGroups.html'
                    }
                },
                params: {
                    isCreateNew: false,
                    readOnly: false,
                    'group': null,
                    'pendingCaptureResults': null
                }
            })
            .state('app.groups.members', {
                url: "/members",
                cache: false,
                views: {
                    "app-groups@app": {
                        templateUrl: 'templates/rubyonic/manageNetwork/parentGroupMemberships.html',
                        controller: 'ManageGroupMembershipsCtrl'
                    }
                },
                params: {
                    'group': null
                }
            })
            .state('app.groups.inviteMembers', {
                url: "/inviteMembers",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/inviteSubGroupMembers.html'
                    }
                },
                params: {
                    group: null
                }
            })
            .state('app.groups.createSubNetwork', {
                url: "/createSubNetwork",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/createSubGroup.html'
                    }
                },
                params: {
                    group: null
                }
            })
            .state('app.groups.listSubNetworks', {
                url: "/listSubNetworks",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/subGroups.html'
                    }
                },
                params: {
                    group: null
                }
            })
            .state('app.groups.subGroupMemberships', {
                url: "/listSubNetworkMembers",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/subGroupMemberships.html'
                    }
                },
                params: {
                    group: null,
                    subgroup: null
                }
            })
            .state('app.groups.updateSubGroupMemberships', {
                url: "/listSubNetworkMembers",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/updateSubGroupMemberships.html'
                    }
                },
                params: {
                    group: null,
                    subgroup: null
                }
            })
            .state('app.groups.subNetworkDetails', {
                url: "/subNetworkDetails",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/updateSubGroupMemberships.html'
                    }
                },
                params: {
                    group: null
                }

            })
            .state('app.groups.networkDetails', {
                url: "/networkDetails",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/manageNetwork/parentGroupDetails.html',
                        controller: 'NewGroupController'
                    }
                },
                params: {
                    isCreateNew: false,
                    readOnly: false,
                    group: null,
                    pendingCaptureResults: null
                }

            })
            .state('app.groups.editGroupDetails', {
                url: "/manage/editGroupDetails/:group",
                views: {
                    "tab-manage": {
                        templateUrl: 'templates/rubyonic/groupDetails.html',
                        controller: 'NewGroupController'
                    },
                    params: {
                        isCreateNew: false,
                        readOnly: false,
                        group: null
                    }
                }
            })
            .state('app.groups.discover', {
                url: "/discover",
                cache: false,
                views: {
                    "tab-discover": {
                        templateUrl: 'templates/rubyonic/groupDiscover.html',
                        controller: 'searchViewCtr'
                    }
                }
            })
            .state('app.groups.requestForMembership', {
                url: "/request-for-membership/{group:json}",
                cache: false,
                views: {
                    "menuContent@app": {
                        templateUrl: 'templates/rubyonic/requestForMembership.html',
                        controller: 'GroupInviteController'
                    }
                }
            });

        var device = AuthServiceProvider.getDevice();
        if (device) {
            console.debug("Registered device found");
        } else {
            console.debug("The registered device was not found to be set");
        }

        // if none of the above states are matched, use this as the fallback
        if (device && device.status == 1 && (!device.hasOwnProperty('active') || device.active)) {
            console.debug("Going to /app/chats");
            AuthServiceProvider.setDevice(device);
            $urlRouterProvider.otherwise('/app/chats');
        } else {
            $urlRouterProvider.otherwise('/login');
        }
    }).run(function ($ionicPlatform, NavigationService, $state, $log, $cordovaToast) {
        function onReadyCB() {
            $ionicPlatform.registerBackButtonAction(function (e) {
                if ($state.is('app.chats')) {
                    ionic.Platform.exitApp();
                } else if ($state.is('app.chat-ui')) {
                    NavigationService.goNativeToView('app.chats', null, 'right');
                } else {
                    NavigationService.goNativeBack();
                }
            }, 100);
            var isAndroid = ionic.Platform.isAndroid();
            if(!isAndroid) {
              ionic.Platform.fullScreen();
            }
            //
            //TestFairy.begin('aba9272f014c219a084ef028f46d2465fb6b5d44');
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                //cordova.plugins.Keyboard.disableScroll(true);
            }
            if (window.StatusBar && !isAndroid) {
                // org.apache.cordova.statusbar required
                // StatusBar.styleDefault();
                StatusBar.hide();
            }
        }

        $ionicPlatform.ready(function () {
            $log.debug("Run tring module");
            var isAndroid6OrGreater = ionic.Platform.isAndroid() && ionic.Platform.version() >= 6;
            $log.debug("isAndroid6OrGreater: " + isAndroid6OrGreater);
            if (isAndroid6OrGreater) {
                var requestPermissions = [];
                cordova.plugins.diagnostic.getPermissionsAuthorizationStatus(function (statuses) {
                    for (var permission in statuses) {
                        switch (statuses[permission]) {
                        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                            $log.debug("Permission granted to use " + permission);
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                            $log.debug("Permission to use " + permission + " has not been requested yet");
                            requestPermissions.push(permission);
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.DENIED:
                            $log.debug("Permission denied to use " + permission + " - ask again?");
                            requestPermissions.push(permission);
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                            $log.debug("Permission permanently denied to use " + permission + " - guess we won't be using it then!");
                            requestPermissions.push(permission);
                            //ionic.Platform.exitApp();
                            break;
                        }
                    }
                    var notPermittedPermissions = [];
                    cordova.plugins.diagnostic.requestRuntimePermissions(function (statuses) {
                        for (var permission in statuses) {
                            switch (statuses[permission]) {
                            case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                console.log("Permission granted to use " + permission);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                notPermittedPermissions.push(permission);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                notPermittedPermissions.push(permission);
                                break;
                            case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                                notPermittedPermissions.push(permission);
                                break;
                            }
                        }
                        if (notPermittedPermissions.length > 0) {
                            notPermittedPermissionsStr = notPermittedPermissions.join(",");
                            $cordovaToast.showLongCenter("The required permissions " + notPermittedPermissionsStr + " have not been granted. Hence exiting.");
                            ionic.Platform.exitApp();
                        } else {
                            onReadyCB();
                        }
                    }, function (error) {
                        console.error("The following error occurred during requestPermissions: " + error);
                    }, requestPermissions);
                }, function (error) {
                    console.error("The following error occurred during check Permissions: " + error);
                }, [
          cordova.plugins.diagnostic.runtimePermission.READ_CONTACTS,
          cordova.plugins.diagnostic.runtimePermission.WRITE_CONTACTS,
          cordova.plugins.diagnostic.runtimePermission.CAMERA,
          cordova.plugins.diagnostic.runtimePermission.SEND_SMS,
          cordova.plugins.diagnostic.runtimePermission.RECEIVE_SMS,
          cordova.plugins.diagnostic.runtimePermission.RECORD_AUDIO,
          cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE,
          cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE
        ]);
            } else {
                onReadyCB();
            }
        });
    });
