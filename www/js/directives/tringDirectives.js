// select-with-auto-suggest is a widget directive that lets you select from a list of elements by typing part of the name
//or number field of the element, which prunes the list so that getting to your selection is easier (this will be expanded
// to any field of element if required in future).
// Example Usage: Assume Your view uses controller TestController. Then the template html file for the view that uses
//  the select-with-auto-suggest directive may look as follows:
//        <div ng-controller="TestController">
//            <h3 class="notification-title">Selected Value: {{selectedValue.name}}</h3>
//               <select-with-auto-suggest initialize-elements="initializeTestElements()" get-elements-paged="getTestElementsPaged(first, last, elements)" select-handler="goToTestElement(element)" keep-suggestions additional-element-markup="&lt;button class=&quot;button-clear&quot;&gt;Invite&lt;/button&gt;" additional-element-click-handler="sendInvite(element)">
//                </select-with-auto-suggest>
//        </div>
// Here, initialize-elements is tied to an input function that should return a promise which is resolved to the input
// elements array, and select-handler is the select callback function, called when an element is selected.
// They are defined in the scope of TestController, which contains the directive scope. The directive itself uses an
//isolated scope, borrowing several functions and data from the enclosing scope. The get-elements-paged function attribute
//is for a function called during pagination. It is exposed as an input hook so that users of this
// directive can do their magic in their passed in function during pagination.
// If the attribute is not set, then a default identity function is used. The keep-suggestions attribute
// will retain the suggestion list even when focus is shifted out of the input text box. Without this attribute,
//the suggestion list disappears as soon as you shift focus out of the input text box.
// The addition-element-markup attribute allows passing html markup, typically for an additional button in every element,
// and additional-element-click-handler allows passing a selection handler for the additional element.
//
angular.module("tring.directives", ['ionic', 'ng', 'qImproved'])
    .directive("selectWithAutoSuggest", ['$timeout', '$log', '$sce', '$compile', '$q', function ($timeout, $log, $sce, $compile, $q) {
        function link(scope, el, attrs /*, ctrls*/ ) {
            //$log.debug("el is " + JSON.stringify(el));
            var PAGE_SIZE = 100;
            var processAutoSuggestTimer;
            scope.nothingToShow = false;
            scope.focus = false;
            scope.inputText = "";
            //$log.debug("attrs are " + JSON.stringify(attrs));
            if ("keepSuggestions" in attrs) {
                scope.keepSuggestions = true;
            }

            scope.$on('callSelectWithAutoSuggestScopeFunction',function(evt,func,args){
                if (scope.hasOwnProperty(func) && typeof scope[func] === 'function') {
                    scope[func].apply(scope,args);
                } else {
                    console.error("No such function on directive scope");
                }
            });

            scope.hasRefreshElements = function () {
                return angular.isDefined(attrs.refreshElements);
            };

            //el.find('selectWithAutoSuggestAdditionalElement').append($sce.trustAsHtml(scope.additionalMarkup));
            scope.refreshElementsWrapper = function () {
                scope.inputText = "";
                scope.nothingToShow = false;
                scope.refreshElements().then(function (cnts) {
                    init(cnts);
                });
            };
            scope.processAutoSuggestTimerFunction = function () {
                $log.debug("inputText is " + JSON.stringify(scope.inputText));
                var listElements = scope.elements;
                if (scope.inputText) {
                    var str = scope.inputText;
                    var processedList = listElements.filter(function (el) {
                        var result;
                        if (el && el.name) {
                            var words = el.name.split(/\s/);
                            result = words.filter(function (w) {
                                return w.toLowerCase().indexOf(str.toLowerCase()) === 0;
                            }).length ? true : false;
                            if (result) {
                                return result;
                            }
                        }
                        if (el && el.number && el.number.indexOf(str) >= 0) {
                            result = true;
                        } else {
                            result = false;
                        }
                        return result;
                    });
                    $log.debug("processedList is " + JSON.stringify(processedList));
                    return processedList;
                } else {
                    return listElements;
                }
            };

            scope.handleInputTextChange = function () {
                if (processAutoSuggestTimer) {
                    $timeout.cancel(processAutoSuggestTimer);
                }
                processAutoSuggestTimer = $timeout(scope.processAutoSuggestTimerFunction, 300);
                processAutoSuggestTimer.then(function (list) {
                    $log.debug("list is " + JSON.stringify(list));

                    //scope.$apply(function () {
                    scope.activeElements = list;
                    scope.firstElementIndex = 0;
                    scope.lastElementIndex = PAGE_SIZE;
                    scope.allRowsLoaded = false;
                    scope.pageElements = [];
                    scope.addElementsPaged(false);
                    if (!scope.activeElements.length) {
                        scope.nothingToShow = true;
                    } else {
                        scope.nothingToShow = false;
                    }
                    //});
                }).catch(function (err) {
                    $log.debug("auto suggest timer cancelled. err is " + err);
                });
            };
            scope.getAdditionalHTML = function () {
                //$log.debug("additional html is " + scope.additionalMarkup);
                return $sce.trustAsHtml(scope.additionalMarkup);
            };

            scope.addElementsPaged = function (noInfiniteScrollBroadcast) {
                var elementsPaged = scope.getElementsPaged ? scope.getElementsPaged : function (args) {
                    return $q.when(args.elements.slice(args.first, args.last));
                };
                var doInfiniteScrollBroadcast = !noInfiniteScrollBroadcast;

                $log.debug('addElementsPaged() called. $scope.firstElementIndex is ' + scope.firstElementIndex);
                $log.debug('$scope.lastElementIndex is ' + scope.lastElementIndex);
                if (!scope.allRowsLoaded) {
                    elementsPaged({
                            first: scope.firstElementIndex,
                            last: scope.lastElementIndex,
                            elements: scope.activeElements
                        })
                        .then(function (els) {
                            $log.debug('then part of getElementsPaged');
                            //$log.debug(JSON.stringify(els));
                            if (els.length === PAGE_SIZE) {
                                scope.lastElementIndex =
                                    scope.lastElementIndex + PAGE_SIZE;
                            }
                            scope.firstElementIndex =
                                scope.firstElementIndex + els.length;
                            if (els.length <= 0) {
                                scope.allRowsLoaded = true;
                            }
                            Array.prototype.push.apply(scope.pageElements, els);
                            if (doInfiniteScrollBroadcast) {
                                scope.$broadcast('scroll.infiniteScrollComplete');
                            }
                        }, function () {
                            console.warn('All elements shown. Cant scroll more');
                            if (doInfiniteScrollBroadcast) {
                                scope.$broadcast('scroll.infiniteScrollComplete');
                            }
                            scope.allRowsLoaded = true;
                        });
                } else {
                    if (doInfiniteScrollBroadcast) {
                        scope.$broadcast('scroll.infiniteScrollComplete');
                    }
                }
            };

            var init = function (els) {
                scope.firstElementIndex = 0;
                scope.lastElementIndex = PAGE_SIZE;
                scope.allRowsLoaded = false;
                scope.pageElements = [];
                scope.elements = [];
                Array.prototype.push.apply(scope.elements, els);
                scope.activeElements = scope.elements;
                if (!scope.activeElements.length) {
                    scope.nothingToShow = true;
                }
                $log.debug(JSON.stringify(scope.activeElements));
                $log.debug(JSON.stringify(scope.additionalElementClickHandler));
                $log.debug(JSON.stringify(scope.handleSelectElement));

                scope.addElementsPaged(true);
            };

            scope.initializeElements().then(init);
        }
        var templateUrlPath = 'templates/rubyonic/selectWithAutoSuggest.html';

        return {
            restrict: 'EA',
            templateUrl: templateUrlPath,
            //transclude: true,
            //require: '^ContactsService',
            //replace: true,
            scope: {
                getElementsPaged: '&',
                handleSelectElement: '&selectHandler',
                initializeElements: '&',
                additionalMarkup: '@additionalElementMarkup',
                additionalElementClickHandler: '&',
                refreshElements: '&',
                profileImageClickHandler: '&'
            },
            compile: function (el, attrs) {
                //selectWithAutoSuggestAdditionalElement
                //$log.debug("compile fn: attr is " + JSON.stringify(attrs));
                var container = el.find('tring-additional-element');
                container.html($sce.trustAsHtml(attrs.additionalElementMarkup));
                container.replaceWith(container.contents());
                //$log.debug("compile fn: container is " + JSON.stringify(container));
                return function (scope, el, attrs) {
                    link(scope, el, attrs);
                };
            }

        };
            }])
    .directive("onlineStatus", ['$rootScope', '$log', function ($rootScope, $log) {
        return {
            restrict: 'A',
            transclude: false,
            link: function (scope, el, attrs) {
                $log.debug("The group status: " + scope.$root.networkStatus);
                if (scope.$root.networkStatus) {
                    el.removeClass('offline-status').addClass('online-status');
                }
                else {
                  el.removeClass('online-status').addClass('offline-status');
                }
                scope.$root.$watch('networkStatus', function (networkStatus) {
                    $log.debug("Watching networkStatus: " + networkStatus);
                    if (networkStatus) {
                        el.removeClass('offline-status').addClass('online-status');
                        el.find('input').attr('placeholder','Type your message')
                            .prop( "disabled",false)
                            .css({'background-color': '#FFFFFF'});
                    } else {
                      el.removeClass('online-status').addClass('offline-status');
                      el.find('input').attr('placeholder','No Internet Connection')
                            .prop( "disabled", true )
                            .css({'background-color': '#9bb8c3'});
                    }
                });
            }
        };
    }])
    .directive("setPlaceholder", ['ConstsService','$log','NetworkService', function (ConstsService, $log, NetworkService) {
      return {
        restrict: 'A',
        transclude: false,
        link: function (scope, el, attrs) {
          $log.debug("The sendMessageType: " + scope.sendMessageType);
          $log.debug("The toHandleName: " + scope.toHandleName);

          if (scope.$root.networkStatus) {
            el.attr('placeholder','Type your message');
          }
          else {
            el.attr('placeholder','No Internet Connection');
          }

          scope.$watch('sendMessageType', function (sendMessageType) {
            if(NetworkService.isOnline()) {
              $log.debug("sendMessageType watch: " + sendMessageType)
              $log.debug("toHandleName watch: " + scope.toHandleName)
              var placeHolder = "Type your message";
              if(sendMessageType === ConstsService.SEND_MESSAGE_TYPE_PVT_MEMBER) {
                placeHolder = "Type your private message to " + scope.toHandleName;
              }
              else if(sendMessageType === ConstsService.SEND_MESSAGE_TYPE_PVT_MODERATOR) {
                placeHolder = "Type your message to admin";
              }
            }
            else {
              placeHolder = "No Internet Connection";
              el.prop( "disabled", true).css({'background-color': '#9bb8c3'});
            }
            el.attr('placeholder',placeHolder);
          });
        }
      };
    }])
    .directive("detectFocus", function () {
        return {
            restrict: "A",
            scope: {
                onFocus: '&onFocus',
                onBlur: '&onBlur',
                focusOnBlur: '=focusOnBlur'
            },
            link: function (scope, elem) {

                elem.on("focus", function () {
                    scope.onFocus();
                    scope.focusOnBlur = true; //note the reassignment here, reason why I set '=' instead of '@' above.
                });

                elem.on("blur", function () {
                    scope.onBlur();
                    if (scope.focusOnBlur)
                        elem[0].focus();
                });
            }
        }
    })
    .directive('goNative', ['$ionicGesture', '$ionicPlatform', function ($ionicGesture, $ionicPlatform) {
        return {
            restrict: 'A',

            link: function (scope, element, attrs) {

                $ionicGesture.on('tap', function (e) {

                    var direction = attrs.direction;
                    var transitiontype = attrs.transitiontype;

                    $ionicPlatform.ready(function () {

                        switch (transitiontype) {
                        case "slide":
                            window.plugins.nativepagetransitions.slide({
                                    "direction": direction
                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                            break;
                        case "flip":
                            window.plugins.nativepagetransitions.flip({
                                    "direction": direction
                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                            break;

                        case "fade":
                            window.plugins.nativepagetransitions.fade({

                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                            break;

                        case "drawer":
                            window.plugins.nativepagetransitions.drawer({
                                    "origin": direction,
                                    "action": "open"
                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                            break;

                        case "curl":
                            window.plugins.nativepagetransitions.curl({
                                    "direction": direction
                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                            break;

                        default:
                            window.plugins.nativepagetransitions.slide({
                                    "direction": direction
                                },
                                function (msg) {
                                    $log.debug("success: " + msg)
                                },
                                function (msg) {
                                    alert("error: " + msg)
                                }
                            );
                        }


                    });
                }, element);
            }
        };
    }])
    .directive("tringMessageInput", ['$timeout', function ($timeout) {
        return {
            restrict: "A",
            scope: {},
            controller: ["$scope", function ($scope) {
                this.keepFocus = function () {
                    $scope.keepFocus();
                };
            }],
            link: function (scope, elem) {
                scope.keepFocus = function () {
                    $timeout(function () {
                        var e = elem.find("textarea")[0];
                        if (e) {
                           e.focus();
                        }
                    }, 0);
                };
            }
        };
}])
    .directive("tringMessageSender", [function () {
        return {
            restrict: "A",
            require: "^tringMessageInput",
            scope: {
                sendMessage: '&'
            },
            link: function (scope, elem, attrs, tringMessageInputController) {
                var sendMessageCallback = function () {
                    tringMessageInputController.keepFocus();
                    scope.sendMessage();
                };
                elem.on("click", sendMessageCallback);
            }
        };
}])
    .directive("limitTo", [function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keydown", function () {
                    if (this.value.length == limit) {
                        return false;
                    }
                });
            }
        };
}])
    .filter('hrefToJS', function ($sce, $sanitize) {
        return function (text) {
            var regex = /href="([\S]+)"/g;
            var newString = $sanitize(text).replace(regex, "href=\"#\" onclick=\"(function(evt){window.open('$1', '_system', 'location=yes'); evt.stopPropagation();})(event)\" class=\"word-break\"");
            //newString = newString.replace(/(&#10;)/g,"\n");
            //console.log("newString is "+ newString);
            return $sce.trustAsHtml(newString);
        };
    })
    .directive("randomColor", function () {
        return {
            restrict: 'EA',
            replace: false,
            link: function (scope, element, attr) {

                //generate random color Math.random().toString(16).slice(2, 8); (Math.random() * 0xFFFFFF << 0).toString(16);
                var color = '#' + (Math.random() * 0xFFFFFF << 0).toString(16);

                //Add random background class to selected element
                element.css('background-color', color);

            }
        }
    })
  .directive('eventFocus', function(focus) {
    return function(scope, elem, attr) {
      elem.on(attr.eventFocus, function() {
        focus(attr.eventFocusId);
      });

      // Removes bound events in the element itself
      // when the scope is destroyed
      scope.$on('$destroy', function() {
        elem.off(attr.eventFocus);
      });
    };
  }).directive('hideTabs', function($rootScope) {
    return {
      restrict: 'A',
      link: function($scope, $el) {
        $rootScope.hideTabs = 'tabs-item-hide';
        $scope.$on('$destroy', function() {
          $rootScope.hideTabs = '';
        });
      }
    };
  })
;
