angular.module('tring.controllers', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'tring.locationService'])

  .controller('GroupMenuController', ['$rootScope', '$scope', '$log', function ($rootScope, $scope, $log) {
    $scope.$on('$ionicView.enter', function () {
      $log.debug("In GroupMenuController:$on enter callback");
      $rootScope.activeScreen.current = $rootScope.activeScreen.groups;
    });
  }])

  .controller('reviewCtrl', ['$scope', '$state', '$ionicSlideBoxDelegate', '$log', function ($scope, $state, $ionicSlideBoxDelegate, $log) {


    $scope.next = function () {
      $log.debug('controller Run next');
      $ionicSlideBoxDelegate.next();
    };
    $scope.previous = function () {
      $log.debug('controller Run previous');
      $ionicSlideBoxDelegate.previous();
    };

    // Called each time the slide changes
    $scope.slideChanged = function (index) {
      $log.debug('controller Run slider' + $scope.slideIndex);
      $scope.slideIndex = index;
    };
  }])

  .controller('searchViewCtr', ['$scope', '$state', '$http', '$ionicPopup', 'ConstsService', 'LocationService', '$q', 'NavigationService', '$cordovaToast', 'AuthService', 'UserService', '$ionicLoading', 'focus', '$timeout', '$log', 'GroupService', '$rootScope', function ($scope, $state, $http, $ionicPopup, ConstsService, LocationService, $q, NavigationService, $cordovaToast, AuthService, UserService, $ionicLoading, focus, $timeout, $log, GroupService, $rootScope) {
    $log.debug('controller Run searchViewCtr');

    //Since this is now the first page seen by the user after registration the following is required here
    //also in addition to ChatController.
    var device = AuthService.getDevice();
    $log.debug("Device found in searchViewCtr " + JSON.stringify(device));
    //Commenting for affiliate marketing version
    /*if (device) {
     if (!device.bootstrapState || device.bootstrapState < 2) {
     //kick in bootstrapping of contacts
     $log.debug("kick in bootstrapping of contacts");
     UserService.bootstrapContacts();
     } else if (device.bootstrapState < 3) {
     //kick in bootstrapping of profile info
     $log.debug("kick in bootstrapping of profile info");
     UserService.bootstrapProfileInfo();
     }
     }*/

    //    var hasRegistered = false;
    //    $scope.$watch(function () {
    //        if (hasRegistered) return;
    //        hasRegistered = true;
    //        // Note that we're using a private Angular method here (for now)
    //        $scope.$$postDigest(function () {
    //            hasRegistered = false;
    //            (function () {
    //                $log.debug("viewResults is " + $scope.viewResults);
    //                $log.debug("showBrowseView is " + $scope.showBrowseView);
    //                $log.debug("showSearchBox is " + $scope.showSearchBox);
    //                $log.debug("groups is " + JSON.stringify($scope.groups));
    //
    //            })();
    //        });
    //    });

    var setNoNetworksFound = function () {
      if (!$scope.groups || !$scope.groups.length) {
        $scope.noNetworksFound = true;
      } else {
        $scope.noNetworksFound = false;
      }
    };


    (function () {
      var value = false;
      $scope.setNetworkConnectionError = function (val) {
        value = val;
      };
      $scope.getNetworkConnectionError = function () {
        return value;
      };
    })();

    $scope.keyword = "";

    $scope.$on('$ionicView.beforeEnter', function () {
      $log.debug("searchViewCtr-beforeEnterFunc called")
      $rootScope.hideTabs="";
      if (!$scope.keyword) {
        $scope.showFeaturedGroups();
      } else {
        $scope.search($scope.keyword);
      }
    });

    $scope.$on('$ionicView.enter', function () {


      //for filter section
      $scope.showSearchBox = true;
      $scope.allGroupsLoaded = false;

      //to show groups fiting into selection criteria
      $scope.viewResults = true;


      //Do Random Browse as soon as you enter
      //$scope.browseOrSearchSelection('search');
      //focus("searchGroupId");
      //invalidateSelections();

    });

    $scope.addElementsPaged = function (noInfiniteScrollBroadcast) {
      var doInfiniteScrollBroadcast = !noInfiniteScrollBroadcast;

      var url;
      $scope.setNetworkConnectionError(false);
      $scope.noNetworksFound = false;

      if (doInfiniteScrollBroadcast) {
        if ($scope.groups && $scope.groups.length < $scope.randomGroups.size) {
          url = ConstsService.MEMBERSHIP_SERVER_URL + '/browse-groups-next-circular/' + $scope.randomGroups.offset + '/20';
          $ionicLoading.show();
          $http.get(url, {
            timeout: ConstsService.HTTP_TIMEOUT
          }).then(function (response) {
            $ionicLoading.hide();
            if (response && response.data && response.data.groups) {
              $scope.groups = $scope.groups.concat(response.data.groups.map(function (grp) {
                grp.groupName = grp.name;
                grp.type = grp.o2m ? 'O2M' : 'M2M';
                return grp;
              }));
              $scope.randomGroups.offset = response.data.offset;
              $scope.$broadcast('scroll.infiniteScrollComplete');
              $log.debug("offset is " + $scope.randomGroups.offset);
              if ($scope.groups.length >= $scope.randomGroups.size) {
                $scope.randomGroups.allGroupsLoaded = true;
              }
              $scope.viewResults = true;
              setNoNetworksFound();
            }
          }).then(null, function (err) {
            $ionicLoading.hide();
            $cordovaToast.showLongCenter("Error getting groups. Please check internet connection");
            $scope.setNetworkConnectionError(true);
            console.error(JSON.stringify(err));
            //setNoNetworksFound();
            $scope.$broadcast('scroll.infiniteScrollComplete');
          });
        } else {
          $scope.$broadcast('scroll.infiniteScrollComplete');
        }
      } else {
        url = ConstsService.MEMBERSHIP_SERVER_URL + '/browse-groups-random/20';
        $ionicLoading.show();
        $http.get(url, {
          timeout: ConstsService.HTTP_TIMEOUT
        }).then(function (response) {
          $ionicLoading.hide();
          $scope.groups = [];
          if (response && response.data && response.data.groups) {
            $scope.groups = $scope.groups.concat(response.data.groups.map(function (grp) {
              grp.groupName = grp.name;
              grp.type = grp.o2m ? 'O2M' : 'M2M';
              return grp;
            }));
            $scope.randomGroups.offset = response.data.offset;
            $scope.randomGroups.initialOffset = response.data.offset;
            $scope.randomGroups.size = response.data.size;
            $log.debug("offset is " + $scope.randomGroups.offset);
            $scope.viewResults = true;
          }

        }).then(null, function (err) {
          $ionicLoading.hide();
          $cordovaToast.showLongCenter("Error getting groups. Please check internet connection");
          $scope.setNetworkConnectionError(true);
          console.error(JSON.stringify(err));
        });
      }
    };


    var invalidateSelections = function () {
      $scope.groups = null;
      $scope.keyword = null;
    };


    $scope.showFeaturedGroups = function () {
      var url = ConstsService.SEARCH_SERVER_URL + '/get-featured-groups';
      $ionicLoading.show();
      $scope.keyword='';
      $http.get(url, {
        timeout: ConstsService.HTTP_TIMEOUT
      }).then(function (response) {
        $ionicLoading.hide();
        $scope.groups = [];
        if (response && response.data && response.data.groups) {
          $scope.groups = $scope.groups.concat(response.data.groups.map(function (grp) {
            grp.groupName = grp.name;
            grp.type = grp.o2m ? 'O2M' : 'M2M';
            return grp;
          }));
          $scope.viewResults = true;
          $scope.browseFeatured = true;
        }

      }).then(null, function (err) {
        $ionicLoading.hide();
        $cordovaToast.showLongCenter("Error getting groups. Please check internet connection");
        $scope.setNetworkConnectionError(true);
        console.error(JSON.stringify(err));
      });
    };


    //$scope.groups = null;
    $scope.search = function (keyword) {
      $scope.keyword = keyword;
      refreshGroups(ConstsService.SEARCH_SERVER_URL + '/paged-search-groups-by-keyword/*/10', {
        'keyword': $scope.keyword
      }, true);
    };
    $scope.searchRest = function (keyword) {
      $scope.keyword = keyword;
      refreshGroups(ConstsService.SEARCH_SERVER_URL + '/paged-search-groups-by-keyword/' + $scope.offset + '/10', {
        'keyword': $scope.keyword
      }, false);
    };
    var refreshGroups = function (getGroupsUrl, postData, initial) {
      $log.debug("refreshGroups() called");
      $scope.setNetworkConnectionError(false);
      $scope.noNetworksFound = false;
      //$scope.groups = [];

      $ionicLoading.show();
      $http.post(getGroupsUrl, {
        map: postData
      }, {
        timeout: ConstsService.HTTP_TIMEOUT
      })
        .then(function (response) {
          $ionicLoading.hide();
          if (initial) {
            $scope.groups = [];
            $scope.browseFeatured = false;
          }
          if (response && response.data && response.data.groups && response.data.groups.length > 0) {
            $scope.groups = $scope.groups.concat(response.data.groups.map(function (grp) {
              grp.groupName = grp.name;
              grp.type = grp.o2m ? 'O2M' : 'M2M';
              return grp;
            }));
            $scope.allGroupsLoaded = false;
            $scope.offset = response.data.offset;
            $scope.size = response.data.size;
            $log.debug("offset is " + $scope.offset);
            $scope.viewResults = true;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          } else {
            $scope.allGroupsLoaded = true;
            $scope.$broadcast('scroll.infiniteScrollComplete');
          }
          setNoNetworksFound();
        }, function (err) {
          $ionicLoading.hide();
          $log.debug('error loading data..' + err.status);
          $cordovaToast.showLongCenter("Request timed out.");
          //setNoNetworksFound();
          $scope.setNetworkConnectionError(true);
        });
    };

    $scope.goToRequestMembership = function (grp) {
      function changeState() {
        NavigationService.goNativeToView('app.groups.join', {
          group: grp
        });
      }
      GroupService.getLocalGroup(ConstsService.GROUPS_START_KEY + (grp.gid||grp._id)).then(function (group) {
        if (angular.isDefined(group.membershipType) && angular.isDefined(group.userGroupStatus) &&
          group.userGroupStatus != ConstsService.USER_GROUP_STATUS_LEFT &&
          group.userGroupStatus != ConstsService.USER_GROUP_STATUS_PENDING) {
          var errormsg = GroupService.getGrouMembershipError(group.userGroupStatus);
          if (errormsg) {
            $cordovaToast.showLongCenter(errormsg);
          }
        }
        changeState();
      }).then(null, function () {
        changeState();
      });
    };
  }]);
