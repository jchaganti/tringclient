var tringmod = angular.module('Tring', ['tring.constService']);
var Tring = {};
var injector = angular.injector(['tring.constService']);
var ConstsService = injector.get('ConstsService');

Tring.getSenderId = function (topic) {
  var userId = null;
  if (topic) {
    userId = topic.substring(topic.lastIndexOf("/") + 1);
  }
  return userId;
};

Tring.getIdFromSid = function (prefix, sid) {
  var result = sid;
  if (sid && prefix && (sid.length > prefix.length)) {
    result = sid.substring(prefix.length, sid.length);
  }
  return result;
};

Tring.findIndex = function (array, id) {
  var low = 0,
    high = array.length,
    mid;
  while (low < high) {
    mid = (low + high) >>> 1;
    if (array[mid]._id > id) {
      low = mid + 1;
    } else {
      high = mid;
    }
  }
  return low;
};

Tring.findMessageIndex = function (array, id) {
  var index = -1;
  if (array) {
    if (array.length == 0 || id > array[0]._id) {
      index = 0;
    } else if (id < array[0]._id && id > array[array.length - 1]._id) {
      index = Tring.findIndex(array, id);
    }
  }
  return index;
};


Tring.findObjBasedOnFieldValue = function (array, field, value) {
  return array.filter(function (obj) {
    return obj[field] === value;
  })[0];
};

Tring.findIndexBasedOnFieldValue = function (array, field, value) {
  return array.map(function (obj) {
    return obj[field];
  }).indexOf(value);
};

Tring.findMessageForSenderId = function (messages, id) {
  //$log.debug("Sender Id: " + id);
  var result;
  if (messages) {
    var len = messages.length;
    result = len;
    for (var i = len - 1; i >= 0; i--) {
      if (messages[i]._id == id) {
        result = i;
        break;
      }
    }
  }
  return result;
};

Tring.isMessage = function (doc) {
  return doc && doc.hasOwnProperty("data") && doc.hasOwnProperty("ts");
};

Tring.isUserSid = function (sid) {
  return this.startsWith(sid, ConstsService.USERS_START_KEY);
};

Tring.isGroupSid = function (sid) {
  return this.startsWith(sid, ConstsService.GROUPS_START_KEY);
};

//Tring.isUserMessage = function (doc) {
//    return doc.type && doc.type === ConstsService.MESSAGE_TYPE_USER;
//};
//
Tring.isGroupMessage = function (doc) {
  //return doc && doc.sid && doc.sid.substring(0, ConstsService.GROUPS_START_KEY.length) === ConstsService.GROUPS_START_KEY;
  return doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_GROUP) === ConstsService.MESSAGE_TYPE_GROUP);
};
Tring.isContentMessage = function (doc) {
  return doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_CONTENT) === ConstsService.MESSAGE_TYPE_CONTENT);
};
Tring.isGroupInviteMessage = function (doc) {
  //return doc.type && doc.type === ConstsService.MESSAGE_TYPE_GROUP_INVITE;
  return doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && doc.appMsgType && ((doc.appMsgType & ConstsService.APP_MSG_TYPE_GROUP_INVITE) === ConstsService.APP_MSG_TYPE_GROUP_INVITE);
};
Tring.isGroupInviteAcceptMessage = function (doc) {
  return doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && doc.appMsgType && ((doc.appMsgType & ConstsService.APP_MSG_TYPE_ACCEPT_GROUP_INVITE) === ConstsService.APP_MSG_TYPE_ACCEPT_GROUP_INVITE);
};
Tring.isBroadcastStateMessage = function (doc) {
  return doc.type && doc.type === ConstsService.MESSAGE_TYPE_BROADCAST_STATE;
};
Tring.isTypingStatusMessage = function (doc) {
  return doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && doc.appMsgType && ((doc.appMsgType & ConstsService.APP_MSG_TYPE_TYPING_STATUS) === ConstsService.APP_MSG_TYPE_TYPING_STATUS);
};

Tring.isSyncUpMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_SYNC_UP)) === ConstsService.APP_MSG_TYPE_SYNC_UP;
};

Tring.isProfileSyncUpMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_SYNC_UP_PROFILE)) === ConstsService.APP_MSG_TYPE_SYNC_UP_PROFILE;
};
Tring.isGroupProfileSyncUpMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_SYNC_UP_GROUP_PROFILE)) === ConstsService.APP_MSG_TYPE_SYNC_UP_GROUP_PROFILE;
};
Tring.isDeliveryStatusMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_DELIVERY_STATUS)) === ConstsService.APP_MSG_TYPE_DELIVERY_STATUS;
};

Tring.isSyncUpAdminRoleMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_SYNC_UP_ADMIN)) === ConstsService.APP_MSG_TYPE_SYNC_UP_ADMIN;
};

Tring.isSyncUpSubgroupMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && msg.appMsgType && ((msg.appMsgType & ConstsService.APP_MSG_TYPE_SYNC_UP_SUBGROUP)) === ConstsService.APP_MSG_TYPE_SYNC_UP_SUBGROUP;
};

Tring.isPrivateMessageModerator = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_PVT) === ConstsService.MESSAGE_TYPE_PVT) && msg.pvtMsgType && ((msg.pvtMsgType & ConstsService.PVT_MSG_MODERATOR)) === ConstsService.PVT_MSG_MODERATOR;
};

Tring.isPrivateMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_PVT) === ConstsService.MESSAGE_TYPE_PVT)
};

Tring.isPrivateMessageMember = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_PVT) === ConstsService.MESSAGE_TYPE_PVT) && msg.pvtMsgType && ((msg.pvtMsgType & ConstsService.PVT_MSG_MEMBER)) === ConstsService.PVT_MSG_MEMBER;
};

Tring.isUserReputationMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_USER_REPUTATION) === ConstsService.MESSAGE_TYPE_USER_REPUTATION)
};

Tring.isAbuseMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_USER_REPUTATION) === ConstsService.MESSAGE_TYPE_USER_REPUTATION) && msg.userReputationMsgType && ((msg.userReputationMsgType & ConstsService.USER_REPUTATION_ABUSE)) === ConstsService.USER_REPUTATION_ABUSE;
};

Tring.isWarningMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_USER_REPUTATION) === ConstsService.MESSAGE_TYPE_USER_REPUTATION) && msg.userReputationMsgType && ((msg.userReputationMsgType & ConstsService.USER_REPUTATION_WARNING)) === ConstsService.USER_REPUTATION_WARNING;
};

Tring.isEvictionMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_USER_REPUTATION) === ConstsService.MESSAGE_TYPE_USER_REPUTATION) && msg.userReputationMsgType && ((msg.userReputationMsgType & ConstsService.USER_REPUTATION_EVICTION)) === ConstsService.USER_REPUTATION_EVICTION;
};

Tring.isClientStatusMessage = function (doc) {
  var status = doc.type && ((doc.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP) && doc.appMsgType && ((doc.appMsgType & ConstsService.APP_MSG_TYPE_CLIENT_STATUS) === ConstsService.APP_MSG_TYPE_CLIENT_STATUS);
  //TODO Remove below later
  if (!status) {
    if (doc.hasOwnProperty("clientStatus")) {
      status = true;
    }
  }
  return status;
};

Tring.isGroupMembershipRequestMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_PVT) === ConstsService.MESSAGE_TYPE_PVT) && msg.pvtMsgType && ((msg.pvtMsgType & ConstsService.PVT_MSG_TYPE_GROUP_MEMBERSHIP_REQUEST)) === ConstsService.PVT_MSG_TYPE_GROUP_MEMBERSHIP_REQUEST;
};

Tring.isSystemMessage = function (msg) {
  return msg.type && (((msg.type & ConstsService.MESSAGE_TYPE_USER_REPUTATION) === ConstsService.MESSAGE_TYPE_USER_REPUTATION) || ((msg.type & ConstsService.MESSAGE_TYPE_APP) === ConstsService.MESSAGE_TYPE_APP));
}
Tring.isMembershipAcceptRejectMessage = function (msg) {
  return msg.type && ((msg.type & ConstsService.MESSAGE_TYPE_PVT) === ConstsService.MESSAGE_TYPE_PVT) && msg.pvtMsgType && ((msg.pvtMsgType & ConstsService.PVT_MSG_TYPE_MEMBERSHIP_ACCEPT_REJECT)) === ConstsService.PVT_MSG_TYPE_MEMBERSHIP_ACCEPT_REJECT;
};

Tring.isChat = function (doc) {
  return doc && doc.hasOwnProperty("sendertype");
};

Tring.getCountryCode = function (mobilenumber) {
  //$log.debug("Parsing " + mobilenumber);
  var parsedMobileNum = parsePhone(mobilenumber);
  //$log.debug("Parsed MobileNum: " + JSON.stringify(parsedMobileNum));
  return parsedMobileNum !== null ? parsedMobileNum.countryISOCode : null;
};

Tring.endsWith = function (thisString, suffix) {
  return thisString.indexOf(suffix, thisString.length - suffix.length) !== -1;
};

Tring.startsWith = function (thisString, prefix) {
  return thisString.substring(0, prefix.length) === prefix;
};

Tring.getUserMQTTtopic = function (topic) {
  return topic.split('/').map(function (e, i, self) {
    return (i === self.length - 1) ? (ConstsService.USERS_START_KEY + e) : e;
  }).join('/');
};

Tring.getGroupMQTTtopic = function (topic) {
  return topic.split('/').map(function (e, i, self) {
    return (i === self.length - 1) ? (ConstsService.GROUPS_START_KEY + e) : e;
  }).join('/');
};
Tring.getHandleTopic = function (groupTopic, handleName) {
  return groupTopic + '/' + handleName;
};

Tring.debounce = function (func, wait, immediate) {
  var timeout;
  var decorator = function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      if (!immediate) {
        func.apply(context, args);
      }
    };
    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait || 200);
    if (callNow) {
      func.apply(context, args);
    }
  };

  return {
    callback: decorator,
    cancel: function () {
      clearTimeout(timeout);
      timeout = null;
    }
  }
};

Tring.isOpenGroup = function (group) {
  return group && !this.isClosedGroup(group);
}

Tring.isClosedGroup = function (group) {
  return group && ((angular.isDefined(group.closed) && group.closed) || (angular.isDefined(group.groupClosed) && group.groupClosed));
}

Tring.isM2MGroup = function (group) {
  return group && !this.isO2MGroup(group);
}

Tring.isO2MGroup = function (group) {
  return group && ((angular.isDefined(group.o2m) && group.o2m) || (angular.isDefined(group.groupO2M) && group.groupO2M));
}

Tring.getGroupMembershipRole = function (group) {
  return group ? group.membershipType : null;
}

Tring.isGroupOwner = function (group) {
  return group && (this.getGroupMembershipRole(group) === ConstsService.OWNER_ROLE)
}

Tring.getUserGroupStatus = function (group) {
  return group ? group.userGroupStatus : null;
}

Tring.isGroupDeleted = function (group) {
  return group && group.deleted;
}

Tring.getParentGroupId = function (group) {
  return group ? group.parentGroupId : null;
}

Tring.isGroupFullyVisible = function (group) {
  return group && group.fullVisibility;
}

Tring.getGroupCreator = function (group) {
  return group ? group.groupCreator : null;
}

Tring.addPrivateMessageMemberToMessage = function (msg) {
  if (msg && msg.type) {
    msg.type = msg.type | ConstsService.MESSAGE_TYPE_PVT;
    msg.pvtMsgType = ConstsService.PVT_MSG_MEMBER;
  }
}

Tring.addPrivateMessageModeratorToMessage = function (msg) {
  if (msg && msg.type) {
    msg.type = msg.type | ConstsService.MESSAGE_TYPE_PVT;
    msg.pvtMsgType = ConstsService.PVT_MSG_MODERATOR;
  }
}

Tring.addAbuseMessage = function (msg) {
  if (msg && msg.type) {
    msg.type = msg.type | ConstsService.MESSAGE_TYPE_USER_REPUTATION;
    msg.userReputationMsgType = ConstsService.USER_REPUTATION_ABUSE;
  }
}

Tring.addWarningMessage = function (msg) {
  if (msg && msg.type) {
    msg.type = msg.type | ConstsService.MESSAGE_TYPE_USER_REPUTATION;
    msg.userReputationMsgType = ConstsService.USER_REPUTATION_WARNING;
  }
}

Tring.addEvictionMessage = function (msg) {
  if (msg && msg.type) {
    msg.type = msg.type | ConstsService.MESSAGE_TYPE_USER_REPUTATION;
    msg.userReputationMsgType = ConstsService.USER_REPUTATION_EVICTION;
  }
}

Tring.isDestinationPrivate = function (topic) {
  return topic && this.endsWith("/pvt");
}

Tring.isGroupObject = function (obj) {
  return obj._id && this.isGroupSid(obj._id);
};

Tring.isContactObject = function (obj) {
  return obj._id && this.isUserSid(obj._id);
};

Tring.clientToServerGroupId = function (gid) {
  return gid.replace(ConstsService.GROUPS_START_KEY, '');
};

Tring.getMembershipTypeString = function (code) {
    var result = "";
    switch (code) {
    case ConstsService.OWNER_ROLE:
        result = "Owner";
        break;
    case ConstsService.MEMBER_ROLE:
        result = "Member";
        break;
    case ConstsService.MODERATOR_ROLE:
        result = "Moderator";
        break;
    case ConstsService.ADMIN_ROLE:
        result = "Administrator";
        break;
    case ConstsService.NON_MEMBER_ROLE:
        result = "Member";
        break;
    default:
        result = "Invalid membership code";
        break;
    }
    return result;
};

Tring.getPopOverActionsTemplate = function (actions, input, actionsPerRow) {
  var template = '';
  var len = actions.length;
  if (!actionsPerRow) {
    actionsPerRow = 3;
  }
  //$log.debug("Actions: " + JSON.stringify(actions));
  var rows = Math.ceil(len / actionsPerRow);
  for (var k = 0; k < rows; k++) {
    template += '<div class="row messageRw">';
    for (var i = 0; i < actionsPerRow; i++) {
      var index = (k * actionsPerRow) + i;
      if(index > len - 1) {
        break;
      }
      template += '<div class="col col-33" style="padding:0px;" ng-click="' + actions[index].cb + '(' + input + ')' + ';">';
      template += '<a href="#" >';
      template += actions[index].img;
      template += '<div style="padding:0px;font-size:12px; color:white">' + actions[index].name + '</div>';
      template += '</a>';
      template += '</div>';
    }
    template += "</div>";
  }
  return template;
}

Tring.trackAndCall = function(trackingCondition, preTrackCB, postTrackCB, delay, $interval, times) {
  if(angular.isFunction(trackingCondition) && angular.isFunction(postTrackCB)) {
    if(trackingCondition()) {
      postTrackCB();
    }
    else {
      var trackingTid = null;
      var count = 0;
      var trackingChecker = function () {
        if (!trackingCondition()) {
          //$log.debug("Not yet passed trackingCondtion");
          if(times && count === times) {
            if(trackingTid) {
              $interval.cancel(trackingTid);
              trackingTid = null;
            }
          }
          else {
            if(times) {
              count++;
            }
            if(angular.isFunction(preTrackCB)) {
              preTrackCB();
            }
          }
        } else {
          $interval.cancel(trackingTid);
          trackingTid = null;
          //$log.debug("The timer trackingChecker  cancelled");
          postTrackCB();
        }
      }
      if(!delay) {
        delay = 100;
      }

      trackingTid = $interval(trackingChecker, delay);
    }
  }
}

Tring.getPopOverTemplate = function (actions, input, actionsPerRow) {
  if (actions) {
    var template = '<ion-popover-view style="min-height: 55px !important;" class="actionViewPop"><ion-content>';
    template += this.getPopOverActionsTemplate(actions, input, actionsPerRow);
    template += "</ion-content></ion-popover-view>";
    return template;
  }
}


Tring.mimeType = function (fname) {
  /*   var mimeTable = {
   '323': 'text/h323',
   '3gp': 'video/3gpp',
   '3gpp': 'video/3gpp',
   '*': 'application/octet-stream',
   'acx': 'application/internet-property-stream',
   'ai': 'application/postscript',
   'aif': 'audio/x-aiff',
   'aifc': 'audio/x-aiff',
   'aiff': 'audio/x-aiff',
   'amr': 'audio/amr',
   'asf': 'video/x-ms-asf',
   'asr': 'video/x-ms-asf',
   'asx': 'video/x-ms-asf',
   'au': 'audio/basic',
   'avi': 'video/x-msvideo',
   'axs': 'application/olescript',
   'bas': 'text/plain',
   'bcpio': 'application/x-bcpio',
   'bin': 'application/octet-stream',
   'bmp': 'image/bmp',
   'c': 'text/plain',
   'cat': 'application/vnd.ms-pkiseccat',
   'cdf': 'application/x-cdf',
   //'cdf': 'application/x-netcdf',
   'cer': 'application/x-x509-ca-cert',
   'class': 'application/octet-stream',
   'clp': 'application/x-msclip',
   'cmx': 'image/x-cmx',
   'cod': 'image/cis-cod',
   'cpio': 'application/x-cpio',
   'crd': 'application/x-mscardfile',
   'crli': 'application/pkix-crl',
   'crt': 'application/x-x509-ca-cert',
   'csh': 'application/x-csh',
   'css': 'text/css',
   'dcr': 'application/x-director',
   'der': 'application/x-x509-ca-cert',
   'dir': 'application/x-director',
   'dll': 'application/x-msdownload',
   'dms': 'application/octet-stream',
   'doc': 'application/msword',
   'dot': 'application/msword',
   'dvi': 'application/x-dvi',
   'dxr': 'application/x-director',
   'eps': 'application/postscript',
   'etx': 'text/x-setext',
   'evy': 'application/envoy',
   'exe': 'application/octet-stream',
   'fif': 'application/fractals',
   'flr': 'x-world/x-vrml',
   'gif': 'image/gif',
   'gtar': 'application/x-gtar',
   'gz	': 'application/x-gzip',
   'h': 'text/plain',
   'hdf': 'application/x-hdf',
   'hlp': 'application/winhlp',
   'hqx': 'application/mac-binhex40',
   'hta': 'application/hta',
   'htc': 'text/x-component',
   'htm': 'text/html',
   'html': 'text/html',
   'htt': 'text/webviewhtml',
   'ico': 'image/x-icon',
   'ief': 'image/ief',
   'iii': 'application/x-iphone',
   'ins': 'application/x-internet-signup',
   'isp': 'application/x-internet-signup',
   'jfif': 'image/pipeg',
   'jpe': 'image/jpeg',
   'jpeg': 'image/jpeg',
   'jpg': 'image/jpeg',
   'js': 'application/x-javascript',
   'latex': 'application/x-latex',
   'lha': 'application/octet-stream',
   'lsf': 'video/x-la-asf',
   'lsx': 'video/x-la-asf',
   'lzh': 'application/octet-stream',
   'm13': 'application/x-msmediaview',
   'm14': 'application/x-msmediaview',
   'm3u': 'audio/x-mpegurl',
   'man': 'application/x-troff-man',
   'mdb': 'application/x-msaccess',
   'me': 'application/x-troff-me',
   'mht': 'message/rfc822',
   'mhtml': 'message/rfc822',
   'mid': 'audio/mid',
   'mny': 'application/x-msmoney',
   'mov': 'video/quicktime',
   'movie': 'video/x-sgi-movie',
   'mp2': 'video/mpeg',
   'mp3': 'audio/mpeg',
   'mpa': 'video/mpeg',
   'mpe': 'video/mpeg',
   'mpeg': 'video/mpeg',
   'mpg': 'video/mpeg',
   'mpp': 'application/vnd.ms-project',
   'mpv2': 'video/mpeg',
   'ms': 'application/x-troff-ms',
   'msg': 'application/vnd.ms-outlook',
   'mvb': 'application/x-msmediaview',
   'nc': 'application/x-netcdf',
   'nws': 'message/rfc822',
   'oda': 'application/oda',
   'p10': 'application/pkcs10',
   'p12': 'application/x-pkcs12',
   'p7b': 'application/x-pkcs7-certificates',
   'p7c': 'application/x-pkcs7-mime',
   'p7m': 'application/x-pkcs7-mime',
   'p7r': 'application/x-pkcs7-certreqresp',
   'p7s': 'application/x-pkcs7-signature',
   'pbm': 'image/x-portable-bitmap',
   'pdf': 'application/pdf',
   'pfx': 'application/x-pkcs12',
   'pgm': 'image/x-portable-graymap',
   'pko': 'application/ynd.ms-pkipko',
   'pma': 'application/x-perfmon',
   'pmc': 'application/x-perfmon',
   'pml': 'application/x-perfmon',
   'pmr': 'application/x-perfmon',
   'pmw': 'application/x-perfmon',
   'pnm': 'image/x-portable-anymap',
   'pot': 'application/vnd.ms-powerpoint',
   'ppm': 'image/x-portable-pixmap',
   'pps': 'application/vnd.ms-powerpoint',
   'ppt': 'application/vnd.ms-powerpoint',
   'prf': 'application/pics-rules',
   'ps': 'application/postscript',
   'pub': 'application/x-mspublisher',
   'qt': 'video/quicktime',
   'ra': 'audio/x-pn-realaudio',
   'ram': 'audio/x-pn-realaudio',
   'ras': 'image/x-cmu-raster',
   'rgb': 'image/x-rgb',
   'rmi': 'audio/mid',
   'roff': 'application/x-troff',
   'rtf': 'application/rtf',
   'rtx': 'text/richtext',
   'scd': 'application/x-msschedule',
   'sct': 'text/scriptlet',
   'setpay': 'application/set-payment-initiation',
   'setreg': 'application/set-registration-initiation',
   'sh': 'application/x-sh',
   'shar': 'application/x-shar',
   'sit': 'application/x-stuffit',
   'snd': 'audio/basic',
   'spc': 'application/x-pkcs7-certificates',
   'spl': 'application/futuresplash',
   'src': 'application/x-wais-source',
   'sst': 'application/vnd.ms-pkicertstore',
   'stl': 'application/vnd.ms-pkistl',
   'stm': 'text/html',
   'sv4cpio': 'application/x-sv4cpio',
   'sv4crc': 'application/x-sv4crc',
   'svg': 'image/svg+xml',
   'swf': 'application/x-shockwave-flash',
   't': 'application/x-troff',
   'tar': 'application/x-tar',
   'tcl': 'application/x-tcl',
   'tex': 'application/x-tex',
   'texi': 'application/x-texinfo',
   'texinfo': 'application/x-texinfo',
   'tgz': 'application/x-compressed',
   'tif': 'image/tiff',
   'tiff': 'image/tiff',
   'tr': 'application/x-troff',
   'trm': 'application/x-msterminal',
   'tsv': 'text/tab-separated-values',
   'txt': 'text/plain',
   'uls': 'text/iuls',
   'ustar': 'application/x-ustar',
   'vcf': 'text/x-vcard',
   'vrml': 'x-world/x-vrml',
   'wav': 'audio/x-wav',
   'wcm': 'application/vnd.ms-works',
   'wdb': 'application/vnd.ms-works',
   'wks': 'application/vnd.ms-works',
   'wmf': 'application/x-msmetafile',
   'wps': 'application/vnd.ms-works',
   'wri': 'application/x-mswrite',
   'wrl': 'x-world/x-vrml',
   'wrz': 'x-world/x-vrml',
   'xaf': 'x-world/x-vrml',
   'xbm': 'image/x-xbitmap',
   'xla': 'application/vnd.ms-excel',
   'xlc': 'application/vnd.ms-excel',
   'xlm': 'application/vnd.ms-excel',
   'xls': 'application/vnd.ms-excel',
   'xlt': 'application/vnd.ms-excel',
   'xlw': 'application/vnd.ms-excel',
   'xof': 'x-world/x-vrml',
   'xpm': 'image/x-xpixmap',
   'xwd': 'image/x-xwindowdump',
   'z': 'application/x-compress',
   'zip': 'application/zip',
   };
   return function (ext) {
   var mType = mimeTable[ext];
   return mType ? mType : 'text/plain';
   };
   */
  /*  Tring.returnTextWithMarkedUpUris(text) {
   var regexp = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/;*/


  return window.mimeType.lookup(fname, false, 'text/plain');
};
tringmod.value('Tring', Tring);
