angular.module('tring.navigationService', [])
    .factory('NavigationService', ['$state', '$ionicHistory', '$log', function ($state, $ionicHistory, $log) {

        var applyDirection = function (direction) {
            window.plugins.nativepagetransitions.slide({
                    "direction": direction
                },
                function (msg) {
                    $log.debug("success: " + msg)
                }, // called when the animation has finished
                function (msg) {
                    alert("error: " + msg)
                } // called in case you pass in weird values
            );

        };


        var goNativeToView = function (view, data, direction) {
            $state.go(view, data);
            //applyDirection(direction);
        };

        var goNativeBack = function () {
            $ionicHistory.goBack();
            //applyDirection('left');
        };

        return {
            goNativeToView: goNativeToView,
            goNativeBack: goNativeBack
        };

    }]);
