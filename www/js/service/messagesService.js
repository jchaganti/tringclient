(function () {
    var module = angular.module('tring.messagesService', ['tring.dbService', 'qImproved', 'tring.constService']);

    module.factory('MessagesService', ['$q', 'DBService', 'ConstsService', '$log',
    function ($q, DBService, ConstsService, $log) {
            $log.debug("Creating MessagesService");

            function getStartKey(senderId) {
                return pouchCollate.toIndexableString([senderId, ConstsService.DEFAULT_START_COUNTER]);
            }

            function getEndKey(senderId) {
                return pouchCollate.toIndexableString([senderId, ConstsService.DEFAULT_END_COUNTER]);
            }

            function handleResponse(response) {
                var _docs = response.rows.map(function (row) {
                    return row.doc;
                });
                return $q.when(_docs);

            }

            var messageOptions = {
                limit: 40,
                include_docs: true,
                descending: true,
            };

            function getFirstMessagePage(senderId) {
                var msgDb = DBService.msgDb();
                messageOptions.startkey = getStartKey(senderId);
                messageOptions.endkey = getEndKey(senderId);

                var p1 = function () {
                    return msgDb.allDocs(messageOptions);
                }

                return $q.serial([p1, handleResponse]);
            }

            var tillLastReadMessageOptions = {
                include_docs: true,
                descending: true
            }

            function getUnReadMessageCount(senderId, lastUnReadMessageId) {
                var msgDb = DBService.msgDb();
                tillLastReadMessageOptions.startkey = getStartKey(senderId);
                if (lastUnReadMessageId) {
                    tillLastReadMessageOptions.endkey = lastUnReadMessageId;
                } else {
                    tillLastReadMessageOptions.endkey = getEndKey(senderId);
                }


                var p1 = function () {
                    return msgDb.allDocs(tillLastReadMessageOptions);
                }

                var p2 = function (response) {
                    var _docs = response.rows.map(function (row) {
                        return row.doc;
                    }).filter(function (doc, index, self) {
                        return doc.dir === 0;
                    });

                    if (lastUnReadMessageId && _docs.length > 0) {
                        var firstMessageAfterLastUnread = _docs[_docs.length - 1];
                        // If firstMessageAfterLastUnread and lastUnReadMessage are same, ignore the first one
                        if (firstMessageAfterLastUnread._id === lastUnReadMessageId) {
                            return $q.when(_docs.length - 1);
                        } else {
                            return $q.when(_docs.length);
                        }

                    } else {
                        return $q.when(_docs.length);
                    }

                }

                return $q.serial([p1, p2]);
            }

            var prevMessageOptions = {
                limit: 40,
                include_docs: true,
                descending: true
            }

            function getPrevMessagePage(senderId, lastMessageId) {
                var msgDb = DBService.msgDb();
                prevMessageOptions.startkey = lastMessageId;
                prevMessageOptions.endkey = getEndKey(senderId);
                prevMessageOptions.skip = 1;

                var p1 = function () {
                    return msgDb.allDocs(prevMessageOptions);
                }

                return $q.serial([p1, handleResponse]);
            }

            function registerMessageDBChange(cb) {
                var msgDb = DBService.msgDb();
                if (cb) {
                    msgDb.changes({
                            live: true,
                            since: 'now',
                            include_docs: true
                        })
                        .on('change', cb);
                }
            }

            var recentMessageOptions = {
                limit: 1,
                include_docs: true,
                descending: true,
            };

            function getRecentMessage(senderId) {
                //$log.debug("senderId: " + senderId);
                var msgDb = DBService.msgDb();
                recentMessageOptions.startkey = getStartKey(senderId);
                recentMessageOptions.endkey = getEndKey(senderId);
                //$log.debug("startkey: " + recentMessageOptions.startkey);
                var p1 = function () {
                    return msgDb.allDocs(recentMessageOptions);
                };
                return $q.serial([p1, handleResponse]);
            }

            function getGroupInviteMessage(group, forSelf) {
                if (forSelf) {
                    return "You have sent an invitation to join the group, '" + group.name + "'";
                } else {
                    return "You have received an invitation to join the group, '" + group.name + "' (Click icon for details)";
                }
            }

            function getGroupAcceptInviteMessage(message, forSelf) {
                var msg = "New User, '" + message.handleName + "' has joined";
                if (forSelf) {
                    msg = "You have joined this group under handle name '" + message.handleName + "'";
                }
                return msg;
            }

            function getGroupRequestAcceptedMessage(message) {
                var msg = "User with handle '" + message.handleName + "' admitted into the group";
                return msg;
            }

            function deleteMessage(messageId) {
                var msgDb = DBService.msgDb();
                var p1 = function () {
                    return msgDb.get(messageId);
                }

                var p2 = function (doc) {
                    return msgDb.remove(doc);
                }

                var p3 = function (res) {
                    return $q.when(true);
                }

                return $q.serial([p1, p2, p3]);
            }

            function getMessageById(messageId) {
                var msgDb = DBService.msgDb();
                return msgDb.get(messageId);
            }

            function updateMessageInLocalDatabase(id, changes) {
                var msgDb = DBService.msgDb();
                msgDb.get(id).then(function (doc) {
                    angular.extend(doc, changes);
                    msgDb.put(doc);
                }).catch(function (err) {
                    console.error(err);
                });
            }

            return {
                getPrevMessagePage: getPrevMessagePage,
                getFirstMessagePage: getFirstMessagePage,
                getUnReadMessageCount: getUnReadMessageCount,
                registerMessageDBChange: registerMessageDBChange,
                getRecentMessage: getRecentMessage,
                getGroupInviteMessage: getGroupInviteMessage,
                getGroupAcceptInviteMessage: getGroupAcceptInviteMessage,
                getGroupRequestAcceptedMessage: getGroupRequestAcceptedMessage,
                deleteMessage: deleteMessage,
                updateMessageInLocalDatabase: updateMessageInLocalDatabase,
                getMessageById: getMessageById
            };
    }]);

})();
