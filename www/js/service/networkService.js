(function () {
    angular.module('tring.networkService', ['tring.navigationService', 'tring.dbService'])
        .factory('NetworkService', ['$cordovaNetwork', '$rootScope', '$log', 'ConstsService', 'NavigationService', '$window', 'DBService', '$timeout',
      function ($cordovaNetwork, $rootScope, $log, ConstsService, NavigationService, $window, DBService, $timeout) {

                $log.debug("Creating Network service");
                var onlineCBs = [];
                var offlineCBs = [];
                var onPauseCBs = [];
                var onResumeCBs = [];
                var onPendingCaptureResultCB = null;
                var onPendingCaptureErrorCB = null;
                var onPauseCBMap = {};
                var onResumeCBMap = {};

                function addOnlineCB(cb) {
                    if (cb && angular.isFunction(cb)) {
                        onlineCBs.push(cb);
                    }
                }

                function addOfflineCB(cb) {
                    if (cb && angular.isFunction(cb)) {
                        offlineCBs.push(cb);
                    }
                }

                function addOnPauseCB(cb) {
                    if (cb && angular.isFunction(cb)) {
                        onPauseCBs.push(cb);
                    }
                }

                function addOnResumeCB(cb) {
                    if (cb && angular.isFunction(cb)) {
                        onResumeCBs.push(cb);
                    }
                }

                function isOnline() {
                    return $cordovaNetwork.isOnline();
                }

                function isOffline() {
                    return $cordovaNetwork.isOffline();
                }

                function startService() {
                    // listen for Online event
                    $rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
                        onlineCBs.forEach(function (cb) {
                            cb();
                        });

                    });

                    // listen for Offline event
                    $rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
                        offlineCBs.forEach(function (cb) {
                            cb();
                        });
                    });

                    document.addEventListener("pause", function () {
                        onPauseCBs.forEach(function (cb) {
                            cb();
                        });
                        for (var k in onPauseCBMap) {
                            onPauseCBMap[k]();
                        }
                    });

                    document.addEventListener("resume", function (event) {
                        onResumeCBs.forEach(function (cb) {
                            cb(event);
                        });
                        for (var k in onResumeCBMap) {
                            onResumeCBMap[k](event);
                        }
                        // The onResumeCBMap will be empty at startup because the setOnResumeFunction() isn't
                        // called, and
                        //We need a default resume for the TRING_PICKER_STATE_DATA results from picker
                        //plugin cordova-camera-plugin, because we need to do something when this will be 
                        //called after the app is restarted . Event arg will contain the result
                        processResumeEvent(event);
                    });

                    document.addEventListener("pendingcaptureresult",
                        pendingResultsFunctionGenerator(ConstsService.APP_STORAGE_KEY));

                    document.addEventListener("pendingcaptureerror",
                        pendingResultsErrorFunctionGenerator(ConstsService.APP_STORAGE_KEY));
                }

                var pendingResultsErrorFunctionGenerator = function (key) {
                    return function (error) {
                        var mbrDb = DBService.mbrDb();
                        mbrDb.get(key).then(function (storedState) {
                            var appState;
                            if (storedState) {
                                //appState = JSON.parse(storedState);
                                //$window.localStorage.setItem(ConstsService.APP_STORAGE_KEY, null);
                                appState = storedState.appState;
                                mbrDb.put(angular.extend(storedState, {
                                    _deleted: true
                                })).then(function () {
                                    $log.debug("pendingcaptureerror called");
                                    $log.debug("appState is " + JSON.stringify(appState));
                                    NavigationService.goNativeToView(storedState.appRouteState, appState, 'right');
                                });
                            }
                        });
                    };
                };

                var pendingResultsFunctionGenerator = function (key) {
                    return function (event) {
                        //onPendingCaptureResultCB(event);
                        console.log("pendingcaptureresult - event is " + JSON.stringify(event));
                        //Example event object is 
                        //"pendingcaptureresult - event is [{"name":"1464595006020.jpg","localURL":"cdvfile://localhost/root/storage/emulated/0/DCIM/Camera/1464595006020.jpg","type":"image/jpeg","lastModified":null,"lastModifiedDate":1464595006000,"size":200504,"start":0,"end":0,"fullPath":"file:/storage/emulated/0/DCIM/Camera/1464595006020.jpg"}]"
                        var mbrDb = DBService.mbrDb();
                        mbrDb.get(key).then(function (storedState) {
                            var appState;
                            if (storedState) {
                                appState = storedState.appState;
                                mbrDb.put(angular.extend(storedState, {
                                    _deleted: true
                                })).then(function () {
                                    $log.debug("pendingcaptureresult called");
                                    if (event[0].fullPath.indexOf('file:///') < 0) {
                                        event[0].fullPath = event[0].fullPath.replace('file:/', 'file:///');
                                    }
                                    appState.pendingCaptureResults = event;
                                    $log.debug("appState is " + JSON.stringify(appState));
                                    $log.debug("appRouteState is " + JSON.stringify(storedState.appRouteState));
                                    $timeout(function () {
                                        NavigationService.goNativeToView(storedState.appRouteState, appState, 'right');
                                    }, 300);
                                });
                            }
                        });
                    };
                };

                function processResumeEvent(event) {
                    $log.debug('In processResumeEvent');
                    if (event.pendingResult) {
                        var result = event.pendingResult.result;
                        var status = event.pendingResult.pluginStatus;
                        var service = event.pendingResult.pluginServiceName;
                        var key;
                        $log.debug('processResumeEvent -  event is ' + JSON.stringify(event));
                        if (service === 'Camera') {
                            key = ConstsService.PICKER_STATE_STORAGE_KEY;
                        } else if (service === 'FileChooser') {
                            key = ConstsService.CONTENT_BROWSER_STORAGE_KEY;
                        }
                        if (status === 'OK' || status === 'ok' && result) {
                            $log.debug('processResumeEvent -  result is ' + JSON.stringify(result));

                            $timeout(function () {
                                pendingResultsFunctionGenerator(key)([{
                                    'fullPath': result,
                                    'fromFileChooser': true
                                    }]);
                            }, 100);
                        } else {
                            $timeout(function () {
                                pendingResultsErrorFunctionGenerator(key)(status);
                            }, 100);
                        }
                    }
                }

                function setOnResumeFunction(key) {
                    var onResume = function () {
                        var mbrDb = DBService.mbrDb();
                        mbrDb.get(key).then(function (storedState) {
                            var appState;
                            if (storedState) {
                                appState = storedState.appState;
                                mbrDb.put(angular.extend(storedState, {
                                    _deleted: true
                                })).then(function () {
                                    $log.debug("onResume called - successfully deleted capture state record");
                                    $log.debug("appState is " + JSON.stringify(appState));
                                });
                            }
                        });
                        delete onPauseCBMap[key];
                        delete onResumeCBMap[key];
                    };
                    onResumeCBMap[key] = onResume;
                }

                return {
                    startService: startService,
                    addOnlineCB: addOnlineCB,
                    addOfflineCB: addOfflineCB,
                    addOnPauseCB: addOnPauseCB,
                    addOnResumeCB: addOnResumeCB,
                    isOnline: isOnline,
                    isOffline: isOffline,
                    setOnResumeFunction: setOnResumeFunction
                };

      }]);
})();