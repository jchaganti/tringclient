angular.module('tring.thumbnailService', ['qImproved', 'ng'])
    .factory('ThumbnailService', ['$q', '$timeout', '$log', function ($q, $timeout, $log) {


        var imageData = function (fileDataURL, thumbSize) {
            var canvas = angular.element('<canvas></canvas>')[0];
            var context = canvas.getContext("2d");
            var deferred = $q.defer();
            var img = new Image();
            img.onload = function (e) {
                var height = this.height;
                var width = this.width;
                if (height > width) {
                    height = width;
                }
                canvas.width = thumbSize;
                canvas.height = thumbSize * height / width;
                context.drawImage(this, 0, 0, width, height, 0, 0, canvas.width, canvas.height);
                deferred.resolve(canvas.toDataURL("image/jpeg"));
            };
            img.src = fileDataURL;
            return deferred.promise;
        };

        var pkVideoData = function (fileDataURL, thumbSize) {
            // "file:/storage/emulated/0/Pictures/1449332595493.jpg"
            var folderPath = fileDataURL.split('/');
            var file = folderPath.pop();
            var fileBaseName = file.split('.')[0];
            var targetThumbnailPath = cordova.file.cacheDirectory + fileBaseName + (new Date()).getTime() + '.png';
            if (fileDataURL.indexOf('file:///') < 0) {
                fileDataURL = fileDataURL.replace('file:/', 'file:///');
            }
            var deferred = $q.defer();

            function success(path) {
                $log.debug("pkVideoData successfully created " + path);
                var canvas = angular.element('<canvas></canvas>')[0];
                var context = canvas.getContext("2d");
                var img = new Image();
                img.onload = function (e) {
                    var height = this.height;
                    var width = this.width;
                    if (height > width) {
                        height = width;
                    }
                    canvas.width = thumbSize;
                    canvas.height = thumbSize * height / width;
                    context.drawImage(this, 0, 0, width, height, 0, 0, canvas.width, canvas.height);
                    deferred.resolve(canvas.toDataURL("image/jpeg"));
                };
                img.src = path;
            }

            function failure(err) {
                console.error("Error creating pkVideoData thumbnail. err is " + JSON.stringify(err));
                deferred.reject(err);
            }
            window.PKVideoThumbnail.createThumbnail(fileDataURL, targetThumbnailPath, success, failure);
            return deferred.promise;
        };

        var videoData = function (fileDataURL, thumbSize) {
            var canvas = angular.element('<canvas></canvas>')[0];
            var context = canvas.getContext("2d");
            canvas.width = thumbSize;
            canvas.height = thumbSize;
            var deferred = $q.defer();
            var vid = angular.element('<video src="' + fileDataURL + '" crossorigin="anonymous"></video>')[0];
            vid.width = thumbSize;
            vid.height = thumbSize;
            // We don't want it to start playing yet.
            vid.autoplay = false;
            vid.loop = false;

            // No need for user to see the video itself.
            vid.style.display = "none";
            vid.type = "video/mp4";
            vid.addEventListener("loadeddata", function (e) {
                $timeout(function () {
                    context.drawImage(vid, 0, 0);
                    deferred.resolve(canvas.toDataURL("image/png"));
                }, 100);
            });
            vid.src = fileDataURL;
            return deferred.promise;
        };

        var getThumbnailFunction = function (type) {
            return type.indexOf('image') === 0 ? imageData :
                (type.indexOf('video') === 0 ? pkVideoData : function () {
                    return $q.resolve("");
                });
        };

        return {
            imageData: imageData,
            videoData: pkVideoData,
            getThumbnailFunction: getThumbnailFunction
        };
            }]);