angular.module('tring.fileServices', ['qImproved', 'tring.constService'])
    .factory('FileBrowseService', ['$q', '$cordovaFileTransfer', 'ConstsService', 'AuthService', '$http', '$cordovaFile', '$timeout', '$log', 'Tring', function ($q, $cordovaFileTransfer, ConstsService, AuthService, $http, $cordovaFile, $timeout, $log, Tring) {
        var File = function () {};
        $log.debug("Entered FileBrowseService");

        /*        function b64toBlob(b64Data, contentType, sliceSize) {
                    contentType = contentType || '';
                    sliceSize = sliceSize || 512;

                    var byteCharacters = window.atob(b64Data);
                    var byteArrays = [];

                    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                        var slice = byteCharacters.slice(offset, offset + sliceSize);

                        var byteNumbers = new Array(slice.length);
                        for (var i = 0; i < slice.length; i++) {
                            byteNumbers[i] = slice.charCodeAt(i);
                        }

                        var byteArray = new Uint8Array(byteNumbers);

                        byteArrays.push(byteArray);
                    }

                    var blob = new Blob(byteArrays, {
                        type: contentType
                    });
                    return blob;
                }*/

        function dataURItoBlob(dataURI, dataTYPE) {
            var binary = atob(dataURI.split(',')[1]),
                array = [];
            for (var i = 0; i < binary.length; i++) array.push(binary.charCodeAt(i));
            return new Blob([new Uint8Array(array)], {
                type: dataTYPE
            });
        }

        File.prototype = {

            getParentDirectory: function (path) {
                var deferred = $q.defer();
                window.resolveLocalFileSystemURL(path, function (fileSystem) {
                    fileSystem.getParent(function (result) {
                        if (result.nativeURL === path) {
                            deferred.reject("problem getting to parent directory");
                        } else {
                            deferred.resolve(result);
                        }
                    }, function (error) {
                        deferred.reject(error);
                    });
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },

            getEntriesAtRoot: function () {
                var deferred = $q.defer();
                window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
                    function (fileSystem) {
                        var directoryReader = fileSystem.root.createReader();
                        directoryReader.readEntries(function (entries) {
                            deferred.resolve(entries);
                        }, function (error) {
                            deferred.reject(error);
                        });
                    },
                    function (error) {
                        deferred.reject(error);
                    });
                return deferred.promise;
            },

            getEntries: function (path) {
                var deferred = $q.defer();
                window.resolveLocalFileSystemURL(path, function (fileSystem) {
                    var directoryReader = fileSystem.createReader();
                    directoryReader.readEntries(function (entries) {
                        deferred.resolve(entries);
                    }, function (error) {
                        deferred.reject(error);
                    });
                }, function (error) {
                    deferred.reject(error);
                });
                return deferred.promise;
            },
            uploadB64Data: function (data, uploadOptions) {
                var contentType = uploadOptions.mimeType;
                var blob = dataURItoBlob(data, contentType);
                var blobUrl = URL.createObjectURL(blob);
                var storageDirectory = cordova.file.documentsDirectory;

                if (ionic.Platform.isAndroid()) {
                    //storageDirectory = cordova.file.externalRootDirectory;
                    storageDirectory = cordova.file.externalApplicationStorageDirectory;
                    if (!storageDirectory) {
                        storageDirectory = cordova.file.externalRootDirectory;
                        if (!storageDirectory) {
                            storageDirectory = cordova.file.dataDirectory;
                            if (!storageDirectory) {
                                console.error("storageDirectory error");
                                $q.reject("storageDirectory error");
                            }
                        }
                    }
                }
                var options = angular.extend({
                    file: blob
                }, uploadOptions);

                var removeFile = function (uploadResult) {
                    if (uploadOptions.fileName) {
                        return $cordovaFile.removeFile(storageDirectory, uploadOptions.fileName).then(function () {
                            $log.debug("Removed " + uploadOptions.fileName + " successfully from " + storageDirectory);
                            return uploadResult;
                        }).then(null, function (err) {
                            $log.debug("Error " + JSON.stringify(err) + " removing " + uploadOptions.fileName + " from " + storageDirectory);
                            return uploadResult;
                        });
                    }
                    return $q.when(uploadResult);
                };
                var uploadResult;
                return {
                    cancel: function () {
                        if (uploadResult) {
                            uploadResult.cancel();
                        }
                    },
                    promise: $cordovaFile.writeFile(storageDirectory, uploadOptions.fileName, blob, true)
                        .then(function (result) {
                            // Success!
                            $log.debug('Success loading file - result is ' + JSON.stringify(result));
                            return result;
                        }, function (err) {
                            // An error occured. Show a message to the user
                            console.error('error loading file - err is ' + JSON.stringify(err));
                            $q.reject(err);
                        })
                        .then(function (result) {
                            $log.debug('result is ' + JSON.stringify(result));
                            uploadResult = File.prototype.upload(result.target.localURL, uploadOptions);
                            return uploadResult;
                        })
                        //                        .then(removeFile, function (err) {
                        //                            return removeFile().then(function () {
                        //                                return $q.reject(err);
                        //                            }).then(null, function (err) {
                        //                                return removeFile().then(function () {
                        //                                    return $q.reject(err);
                        //                                });
                        //                            });
                        //                        })
                };
                //                return $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.CONTENT_URL, options);
            },
            upload: function (path, uploadOptions, onprogress) {
                $log.debug('Auth info for upload : ' + JSON.stringify(AuthService.getDevice().did));
                var uploadDeferred = $q.defer();

                var options = {
                    headers: {}
                };
                if (uploadOptions) {
                    angular.extend(options, uploadOptions);
                }
                angular.extend(options.headers, {
                    'Authorization': AuthService.getDevice().did
                });
                var trustHosts = true;
                var server = ConstsService.MEMBERSHIP_SERVER_URL +
                    ConstsService.CONTENT_URL;
                $log.debug(server);
                var success = function (response) {
                    uploadDeferred.resolve(response);
                };
                var failure = function (err) {
                    console.error("Upload error is " + JSON.stringify(err));
                    uploadDeferred.reject(err);
                };
                var ft = new FileTransfer();
                ft.onprogress = onprogress;

                ft.upload(path, server, success, failure, options, trustHosts);

                var result = {
                    promise: uploadDeferred.promise,
                    cancel: function () {
                        ft.abort();
                    }
                };
                return result;
            },

            download: function (contentId, path, downloadOptions, onprogress) {
                $log.debug('Auth info for download : ' + JSON.stringify(AuthService.getDevice().did));
                var downloadDeferred = $q.defer();
                var options = {};
                if (downloadOptions) {
                    angular.extend(options, downloadOptions);
                }
                angular.extend(options, {
                    headers: {
                        'Authorization': AuthService.getDevice().did
                    }
                });
                var trustHosts = true;
                var server = ConstsService.MEMBERSHIP_SERVER_URL +
                    ConstsService.CONTENT_URL + '/' + contentId;
                $log.debug(server);
                var ft = new FileTransfer();
                var success = function (response) {
                    downloadDeferred.resolve(response);
                };
                var failure = function (err) {
                    console.error("Download error is " + JSON.stringify(err));
                    downloadDeferred.reject(err);
                };
                ft.onprogress = onprogress;
                ft.download(server, path, success, failure, trustHosts, options);
                var retVal = {
                    promise: downloadDeferred.promise,
                    cancel: function () {
                        ft.abort();
                    }
                };
                return retVal;
            },
            confirmContentUploadOptions: function (scope, filename, filetype, filesize, thumbnailDataUrl) {
                return {
                    scope: angular.extend(scope, {
                        data: {
                            caption: ""
                        }
                    }),
                    title: 'Confirm File Upload',
                    template: 'File named ' + filename + ' of type ' + filetype + ' and size ' + filesize + ' bytes being uploaded ' + (thumbnailDataUrl ? ('<img  class="row" src="' + thumbnailDataUrl + '" style="width:192px; "/>') : '') +
                        '<div class="row"><input class="col" ng-model="data.caption" type="text" placeholder="Caption(optional)" /></div>',
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive',
                        onTap: function (e) {
                            // Returning a value will cause the promise to resolve with the given value.
                            return scope.data.caption;
                        }
                }, {
                        text: 'Cancel',
                        type: 'button-positive',
                        onTap: function (e) {
                            // Returning a value will cause the promise to resolve with the given value.
                            return {
                                error: -1
                            };
                        }
                }]
                };
            },
            getExtension: function (contentContainer) {
                var ext = (contentContainer.contentName ? ('.' + contentContainer.contentName.split('.').pop()) : '');
                if (!ext && contentContainer.contentType) {
                    ext = '.' + contentContainer.contentType.split('/').pop();
                    if (ext === '.jpeg') {
                        ext = '.jpg';
                    }
                }
                $log.debug("Extension of downloaded content is " + ext);
                return ext;
            },
            getFileStats: function (path) {
                var deferred = $q.defer();
                window.resolveLocalFileSystemURL(path, function (fileEntry) {
                    $log.debug("fileEntry is " + JSON.stringify(fileEntry));
                    fileEntry.file(function (file) {
                        //$log.debug("file name is " + file.name);
                        //$log.debug("file type is " + file.type);
                        var contentType = file.type ? file.type : Tring.mimeType(file.name);
                        $log.debug("file is " + JSON.stringify(file));
                        window.FilePath.resolveNativePath(fileEntry.nativeURL, function (result) {
                            //There seems to be a crazy bug for pdf media types where you get 
                            //a double 'file://'. Take care of it here
                            var path = result.replace('file://file://', 'file://');
                            $log.debug("fullPath is " + path);
                            deferred.resolve({
                                contentType: contentType,
                                contentName: file.name,
                                contentSize: file.size,
                                fullPath: path,
                                localURL: file.localURL
                            });
                        }, function (err) {
                            $log.error("resolveNativePath error -" + JSON.stringify(err));
                            deferred.reject(err);
                        });

                    }, function (err) {
                        $log.error("Error from fileEntry.file is " + JSON.stringify(err));
                        deferred.reject(err);
                    });
                }, function (err) {
                    deferred.reject(err);
                });
                return deferred.promise;
            },
            fileChooser: function () {
                return $q(function (onSuccess, onFailure) {

                    var successCallback = function (uri) {
                        //alert(uri);
                        $log.debug("fileChooser output uri - " + uri);
                        onSuccess([{
                            fullPath: uri,
                            fromFileChooser: true
                        }]);
                    };
                    var errorCallback = function (err) {
                        onFailure(err);
                    };
                    if (ionic.Platform.isAndroid()) {
                        fileChooser.open(successCallback, errorCallback);
                    } else {
                        var utis = ["public.image", "public.audio", "public.movie", "public.data"];
                        window.FilePicker.pickFile(successCallback, errorCallback, utis);

                    }
                });

            }

        };

        return File;

                    }]);