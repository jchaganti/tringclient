(function () {
  angular.module('tring.dbService', ['ng', 'qImproved', 'tring.constService'])
    .factory('DBService', ['$q', 'ConstsService', '$log', function ($q, ConstsService, $log) {
      $log.debug("Creating DBService");
      var _msgDb;
      var _mbrDb;

      function createDesignDoc(name, mapFunction) {
        var ddoc = {
          _id: '_design/' + name,
          views: {}
        };
        ddoc.views[name] = {
          map: mapFunction.toString()
        };
        return ddoc;
      }

      function createDB(name) {
        return new PouchDB(name, {
          adapter: 'websql',
          auto_compaction: true,
          location: 'default',
          androidDatabaseImplementation: 2
        });
      }

      function postInitDB(cb) {
        if (cb) {
          cb();
        }
      }

      function initMbrDB(recreate, cb) {
        if (!_mbrDb) {
          _mbrDb = createDB('tring_mbr');
          _mbrDb.setMaxListeners(40);

          //var designDoc = createDesignDoc('phoneNumberIndex', function (doc) {
          //  emit(doc.phoneNumber);
          //});
          //_mbrDb.put(designDoc).then(function (doc) {
          //  // design doc created!
          //  _mbrDb.query("phoneNumberIndex", {
          //    stale: 'update_after'
          //  }).then(function (doc) {
          //    $log.debug('phoneNumberIndex updated');
          //  });
          //}).catch(function (err) {
          //  // if err.name === 'conflict', then
          //  // design doc already exists
          //  $log.debug("Creating views(index) - error is " + err.name);
          //  if (err.name === 'conflict') {
          //    _mbrDb.query("phoneNumberIndex", {
          //      stale: 'update_after'
          //    }).then(function (doc) {
          //      $log.debug('phoneNumberIndex updated');
          //    });
          //  }
          //});
        }

        _mbrDb.info().then($log.debug.bind(console))

        if (recreate) {
          $log.debug("Recreate true..");
          var p1 = function () {
            $log.debug("Destroying old mbr db");
            return _mbrDb.destroy();
          }

          var p2 = function () {
            $log.debug("Recreating mbr db");
            _mbrDb = createDB('tring_mbr');
            postInitDB(cb);
            return $q.when();
          }
          return $q.serial([p1, p2])
        } else {
          postInitDB(cb);
          return $q.when();
        }
      }

      function initMsgDB(recreate, cb) {
        if (!_msgDb) {
          _msgDb = createDB('tring_msg');
          _msgDb.setMaxListeners(40);
        }

        if (recreate) {
          var p1 = function () {
            $log.debug("Destroying old msg db");
            return _msgDb.destroy();
          }

          var p2 = function () {
            $log.debug("Recreating msg db");
            _msgDb = createDB('tring_msg');
            postInitDB(cb);
            return $q.when();
          }
          return $q.serial([p1, p2]);
        } else {
          postInitDB(cb);
          return $q.when();
        }

      }

      function msgDb() {
        if (!_msgDb) {
          _msgDb = createDB('tring_msg');
          _msgDb.setMaxListeners(40);
        }
        return _msgDb;
      }

      function mbrDb() {
        if (!_mbrDb) {
          _mbrDb = createDB('tring_mbr');
          _mbrDb.setMaxListeners(40);
        }
        return _mbrDb;
      }

      return {
        initMsgDB: initMsgDB,
        initMbrDB: initMbrDB,
        msgDb: msgDb,
        mbrDb: mbrDb
      };
    }]);
})();
