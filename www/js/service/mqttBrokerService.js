(function () {
  angular.module('tring.mqttBrokerService', ['tring.dbService', 'tring.constService', 'qImproved', 'tring.authService', 'Tring'])
    .factory('MqttBrokerService', ['$q', '$http', 'DBService',
      'ConstsService', 'AuthService', '$interval', '$timeout', 'Tring', '$cordovaNetwork', '$window', 'ContactsService', 'SharedDataService', '$rootScope', 'ChatService', 'GroupService', 'MessagesService', 'UserService', '$log',
      function ($q, $http, DBService, ConstsService, AuthService, $interval, $timeout, Tring, $cordovaNetwork, $window, ContactsService, SharedDataService, $rootScope, ChatService, GroupService, MessagesService, UserService, $log) {
        $log.debug("Creating MqttBrokerService");

        var device;
        var msgDb = DBService.msgDb();
        var clientId;
        // Explicit disconnection
        var explicitlyDisconnected = false;
        var connectionInProgress = false;
        var client;
        var additionalSubscribeToArray = [];
        var userStatuses = [];
        var userStatusCBs = [];
        var groupHandles = {};
        var typingStatuses = [];
        var typingStatusCBs = [];
        var wip = false;
        var arrivedMessagesQueue = [];
        var sendMessagesQueue = [];
        var arrivedQueueMonitor = null;
        var sendQueueMonitor = null;
        var countDoc = null;
        var userTopics = null;
        var userTopicFetchingInProgress = false;
        var userTopicFetchMonitor = null;
        var userStatusBuffers = [];
        var isUserStatusBufferingOn = [];
        var unDispatchedMsgsQueue = [];
        var unDispatchedQueueMonitor = null;
        var dispatchingInProgress;
        var subscriptionInProgress;
        var unsubscriptionInProgress;
        var CURRENT_COUNTER = 0;
        var successCb = null;
        var failureCb = null;
        var dupMessagesQueue = {};
        var dupMessagesKeys = [];
        var dupMessagesCounter = 0;
        var currentDupMessageCounter = 0;

        function handleArrivedMessage() {
          if (wip) {
            //$log.debug("Work in Progress. Messages are being persisted")
          } else {
            wip = true;
            var msgData = arrivedMessagesQueue.shift();
            if (msgData) {
              getCounter().then(function (_counter) {
                _handleArriveMessage(_counter, msgData.msg, msgData.ctxData, msgData.ackCB);
                wip = false;
              }).catch(function (err) {
                if(msgData.ackCB) {
                  msgData.ackCB();
                }
                $log.debug("Error while getting counter: " + JSON.stringify(err));
                cancelArrivedQueueMonitor();
              });
            } else {
              $log.debug("No work is pending. Hence cancelling the arrivedQueueMonitor");
              cancelArrivedQueueMonitor();
            }
          }
        }


        function cancelArrivedQueueMonitor() {
          wip = false;
          $interval.cancel(arrivedQueueMonitor);
          arrivedQueueMonitor = null;
        }

        function handleSendMessage() {
          if (wip) {
            //$log.debug("Work in Progress. Messages are being persisted")
          } else {
            wip = true;
            var msgData = sendMessagesQueue.shift();
            if (msgData) {
              getCounter().then(function (_counter) {
                _handleSendMessage(_counter, msgData);
                wip = false;
              }).catch(function (err) {
                $log.debug("Error while getting counter for the message being sent: " + JSON.stringify(msgData.msg) + " due to error: " + JSON.stringify(err));
                msgData.deferred.reject(msgData.msg);
                cancelSendQueueMonitor();
              });
            } else {
              $log.debug("No work is pending. Hence cancelling the sendQueueMonitor");
              cancelSendQueueMonitor();
            }
          }
        }

        function cancelSendQueueMonitor() {
          wip = false;
          $interval.cancel(sendQueueMonitor);
          sendQueueMonitor = null;
        }

        device = AuthService.getDevice();
        clientId = device.did;
        client = new Paho.MQTT.Client(device.host, Number(device.port), clientId);
        client.onMessageArrived = onMessageArrivedCB;
        client.onMessageDelivered = onMessageDeliveredCB;
        client.onConnectionLost = function (responseObject) {
          $log.debug("Connection lost. Error code: " + responseObject.errorCode + "  Reason: " + responseObject.errorMessage);
          // If explicitly disconnected, do not attempt to reConnect
          if (!explicitlyDisconnected) {
            reConnect();
          }
        };

        var onlineMessage = {
          type: ConstsService.MESSAGE_TYPE_APP | ConstsService.MESSAGE_TYPE_P2P,
          appMsgType: ConstsService.APP_MSG_TYPE_CLIENT_STATUS,
          clientStatus: 1,
          sid: Tring.getSenderId(device.p2p)
        };

        var offlineMessage = {
          type: ConstsService.MESSAGE_TYPE_APP | ConstsService.MESSAGE_TYPE_P2P,
          appMsgType: ConstsService.APP_MSG_TYPE_CLIENT_STATUS,
          clientStatus: 0,
          sid: Tring.getSenderId(device.p2p)
        };

        var typingStatusMessage = {
          type: ConstsService.MESSAGE_TYPE_APP | ConstsService.MESSAGE_TYPE_P2P,
          appMsgType: ConstsService.APP_MSG_TYPE_TYPING_STATUS,
          sid: Tring.getSenderId(device.p2p)
        };

        function reConnect() {
          $log.debug("Reconnecting to MQTT broker");
          $timeout(connect, 100);
        }

        function getMqttClient() {
          return client;
        }

        function onConnect() {
          // Once a connection has been made, make a subscription and send a message.
          $log.debug("onConnect called")
          subscribeTo(device.p2p);
          angular.forEach(additionalSubscribeToArray, function (topic) {
            subscribeTo(topic);
          });

          // Send online message
          sendOnlineMessage();
        }

        function onMessageSentCB(message, status, ackCB) {
          msgDb = DBService.msgDb();
          var msg = JSON.parse(message.payloadString);
          $log.debug("Message delivered = " + JSON.stringify(msg));
          if (!Tring.isClientStatusMessage(msg) && !Tring.isTypingStatusMessage(msg) && !angular.isDefined(msg.toAbuser)) {
            var options = {
              key: msg._id,
              include_docs: true,
            };
            var p1 = function () {
              return msgDb.allDocs(options);
            };

            var p2 = function (response) {
              var _docs = response.rows.map(function (row) {
                return row.doc;
              });
              var msg = _docs[0];
              msg.status = status;
              return msgDb.put(msg);

            };

            $q.serial([p1, p2]).then(function (res) {
              if (status === 1) {
                $log.debug("Message status set to delivered");
              } else {
                $log.debug("Message status set to un-sent");
              }
              if(ackCB) {
                ackCB();
              }
            });
          }
          else {
            if(ackCB) {
              ackCB();
            }
          }
        }

        function onMessageDeliveredCB(message) {
          onMessageSentCB(message, 1);
        }

        function addMsgDataToBuffer(msgData) {
          var userStatusBuffer = userStatusBuffers[msgData.sid];
          if (!userStatusBuffer) {
            userStatusBuffer = [];
          }
          userStatusBuffer.push(msgData);
          return userStatusBuffer;
        }

        function onMessageArrivedCB(message, ackCB) {
          var msgData = JSON.parse(message.payloadString);
          $log.debug("onMessageArrivedCB Payload: " + JSON.stringify(msgData));
          $log.debug("onMessageArrivedCB Retained: " + message.retained + " destinationName: " + message.destinationName + " QoS: " + message.qos + " Duplicate:" + message.duplicate);

          if(!ackCB) {
            ackCB = function () {}
          }

          // The below processing needs a complete overhaul. But for now ensure that the function
          // ackCB gets called EXACTLY ONCE in all code blocks
          if (angular.isDefined(msgData._id)) {
            var key = msgData._id;
            if (angular.isDefined(msgData.handleName)) {
              key += msgData.handleName;
            }
            // If this key has been already handled, we do not process it
            if (dupMessagesQueue[key]) {
              ackCB();
              return;
            }
            dupMessagesQueue[key] = 1;
            if (dupMessagesCounter === 50) {
              dupMessagesCounter = 0;
            }
            var oldKey = dupMessagesKeys[dupMessagesCounter];
            if (oldKey) {
              delete dupMessagesQueue[oldKey];
            }
            dupMessagesKeys[dupMessagesCounter] = key;
            dupMessagesCounter++;
          }

          msgDb = DBService.msgDb();
          if (Tring.isClientStatusMessage(msgData)) {
            //$log.debug("The Other side client is " + msgData.sid + " and this client is " + Tring.getSenderId(device.p2p));
            ackCB();
            if (!Tring.startsWith(message.destinationName, "/p2p") && msgData.clientStatus === 1) {
              if (!userTopics) {
                if (userTopicFetchingInProgress) {
                  userTopicFetchMonitor = $interval(function () {
                    if (!userTopicFetchingInProgress) {
                      $interval.cancel(userTopicFetchMonitor);
                      userTopicFetchMonitor = null;
                      var p2pTopic = userTopics[msgData.sid];
                      sendOnlineMessage(onlineMessage, p2pTopic, true, null, true);
                    }
                    else {
                      $log.debug("waiting for user topics to be fetched")
                    }
                  }, 100);
                }
                else {
                  userTopicFetchingInProgress = true;
                  userTopics = {};
                  UserService.getAllRegisteredUsers().then(function (users) {
                    angular.forEach(users, function (user) {
                      userTopics[Tring.getSenderId(user.topic)] = user.topic;
                    });
                    userTopicFetchingInProgress = false;
                    var p2pTopic = userTopics[msgData.sid];
                    sendOnlineMessage(onlineMessage, p2pTopic, true, null, true);
                  });
                }
              }
              else {
                var p2pTopic = userTopics[msgData.sid];
                sendOnlineMessage(onlineMessage, p2pTopic, true, null, true);
              }
            }

            var isSidStatusBuffering = isUserStatusBufferingOn[msgData.sid];
            if (isSidStatusBuffering) {
              addMsgDataToBuffer(msgData);
            } else {
              isUserStatusBufferingOn[msgData.sid] = true;
              var userStatusBuffer = addMsgDataToBuffer(msgData);
              setTimeout(function () {
                var lastMsgData = userStatusBuffer[userStatusBuffer.length - 1];
                // Reset the bufferingOn to false
                isUserStatusBufferingOn[msgData.sid] = false;
                // Reset the buffer to empty array
                userStatusBuffers[msgData.sid] = [];
                // Continue doing whatever you want to do with lastMsgData
                userStatuses[msgData.sid] = lastMsgData;
                angular.forEach(userStatusCBs, function (userStatusCB) {
                  userStatusCB(lastMsgData);
                });
              }, 100);
            }
          } else if (Tring.isTypingStatusMessage(msgData)) {
            ackCB();
            typingStatuses[msgData.sid] = msgData;
            angular.forEach(typingStatusCBs, function (typingStatusCB) {
              typingStatusCB(msgData);
            });
          } else if (Tring.isDeliveryStatusMessage(msgData)) {
            onMessageSentCB(message, 2, ackCB);
          } else if (Tring.isSyncUpMessage(msgData)) {
            ackCB();
            handleSyncUpMessage(msgData);
          } else if (Tring.isProfileSyncUpMessage(msgData)) {
            ackCB();
            handleProfileSyncUpMessage(msgData);
          } else if (Tring.isGroupProfileSyncUpMessage(msgData)){
            ackCB();
            $log.debug("Got GroupProfileSyncUpMessage - msg is "+JSON.stringify(msgData));
            handleGroupProfileSyncUp(msgData);
          } else if (Tring.isSyncUpSubgroupMessage(msgData)) {
            ackCB();
            handleSubgroupSyncUpMessageAndDeleteSyncId(msgData);
          } else if (Tring.isSyncUpAdminRoleMessage(msgData)) {
            ackCB();
            handleAdminSyncUpMessageAndDeleteSyncId(msgData);
          } else if (Tring.isMembershipAcceptRejectMessage(msgData)) {
            ackCB();
            var gid = msgData.gid;
            msgData.gid = Tring.startsWith(gid, ConstsService.GROUPS_START_KEY) ? gid : ConstsService.GROUPS_START_KEY + gid;
            handleMembershipAcceptRejectMessage(msgData)
              .then(function () {
                if (!msgData.rejected) {
                  try {
                    sendNewMemberJoinedGroupMessage(msgData,
                      function () {
                        $log.debug("Sent New member created message");
                      });
                    //$cordovaToast.showShortCenter("Successfully created group member with handle " + msgData.handleName);
                    $log.debug("Successfully created group member with handle " + msgData.handleName);
                  } catch (err) {
                    console.error("err is " + JSON.stringify(err));
                  }
                } else {
                  //$cordovaToast.showLongCenter("Request for group membership to group "+msgData.groupName+" with handle " + msgData.handleName+" has been rejected");
                  $log.debug("Request for group membership to group " + msgData.groupName + " with handle " + msgData.handleName + " has been rejected");
                }
              });
          } else {
            // Do not process the message that has been sent by the current user to a group topic
            if (Tring.isGroupMessage(msgData) && groupHandles && (groupHandles[msgData.sid] === msgData.handleName)) {
              ackCB();
              return;
            }
            if (Tring.isGroupMessage(msgData)) {
              ChatService.getSenderIdData(msgData.sid, ConstsService.GROUP).then(function (senderData) {
                if(senderData && (senderData.userGroupStatus === ConstsService.USER_GROUP_STATUS_EVICTED) || (senderData.userGroupStatus === ConstsService.USER_GROUP_STATUS_LEFT) ||
                   (senderData.userGroupStatus === ConstsService.USER_GROUP_STATUS_REJECTED)
                  ) {
                  ackCB();
                  $log.debug("Not consuming the message since user has left, was rejected or has been evicted");
                }
                else if (senderData && (!Tring.isO2MGroup(senderData) && Tring.isPrivateMessageMember(msgData))) {
                  ChatService.getHandleData(msgData.handleName, msgData.sid, false).then(function (handleData) {
                    if (handleData && handleData.blocked) {
                      ackCB();
                      $log.debug("Not consuming the message for the handleData : " + JSON.stringify(handleData));
                    }
                    else {
                      addMessageToDB(msgData, ackCB);
                    }
                  }).catch(function (err) {
                    ackCB();
                  });
                } else if (Tring.isEvictionMessage(msgData)) {
                  // Check if this user has already been marked as evicted
                  var sid = msgData.sid;
                  $log.debug("Checking user group status for sid: " + sid);
                  ChatService.findById(sid).then(function (group) {
                    if (Tring.getUserGroupStatus(group) === ConstsService.USER_GROUP_STATUS_EVICTED) {
                      $log.debug("User is already evicted!");
                      ackCB();
                      return;
                    } else {
                      addMessageToDB(msgData, ackCB);
                    }
                  }).catch(function (err) {
                    ackCB();
                    $log.debug("Error while finding group data: " + JSON.stringify(err));
                  });
                } else {
                  addMessageToDB(msgData, ackCB);
                }
              }).catch(function (err) {
                ackCB();
              });
            } else {
              addMessageToDB(msgData, ackCB);
            }
          }
        }

        function addMessageToDB(msgData, ackCB) {
          var msg = {
            ts: msgData.ts,
            type: msgData.type,
            sid: msgData.sid,
            data: msgData.data,
            dir: 0
          };

          if (Tring.isContentMessage(msgData)) {
            msg.contentId = msgData.contentId;
            msg.contentType = msgData.contentType;
            msg.contentName = msgData.contentName;
            msg.thumbnailData = msgData.thumbnailData;
          } else if (Tring.isGroupInviteMessage(msgData)) {
            msg.groupName = msgData.groupName;
            msg.gid = msgData.gid;
            msg.groupTopic = msgData.groupTopic;
            msg.groupO2M = msgData.groupO2M;
            msg.groupClosed = msgData.groupClosed;
            msg.groupDescription = msgData.groupDescription;
            msg.groupRules = msgData.groupRules;
            msg.appMsgType = msgData.appMsgType;
            msg.ordinaryInvite = msgData.ordinaryInvite;
          } else if (Tring.isGroupMembershipRequestMessage(msgData)) {
            msg.gid = msgData.gid;
            msg.handleName = msgData.handleName;
            msg.groupName = msgData.groupName;
          } else if (Tring.isMembershipAcceptRejectMessage(msgData)) {
            msg.gid = msgData.gid;
            msg.handleName = msgData.handleName;
            msg.syncId = msgData.syncId;
            msg.rejected = msgData.rejected;
            msg.groupName = msgData.groupName;
            msg.groupTopic = msgData.groupTopic;
            msg.groupO2M = msgData.groupO2M;
            msg.groupClosed = msgData.groupClosed;
          }

          if (Tring.isGroupMessage(msgData)) {
            msg.handleName = msgData.handleName;
          }

          if (Tring.isPrivateMessage(msgData)) {
            msg.pvtMsgType = msgData.pvtMsgType
          }

          if (Tring.isUserReputationMessage(msgData)) {
            msg.userReputationMsgType = msgData.userReputationMsgType;
            if (Tring.isEvictionMessage(msgData) || Tring.isWarningMessage(msgData)) {
              msg.syncId = msgData.syncId;
            }
            else if (Tring.isAbuseMessage(msgData)) {
              if (angular.isDefined(msgData.toModerator)) {
                msg.toModerator = true;
                var prefix = "You have reported for";
                msg.data = msg.data.substring(prefix.length, msg.data.length);
                msg.data = "There is a report of " + msg.data;
              }
              else if (angular.isDefined(msgData.toAbuser)) {
                msg.toAbuser = true;
              }
            }
          }

          if (Tring.isSyncUpSubgroupMessage(msgData) || Tring.isSyncUpAdminRoleMessage(msgData)) {
            msg.appMsgType = msgData.appMsgType;
          }

          var _msgData = {
            msg: msg,
            ctxData: null,
            ackCB: ackCB
          }
          arrivedMessagesQueue.push(_msgData);
          if (!arrivedQueueMonitor) {
            arrivedQueueMonitor = $interval(handleArrivedMessage, 100);
            if (angular.isFunction($interval.flush)) {
              $interval.flush(1000);
            }
          }
        }

        function handleAdminRoleSyncUpMessage(msgData) {
          var action = msgData.action;
          var gid = msgData.gid;
          $log.debug("parent group id: " + gid);
          var groupId = ConstsService.GROUPS_START_KEY + gid;
          var parentGroup;
          var p1 = function () {
            return ChatService.findById(groupId);
          }

          var p2 = function (group) {
            $log.debug("Existing group membership type: " + group.membershipType);
            group.membershipType = action === "add" ? ConstsService.ADMIN_ROLE : ConstsService.MEMBER_ROLE;
            parentGroup = group;
            var mbrDb = DBService.mbrDb();
            return mbrDb.put(group);
          }

          var p3 = function (res) {
            var data = action === "add" ? "You have been added as admin to this group" : "You have been removed as admin to this group";
            if (parentGroup) {
              var pvtTopic = parentGroup.topic + ConstsService.PVT_SUB_TOPIC;
              if (action === "add") {
                subscribeTo(pvtTopic);
              }
              else {
                unsubscribeTo(pvtTopic);
              }
            }
            msgData.data = data;
            msgData.sid = groupId;
            ChatService.removeSenderIdDataInCache(groupId);
            addMessageToDB(msgData);
            return $q.when(true);
          }

          return $q.serial([p1, p2, p3]);

        }

        function deleteSyncId(url, syncId) {
          return $http.delete(url + '/' + syncId);
        }

        function handleSubgroupSyncUpMessageAndDeleteSyncId(msgData, ackCB) {
          var p1 = function () {
            return handleSubgroupSyncUpMessage(msgData);
          }

          var p2 = function () {
            $log.debug("Deleting SyncId: " + msgData._id);
            return deleteSyncId(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL, msgData._id);
          }


          return $q.serial([p1, p2]).then(function (result) {
            $log.debug("Result from handling sync create subgroup message " + JSON.stringify(result));
            return result;
          }).catch(function (err) {
            $log.debug("Error while handling sync create subgroup message " + JSON.stringify(err));
          });

        }

        function handleAdminSyncUpMessageAndDeleteSyncId(msgData) {
          var p1 = function () {
            return handleAdminRoleSyncUpMessage(msgData);
          }

          var p2 = function () {
            return deleteSyncId(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_ADMIN_URL, msgData._id);
          }

          return $q.serial([p1, p2]).then(function (result) {
            $log.debug("Result from handling sync admin message " + JSON.stringify(result));
            return result;
          }).catch(function (err) {
            $log.debug("Error while handling sync admin message " + JSON.stringify(err));
          });
        }


        function handleSubgroupSyncUpMessage(msgData) {
          $log.debug("Subgroup sync message: " + JSON.stringify(msgData));
          // Store the original message
          var action = msgData.action;
          var role = msgData.memberType;
          var parentGroupId = msgData.pgid;
          var localParentGrpId = ConstsService.GROUPS_START_KEY + parentGroupId;
          var localSubGrpId = ConstsService.GROUPS_START_KEY + msgData.subGroupId;
          var syncId = msgData._id;

          function getOtherSubGroupsWithRole(subGroup, role) {
            var mbrDb = DBService.mbrDb();
            var options = {
              include_docs: true,
              startkey: ConstsService.GROUPS_START_KEY,
              endkey: ConstsService.GROUPS_END_KEY
            };
            var _p1 = function () {
              $log.debug("getOtherSubGroupsWithRole_p1: " + role);
              return mbrDb.allDocs(options);
            }

            var _p2 = function (result) {
              $log.debug("getOtherSubGroupsWithRole_p2: " + role);
              if (result && result.rows) {
                $log.debug("getOtherSubGroupsWithRole:  " + JSON.stringify(result.rows))
                var rows = result.rows.map(function (row) {
                  return row.doc;
                }).filter(function (group) {
                  var parentGroupId = group.parentGroupId;
                  var groupRole = group.membershipType;
                  return parentGroupId && group._id !== subGroup._id && parentGroupId === (ConstsService.GROUPS_START_KEY + msgData.pgid) && groupRole === role;
                });

                return $q.when(rows);
              }
              else {
                $log.debug("getOtherSubGroupsWithRole returning null")
                return $q.when(null);
              }
            }

            return $q.serial([_p1, _p2]);
          }

          function handleSubGroupCreation(parentGroup) {
            $log.debug("Parent group is: " + JSON.stringify(parentGroup));
            var group = {
              _id: ConstsService.GROUPS_START_KEY + msgData.subGroupId,
              name: msgData.name,
              handleName: parentGroup.handleName,
              parentGroupId: localParentGrpId,
              membershipType: msgData.memberType,
              topic: Tring.getGroupMQTTtopic(msgData.topic)
            }

            $log.debug("Sub group is: " + JSON.stringify(group));

            var mbrDb = DBService.mbrDb();

            var _p1 = function () {
              return mbrDb.put(group);
            }

            var _p2 = function (res) {
              group._rev = res.rev;
              var chatId = ConstsService.CHAT_PREFIX + ConstsService.GROUPS_START_KEY + msgData.subGroupId;
              return ChatService.saveGroupChat(group, chatId);
            }

            var _p3 = function (chat) {
              $log.debug("Group Chat: " + JSON.stringify(chat));
              if (group.membershipType === ConstsService.MEMBER_ROLE) {
                // Update the parentGroup's ROLE to NONMEMBER and unsubscribe
                return getOtherSubGroupsWithRole(group, ConstsService.MODERATOR_ROLE).then(function (groups) {
                  if (!groups || (groups && groups.length === 0)) {
                    parentGroup.membershipType = ConstsService.NON_MEMBER_ROLE;
                    msgData.nonModeratorOfOtherSubGroups = true;
                    unsubscribeTo(parentGroup.topic);
                    return mbrDb.put(parentGroup).then(function (res) {
                      return $q.when(group);
                    });
                  } else {
                    $log.debug("getOtherSubGroupsWithModeratorRole returned null")
                    return $q.when(group);
                  }
                })
              } else {
                return $q.when(group);
              }
            }
            return $q.serial([_p1, _p2, _p3]);
          }

          function updateGroupMemberShip(result, group, role) {
            var mbrDb = DBService.mbrDb();
            group.membershipType = role;
            return mbrDb.put(group).then(function (res) {
              group._rev = res.rev;
              result.done = true;
              return $q.when(result);
            });
          }

          function storeSubGroupMessage(group) {
            addToUserGroupHandle(group._id, group.handleName);
            var ts = msgData.ts;
            if(!ts) {
              ts = moment().format('x');
            }
            var subGroupMsg = {
              sid: localSubGrpId,
              type: msgData.type,
              appMsgType: msgData.appMsgType,
              ts: ts
            }
            ChatService.removeSenderIdDataInCache(localSubGrpId);
            ChatService.removeSenderIdDataInCache(localParentGrpId);
            if (group.membershipType === ConstsService.MODERATOR_ROLE) {
              subGroupMsg.data = "Welcome to sub-group: " + msgData.name + ". You have been added as moderator.";
              subscribeTo(group.topic + ConstsService.PVT_SUB_TOPIC);
            }
            else {
              subGroupMsg.data = "Welcome to sub-group: " + msgData.name + ". You have been added as member.";
            }
            addMessageToDB(subGroupMsg);

            if (group.membershipType === ConstsService.MEMBER_ROLE) {
              var parentGroupMsg = {
                type: msgData.type,
                appMsgType: msgData.appMsgType,
                sid: localParentGrpId,
                ts: ts
              }
              if (msgData.nonModeratorOfOtherSubGroups) {
                parentGroupMsg.data = "You are now member of the sub-group: " + msgData.name + ". Hence you are no more a member of the group.";
              }
              else {
                parentGroupMsg.data = "You are member of the sub-group: " + msgData.name + ".";
              }
              addMessageToDB(parentGroupMsg);
            }
          }

          if (msgData.action === "CREATE") {
            // Create chat record against the sid
            var p0 = function () {
              return ChatService.findById(localSubGrpId);
            }

            var p1 = function (localSubGroup) {
              if (!localSubGroup) {
                $log.debug("localSubGroup found not existing and hence creating.");
                return ChatService.findById(localParentGrpId);
              } else {
                $log.debug("localSubGroup found existing and hence not creating.")
                return $q.when(null);
              }
            }

            var p2 = function (parentGroup) {
              if (parentGroup && (parentGroup.membershipType === ConstsService.OWNER_ROLE || parentGroup.membershipType === ConstsService.ADMIN_ROLE)) {
                if (msgData.memberType === ConstsService.MEMBER_ROLE) {
                  throw "Owner/Admin's role can only Moderator!!";
                }
              }
              if (parentGroup) {
                return handleSubGroupCreation(parentGroup);
              } else {
                $log.debug("localSubGroup found existing and hence returning null.");
                return $q.when(null);
              }
            }


            var p3 = function (group) {
              $log.debug("Adding sync message to DB for group: " + JSON.stringify(group));
              if (group) {
                // Create Messages and re-direct to appropriate chat windows
                subscribeTo(group.topic);
                storeSubGroupMessage(group);
              }
              return $q.when(true);
              // Finally delete the server side's sync record
            }

            return $q.serial([p0, p1, p2, p3]);
          } else if (msgData.action === "EDIT") {
            var newMembertype = msgData.memberType;
            var p0 = function () {
              $log.debug("Editing subgroup and localSubGrpId: " + localSubGrpId)
              return ChatService.findById(localSubGrpId);
            }

            var p1 = function (localSubGroup) {
              $log.debug("Editing subgroup and found local SubGroup: " + JSON.stringify(localSubGroup));
              var result = {};
              if (localSubGroup) {
                var _p1 = function () {
                  return ChatService.findById(localParentGrpId);
                }

                var _p2 = function (localParentGroup) {
                  result.localParentGroup = localParentGroup;
                  result.subGroupExists = true;
                  result.localSubGroup = localSubGroup;
                  return $q.when(result);
                }
                return $q.serial([_p1, _p2]);
              } else {
                var _p1 = function () {
                  return ChatService.findById(localParentGrpId);
                }

                var _p2 = function (localParentGroup) {
                  result.localParentGroup = localParentGroup;
                  return handleSubGroupCreation(localParentGroup);
                }

                var _p3 = function (localSubGroup) {
                  result.subGroupExists = false;
                  result.localSubGroup = localSubGroup;
                  result.done = true;
                  return $q.when(result);
                }
                return $q.serial([_p1, _p2, _p3]);
              }
            }

            var p2 = function (result) {
              $log.debug("Result in stage2: " + JSON.stringify(result));
              $log.debug("New member type: " + newMembertype);
              if (result.subGroupExists) {
                var parentGroupRole = result.localParentGroup.membershipType;
                if (parentGroupRole === ConstsService.ADMIN_ROLE || parentGroupRole === ConstsService.OWNER_ROLE) {
                  if (newMembertype === ConstsService.NON_MEMBER_ROLE) {
                    unsubscribeTo(result.localSubGroup.topic);
                    unsubscribeTo(result.localSubGroup.topic + ConstsService.PVT_SUB_TOPIC);
                    return updateGroupMemberShip(result, result.localSubGroup, newMembertype);
                  } else if (newMembertype === ConstsService.MODERATOR_ROLE) {
                    subscribeTo(result.localSubGroup.topic);
                    subscribeTo(result.localSubGroup.topic + ConstsService.PVT_SUB_TOPIC);
                    var chatId = ConstsService.CHAT_PREFIX + ConstsService.GROUPS_START_KEY + result.localSubGroup._id;
                    var _p1 = function () {
                      return ChatService.findById(chatId);
                    }

                    var _p2 = function (chat) {
                      if(chat) {
                        return $q.when(chat);
                      }
                      else {
                        return ChatService.saveGroupChat(result.localSubGroup, chatId);
                      }
                    }

                    var _p3 = function(chat) {
                      return updateGroupMemberShip(result, result.localSubGroup, newMembertype);
                    }

                    return $q.serial([_p1, _p2, _p3]);
                  } else {
                    $log.debug("Invalid case of newMembertype");
                    result.done = true;
                    result.error = "Invalid case of newMembertype: " + newMembertype;
                    return $q.when(result);
                  }
                } else if (newMembertype === ConstsService.MODERATOR_ROLE) {
                  var _p1 = function () {
                    subscribeTo(result.localSubGroup.topic);
                    subscribeTo(result.localSubGroup.topic + ConstsService.PVT_SUB_TOPIC);
                    return updateGroupMemberShip(result, result.localSubGroup, newMembertype);
                  }

                  var _p2 = function (result) {
                    subscribeTo(result.localParentGroup.topic);
                    // Make the memberType to MEMBER for the parent group
                    return updateGroupMemberShip(result, result.localParentGroup, ConstsService.MEMBER_ROLE);
                  }
                  return $q.serial([_p1, _p2]);
                } else if (newMembertype === ConstsService.MEMBER_ROLE) {
                  var _p1 = function () {
                    subscribeTo(result.localSubGroup.topic);
                    unsubscribeTo(result.localSubGroup.topic + ConstsService.PVT_SUB_TOPIC);
                    return updateGroupMemberShip(result, result.localSubGroup, newMembertype);
                  }

                  var _p2 = function (localSubGroup) {
                    return getOtherSubGroupsWithRole(result.localSubGroup, ConstsService.MODERATOR_ROLE)
                  }

                  var _p3 = function (groups) {
                    if (!groups || groups.length === 0) {
                      $log.debug("Result object: " + JSON.stringify(result));
                      unsubscribeTo(result.localParentGroup.topic);
                      msgData.nonMemberOfParentGroup = true;
                      return updateGroupMemberShip(result, result.localParentGroup, ConstsService.NON_MEMBER_ROLE);
                    } else {
                      return $q.when(result);
                    }
                  }
                  return $q.serial([_p1, _p2, _p3]);
                } else if (newMembertype === ConstsService.NON_MEMBER_ROLE) {
                  var _p1 = function () {
                    unsubscribeTo(result.localSubGroup.topic);
                    unsubscribeTo(result.localSubGroup.topic + ConstsService.PVT_SUB_TOPIC);
                    return updateGroupMemberShip(result, result.localSubGroup, newMembertype);
                  }

                  var _p2 = function (result) {
                    return getOtherSubGroupsWithRole(result.localSubGroup, ConstsService.MODERATOR_ROLE)
                  }

                  var _p3 = function (asModeratorSubGroups) {
                    if (!asModeratorSubGroups || asModeratorSubGroups.length === 0) {
                      return getOtherSubGroupsWithRole(result.localSubGroup, ConstsService.MEMBER_ROLE).then(function (asMemberSubGroups) {
                        if (!asMemberSubGroups || asMemberSubGroups.length === 0) {
                          msgData.nonMemberOfOtherSubGroups = true;
                          subscribeTo(result.localParentGroup.topic);
                          return updateGroupMemberShip(result, result.localParentGroup, ConstsService.MEMBER_ROLE);
                        }
                        else {
                          unsubscribeTo(result.localParentGroup.topic);
                          return updateGroupMemberShip(result, result.localParentGroup, ConstsService.NON_MEMBER_ROLE);
                        }
                      })
                    } else {
                      return $q.when(result);
                    }
                  }
                  return $q.serial([_p1, _p2, _p3]);
                }
              } else {
                $log.debug("Sub group did not existed. Hence created");
                subscribeTo(result.localSubGroup.topic);

                return $q.when(result);
              }
            }

            var p3 = function (result) {
              $log.debug("Result in stage3: " + JSON.stringify(result));
              if (result.subGroupExists) {
                var subGroup = result.localSubGroup;
                var ts = msgData.ts;
                if(!ts) {
                  ts = moment().format('x');
                }
                var subGroupMsg = {
                  sid: localSubGrpId,
                  type: msgData.type,
                  appMsgType: msgData.appMsgType,
                  ts: ts
                }
                ChatService.removeSenderIdDataInCache(localSubGrpId);
                ChatService.removeSenderIdDataInCache(localParentGrpId);
                if (subGroup.membershipType === ConstsService.MODERATOR_ROLE) {
                  subGroupMsg.data = "You are now moderator of the sub-group: " + msgData.name;
                  addMessageToDB(subGroupMsg);
                  addToUserGroupHandle(localSubGrpId, subGroup.handleName);
                }
                else if (subGroup.membershipType === ConstsService.MEMBER_ROLE) {
                  subGroupMsg.data = "You are now member of the sub-group: " + msgData.name;
                  addToUserGroupHandle(localSubGrpId, subGroup.handleName);
                  addMessageToDB(subGroupMsg);
                  if(msgData.nonMemberOfParentGroup) {
                    var parentGroupMsg = {
                      sid: localParentGrpId,
                      type: msgData.type,
                      appMsgType: msgData.appMsgType,
                      ts: ts,
                      data: "You are not member of the group: " + result.localParentGroup.name
                    }
                    addMessageToDB(parentGroupMsg);
                  }
                }
                else if (subGroup.membershipType === ConstsService.NON_MEMBER_ROLE) {
                  subGroupMsg.data = "You have been removed from the sub-group: " + msgData.name;
                  addMessageToDB(subGroupMsg);
                  if (msgData.nonMemberOfOtherSubGroups) {
                    var parentGroupMsg = {
                      sid: localParentGrpId,
                      type: msgData.type,
                      appMsgType: msgData.appMsgType,
                      ts: ts,
                      data: "You are now member of the group: " + result.localParentGroup.name
                    }
                    addToUserGroupHandle(localParentGrpId, result.localParentGroup.handleName);
                    addMessageToDB(parentGroupMsg);
                  }
                }
              }
              else {
                storeSubGroupMessage(result.localSubGroup);
              }
              return $q.when(result.done && !angular.isDefined(result.error));
            }
            return $q.serial([p0, p1, p2, p3]);
          }
        }

        var handleGroupProfileSyncUp = function(msg) {
            GroupService.storeGroup({_id: msg.gid, thumbnailData: msg.thumbnailData, contentId: msg.contentId})
            .then(function(grp) {
                $log.debug("After storing new Group profile, grp is "+JSON.stringify(grp));
            });
        };

        var handleMembershipAcceptRejectMessage = function (msg) {
          var subscribeFunc = function () {
              var syncId = msg.syncId;
              var hdlName = msg.handleName;
              if (!msg.rejected) {
                subscribeTo(msg.groupTopic);
                subscribeToStatus(msg.groupTopic);
                $log.debug("Before calling addToUserGroupHandle");
                addToUserGroupHandle(msg.gid, hdlName);
              }
              return $q.resolve(syncId);
            };
          var deleteSyncId_ = function (syncId) {
            var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.MEMBERSHIP_ACCEPT_REJECT_SYNC_URL;
            return deleteSyncId(url, syncId).then(function () {
              $log.debug("Successfully deleted syncId " + syncId);
              return $q.resolve();
            }).then(null, function (err) {
              $log.debug("Error deleting syncId " + syncId + " error is " + JSON.stringify(err));
              //Non-serious error hence resolve, not reject
              return $q.resolve();
            });
          };
          if (!msg.rejected) {
            return GroupService.createLocalGroupMemberAndChat(msg).then(subscribeFunc).then(deleteSyncId_);
          } else {
            var gid = msg.gid || msg._id;
            gid = Tring.startsWith(gid, ConstsService.GROUPS_START_KEY) ? gid : ConstsService.GROUPS_START_KEY + gid;
            msg._id = gid;
            return $q.when(GroupService.getMyGroupAndSubgroupsFor(msg)).then(function (groupArray) {
                return GroupService.setStatusForGroupsFromLocalDb(groupArray, ConstsService.USER_GROUP_STATUS_REJECTED).then(function(){
                    angular.forEach(groupArray,function(grp){
                        ChatService.removeSenderIdDataInCache(grp._id);
                        unsubscribeTo(grp.topic);
                        obj.unSubscribeToStatus(grp.topic);
                    });
                    return true;
                }).then(function(){
                  return deleteSyncId_(msg.syncId);
                });
            });
          }
        };

        function handleProfileSyncUpMessage(msgInfo) {
          return ContactsService.synchronizerGeneric.run(function (msgData) {
            var data = msgData.data;
            var number = msgData.data.syncUpMobileNo;
            var mbrDb = DBService.mbrDb();
            var profileCounter = data.profileCounter;
            var profile = {
              customMessage: data.customMessage,
              profileName: data.name,
              thumbnailData: data.thumbnailData,
              contentId: data.contentId
            };

            var contacts = ContactsService.getContacts();
            return $q.when(contacts).then(function (cts) {
              var result = $q.when();
              cts = cts.filter(function (ct) {
                return (ct.phoneNumber === number) &&
                  (!ct.profileCounter || profileCounter > ct.profileCounter);
              });
              angular.forEach(cts, function (ct) {
                angular.extend(ct, {
                  number: number,
                  syncId: msgData.data.syncId,
                  profileCounter: profileCounter
                });
                angular.extend(ct, profile);
                delete ct.localURL;
                result = result.then(function () {
                  return ContactsService.saveContact(ct);
                }, function (err) {
                  $log.debug("onMessageArrivedCB profileSyncUp: Error while getting contact from DB/server: " + JSON.stringify(err));
                  return $q.reject(" error");
                }).then(function (contact) {
                  $http.delete(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.UPDATE_PROFILE_URL + '/' + contact.syncId).then(function (result) {
                    $log.debug("Result from profileSyncup delete syncid is " + JSON.stringify(result));
                  });
                  $rootScope.$broadcast("contactProfileChanged", contact);
                });
              });
              if (!cts || !cts.length) {
                console.error("Got syncUp message for contact " + number + " that is not in address book");
                result = $q.reject("Got syncUp message for contact " + number + " that is not in address book");
              }
              return result;
            });
          }, msgInfo);
        }

        function handleSyncUpMessage(msgInfo) {
          return ContactsService.synchronizerGeneric.run(function (msgData) {
            var number = msgData.data.syncUpMobileNo;
            $log.debug("SyncUp message received: msgData.syncUpMobileNo is " + number);
            $log.debug("SyncUp message received: msgData.syncId is " + msgData.data.syncId);
            var contacts = ContactsService.getContacts();
            return $q.when(contacts).then(function (cts) {
              var result = $q.when();
              cts = cts.filter(function (ct) {
                return ct.phoneNumber === number;
              });
              angular.forEach(cts, function (ct) {
                angular.extend(ct, {
                  number: number,
                  syncId: msgData.data.syncId
                });
                result = result.then(function () {
                    return ContactsService.pingMbrSrvrForContact(ct);
                  })
                  .then(function (cntct) {
                    // Subscribe to the special topic of this user
                    subscribeToStatus(cntct.topic);
                    $log.debug("syncUp complete for contact: " + JSON.stringify(cntct));
                    return cntct;
                  }, function (err) {
                    $log.debug("onMessageArrivedCB syncUp: Error while getting contact from DB/server: " + JSON.stringify(err));
                    return $q.reject("pingMbrSrvrForContact error");
                  });
              });
              if (!cts || !cts.length) {
                console.error("Got syncUp message for contact " + number + " that is not in address book");
                result = $q.reject("Got syncUp message for contact " + number + " that is not in address book");
              }
              return result;
            });
          }, msgInfo);
        }

        function setRecentMessage(msg) {
          var recentMsgData = {
            ts: msg.ts,
            data: msg.data,
            dir: msg.dir
          }
          $log.debug("Setting Recent Message Data: " + JSON.stringify(recentMsgData))
          SharedDataService.setValue(ConstsService.RECENT_MESSAGE_PREFIX + msg.sid, recentMsgData);
        }

        function _handleArriveMessage(_counter, msg, ctxData, ackCB) {
          if (msg.handleName) {
            msg._id = pouchCollate.toIndexableString([msg.sid, _counter, msg.handleName]);
          } else {
            msg._id = pouchCollate.toIndexableString([msg.sid, _counter]);
          }

          setRecentMessage(msg);

          msgDb = DBService.msgDb();
          msgDb.put(msg).then(function (res) {
            if(ackCB) {
              ackCB();
            }
            $log.debug("New Message has been added to DB");
          }).catch(function (err) {
            if(ackCB) {
              ackCB();
            }
            if (err.status === 409) {
              $log.debug("Conflict occured while inserting message: " + JSON.stringify(msg));
            } else {
              $log.debug("Should not have reached here!: " + JSON.stringify(msg))
            }
          });
        }

        function cancelUnDispatchedQueueMonitor() {
          dispatchingInProgress = false;
          if(unDispatchedQueueMonitor) {
            $interval.cancel(unDispatchedQueueMonitor);
          }
          unDispatchedQueueMonitor = null;
        }

        function dispatchMessage(message, cb) {
          try {
            $log.debug("Dispatching message to MQTT client: " + message.payloadString + "  to topic: " + message.destinationName);
            client.send(message);
            if (angular.isFunction(cb)) {
              cb();
            }
          } catch (e) {
            $log.debug("Error:  " + e + "  while dispatching message " + message.payloadString);
            var msg = angular.fromJson(message.payloadString);
            if (!Tring.isClientStatusMessage(msg) && !Tring.isTypingStatusMessage(msg)) {
              onMessageSentCB(msg, -1);
            }
            cancelUnDispatchedQueueMonitor();
          }
        }

        function __sendMessage() {
          if (dispatchingInProgress) {
            $log.debug("Message Dispatching in Progress.");

          } else {
            dispatchingInProgress = true;
            var messageCbData = unDispatchedMsgsQueue.shift();
            if (messageCbData) {
              if (client.isConnected()) {
                dispatchMessage(messageCbData.message, messageCbData.cb);
                dispatchingInProgress = false;
              } else {
                var connectDuringDispatchTid = null;
                var count = 0;
                var connectDuringDispatchChecker = function () {
                  if (!client.isConnected()) {
                    $log.debug("Not yet connected during dispatch");
                    if(count === 20) {
                      if(connectDuringDispatchTid) {
                        $interval.cancel(connectDuringDispatchTid);
                        connectDuringDispatchTid = null;
                      }
                      cancelUnDispatchedQueueMonitor();
                      //Put the message back to head since we could not dispatch
                      unDispatchedMsgsQueue.unshift(messageCbData);
                    }
                    else {
                      connect();
                      count++;
                    }
                  } else {
                    $interval.cancel(connectDuringDispatchTid);
                    connectDuringDispatchTid = null;
                    $log.debug("The timer connectDuringDispatchTid  cancelled");
                    dispatchMessage(messageCbData.message, messageCbData.cb);
                    dispatchingInProgress = false;
                  }
                }
                connectDuringDispatchTid = $interval(connectDuringDispatchChecker, 1000);
                if (angular.isFunction($interval.flush)) {
                  $interval.flush(1000);
                }
              }
            } else {
              $log.debug("Nothing to dispatch. Hence cancelling the unDispatchedQueueMonitor");
              cancelUnDispatchedQueueMonitor();
            }
          }
        }

        function _sendMessage(msg, topic, retained, cb, qos0) {
          $log.debug("Sending message: " + JSON.stringify(msg) + "  to topic: " + topic);
          var message = new Paho.MQTT.Message(JSON.stringify(msg));
          message.destinationName = topic;
          if (retained) {
            message.retained = true;
          }
          if (qos0) {
            message.qos = 0;
          }
          else {
            message.qos = 1;
          }

          var messageCbData = {
            message: message,
            cb: cb
          }
          unDispatchedMsgsQueue.push(messageCbData);
          if (!unDispatchedQueueMonitor) {
            unDispatchedQueueMonitor = $interval(__sendMessage, 100);
            if (angular.isFunction($interval.flush)) {
              $interval.flush(1000);
            }

          }
        }

        function sendMessage(type, appMsgType, otherMsgTypeCB, senderId, messageTopicCB, data, cb, handleName, contentIdPromise, options) {
          var sid = senderId; //Tring.getSenderId(topic, );
          var deferred = $q.defer();
          if (sid) {
            msgDb = DBService.msgDb();
            var msg = {
              type: type,
              ts: moment().format('x'),
              sid: sid,
              data: data,
              status: 0, // Transit - 0, Unsent - -1, Sent - 1, Delivered - 2
              dir: 1
            };
            if (appMsgType) {
              msg.appMsgType = appMsgType;
            }
            if (angular.isFunction(otherMsgTypeCB)) {
              otherMsgTypeCB(msg);
            }
            if (handleName) {
              msg.handleName = handleName;
            }
            if (contentIdPromise) {
              msg.contentId = "";
            }
            if (options) {
              angular.extend(msg, options);
            }
            var ctxData = {
              contentIdPromise: contentIdPromise,
              cb: cb
            }

            if (angular.isFunction(messageTopicCB)) {
              ctxData.topic = messageTopicCB();
            }

            var msgData = {
              msg: msg,
              ctxData: ctxData,
              deferred: deferred
            }
            sendMessagesQueue.push(msgData);
            if (!sendQueueMonitor) {
              sendQueueMonitor = $interval(handleSendMessage, 100);
              if (angular.isFunction($interval.flush)) {
                $interval.flush(1000);
              }
            }
          } else {
            $log.debug("Invalid sender Id");
            deferred.reject("Invalid sender Id")
          }
          return deferred.promise;
        }

        function _handleSendMessage(_counter, msgData) {
          var msg = msgData.msg;
          var ctxData = msgData.ctxData;
          var contentIdPromise = ctxData.contentIdPromise;
          var topic = ctxData.topic;
          var cb = ctxData.cb;
          var recentMsg;
          msg._id = pouchCollate.toIndexableString([msg.sid, _counter]);
          if (Tring.isGroupInviteMessage(msg) || Tring.isGroupInviteAcceptMessage(msg)) {
             recentMsg=angular.extend(angular.copy(msg), {data: msg.dataForSelf});
          } else {
              recentMsg =msg;
           }
          setRecentMessage(recentMsg);
          msgData.deferred.resolve(msg);
          var msgClone = msg;
          var otherSideSenderId = msg.sid;
          // Once the message is stored, change the sid to the current user
          if (otherSideSenderId.indexOf(ConstsService.USERS_START_KEY) === 0) {
            msgClone = angular.copy(msg);
            msgClone.sid = Tring.getSenderId(device.p2p);
          } else if (Tring.isGroupInviteAcceptMessage(msg)) {
            msgClone = angular.copy(msg);
          }
          if (Tring.isGroupInviteMessage(msg) || Tring.isGroupInviteAcceptMessage(msg)) {
            msg.data = msg.dataForSelf;
            delete msg.dataForSelf;
            delete msgClone.dataForSelf;
            msg.sid = recentMsg.sid;
          }
          if (contentIdPromise) {
            msg.uploadInProgress = true;
            contentIdPromise.then(function (contentId) {
                msg.contentId = contentId;
                msg.uploadAborted = false;
              })
              .catch(function (err) {
                msg.contentId = "";
                msg.uploadAborted = true;
              })
              .finally(function () {
                msg.uploadInProgress = undefined;
                $log.debug("Updating contentId: " + msg.contentId + " _id: " + msg._id);
                msgDb.put(angular.extend(msg, {
                  sid: otherSideSenderId
                })).then(function (res) {
                  if (!msg.uploadAborted) {
                    msgClone.contentId = msg.contentId;
                    _sendMessage(msgClone, topic, false, cb);
                  }
                }).catch(function (err) {
                  $log.debug("Could not persist the message being sent: " + " due to error: " + JSON.stringify(err.message));
                });
              });
          } else {
            msgDb.put(msg).then(function (res) {
              _sendMessage(msgClone, topic, false, cb);
            }).catch(function (err) {
              if (angular.isFunction($interval.flush)) {
                $interval.flush(1000);
              } else {
                $log.debug("Could not send the message due to error: " + JSON.stringify(err.message));
                msgData.deferred.reject(msg);
              }
            });
          }
        }

        function subscribeTo(topic) {
          //$log.debug("Connected client id " + clientId + " and subscribing to topic: " + topic);
          var trackingCondition = function () {
            return client.isConnected();
          }

          var preTrackCB = function () {
            connect();
          }

          var postTrackCB = function () {
            client.subscribe(topic, {
              qos: 1
            });
          }
          Tring.trackAndCall(trackingCondition, preTrackCB, postTrackCB, 100, $interval, 2);
        }

        function unsubscribeTo(topic) {
          $log.debug("Connected client id " + clientId + " and unsubscribing to topic: " + topic);
          var trackingCondition = function () {
            return client.isConnected();
          }

          var preTrackCB = function () {
            connect();
          }

          var postTrackCB = function () {
            client.unsubscribe(topic);
          }
          Tring.trackAndCall(trackingCondition, preTrackCB, postTrackCB, 100, $interval, 2);
        }

        function connectionFailureCB(msg) {
          if(msg) {
            $log.debug("Could not establish connection to broker. Failed due to  " + msg);
          }
          if (failureCb) {
            failureCb();
          }
          cancelUnDispatchedQueueMonitor();
          connectionInProgress = false;
        }

        function connect(invokeSuccessCB) {
          $log.debug("Connected: " + client.isConnected() + " connectionInProgress: " + connectionInProgress);
          if (client.isConnected() || connectionInProgress) {
            return;
          }
          connectionInProgress = true;
          if ($cordovaNetwork.isOnline()) {
            $log.debug("Connecting to MQTT Broker");
            var connectionSuccessCB = function () {
              $log.debug("Connected to MQTT Broker");
              explicitlyDisconnected = false;
              if (invokeSuccessCB && client.isConnected() && successCb) {
                successCb();
              }
              connectionInProgress = false;
            };

            var lwtMsg = {
              type: ConstsService.MESSAGE_TYPE_APP | ConstsService.MESSAGE_TYPE_P2P,
              appMsgType: ConstsService.APP_MSG_TYPE_CLIENT_STATUS,
              clientStatus: 0,
              isLwt: true,
              sid: Tring.getSenderId(device.p2p)
            };
            var message = new Paho.MQTT.Message(JSON.stringify(lwtMsg));
            message.destinationName = getStatusTopic(device.p2p);
            message.retained = true;
            message.qos = 0;

            try {
              client.connect({
                onSuccess: connectionSuccessCB,
                onFailure: function (message) {
                  connectionFailureCB(message.errorMessage);
                },
                keepAliveInterval: 40, // 1 min
                willMessage: message,
                cleanSession: false
              });
            }
            catch (e) {
              connectionFailureCB(JSON.stringify(e));
            }
          } else {
            $log.debug("Network connection is not available. Hence not able to connect to broker");
            connectionFailureCB();
          }
        }

        function getUserStatus(sid) {
          return userStatuses[sid];
        }

        function addUserStatusCB(cb) {
          userStatusCBs.push(cb);
        }

        function getStatusTopic(topic) {
          return Tring.endsWith(topic, "/") ? topic + "status" : topic + "/status";
        }

        function getProfileStatusTopic(topic) {
          return Tring.endsWith(topic, "/") ? topic + "profile" : topic + "/profile";
        }

        function setUserGroupHandles(_groupHandles) {
          angular.extend(groupHandles, _groupHandles);
        }

        function addToUserGroupHandle(gid, handleName) {
          if (groupHandles) {
            groupHandles[gid] = handleName;
          }
        }

        function sendTypingMessage(status, topic) {
          typingStatusMessage.status = status;
          _sendMessage(typingStatusMessage, topic, false, null, true);
        }

        function addTypingStatusCB(cb) {
          typingStatusCBs.push(cb);
        }

        var counterOptions = {
          limit: 1,
          include_docs: true,
          startkey: ConstsService.COUNTER_KEY
        };

        function updateCounter() {
          countDoc.count = countDoc.count + ConstsService.MSG_COUNT_IN_MEMORY;
          $log.debug("Getting new slot of counters till: " + countDoc.count);
          var msgDb = DBService.msgDb();
          var p1 = function () {
            return msgDb.put(countDoc);
          }
          var p2 = function (res) {
            countDoc._rev = res.rev;
            return $q.when(countDoc.count)
          }
          return $q.serial([p1, p2]);
        }


        function getCounter() {
          if (!countDoc) {
            var msgDb = DBService.msgDb();
            var p1 = function () {
              return msgDb.allDocs(counterOptions);
            };

            var p2 = function (response) {
              if (response.rows.length > 0) {
                var _docs = response.rows.map(function (row) {
                  return row.doc;
                });
                countDoc = _docs[0];
              } else {
                countDoc = {
                  _id: ConstsService.COUNTER_KEY,
                  count: 0
                }
              }
              CURRENT_COUNTER = countDoc.count + 1;
              return updateCounter();
            }

            var p3 = function (availableCounter) {
              $log.debug("Starting counter in this session: " + CURRENT_COUNTER + " and available counters till: " + availableCounter);
              return $q.when(CURRENT_COUNTER);
            }

            return $q.serial([p1, p2, p3]);
          } else {
            if (CURRENT_COUNTER >= countDoc.count) {
              var p1 = function () {
                CURRENT_COUNTER = countDoc.count + 1;
                return updateCounter();
              }

              var p2 = function (availableCounter) {
                $log.debug("New slot of counters in this session and current counter: " + CURRENT_COUNTER + " and available counters till: " + availableCounter);
                return $q.when(CURRENT_COUNTER);
              }

              return $q.serial([p1, p2]);
            } else {
              CURRENT_COUNTER++;
              $log.debug("Counter from memory in this session: " + CURRENT_COUNTER);
              return $q.when(CURRENT_COUNTER);
            }
          }
        }

        function disconnect() {
          explicitlyDisconnected = true;
          connectionInProgress = false;
          if (client.isConnected()) {
            client.disconnect();
          } else {
            $log.debug("Client is not connected to be disconnected");
          }
          cancelUnDispatchedQueueMonitor();
        }

        function subscribeToStatus(topic) {
          if (topic) {
            subscribeTo(getStatusTopic(topic));
          }
        }

        function sendOnlineMessage() {
          _sendMessage(onlineMessage, getStatusTopic(device.p2p), true, null, true);
        }

        function sendOfflineMessage(cb) {
          _sendMessage(offlineMessage, getStatusTopic(device.p2p), true, cb, true);
        }

        function sendDataMessage(msg, topic, retained, cb, qos0) {
          _sendMessage(msg, topic, retained, cb, qos0);
        }

        function registerCallBacks(_successCb, _failureCb) {
          if (angular.isFunction(_successCb) && angular.isFunction(_failureCb)) {
            successCb = _successCb;
            failureCb = _failureCb;
          }
        }

        function getMessageId(sid) {
          var p1 = function () {
            return getCounter();
          }

          var p2 = function (counter) {
            var id = pouchCollate.toIndexableString([sid, counter]);
            return $q.when(id);
          }

          return $q.serial([p1, p2])
        }

        var sendNewMemberJoinedGroupMessage = function (message, callbackf, options) {
          $log.debug("Sending group invite accept message");
          var data = MessagesService.getGroupAcceptInviteMessage(message);
          var dataForSelf = MessagesService.getGroupAcceptInviteMessage(message, true);
          var getTopic = function () {
              if ( message.o2m || message.groupO2M) {
                  return message.groupTopic + ConstsService.PVT_SUB_TOPIC;
              } else {
                  return message.groupTopic;
              }
          };
          if (Tring.isMembershipAcceptRejectMessage(message)) {
              data = MessagesService.getGroupRequestAcceptedMessage(message);
              dataForSelf = data;
          }
          sendMessage(ConstsService.MESSAGE_TYPE_APP, ConstsService.APP_MSG_TYPE_ACCEPT_GROUP_INVITE, null, message.gid,
            getTopic, data,
            callbackf, message.handleName, null, {
              handleName: message.handleName,
              dataForSelf: dataForSelf
            });
        };

        return {
          sendTypingMessage: sendTypingMessage,
          sendMessage: sendMessage,
          onConnect: onConnect,
          connect: connect,
          disconnect: disconnect,
          getUserStatus: getUserStatus,
          addUserStatusCB: addUserStatusCB,
          addToSubscription: function (topic) {
            additionalSubscribeToArray.push(topic);
          },
          removeFromSubscription: function (topic) {
            additionalSubscribeToArray.splice(additionalSubscribeToArray.indexOf(topic), 1);
          },
          getStatusTopic: getStatusTopic,
          getProfileStatusTopic: getProfileStatusTopic,
          subscribeToStatus: subscribeToStatus,
          unsubscribeToStatus: function (topic) {
            if (topic) {
              unsubscribeTo(getStatusTopic(topic));
            }
          },
          setUserGroupHandles: setUserGroupHandles,
          addToUserGroupHandle: addToUserGroupHandle,
          subscribeTo: subscribeTo,
          unsubscribeTo: unsubscribeTo,
          addTypingStatusCB: addTypingStatusCB,
          sendOnlineMessage: sendOnlineMessage,
          sendOfflineMessage: sendOfflineMessage,
          registerCallBacks: registerCallBacks,
          handleSubgroupSyncUpMessage: handleSubgroupSyncUpMessage,
          deleteSyncId: deleteSyncId,
          handleAdminRoleSyncUpMessage: handleAdminRoleSyncUpMessage,
          sendNewMemberJoinedGroupMessage: sendNewMemberJoinedGroupMessage,
          sendDataMessage: sendDataMessage,
          getMessageId: getMessageId
        };
      }]);
})();
