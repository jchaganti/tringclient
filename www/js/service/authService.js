(function () {
  var module = angular.module('tring.authService', ['tring.dbService', 'ng', 'qImproved', 'tring.constService']);
  var device;

  module.provider('AuthService', [function () {
    //$log.debug("Creating AuthService");

    function getDevice() {
      return device;
    }

    function setDevice(_device) {
      //$log.debug("Setting device : " + JSON.stringify(_device))
      device = _device;
    }

    //$log.debug("Creating AuthService end");
    return {
      getDevice: getDevice,
      setDevice: setDevice,
      $get: ['$q', function ($q) {
        return this;
      }]
    };
  }]);


  module.factory('tringHttpInterceptors', ['$rootScope', '$log', function ($rootScope) {
    var interceptors = {
      request: function (config) {
        if (device && device.did) {
          config.headers['Authorization'] = device.did;
        }

        return config;
      },

      response: function (response) {
        //$log.debug("HTTP Response: " + JSON.stringify(response));
        if (response.data.hasOwnProperty('validDevice')) {
          //$log.debug("Is Device valid response: " + JSON.stringify(response));
          if (!response.data.validDevice) {
            $rootScope.$broadcast("inValidDeviceFound");
          }
        }
        return response;
      }
    };
    //$log.debug("Returning interceptors");
    return interceptors;
  }]);
  module.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('tringHttpInterceptors');
  }]);
})();
