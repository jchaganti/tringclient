angular.module('tring.sharedDataService', ['qImproved', 'tring.constService'])
    .factory('SharedDataService', function () {
        var store = {};
        return {
            getValue: function (key) {
                return store[key];
            },
            setValue: function (key, value) {
                store[key] = value;
            },
            consumeValue: function (key) {
                var temp = store[key];
                delete store[key];
                return temp;
            },
            deleteValues: function () {
                var args = Array.prototype.slice.call(arguments);
                angular.forEach(args, function (key) {
                    delete store[key];
                });
            }

        };

    });