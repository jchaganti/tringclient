(function () {
  var module = angular.module('tring.manageGroupService', ['tring.dbService', 'tring.constService', 'qImproved', 'tring.authService', 'Tring'])
  module.factory('ManageGroupService', ['$q', '$http', 'DBService',
    'ConstsService', 'AuthService', '$timeout', 'Tring', '$log',
    function ($q, $http, DBService, ConstsService, AuthService, $timeout, Tring, $log) {
      $log.debug("Creating ManageGroupService");

      var getAllParentGroups = function () {
        var mbrDb = DBService.mbrDb();
        var options = {
            include_docs: true,
            startkey: ConstsService.GROUPS_START_KEY,
            endkey: ConstsService.GROUPS_END_KEY
        };

        var p1 = function () {
          return mbrDb.allDocs(options);
        };

        var p2 = function (result) {
          var rows = result.rows.map(function (row) {
            return row.doc;
          }).filter(function (group) {
            return !Tring.getParentGroupId(group) && (group.membershipType === ConstsService.OWNER_ROLE || group.membershipType === ConstsService.ADMIN_ROLE);
          });
          return $q.when(rows);
        };

        return $q.serial([p1, p2]);
      }

      var getAllSubgroups = function (pgid) {
        var mbrDb = DBService.mbrDb();
        var options = {
          include_docs: true,
          startkey: ConstsService.GROUPS_START_KEY,
          endkey: ConstsService.GROUPS_END_KEY
        };

        var p1 = function () {
          return mbrDb.allDocs(options);
        };

        var p2 = function (result) {
          var rows = result.rows.map(function (row) {
            return row.doc;
          }).filter(function (group) {
            return Tring.getParentGroupId(group) === pgid;
          });
          return $q.when(rows);
        };

        return $q.serial([p1, p2]);
      }

      var getAllMembers = function (gid, handleName, pageNum, pageSize) {
        var result = null;
        if(gid && handleName) {
          $log.debug("Get all members of group: " + gid + " handleName: " + handleName);

          if(!pageNum) {
            pageNum = 1;
          }

          if(!pageSize) {
            pageSize = -1;
          }
          var p1 = function () {
            var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL + "/" + gid + "/" + handleName + "?page=" + pageNum + "&pageSize=" + pageSize;

            $log.debug("The getAllMembers url: " + url);
            return $http.get(url)
          }

          var p2 = function (response) {
            var data = response.data;
            return $q.when(data);
          }

          return $q.serial([p1, p2]);
        }
        else {
          return $q.when(null);
        }
      }

      var getAllSubGroupMembers = function (gid, handleName, sgid, pageNum, pageSize, includeNonMembers) {
        var result = null;
        if(gid && handleName && sgid) {
          $log.debug("Get all members of group: " + gid + " handleName: " + handleName + " subgroup: " + sgid);

          if(!pageNum) {
            pageNum = 1;
          }

          if(!pageSize) {
            pageSize = 50;
          }

          if(!angular.isDefined(includeNonMembers)) {
            includeNonMembers = false;
          }

          var p1 = function () {
            var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL + "/" + gid + "/" + handleName + "/" + sgid + "?page=" + pageNum + "&pageSize=" + pageSize + "&includeNonMembers=" + includeNonMembers;

            $log.debug("The getAllSubGroupMembers url: " + url);
            return $http.get(url)
          }

          var p2 = function (response) {
            var data = response.data;
            return $q.when(data);
          }

          return $q.serial([p1, p2]);
        }
        else {
          return $q.when(null);
        }
      }

      var handleAdmin = function(gid, handleName, action, adminHid) {
        var p1 = function () {
          return $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_ADMIN_URL, {
            gid: gid,
            handleName: handleName,
            action: action,
            adminHid: adminHid
          });
        };

        var p2 = function (response) {
          return $q.when(response.data);
        }

        return $q.serial([p1, p2]);
      }

      var getNonMembersFromContacts = function(gid, handleName) {
        if(gid && handleName) {
          var p1 = function () {
            var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL + "/" + gid + "/" + handleName;
            return $http.get(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_ADMIN_URL);
          };

          var p2 = function (response) {
            return $q.when(response.data);
          }
          return $q.serial([p1, p2]);
        }
        else {
          return $q.when(null);
        }
      }

      var handleGroup = function(gid, subGroupId, handleName, subGroupName, action, adminAsModerators, asModerators, asMembers, adminAsNonMembers, asNonMembers) {
        if(gid && handleName) {
          var p1 = function () {
            var data = {
              gid: gid,
              handleName: handleName,
              action: action,
              name: subGroupName,
              adminAsModerator: adminAsModerators,
              asModerator: asModerators,
              asMember: asMembers
            }
            if(action === "EDIT") {
              data.subGroupId = subGroupId;
              data.adminAsNonMember = adminAsNonMembers;
              data.asNonMember = asNonMembers;
            }
            return $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_MEMBERS_URL, data);
          };

          var p2 = function (response) {
            return $q.when(response.data);
          }
          return $q.serial([p1, p2]);
        }
        else {
          return $q.when(null);
        }
      }

      return {
        getAllParentGroups: getAllParentGroups,
        getAllSubgroups: getAllSubgroups,
        getAllMembers: getAllMembers,
        getAllSubGroupMembers: getAllSubGroupMembers,
        handleAdmin: handleAdmin,
        getNonMembersFromContacts: getNonMembersFromContacts,
        handleGroup: handleGroup
      };
    }]);
})();
