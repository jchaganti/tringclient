(function () {
  var module = angular.module('tring.chatService', ['tring.dbService', 'tring.constService', 'qImproved', 'tring.authService', 'Tring'])
  module.factory('ChatService', ['$q', '$http', 'DBService',
    'ConstsService', 'AuthService', '$timeout', 'Tring', '$log',
    function ($q, $http, DBService, ConstsService, AuthService, $timeout, Tring, $log) {
      $log.debug("Creating ChatService");
      var chatOptions;
      var senderIdData = [];
      var handleData = [];
      var lastReadMsgData = [];
      var groupsRestoreInProgress = false;

      function resetChatOptions() {
        chatOptions = {
          //limit: 200,
          include_docs: true,
          startkey: ConstsService.CHAT_PREFIX,
          endkey: ConstsService.CHAT_PREFIX_END
        };
      }

      resetChatOptions();

      function getNextChatPage() {
        var mbrDb = DBService.mbrDb();
        var p1 = function () {
          return mbrDb.allDocs(chatOptions);
        }

        var p2 = function (response) {
          $log.debug("Chats response.rows.length = " + response.rows.length);
          if (response && response.rows.length > 0) {
            chatOptions.startkey = response.rows[response.rows.length - 1].key;
            chatOptions.skip = 1;
          }
          var _docs = response.rows.map(function (row) {
            row.doc._id = row.doc._id.substring(ConstsService.CHAT_PREFIX.length);
            return row.doc;
          });
          return $q.when(_docs);
        }

        return $q.serial([p1, p2]);
      }

      function initDbs(reCreate, loadDB) {
        var p1 = function () {
          //return $timeout(DBService.initMbrDB, 100, false, reCreate)
          return DBService.initMbrDB(reCreate);
        }

        var p2 = function () {
          //return $timeout(DBService.initMsgDB, 100, false, reCreate)
          return DBService.initMsgDB(reCreate);
        }

        return $q.serial([p1, p2]);

      }

      function getLastReadMessageIdForChat(sid) {
        return lastReadMsgData[sid];
      }

      function setLastReadMessageIdForChat(chat, lastReadMsgId) {
        $log.debug("setLastReadMessageIdForChat: " + JSON.stringify(chat) + "   lastReadMsgId: " + lastReadMsgId);
        lastReadMsgData[chat.sid] = lastReadMsgId;
        var mbrDb = DBService.mbrDb();
        chat.lastReadMsgId = lastReadMsgId;
        return mbrDb.put(chat);
      }

      function findById(id) {
        var mbrDb = DBService.mbrDb();
        var options = {
          include_docs: true,
          key: id
        }

        var p1 = function () {
          return mbrDb.allDocs(options);
        }

        var p2 = function (res) {
          $log.debug("res.rows.length: " + res.rows.length);
          var _docs = res.rows.map(function (row) {
            return row.doc;
          });
          return $q.when(_docs[0]);
        }

        return $q.serial([p1, p2]);
      }

      var findByGroupIds = function (gids) {
        var options = {
          include_docs: true,
          keys: gids.map(function (gid) {
            return ConstsService.CHAT_PREFIX + gid;
          })
        };
        var mbrDb = DBService.mbrDb();
        return mbrDb.allDocs(options)
          .then(function (res) {
            $log.debug("res.rows.length: " + res.rows.length);
            var docs = res.rows.map(function (row) {
              return row.doc;
            });
            return docs;
          })
          .catch(function (err) {
            $log.debug("Error while searching groupchats in FindByIds- err is " + JSON.stringify(err));
          });
      }

      function getSenderIdData(senderId, type) {
        $log.debug(" senderId : " + senderId + "  type: " + type);
        var senderData = senderIdData[senderId];
        if (senderData) {
          //$log.debug("SenderData found in cache: " + JSON.stringify(senderData));
          return $q.when(senderData);
        } else {
          var p1 = function () {
            return findById(senderId);
          }

          var p2 = function (doc) {
            $log.debug("SenderData found from DB: " + JSON.stringify(doc));
            if (!doc) {
              return $q.reject("sender data not found");
            }
            if (type === ConstsService.GROUP) {
              var pgid = Tring.getParentGroupId(doc);
              if (pgid) {
                var _p1 = function () {
                  return findById(pgid);
                }

                var _p2 = function (groupData) {
                  if (groupData) {
                    doc.o2m = groupData.o2m;
                    doc.userGroupStatus = groupData.userGroupStatus;
                    doc.pgName = groupData.name;
                    doc.pgTopic = groupData.topic;
                    doc.pgid = doc.pgid;
                    senderIdData[senderId] = doc;
                    return $q.when(doc);
                  }
                  else {
                    return $q.reject("This is a sub group and could not find its parent group's sender data");
                  }
                }
                return $q.serial([_p1, _p2]);
              }
              else {
                senderIdData[senderId] = doc;
                return $q.when(doc);
              }
            }
            else {
              senderIdData[senderId] = doc;
              return $q.when(doc);
            }
          }
          return $q.serial([p1, p2]);
        }
      }

      function updateSenderIdDataInCache(senderId, data) {
        $log.debug('555');
        senderIdData[data._id] = data;
      }

      function removeSenderIdDataInCache(senderId) {
        if(senderIdData[senderId]) {
          delete senderIdData[senderId];
        }
        else {
          $log.debug("The senderId " + senderId + " not found in cache");
        }

      }

      function findChat(chatId) {
        $log.debug("Finding chatId is " + chatId);
        return findById(chatId);
      }


      function saveSingleUserChat(chatId, senderId, localSenderId) {
        //localSenderId is required for getSenderIdData(), since we do a local db query on the Contacts/Group data there,
        //and the Contacts record will have a local id that is not based on the server id for the Contact. In
        //the case of Groups where there is no concept of a 'local' Group (unlike Contacts), the localSid and
        //sid are the same.
        $log.debug("Saving single user chatId: " + chatId);
        var deferred = $q.defer();
        var mbrDb = DBService.mbrDb();
        var doc = {
          _id: chatId,
          sendertype: 1,
          sid: senderId,
          localSid: localSenderId
        };
        mbrDb.put(doc).then(function (res) {
            deferred.resolve(doc);
          })
          .catch(function (err) {
            deferred.reject(err);
          });
        return deferred.promise;
      }

      function registerMbrDBChange(cb) {
        var mbrDb = DBService.mbrDb();
        if (cb) {
          mbrDb.changes({
              live: true,
              since: 'now',
              filter: function (doc) {
                return Tring.isChat(doc) || Tring.isGroupObject(doc);
              },
              include_docs: true
            })
            .on('change', cb);
        }
      }

      var saveGroupChat = function (group, chatId) {
        $log.debug("In saveGroupChat - group is " + JSON.stringify(group));
        var id =
          Tring.startsWith(group._id, ConstsService.GROUPS_START_KEY) ? group._id : ConstsService.GROUPS_START_KEY + group._id;
        var mbrDb = DBService.mbrDb();
        var doc = angular.extend({}, {
          _id: chatId,
          sendertype: 2,
          senderName: group.name,
          name: group.name,
          sid: id,
          localSid: id
        });
        return mbrDb.put(doc).then(function (res) {
            doc._rev = res.rev;
            return doc;
          })
          .then(null, function (err) {
            $log.debug('savegroupchat error is ' + JSON.stringify(err));
            throw err;
          });
      };

      var correctLocalSid = function(chat, localSid) {
          var mbrDb = DBService.mbrDb();
          chat.localSid = localSid;
          return mbrDb.put(chat).then(function(res){
              chat._rev = res.rev;
              return chat;
          }).then(null, function(err){
              $log.error("Error saving corrected chat record in correctLocalSid - err is "+JSON.stringify(err));
              return chat;
          });
      };
        
      //Not used
      var deleteGroupChats = function (groupsWithRoles) {
        var mbrDb = DBService.mbrDb();
        var chatsToBeDeleted = findByGroupIds(groupsWithRoles.map(function (grp) {
          return grp._id;
        })).map(function (chat) {
          return angular.extend(chat, {
            _deleted: true
          });
        });
        return mbrDb.bulkDocs(chatsToBeDeleted)
          .then(function (result) {
            var len;
            if (result && (len = result.filter(function (row) {
                return row.error;
              }).length)) {
              console.error("failed to delete " + len + " group chats");
              return $q.reject(false);
            }
            return result;
          })
          .then(null, function (err) {
            $log.debug(JSON.stringify(err));
            return $q.reject(false);
          });
      };

      var getHandleData = function (handleName, sid, isO2M) {
        $log.debug("Getting handleData for handleName: " + handleName + " SenderId: " + sid);
        var handleId = pouchCollate.toIndexableString([ConstsService.HANDLE_PREFIX, sid, handleName]);
        var handleInfo = handleData[handleId];
        var mbrDb = DBService.mbrDb();
        if (handleInfo) {
          $log.debug("handleData found in cache: " + JSON.stringify(handleInfo));
          return $q.when(handleInfo);
        } else {
          var p1 = function () {
            return findById(handleId);
          }

          var p2 = function (doc) {
            if (!isO2M || !doc || !doc.p2p) {
              $log.debug("Getting it from the server.");
              var _p0 = function () {
                return findById(sid);
              }
              var _p1 = function (group) {
                var pgid = Tring.getParentGroupId(group);
                var gid, sgid;
                if (pgid) {
                  gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, pgid);
                  sgid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, sid);
                }
                else {
                  gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, sid);
                }
                var url = ConstsService.MEMBERSHIP_SERVER_URL + "/group/" + gid + "/member/" + handleName;
                if (sgid) {
                  url += "?subGroupId=" + sgid;
                }

                return $http.get(url);
              };

              var _p2 = function (res) {
                var userData = res.data;
                $log.debug("Handle response data from server: " + JSON.stringify(userData));
                $log.debug("Handle data doc from client: " + JSON.stringify(doc));
                if (!doc) {
                  doc = {
                    _id: handleId,
                    p2p: Tring.getUserMQTTtopic(userData.p2p),
                    isModerator: userData.isModerator
                  }
                } else {
                  doc.p2p = Tring.getUserMQTTtopic(userData.p2p);
                  doc.isModerator = userData.isModerator;
                }
                return mbrDb.put(doc);
              }

              var _p3 = function (_res) {
                $log.debug("handleData added to cache obtained from server: " + JSON.stringify(doc));
                handleData[handleId] = doc;
                return $q.when(doc);
              }
              return $q.serial([_p0, _p1, _p2, _p3]);
            } else {
              $log.debug("handleData added to cache: " + JSON.stringify(doc));
              handleData[handleId] = doc;
              return $q.when(doc);
            }
          }
          return $q.serial([p1, p2]);
        }
      }

      var blockUnblockHandle = function (handleName, groupId, blockStatus) {
        var handleId = pouchCollate.toIndexableString([ConstsService.HANDLE_PREFIX, groupId, handleName]);
        var mbrDb = DBService.mbrDb();
        var p1 = function () {
          return findById(handleId);
        }

        var p2 = function (doc) {
          if (!doc) {
            doc = {
              _id: handleId,
              blocked: blockStatus
            }
          } else {
            doc.blocked = blockStatus;
          }
          handleData[handleId] = doc;
          return mbrDb.put(doc);
        }

        return $q.serial([p1, p2]);

      }

      var updateDocument = function (sid, key, value) {
        var updatedDoc = null;
        var mbrDb = DBService.mbrDb();
        var p1 = function () {
          return findById(sid);
        }

        var p2 = function (doc) {
          if (doc) {
            doc[key] = value;
            updatedDoc = doc;
            updateSenderIdDataInCache(sid, updatedDoc);
            return mbrDb.put(doc);
          } else {
            throw "Doc not found with sid: " + sid;
          }
        }

        var p3 = function (res) {
          return $q.when(updatedDoc);
        }

        return $q.serial([p1, p2, p3]);
      }

      var getAllExistingGroups = function () {
        var url = ConstsService.MEMBERSHIP_SERVER_URL + "/user/allgroups";

        var populateGroups = function (inGroups, groups, membershipType) {
          if (inGroups) {
            var keys = Object.keys(inGroups);
            if (keys && keys.length > 0) {
              angular.forEach(keys, function (key) {
                var group = inGroups[key];
                if(group.pgid) {
                  group.parentGroupId = ConstsService.GROUPS_START_KEY + group.pgid;
                  delete group.pgid;
                }
                group.topic = Tring.getGroupMQTTtopic(group.topic);
                group._id = ConstsService.GROUPS_START_KEY + key;
                if(membershipType) {
                  group.membershipType = membershipType;
                }
                if(angular.isDefined(group.membershipStatus)) {
                  group.userGroupStatus = group.membershipStatus;
                  delete group.membershipStatus;
                }
                groups.push(group);

                var chat = {
                  _id: ConstsService.CHAT_PREFIX + group._id,
                  sendertype: 2,
                  senderName: group.name,
                  name: group.name,
                  sid: group._id,
                  localSid: group._id
                }
                groups.push(chat);
              });
            }
          }
        }

        var deleteRecords = function (startKey, endKey) {
          var _p1 = function () {
            return mbrDb.allDocs({
              include_docs: true,
              startkey: startKey,
              endkey: endKey
            });
          }

          var _p2 = function (result) {
            $log.debug("Results for key " + startKey + " are: " + JSON.stringify(result.rows));
            var rows = result.rows.map(function (row) {
              row.doc._deleted = true;
              return row.doc;
            });

            if(rows && rows.length > 0) {
              $log.debug("Records found to be deleted for the key: " + startKey);
              return mbrDb.bulkDocs(rows).then(function (res) {
                return $q.when(true);
              }).catch(function (err) {
                $log.debug(err);
              });
            }
            else {
              $log.debug("No Records found to be deleted for the key: " + startKey);
              return $q.when(true);
            }
          }

          return $q.serial([_p1, _p2]);

        }

        var mbrDb = DBService.mbrDb();

        var p1 = function () {
          return $http.get(url);
        }

        var p2 = function (res) {
          var data = res.data;
          $log.debug("Groups: " + JSON.stringify(data));
          var groups = [];
          populateGroups(data["parentGroups"], groups);
          populateGroups(data["asModeratorSubGroups"], groups, ConstsService.MODERATOR_ROLE);
          populateGroups(data["asMemberSubGroups"], groups, ConstsService.MEMBER_ROLE);
          return $q.when(groups);
        }

        var p3 = function (groups) {
          if(groups.length > 0) {
            var _p1 = function () {
              return deleteRecords(ConstsService.GROUPS_START_KEY, ConstsService.GROUPS_END_KEY);
            }

            var _p2 = function () {
              return deleteRecords(ConstsService.CHAT_PREFIX, ConstsService.CHAT_PREFIX_END);
            }

            var _p3 = function () {
              return $q.when(groups);
            }
            return $q.serial([_p1, _p2, _p3]);
          }
          else {
            return $q.when(groups);
          }
        }

        var p4 = function (groups) {
          if(groups.length > 0) {
            return mbrDb.bulkDocs(groups).then(function (res) {
              $log.debug("Result of storing groups: " + JSON.stringify(res));
              return $q.when({
                count: res.length,
                groupsRestoredSuccess: true,
                wasGroupRestoringInProgress: false
              });
            });
          }
          else {
            $log.debug("There were no existing groups for this user to be restored");
            return $q.when({
              count:0,
              groupsRestoredSuccess: true,
              wasGroupRestoringInProgress: false
            });
          }
        }

        return $q.serial([p1, p2, p3, p4]);
      }

      var startRestoringGroups = function () {
        if(!groupsRestoreInProgress) {
          groupsRestoreInProgress = true;
          return getAllExistingGroups();
       }
        else {
          return $q.when({
            groupsRestoredSuccess: false,
            wasGroupRestoringInProgress: true
          });
       }
      }

      return {
        setLastReadMessageIdForChat: setLastReadMessageIdForChat,
        getLastReadMessageIdForChat: getLastReadMessageIdForChat,
        getNextChatPage: getNextChatPage,
        resetChatOptions: resetChatOptions,
        initDbs: initDbs,
        getSenderIdData: getSenderIdData,
        findById: findById,
        findChat: findChat,
        saveSingleUserChat: saveSingleUserChat,
        registerMbrDBChange: registerMbrDBChange,
        saveGroupChat: saveGroupChat,
        updateSenderIdDataInCache: updateSenderIdDataInCache,
        removeSenderIdDataInCache: removeSenderIdDataInCache,
        getHandleData: getHandleData,
        blockUnblockHandle: blockUnblockHandle,
        updateDocument: updateDocument,
        deleteGroupChats: deleteGroupChats,
        startRestoringGroups: startRestoringGroups,
        correctLocalSid: correctLocalSid
      };
    }]);

})();
