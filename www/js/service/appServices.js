angular.module('tring.appServices', ['tring.dbService', 'tring.constService', 'qImproved', 'tring.authService', 'Tring'])
    .factory('GroupService', ['$http', 'ConstsService', '$q', 'DBService', 'Tring', 'ChatService', '$log', function ($http, ConstService, $q, DBService, Tring, ChatService, $log) {
        var obj = {};
        var groups = [];
        var ownedGroups = [];
        var joinedGroups = [];

        $log.debug("Creating GroupService");

        obj.setGroups = function (grpsArr) {
            ownedGroups = grpsArr[0];
            joinedGroups = grpsArr[1];
            return true;
        };

        obj.getOwnedGroups = function () {
            return ownedGroups;
        };

        obj.getJoinedGroups = function () {
            return joinedGroups;
        };

        obj.partitionGroups = function (grps) {
            angular.forEach(grps, function (grp) {
                if (grp.membershipType === ConstService.OWNER_ROLE ||
                    grp.membershipType === ConstService.ADMIN_ROLE) {
                    ownedGroups.push(grp);
                } else {
                    joinedGroups.push(grp);
                }
            });
            return [ownedGroups, joinedGroups];
        };

        obj.getGroups = function () {
            groups = [];
            joinedGroups = [];
            ownedGroups = [];
            //var deferred = $q.defer();
            var mbrDb = DBService.mbrDb();
            var options = {
                include_docs: true,
                startkey: ConstService.GROUPS_START_KEY,
                endkey: ConstService.GROUPS_END_KEY
            };
            var localGroupMap = {};
            return mbrDb.allDocs(options).then(function (result) {
                    $log.debug("Result is :-");
                    $log.debug(JSON.stringify(result));
                    var rows = result.rows.map(function (row) {
                        return row.doc;
                    });

                    angular.forEach(rows, function (row) {
                        localGroupMap[row._id] = row;
                    });
                    $log.debug("find local group docs");
                    $log.debug(JSON.stringify(rows));
                    return $http.get(ConstService.MEMBERSHIP_SERVER_URL + ConstService.USER_GROUPS_MEMBERSHIP_URL, {
                        timeout: ConstService.HTTP_TIMEOUT
                    });
                })
                .then(function (result) {
                    $log.debug("process server groups");
                    $log.debug(JSON.stringify(result));
                    groups = angular.forEach(result.data, function (row) {
                        var id = ConstService.GROUPS_START_KEY + row._id;
                        row._id = id;
                        row.topic = Tring.getGroupMQTTtopic(row.topic);
                        if (id in localGroupMap) {
                            row._rev = localGroupMap[id]._rev;
                            row.userGroupStatus = row.membershipStatus;
                            delete row.membershipStatus;
                        }
                    });
                    return mbrDb.bulkDocs(groups);
                })
                .then(function (result) {
                    $log.debug("wrote local db");
                    groups = result.map(function (row, i) {
                        if (!row.ok) {
                            throw "row ok not true";
                        }
                        groups[i]._rev = groups[i].rev = row.rev;
                        return groups[i];
                    });
                    $log.debug(JSON.stringify(groups));
                    //deferred.resolve(groups);
                    return groups;
                })
                .then(null, function (err) {
                    $log.debug("problems getting groups");
                    $log.debug(JSON.stringify(err));
                    //deferred.reject(err);
                    return $q.reject({
                        type: "timedOut"
                    });
                });
            //return deferred.promise;
        };

        obj.noGroups = function () {
            return !groups || groups.length === 0;
        };

        obj.noJoinedGroups = function () {
            return !joinedGroups || joinedGroups.length === 0;
        };

        obj.noOwnedGroups = function () {
            return !ownedGroups || ownedGroups.length === 0;
        };

        obj.getGroupsPaged = function (from, to) {
            return groups.slice(from, to);
        };

        obj.getLocalGroups = function () {
            var mbrDb = DBService.mbrDb();
            var options = {
                include_docs: true,
                startkey: ConstService.GROUPS_START_KEY,
                endkey: ConstService.GROUPS_END_KEY
            };

            var p1 = function () {
                return mbrDb.allDocs(options);
            };

            var p2 = function (result) {
                var rows = result.rows.map(function (row) {
                    return row.doc;
                });
                return $q.when(rows);
            };

            return $q.serial([p1, p2]);
        };

        obj.getLocalGroup = function (gid) {
            var mbrDb = DBService.mbrDb();
            return mbrDb.get(gid);
        };

        obj.getServerGroup = function (gName) {
            $log.debug("9999");
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.MY_GROUP_INFO_URL.replace('@0', gName);
            $log.debug(url);
            return $http.get(url, {
                    timeout: ConstService.HTTP_TIMEOUT
                }).then(function (result) {
                    $log.debug('121212');
                    var retval;
                    if (result && result.data) {
                        retval = result.data;
                    } else {
                        retval = {
                            success: false
                        };
                    }
                    $log.debug("from getServerGroup - retval is " + JSON.stringify(retval));
                    return retval;
                })
                .then(null, function (err) {
                    $log.debug("error from getServerGroup is " + err);
                    throw err;
                });
        };

        obj.checkHandleAvailabilityForGroup = function (handleName, gid) {
            if (!handleName) {
                return $q.resolve(false);
            }
            var gidServer = Tring.clientToServerGroupId(gid);
            var url = ConstService.HANDLE_IS_AVAILABLE_URL.replace('@1', handleName);
            url = url.replace('@0', gidServer);
            var nameAvailable;
            return $http.get(ConstService.MEMBERSHIP_SERVER_URL + url).then(function (result) {
                if (result && result.data && result.data.success) {
                    nameAvailable = true;
                } else {
                    nameAvailable = false;
                }
                return nameAvailable;
            });
        };
        obj.checkHandleAvailabilityForGroupName = function (handleName, gname) {
            if (!handleName) {
                return $q.resolve(false);
            }
            var url = ConstService.HANDLE_FOR_GROUP_NAME_IS_AVAILABLE_URL.replace('@1', handleName);
            url = url.replace('@0', gname);
            var nameAvailable;
            return $http.get(ConstService.MEMBERSHIP_SERVER_URL + url).then(function (result) {
                if (result && result.data && result.data.success) {
                    nameAvailable = true;
                } else {
                    nameAvailable = false;
                }
                return nameAvailable;
            });
        };

        obj.storeGroup = function (group) {
            var mbrDb = DBService.mbrDb();
            var gid = Tring.startsWith(group._id, ConstService.GROUPS_START_KEY) ? group._id : ConstService.GROUPS_START_KEY + group._id;
            return mbrDb.get(gid)
                .then(function (grp) {
                    if (grp !== null) {
                        delete group._rev;
                        angular.extend(grp, group);
                        return saveGroup(grp);
                    } else {
                        return saveGroup(group);
                    }
                })
                .then(null, function () {
                    return saveGroup(group);
                });
        };

        obj.formGroupMemberObjectFromServerResponse = function (obj, groupName) {
            var gid = Tring.startsWith(obj.gid, ConstService.GROUPS_START_KEY) ? obj.gid : ConstService.GROUPS_START_KEY + obj.gid;
            return {
                gid: gid,
                handleName: obj.handleName,
                membershipType: obj.membershipType,
                groupName: groupName,
                groupTopic: Tring.getGroupMQTTtopic(obj.topic),
                userGroupStatus: obj.membershipStatus,
                groupO2M: obj.groupO2M,
                thumbnailData: obj.thumbnailData
            };
        };
        var createLocalGroupMemberAndChat = function (message) {
            //message is somtimes the group obj from mbrship srvr
            var gid = message.gid || message._id;
            gid = Tring.startsWith(gid, ConstService.GROUPS_START_KEY) ? gid : ConstService.GROUPS_START_KEY + gid;
            var group = {
                _id: gid,
                name: message.groupName,
                membershipType: message.membershipType || ConstService.MEMBER_ROLE,
                topic: message.groupTopic,
                handleName: message.handleName,
                userGroupStatus: message.userGroupStatus || ConstService.USER_GROUP_STATUS_ACTIVE,
                o2m: message.groupO2M,
                thumbnailData: message.thumbnailData,
                closed: message.groupClosed
            };
            var p1 = function (err) {
                if (err.status !== 409) {
                    return $q.reject(err);
                } else {
                    return group;
                }
            };
            var p2 = function (grp) {
                $log.debug("About to call saveGroupChat");
                //Create a chat for the Group just created
                var chatId = ConstService.CHAT_PREFIX + grp._id;
                return ChatService.saveGroupChat(grp, chatId)
                    .then(function () {
                        return message.syncId;
                    })
                    .then(null, function (err) {
                        if (err.status !== 409) {
                            return $q.reject(err);
                        }
                        return message.syncId;
                    });
            };

            var p3 = function (err) {
                console.error("Error during createLocalGroupMemberAndChat processing is " + err);
                $q.reject(err);
            };
            return obj.storeGroup(group).then(null, p1).then(p2).then(null, p3);
        };

        obj.createLocalGroupMemberAndChat = createLocalGroupMemberAndChat;

        obj.extendGroupFromServer = function (group, srvrGroup) {
            angular.extend(group, srvrGroup);
            group._id = ConstService.GROUPS_START_KEY + srvrGroup.gid;
            group.topic = Tring.getGroupMQTTtopic(srvrGroup.topic);
        };

        var saveGroup = function (group) {
            var gid = Tring.startsWith(group._id, ConstService.GROUPS_START_KEY) ? group._id : ConstService.GROUPS_START_KEY + group._id;
            group._id = gid;
            $log.debug("in saveGroup - group is " + JSON.stringify(group));
            //var deferred = $q.defer();
            var mbrDb = DBService.mbrDb();
            $log.debug("mbrDb is " + mbrDb);
            $log.debug("Hi Hi");
            return $q.when(mbrDb.put(group)).then(function (response) {
                    $log.debug("111");
                    group.rev = group._rev = response.rev;
                    $log.debug("Save Group to local db successful- group is " + JSON.stringify(group));
                    ChatService.updateSenderIdDataInCache(group._id, group);
                    $log.debug('666');
                    //deferred.resolve(group);
                    return group;
                })
                .then(null, function (err) {
                    $log.debug("Error in saveGroup - err is " + err);
                    //deferred.reject(err);
                    return $q.reject(err);
                });
            //return deferred.promise;
        };

        obj.saveGroup = saveGroup;

        obj.createGroup = function (group, ownerHandle) {
            return $http.post(ConstService.MEMBERSHIP_SERVER_URL + ConstService.CREATE_GROUP_URL, {
                    groupData: group,
                    ownerHandleName: ownerHandle
                }, {
                    timeout: ConstService.HTTP_TIMEOUT
                })
                .then(function (response) {
                    var groupResp;
                    if (response && response.data) {
                        groupResp = response.data;
                        if (!groupResp.success) {
                            console.error("createGroup no success");
                            return $q.reject("createGroup no success");
                        }
                        $log.debug("In createNewGroup, after http call, response.data is " + JSON.stringify(response.data));
                        return angular.extend(groupResp, {
                            membershipType: ConstService.OWNER_ROLE
                        });
                    } else {
                        console.error("createGroup error");
                        return $q.reject("createGroup error");
                    }
                });
        };

        obj.createGroupMember = function (gid, hdlName) {
            var url = ConstService.CREATE_GROUP_MEMBER_URL.replace('@0', Tring.clientToServerGroupId(gid));
            url = ConstService.MEMBERSHIP_SERVER_URL + url;
            return $http.post(url, {
                handleName: hdlName
            }, {
                timeout: ConstService.HTTP_TIMEOUT
            });
        };

        obj.updateGroupInServer = function (gid, gname, group) {
            gid = Tring.clientToServerGroupId(gid);
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.GROUP_DETAILS_UPDATE_URL;
            var groupData = angular.extend({
                _id: gid,
                name: gname
            }, group);
            return $http.post(url, {
                groupData: groupData
            }, {
                timeout: ConstService.HTTP_TIMEOUT
            }).then(function (response) {
                if (response && response.data && response.data.success) {
                    return groupData;
                } else {
                    console.error("error in updateGroupInServer. falsy response data ");
                    return $q.reject("updateGroupInServer error");
                }
            }).catch(function (err) {
                console.error("error in updateGroupInServer is " + JSON.stringify(err));
            });
        };
        obj.getMyGroupAndSubgroupsFor = function (group) {
            var mbrDb = DBService.mbrDb();
            $log.debug("entered getMyGroupAndSubgroupsFor mbrDb is " + mbrDb);
            return mbrDb.allDocs({
                startkey: ConstService.GROUPS_START_KEY,
                endkey: ConstService.GROUPS_END_KEY,
                include_docs: true
            }).then(function (result) {
                $log.debug('getMyGroupAndSubgroupsFor - then - result is ' + result);
                return result.rows.filter(function (_row) {
                    return (_row.doc._id === group._id || Tring.getParentGroupId(_row.doc) === group._id);
                }).map(function (row) {
                    return angular.extend(row.doc, {
                        _id: row.doc._id,
                        _rev: row.doc._rev,
                        membershipType: Tring.getGroupMembershipRole(row.doc),
                        topic: row.doc.topic
                    });
                });
            });
        };
        obj.leaveGroup = function (group) {
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.LEAVE_GROUP_URL.replace("@0", Tring.clientToServerGroupId(group._id));
            return $http.post(url, {}, {
                timeout: ConstService.HTTP_TIMEOUT
            }).then(function (response) {
                if (response && response.data) {
                    return response.data.success;
                } else {
                    return $q.reject("leave failed");
                }
            }).catch(function (err) {
                $log.debug("leave group Error is " + JSON.stringify(err));
                return $q.reject("leave failed");
            });
        };

        obj.getGroupReviewsAndRatings = function (group) {
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.GROUP_GET_RATINGS_REVIEWS_URL;
            url = url.replace('@0', Tring.clientToServerGroupId(group._id));
            return $http.get(url).then(function (response) {
                if (response && response.data) {
                    return response.data;
                } else {
                    throw -1;
                }
            }).then(null, function (err) {
                throw err;
            });
        };

        obj.getNextGroupReviewsAndRatings = function (group, offset) {
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.GROUP_GET_RATINGS_REVIEWS_URL;
            url = url.replace('@0', Tring.clientToServerGroupId(group._id));
            url = url.replace('@1', offset);
            return $http.get(url).then(function (response) {
                if (response && response.data) {
                    return response.data;
                } else {
                    throw -1;
                }
            }).then(null, function (err) {
                throw err;
            });
        };

        obj.pushRatingAndReview = function (ratingAndReview, group) {
            var url = ConstService.MEMBERSHIP_SERVER_URL + ConstService.PUSH_RATING_AND_REVIEW_URL;
            url = url.replace('@0', Tring.clientToServerGroupId(group._id));
            return $http.post(url, ratingAndReview).then(function (response) {
                if (response && response.data && response.data.success) {
                    return true;
                }
                throw -1;
            });
        };

        obj.setStatusForGroupsFromLocalDb = function (groupsWithRoles, status) {
            var mbrDb = DBService.mbrDb();
            var groups = groupsWithRoles.map(function (grp) {
                return angular.extend(grp, {
                    userGroupStatus: status
                });
            });

            return mbrDb.bulkDocs(groups)
                .then(function (result) {
                    var len;
                    if (result && (len = result.filter(function (row) {
                            return row.error;
                        }).length)) {
                        console.error("failed to delete " + len + " groups");
                        return $q.reject("failed to delete " + len + " groups");
                    }
                    return result;
                })
                .catch(function (err) {
                    $log.debug(JSON.stringify(err));
                    return $q.reject("Error during delete groups");
                });
        };

        obj.getGroupMeanRatingAndReview = function (group) {

        };
        obj.getGrouMembershipError = function (status) {
          switch (status) {
            case ConstsService.USER_GROUP_STATUS_ACTIVE:
              return "You are already a member of the group";
              break;
            case ConstsService.USER_GROUP_STATUS_REJECTED:
              return "You cannot join the group since your  membership request was rejected earlier";
              break;
            case ConstsService.USER_GROUP_STATUS_EVICTED:
              return "You cannot join the group since you were evicted from the group earlier";
              break;
            default:
              break;
          }
          return "";
        };
        return obj;
            }])

.factory('ContactsService', ['$http', '$q', 'DBService', '$cordovaContacts', 'ConstsService', 'AuthService', '$window', 'Tring', '$timeout', 'ChatService', '$log', function ($http, $q, DBService, $cordovaContacts, ConstsService, AuthService, $window, Tring, $timeout, ChatService, $log) {
        var obj = {};
        $log.debug("Creating ContactService");
        var cntcts = [];

        obj.getContacts = function () {
            var cts = cntcts;
            if (!cts || !cts.length) {
                cts = getAllDbContacts();
            }
            return $q.when(cts).then(function (contacts) {
                cntcts = contacts;
                return cntcts;
            });
        };

        obj.setContacts = function (contacts) {
            cntcts = contacts;
        };

        obj.getNonMemberContactsFromServer = function (gid, handle) {
            var groupid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, gid);
            return $http.get(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.GROUP_NON_MEMBERS_URL + '/' + groupid + '/' + handle, {
                timeout: ConstsService.HTTP_CONTACTS_TIMEOUT
            }).then(function (response) {
                if (response && response.data && response.data.nonMembers) {
                    var nonMemberNumbers = {};
                    angular.forEach(response.data.nonMembers, function (num) {
                        nonMemberNumbers[num] = true;
                    });
                    return obj.getContacts().then(function (cts) {
                        return cts.filter(function (ct) {
                            if (ct.phoneNumber in nonMemberNumbers) {
                                return true;
                            }
                            return false;
                        });
                    });
                } else {
                    return [];
                }
            }).then(null, function (err) {
                $log.debug("Error :" + JSON.stringify(err));
                throw err;
            });
        };
        //The following object is used for invoking functions (func) in a synchronized manner as follows :
        // ContactsService.synchronizer.run(func, arg1, arg2,...);
        //Use discreetly to avoid deadlock
        var synchronizer = (function () {
            var locks = {
                "generic": false,
                "refresh": false
            };

            return function (lock, failOnBusy) {
                var BLOCKED_COUNT_LIMIT = 50;
                var blockedCount = 0;
                //var busy = false;
                var isBusy = function () {
                    return locks[lock];
                };
                var setBusy = function (value) {
                    locks[lock] = value;
                };
                var run = function (func) {
                    var args = Array.prototype.slice.call(arguments, 1);
                    var deferred = $q.defer();
                    var timeoutFunc = function () {
                        if (isBusy()) {
                            ++blockedCount;
                            $log.debug("Lock " + lock + ": blocked count is " + blockedCount);
                            if (failOnBusy || blockedCount >= BLOCKED_COUNT_LIMIT) {
                                console.error("Lock " + lock + ": ERROR: blockedCount >= BLOCKED_COUNT_LIMIT");
                                return deferred.reject("blockedCount >= BLOCKED_COUNT_LIMIT");
                            } else {
                                $timeout(timeoutFunc, 1000);
                            }
                        } else {
                            blockedCount = 0;
                            setBusy(true);
                            $q.when(func.apply(null, args)).then(function (result) {
                                setBusy(false);
                                return deferred.resolve(result);
                            }, function (err) {
                                setBusy(false);
                                console.error("Lock " + lock + ": Function failed to run properly via ContactsService.busyService. Error is " +
                                    JSON.stringify(err));
                                return deferred.reject(err);

                            });
                        }
                    };
                    timeoutFunc();
                    return deferred.promise;
                };

                return {
                    run: run
                };
            };
        })();

        obj.synchronizerGeneric = synchronizer("generic");

        obj.synchronizerRefresh = synchronizer("refresh", true);

        var refreshFromAddressBook = function () {
            cntcts = [];
            $log.debug('Calling refreshFromAddressBook()');
            var deferred = $q.defer();
            var onSuccess = function (contacts) {
                var cSort = function (a, b) {
                    var aName = (a.displayName) ? a.displayName.trim().toLowerCase() : '0';
                    var bName = (b.displayName) ? b.displayName.trim().toLowerCase() : '0';
                    return aName < bName ? -1 : (aName === bName ? 0 : 1);
                };
                contacts = contacts.sort(cSort);
                var pushContacts = function (num, idx) {
                    //$log.debug(JSON.stringify(contacts));
                    //$log.debug(JSON.stringify(num));
                    var name = contacts[idx].displayName;
                    var addressBookId = contacts[idx].id;
                    cntcts.push({
                        addressBookId: addressBookId,
                        name: ((name) ? name : "Anonymous"),
                        number: num
                    });
                };
                for (var i = 0; i < contacts.length; i++) {
                    var contact = contacts[i];
                    /*jshint -W083 */
                    if (contact.phoneNumbers) {
                        angular.forEach(contact.phoneNumbers
                            .map(function (pnum) {
                                return pnum.normalizedNumber;
                            })
                            .filter(function (val, index, self) {
                                return val && self.indexOf(val) === index;
                            })
                            .map(function (number) {
                                try {
                                    var countryCode = Tring.getCountryCode(number);
                                    if (countryCode === null || (countryCode === "nothing")) {
                                        var device = AuthService.getDevice();
                                        countryCode = device.countryCode;
                                    }
                                    var numberType;
                                    if (countryCode) {
                                        //$log.debug('pnum.value is ' + pnum.value);
                                        numberType = $window.phoneUtils.getNumberType(number, countryCode);
                                    }
                                    //var numberType = 'MOBILE';
                                    //$log.debug("Number type of " + pnum.value + " is " + JSON.stringify(numberType) + " CountryCode: " + countryCode);
                                    if (numberType === "MOBILE" || numberType === "FIXED_LINE_OR_MOBILE") {
                                        var num;
                                        num = $window.phoneUtils.formatE164(number, countryCode);
                                        return num;
                                    } else {
                                        // Non-Mobile numbers
                                        return null;
                                    }
                                } catch (err) {
                                    return null;
                                }

                            }).filter(function (val, index, self) {
                                return val && self.indexOf(val) === index;
                            }),
                            function (num) {
                                var idx = i;
                                pushContacts(num, idx);
                            });
                    }
                }
                //                $log.debug('From refresh():onSuccess(). Device contacts are :-');
                //                $log.debug(JSON.stringify(contacts));
                //                $log.debug('From refresh():onSuccess(). Modified device contacts are :-');
                //                $log.debug(JSON.stringify(cntcts));
                deferred.resolve(cntcts);
                return cntcts;
            };

            var onError = function (contactError) {
                console.error("error is " + JSON.stringify(contactError));
                deferred.reject(contactError);
            };
            //        var options = {
            //            desiredFields: ['id', 'name', 'phoneNumbers']
            //        };
            //        options.multiple = true;
            navigator.contactsPhoneNumbers.list(onSuccess, onError);
            //                    .then(function (cnts) {
            //                        deferred.resolve(cnts);
            //                    }, onError);
            return deferred.promise;
        };

        var cSort = function (a, b) {
            var aRegistered = a.registered;
            var bRegistered = b.registered;
            if (aRegistered && !bRegistered) {
                return -1;
            } else if (bRegistered && !aRegistered) {
                return 1;
            }
            var aName = (a.name) ? a.name.trim().toLowerCase() : '0';
            var bName = (b.name) ? b.name.trim().toLowerCase() : '0';
            return aName < bName ? -1 : (aName === bName ? 0 : 1);
        };

        obj.sort = function (contacts) {
            return contacts.sort(cSort);
        };

        var getAllDbContacts = function () {

            var mbrDb = DBService.mbrDb();
            return mbrDb.allDocs({
                startkey: ConstsService.USERS_START_KEY,
                endkey: ConstsService.USERS_END_KEY,
                include_docs: true
            }).then(function (result) {
                return result.rows.filter(function (_row) {
                        return !(_row.doc.hasOwnProperty('srvrUser') && _row.doc.srvrUser);
                    })
                    .map(function (row) {
                        return row.doc;
                    }).sort(cSort);
            });
        };


        obj.filterRegisteredContacts = function (cts) {
            return cts.filter(function (ct) {
                return ct.registered;
            });
        };

        var getDbContacts = function (phoneNumbers) {
            var mbrDb = DBService.mbrDb();
            var options = {};
            options.keys = phoneNumbers.map(function (ph) {
                return generateLocalDbContactId(ph);
            });
            $log.debug('bbb');
            //$log.debug('Entered getDbContacts() - device phone numbers subset :- ');
            //$log.debug(JSON.stringify(options.keys));

            options.include_docs = true;
            //var deferred = $q.defer();
            //We want to perform tasks p1 and p2 only if p0 fails. This is a temporary
            //solution, till we find out why p0 (based on secondary indexes) does not succeed
            //on my ionic platform - Sachin
            var p0 = function () {
                return mbrDb.allDocs(options);

            };
            return p0().then(function (result) {
                if (result && result.rows && result.rows.length > 0) {
                    var finalResult = {
                        rows: result.rows.filter(function (row) {
                            return row.error ? false : true;
                        })
                    };
                    //deferred.resolve(finalResult);
                    $log.debug(JSON.stringify(finalResult));
                    return finalResult;
                } else {
                    //deferred.reject("error in getDbContacts:p0");
                    $log.debug('ccc');
                    throw "error in getDbContacts:p0";
                }
            }).then(null, function (err) {
                $log.debug("error in p0.catch " + err);
                //deferred.reject(err);
                throw err;
            });
            //return deferred.promise;
        };

        obj.getDbContacts = getDbContacts;

        obj.getContactsPaged = function (from, to, cntcts) {
            $log.debug('Entered getContactsPaged');
            var deferred = $q.defer();
            //$log.debug('Calling getDbContacts with ' + from + ' and ' + to);
            var partialContacts = cntcts.slice(from, to);
            $q.when(getDbContacts(partialContacts))
                .then(function (result) {
                    //var count = result.rows.length;
                    //$log.debug('Entered then (resolve)part of getDbContacts');
                    //$log.debug('count is ' + count);

                    var dbMap = {};
                    angular.forEach(result.rows, function (row, i) {
                        dbMap[row.doc._id] = row.doc;
                    });

                    var contacts = partialContacts.map(function (row) {
                        var retval = dbMap[generateLocalDbContactId(row)];
                        if (retval) {
                            retval = angular.extend(row, retval);
                        } else {
                            retval = row;
                        }
                        //$log.debug('From map function - retval is ' + JSON.stringify(retval));
                        return retval;
                    });
                    deferred.resolve(contacts);
                }, function (error) {
                    $log.debug('Entered then (reject) part of getDbContacts. error is ' + error);
                    deferred.reject(error);
                });
            return deferred.promise;
        };

        obj.normalizePhoneNumber = function (num) {
            return num;
        };

        obj.refreshFromAddressBook = function () {
            return refreshFromAddressBook();
        };

        obj.saveContact = function (contact) {
            var mbrDb = DBService.mbrDb();
            return mbrDb.put(contact)
                .then(function (response) {
                    contact._rev = response.rev;
                    $log.debug("saveContact - contact is " + JSON.stringify(contact));
                    angular.extend(contact, response);
                    ChatService.updateSenderIdDataInCache(contact._id, contact);
                    return contact;
                })
                .catch(function (err) {
                    return $q.reject(err);
                });
        };
        var createPouchDbContact = function (addressBookContact, contact) {
            var deferred = $q.defer();
            DBService.initMbrDB(false);
            var mbrDb = DBService.mbrDb();
            var dbContact = extendContactFromServer(addressBookContact, contact);
            mbrDb.put(dbContact)
                .then(function (response) {
                    angular.extend(dbContact, response);
                    ChatService.updateSenderIdDataInCache(dbContact._id, dbContact);
                    deferred.resolve(dbContact);
                })
                .catch(function (err) {
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        var generateLocalDbContactId = function (contact) {
            return ConstsService.USERS_START_KEY + contact.addressBookId + '_' + contact.number;
        };

        var extendContactFromServer = function (addressBookContact, contact) {
            var dbContact = {
                _id: generateLocalDbContactId(addressBookContact),
                addressBookId: addressBookContact.addressBookId,
                _rev: addressBookContact._rev,
                status: contact.customMessage,
                name: addressBookContact.name,
                phoneNumber: contact.mobileNo,
                number: contact.mobileNo,
                topic: contact.p2p ? contact.p2p.split('/').map(function (e, i, self) {
                    return (i === self.length - 1) ? (ConstsService.USERS_START_KEY + e) : e;
                }).join('/') : undefined,
                registered: contact.p2p ? true : false,
                profileName: contact.name,
                customMessage: contact.customMessage
            };
            return dbContact;
        };

        obj.fetchContact = function (id, rev) {
            DBService.initMbrDB(false);
            var mbrDb = DBService.mbrDb();
            return mbrDb.get(id, {
                rev: rev
            });
        };

        var updateScopeContact = function (contact, ctct) {
            $log.debug("updateScopeContact Source contact is " + JSON.stringify(ctct));
            contact.topic = ctct.topic;
            contact._id = ctct._id;
            contact.type = 1;
            contact.registered = ctct.topic ? true : false;
            contact._rev = ctct.rev;
            $log.debug("updateScopeContact Target contact is " + JSON.stringify(contact));
            return $q.when(contact);
        };

        obj.updateScopeContact = updateScopeContact;

        var normalizeContactNumber = function (contact) {
            var countryCode = Tring.getCountryCode(contact.number);
            if (countryCode === null || (countryCode === "nothing")) {
                var device = AuthService.getDevice();
                countryCode = device.countryCode;
            }
            var number = $window.phoneUtils.formatE164(contact.number, countryCode);
            $log.debug("The contactNo being searched: " + number);

            return number;
        };

        obj.getNewContacts = function (cnts) {
            var dbContactsMap = {};
            return getAllDbContacts().then(function (dbContacts) {
                angular.forEach(dbContacts, function (dbContact) {
                    dbContactsMap[dbContact._id] = dbContact;
                });
                return $q.resolve();
            }).then(function () {
                var cntcts = [];
                angular.forEach(cnts, function (cnt) {
                    var dbContact = dbContactsMap[generateLocalDbContactId(cnt)];
                    if (!dbContact || dbContact.name !== cnt.name) {
                        cntcts.push(cnt);
                        if (dbContact) {
                            var name = cnt.name;
                            angular.extend(cnt, dbContact);
                            cnt.name = name;
                        }
                    }

                });
                return cntcts;
            });
        };

        obj.syncNewContactsWithServer = function (contacts) {
            return syncContactsWithServer("usersInfo")(contacts);
        };

        obj.getProfileInfoFromServer = function (contacts) {
            return syncContactsWithServer('profilesInfo')(contacts);
        };

        var addProfileInfoFromServer = function (contact, serverContact) {
            return angular.extend(contact, {
                contentId: serverContact.contentId,
                thumbnailData: serverContact.thumbnailData
            });
        };

        var syncContactsWithServer = function (info) {
            var url;
            var extendFromServer;
            if (info === 'usersInfo') {
                url = ConstsService.CONTACTS_URL;
                extendFromServer = extendContactFromServer;
            } else if (info === 'profilesInfo') {
                url = ConstsService.GET_USER_PROFILES_URL;
                extendFromServer = addProfileInfoFromServer;
            }

            return function (contacts) {
                // Get from server
                var p1 = function () {
                    return $http.post(ConstsService.MEMBERSHIP_SERVER_URL + url, {
                        mobileNos: contacts.map(function (ctct) {
                            return ctct.number;
                        }, {
                            timeout: ConstsService.HTTP_CONTACTS_TIMEOUT
                        })
                    });
                };
                var synchedContacts = [];
                var p2 = function (response) {
                    var infoArray = response.data[info];
                    var serverUsersMap = {};
                    var mbrDb = DBService.mbrDb();
                    var contactsMap = {};
                    angular.forEach(infoArray, function (contact) {
                        serverUsersMap[contact.mobileNo] = contact;
                    });
                    angular.forEach(contacts, function (contact) {
                        var serverContact = serverUsersMap[contact.number];
                        if (!serverContact) {
                            serverContact = {
                                p2p: "",
                                mobileNo: contact.number
                            };
                        }
                        angular.extend(contact, extendFromServer(contact, serverContact));
                        contactsMap[generateLocalDbContactId(contact)] = contact;
                        synchedContacts.push(contact);
                    });
                    return mbrDb.bulkDocs(synchedContacts)
                        .then(function (results) {
                            var savedContacts = results.filter(function (result) {
                                return result.ok;
                            });
                            angular.forEach(savedContacts, function (result) {
                                var dbContact = contactsMap[result.id];
                                dbContact._rev = result.rev;
                            });
                            return savedContacts.map(function (result) {
                                return contactsMap[result.id];
                            });
                        }, function (err) {
                            return $q.reject(err);
                        });
                };
                return p1().then(p2);
            };
        };

        obj.pingMbrSrvrForContact = function (contact) {
            contact.number = normalizeContactNumber(contact);
            var contactIsRegisteredOnServer = false;
            var localDbContactIsRegistered = false;
            var p1 = function () {
                $log.debug("pingMbrSrvrForContact: p1");
                return getDbContacts([contact]);
            };

            var p2 = function (docs) {
                var contactExistsInLocalDb = docs.rows && docs.rows.length > 0 && !docs.rows[0].error;
                localDbContactIsRegistered = contactExistsInLocalDb ? docs.rows[0].doc.registered : false;
                if (localDbContactIsRegistered && !contact.syncId) {
                    $log.debug("The already-registered contact obtained from DB: " + JSON.stringify(docs.rows[0].doc));
                    return $q.when(docs.rows[0].doc);
                } else {
                    var retval;
                    // Get from server
                    var _p1 = function () {
                        $log.debug("pingMbrSrvrForContact: _p1");
                        return $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.CONTACTS_URL, {
                            mobileNos: [contact.number],
                            syncId: contact.syncId //will be defined only when this is called for SyncUp message processing
                        }, {
                            timeout: ConstsService.HTTP_CONTACTS_TIMEOUT
                        });
                    };

                    var _p2 = function (response) {
                        var userArray = response.data.usersInfo;
                        contactIsRegisteredOnServer = userArray.length;
                        if (!contactIsRegisteredOnServer) {
                            userArray = [{
                                p2p: "",
                                mobileNo: contact.number
                        }];
                        }
                        if (!contactExistsInLocalDb) {
                            retval = createPouchDbContact(contact, userArray[0]);
                        } else if (contactIsRegisteredOnServer) {
                            retval = createPouchDbContact(contact, userArray[0]);
                        } else { // contactExistsInLocalDb && !contactIsRegisteredOnServer
                            //throw "not found";
                            retval = $q.reject("not found");
                        }
                        return retval;
                    };

                    var _p3 = function (cntct) {
                        $log.debug("The contact obtained from server: " + JSON.stringify(cntct));
                        return $q.when(cntct);
                    };

                    return $q.serial([_p1, _p2, _p3]);
                }
            };

            var p3 = function (cntct) {
                return updateScopeContact(contact, cntct).then(function (contact) {
                    if (localDbContactIsRegistered || contactIsRegisteredOnServer) {
                        return $q.when(contact);
                    } else {
                        return $q.reject("not found");
                    }
                });
            };

            return $q.serial([p1, p2, p3]);
        };

        $log.debug(JSON.stringify(obj));
        return obj;
            }])
    .factory('focus', function ($timeout, $window) {
        return function (id) {
            // timeout makes sure that it is invoked after any other event has been triggered.
            // e.g. click events that need to run before the focus or
            // inputs elements that are in a disabled state but are enabled when those events
            // are triggered.
            $timeout(function () {
                var element = $window.document.getElementById(id);
                if (element) {
                    element.focus();
                }
            }, 100);
        };
    });
