angular.module('tring.contacts.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller('ContactsController', ['$scope', '$state', 'ContactsService', 'ConstsService',
    '$ionicLoading', 'ChatService', '$timeout', '$http', '$q', 'AuthService', '$window', 'Tring', 'MqttBrokerService', '$cordovaSms', '$ionicPopup', '$cordovaToast', '$rootScope', 'NavigationService', 'SharedDataService','$log',
    function ($scope, $state, ContactsService, ConstsService, $ionicLoading, ChatService, $timeout,
            $http, $q, AuthService, $window, Tring, MqttBrokerService, $cordovaSms, $ionicPopup, $cordovaToast, $rootScope, NavigationService, SharedDataService, $log) {
            $log.debug('ContactsController created');
            //var contacts;

            $scope.getContactsPaged = function (f, l, cnts) {
                return $q.when(cnts.slice(f, l));
            };

            $scope.sendInvite = function (contact) {
                $log.debug("sendInvite called");
                $scope.smsData = {
                    message: "Introducing Tring, the mobile chat app which opens you to a world of unlimited open discoverable groups. Download from Android or iOS app store and register today!"
                };

                // SMS popup
                var smsPopup = $ionicPopup.show({
                    template: '<textArea rows="10" cols="50" ng-model="smsData.message"></textArea>',
                    title: 'Edit Message',
                    subTitle: 'Select OK to send SMS',
                    scope: $scope,
                    buttons: [
                        {
                            text: 'Cancel',
                            onTap: function (e) {
                                $scope.smsData.message = "";
                            }
            },
                        {
                            text: '<b>OK</b>',
                            type: 'button-positive',
                            onTap: function (e) {
                                if (!$scope.smsData.message) {
                                    e.preventDefault();
                                } else {
                                    return $scope.smsData.message;
                                }
                            }
            }]
                });
                smsPopup.then(function (msg) {
                    if (msg) {
                        $cordovaSms.send(contact.number, msg, {})
                            .then(function () {
                                // Success! SMS was sent
                                $log.debug("Success! SMS was sent");
                                $cordovaToast.showLongCenter('Success! SMS was sent').then(function () {
                                    // success
                                }, function () {
                                    // error
                                });
                            }, function (error) {
                                // An error occurred
                                console.error("Error occurred sending SMS. error is " + error);
                                $cordovaToast.showLongCenter('Failure sending SMS !');
                            });
                    }
                });
            };
            var syncNewContactsWithServer = function (cnts) {
                return ContactsService.getNewContacts(cnts).then(ContactsService.syncNewContactsWithServer)
                    .then(function (newContacts) {
                        $log.debug("After ContactsService.syncNewContactsWithServer, new contacts.length is " + newContacts.length);
                        return newContacts;
                    }, function (err) {
                        console.error("Error from ContactsService.syncNewContactsWithServer is " + JSON.stringify(err));
                        $ionicLoading.hide();
                    });
            };
            $scope.refreshContacts = function () {
                var contacts;
                return ContactsService.synchronizerRefresh.run(function () {
                    return ContactsService.synchronizerGeneric.run(function () {
                        $ionicLoading.show();
                        var result = ContactsService.refreshFromAddressBook()
                            .then(function (cnts) {
                                contacts = cnts;
                                return cnts;
                            })
                            .then(syncNewContactsWithServer, function (err) {
                                $ionicLoading.hide();
                                $cordovaToast.showLongCenter("Refresh Contacts failed !");
                                return $q.reject(err);
                            })
                            .then(function () {
                                $ionicLoading.hide();
                                var first = 0;
                                var last = contacts.length;
                                $cordovaToast.showLongCenter("Refresh Contacts done");
                                return ContactsService.getContactsPaged(first, last, contacts);
                            }, function (err) {
                                $ionicLoading.hide();
                                return $q.reject(err);
                            }).then(ContactsService.sort);
                        result.then(function (cts) {
                            var newRegisteredCts = cts.filter(function (ct) {
                                return ct.registered;
                            });
                            return $timeout(function () {
                                return ContactsService.synchronizerGeneric.run(function () {
                                    ContactsService.getProfileInfoFromServer(newRegisteredCts);
                                });
                            }, 0);
                        });
                        return result;
                    });
                });
            };

            $scope.goToMessage = function (contact) {
                $log.debug("Clicked Chat Info: " + JSON.stringify(contact));
                if (contact._id && contact.registered) {
                    var sid = Tring.getSenderId(contact.topic);
                    var localSid = contact._id;
                    //Create chat record if not present in pouchdb
                    var chatId = ConstsService.CHAT_PREFIX + sid;
                    var p1 = function () {
                        return ChatService.findChat(chatId).then(function(chat){
                            if (chat && chat.localSid !== localSid) { 
                               return ChatService.correctLocalSid(chat, localSid);
                            }
                            return chat;
                        });
                    };

                    var p2 = function (chat) {
                        if (chat) {
                            return $q.when(chat);
                        } else {
                            return ChatService.saveSingleUserChat(chatId, sid, localSid);
                        }
                    };
                    $q.serial([p1, p2]).then(function (chat) {
                        var _data = {
                            "senderId": sid,
                            //This is required for getSenderIdData(), since we do a local db query on the Contacts/Group data there,
                            //and the Contacts record will have a local id that is not based on the server id for the Contact. In
                            //the case of Groups where there is no concept of a 'local' Group (unlike Contacts), the localSid and
                            //sid are the same.
                            "localSenderId": localSid
                        };
                        NavigationService.goNativeToView('app.chat-ui', _data, 'right');
                    }).catch(function (err) {
                        $log.debug("Error during goToMessage with senderId: " + sid + " localSenderId: " + localSid + " Error: " + JSON.stringify(err));
                    });

                } else {
                    $ionicLoading.show();
                    ContactsService.synchronizerGeneric.run(function () {
                        ContactsService.pingMbrSrvrForContact(contact).then(function (cntct) {
                                // Subscribe to the special topic of this user
                                MqttBrokerService.subscribeToStatus(cntct.topic);
                                $ionicLoading.hide();
                                $log.debug("Modified contact: " + JSON.stringify(cntct));
                                $scope.goToMessage(cntct);
                            })
                            .catch(function (err) {
                                $log.debug("Error while getting contact from DB/server: " + JSON.stringify(err));
                                $ionicLoading.hide();
                                $cordovaToast.showLongCenter("Contact not a Tring User");
                            });
                    });
                }
            };

            $scope.contactsMap = {};

            $scope.initializeContacts = function () {
                return ContactsService.synchronizerGeneric.run(function () {
                    var deferred = $q.defer();
                    $ionicLoading.show();
                    ContactsService.getContacts().then(function (cntcts) {
                        $ionicLoading.hide();
                        angular.forEach(cntcts, function (ct) {
                            $scope.contactsMap[ct._id] = ct;
                        });
                        deferred.resolve(cntcts);
                        //return ContactsService.getContactsPaged(first, last, cntcts);
                    }, function (err) {
                        $ionicLoading.hide();
                        deferred.reject(err);
                        $q.reject(err);
                    });
                    /*.then(function (cnts) {
                     $log.debug("getContactsPaged is done ! And contacts are " + JSON.stringify(cnts));
                     return cnts;
                     })*/

                    return deferred.promise;
                });
            };
            $scope.$on("$ionicView.enter", function () {
                $rootScope.activeScreen.current = $rootScope.activeScreen.contacts;
            });
    }]);
