angular.module('tring.newGroup.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'tring.locationService', 'tring.navigationService', 'ionic.rating'])
    .controller('NewGroupController', ['$scope', '$stateParams', '$http', 'ConstsService', 'ChatService', 'GroupService', 'ContactsService', 'MqttBrokerService', 'MessagesService', '$ionicPopup', '$ionicLoading', 'Tring', '$q', 'LocationService', 'NavigationService', '$cordovaToast', '$log', '$rootScope', '$state', '$window', 'focus', '$filter', '$timeout', function ($scope, $stateParams, $http, ConstsService, ChatService, GroupService, ContactsService, MqttBrokerService, MessagesService, $ionicPopup, $ionicLoading, Tring, $q, LocationService, NavigationService, $cordovaToast, $log, $rootScope, $state, $window, focus, $filter, $timeout) {

        var onEnter = function () {
            $log.debug("NewGroupController onEnter enter");
            $scope.isCreateNew = $stateParams.isCreateNew;
            $scope.readOnly = $stateParams.readOnly;
            if ($stateParams.group) {
                $scope.group = $stateParams.group;
            }
            $scope.countries = LocationService.getCountries();

            $scope.editGroupDetails = !($scope.isCreateNew || $scope.readOnly);
            $scope.seeGroupDetails = !$scope.isCreateNew && $scope.readOnly && angular.isDefined($scope.group);
            $scope.newGroupCreate = $scope.isCreateNew && !$scope.readOnly && !angular.isDefined($scope.group);

            if ($scope.editGroupDetails) {
                $scope.showBackButton();
            }

            //            if ($scope.newGroupCreate) {
            //                $scope.showBurger();
            //            }

            $log.debug('NewGroupController' + $scope.editGroupDetails + ' ' + $scope.seeGroupDetails + " " + $scope.newGroupCreate);
            if (!$scope.groupInput || !$scope.newGroupCreate) {
                $scope.groupInput = {
                    groupInfo: {
                        name: "",
                        categories: [],
                        rules: "",
                        description: "",
                        o2m: true,
                        closed: false,
                        //subgroupsVisibleToOwner: false,
                        country: '0',
                        state: "",
                        city: "",
                        contentId: "",
                        thumbnailData: "",
                        ratingsAndReviews: {},
                        meanRating: null,
                        ratingCount: 0
                    },
                    members: [],
                    ownerHandle: "",
                    category: "",
                    invitee: {
                        number: "",
                        type: ConstsService.MEMBER_ROLE
                    },
                    nameAvailable: null,
                    rating: null,
                    review: ""
                };
            }
            $scope.pageSize = ConstsService.REVIEWS_PAGE_SIZE;
            $scope.allReviewsLoaded = false;

            var broadcastPendingCaptureResults = function () {
                var pendingCaptureResults = $stateParams.pendingCaptureResults;
                if (pendingCaptureResults) {
                    $scope.$broadcast("processCapturedResult", pendingCaptureResults);
                }
                return true;
            };

            if ($scope.editGroupDetails || $scope.seeGroupDetails) {
                //group info from server
                $scope.groupsFromServerPromiseForUnitTests =
                    GroupService.getServerGroup($scope.group.name)
                    .then(function (grp) {
                        $log.debug("101010");
                        var id = $scope.group._id;
                        var topic = $scope.group.topic;
                        angular.extend($scope.groupInput.groupInfo, grp, {
                            _id: id,
                            topic: topic
                        });
                        delete $scope.groupInput.groupInfo.topicPrefix;
                        $log.debug("onEnter NewGroupController edit mode . Group is " + JSON.stringify($scope.groupInput.groupInfo));
                        cloneGroupInfoIntoGroupEdit();
                        //Update the $scope.group with fresh info from server, for nested controllers
                        angular.extend($scope.group, {
                            contentId: $scope.groupInput.groupInfo.contentId
                        });
                        $scope.$broadcast("updateGroup", $scope.group);

                        return true;
                    }).then(null, function (err) {
                        $log.debug("error in editGroupDetails initialization - err is " + JSON.stringify(err));
                        $cordovaToast.showLongCenter("Error getting group details from server. Please check connection.");
                        throw err;
                    }).then(broadcastPendingCaptureResults);

                $scope.groupReviewsAndRatingsPromiseForUnitTests =
                    GroupService.getGroupReviewsAndRatings($scope.group).then(function (data) {
                        var groupInfo = $scope.groupInput.groupInfo;
                        var reviews = data.reviews;
                        angular.forEach(reviews, function (ratingNreview) {
                            groupInfo.ratingsAndReviews[ratingNreview.handleName] = {
                                rating: ratingNreview.rating,
                                review: ratingNreview.review
                            };
                        });
                        var meanRating = data.meanRating;
                        var ratingCount = data.ratingCount;
                        var pageOffset = data.pageOffset;
                        var lastPageOffset = $scope.pageOffset ? $scope.pageOffset : 0;
                        if ((pageOffset - lastPageOffset) < $scope.pageSize) {
                            $scope.allReviewsLoaded = true;
                        }
                        $scope.pageOffset = pageOffset;
                        groupInfo.meanRating = meanRating;
                        groupInfo.ratingCount = ratingCount;
                        $log.debug("ratingsAndReviews are " + JSON.stringify($scope.groupInput.groupInfo.ratingsAndReviews));
                    }).then(null, function (err) {
                        //$cordovaToast.showLongCenter("Error getting next chunk of reviews from server");
                    });
            } else {
                //For Create New Group
                $scope.$watch('groupInput.groupInfo.name', function (val) {
                    checkAvailability();
                    $scope.groupInput.groupInfo.name = $filter('lowercase')(val);
                }, true);
                $scope.$watch('groupInput.ownerHandle', function (val) {
                    $scope.groupInput.ownerHandle = $filter('lowercase')(val);
                }, true);
                $timeout(broadcastPendingCaptureResults, 100);
            }
        };

        $scope.goBackAStep = function () {
            $log.debug('clicked');
            //$ionicHistory.goBack();
            NavigationService.goNativeBack();
        };

        var cloneGroupInfoIntoGroupEdit = function () {
            //this is required only while editing a group
            $scope.groupEdited = angular.copy($scope.groupInput);
            delete $scope.groupEdited.groupInfo.ratingsAndReviews;
            delete $scope.groupEdited.groupInfo.ratingCount;
            delete $scope.groupEdited.groupInfo.meanRating;
        };

        $scope.$on('$ionicView.enter', onEnter);

        $scope.getNextGroupReviewsAndRatings = function () {
            GroupService.getNextGroupReviewsAndRatings($scope.group, $scope.pageOffset).then(function (data) {
                var groupInfo = $scope.groupInput.groupInfo;
                var reviews = data.reviews;
                var pageOffset = data.pageOffset;
                if ((pageOffset - $scope.pageOffset) < $scope.pageSize) {
                    $scope.allReviewsLoaded = true;
                }
                $scope.pageOffset = pageOffset;
                angular.forEach(reviews, function (ratingNreview) {
                    groupInfo.ratingsAndReviews[ratingNreview.handleName] = {
                        rating: ratingNreview.rating,
                        review: ratingNreview.review
                    };
                });
            }).then(null, function (err) {
                $cordovaToast.showLongCenter("Error getting next chunk of reviews from server");
            });
        };

        //Do a diff to find what was edited
        var keepOnlyChangedFields = function () {
            var x;
            //We dont cater to arrays here since we dont allow editing array like categories yet
            for (x in $scope.groupInput.groupInfo) {
                if (angular.isDefined($scope.groupEdited.groupInfo[x]) && ($scope.groupEdited.groupInfo[x] === $scope.groupInput.groupInfo[x] || angular.isArray($scope.groupInput.groupInfo[x]))) {
                    delete $scope.groupEdited.groupInfo[x];
                } else {
                    $scope.groupEdited.groupInfo[x] = angular.copy($scope.groupInput.groupInfo[x]);
                }
            }
            delete $scope.groupEdited.groupInfo.ratingsAndReviews;
            delete $scope.groupEdited.groupInfo.ratingCount;
            delete $scope.groupEdited.groupInfo.meanRating;
            delete $scope.groupEdited.groupInfo.created;
            delete $scope.groupEdited.groupInfo.modified;
        };

        //Used by editGroup template
        $scope.doneEditHandler = function () {
            keepOnlyChangedFields();
            $log.debug("groupEdited.groupInfo is" + JSON.stringify($scope.groupEdited.groupInfo));
            if (Object.keys($scope.groupEdited.groupInfo).length > 0) {
                return GroupService.updateGroupInServer($scope.group._id, $scope.group.name, $scope.groupEdited.groupInfo).then(function (grp) {
                    //GroupService.storeGroup(grp);
                    $cordovaToast.showLongCenter("Successfully edited group details");
                    cloneGroupInfoIntoGroupEdit();
                }).then(null, function (err) {
                    console.error("Error editing group details-err is " + JSON.stringify(err));
                    $cordovaToast.showLongCenter("Error editing group details");
                });
            } else {
                cloneGroupInfoIntoGroupEdit();
                return $q.resolve();
            }
        };

        //        $scope.handleEditField = function (field) {
        //            $scope.groupEdited.groupInfo[field] = angular.copy($scope.groupInput.groupInfo[field]);
        //        };

        $scope.leave = function (group) {
            //Disallow if owner or moderator or admin of the group
            $log.debug('entering leave()');
            return $q.when(GroupService.getMyGroupAndSubgroupsFor(group)).then(function (groupArray) {
                var roleValues = groupArray.map(function (grp) {
                    return grp.membershipType;
                });
                if (ConstsService.OWNER_ROLE in roleValues || ConstsService.MODERATOR_ROLE in roleValues ||
                    ConstsService.ADMIN_ROLE in roleValues) {
                    $cordovaToast.showLongCenter("Error: Admins, Owners and Moderators can't leave a group");
                    $log.debug("Error: Admins, Owners and Moderators can't leave a Group");
                    return $q.reject(false);
                } else {
                    //confirm first
                    return $ionicPopup.confirm({
                        template: 'Are you sure you want to leave the group ' + group.name + ' ?'
                    }).then(function (res) {
                        if (res) {
                            $ionicLoading.show();
                            return GroupService.leaveGroup(group).then(function (success) {
                                $ionicLoading.hide();
                                if (success) {
                                    //finally delete from local db
                                    return GroupService.deleteGroupsFromLocalDb(groupArray)
                                        .then(function () {
                                            $cordovaToast.showShortCenter("Success leaving group " + group.name);
                                            return groupArray;
                                        })
                                        .then(ChatService.deleteGroupChats);
                                } else {
                                    $cordovaToast.showLongCenter("Error: server error leaving group");
                                    return $q.reject(false);
                                }
                            }).then(null, function (err) {
                                $ionicLoading.hide();
                                $cordovaToast.showLongCenter("Error: server error leaving a group ");
                                console.error("Error: server error leaving a group - err is " + JSON.stringify(err));
                                return $q.reject(false);
                            });
                        } else {
                            $log.debug("Cancelled leave group");
                            return $q.reject(false);
                        }
                    });
                }
            });
        };

        $scope.go = function (path, params) {
            NavigationService.goNativeToView(path, params, 'left');
            //$log.debug($ionicHistory.viewHistory());
        };

        $scope.$on("groupProfilePictureSet", function (evt, profile) {
            $scope.groupInput.groupInfo.contentId = profile.contentId;
            $scope.groupInput.groupInfo.thumbnailData = profile.profileThumbnailData;
        });

        var convertToCanonicalCategory = function (tag) {
            if (tag) {
                tag = tag.replace(/(^|\s)[a-z]/g, function (f) {
                    return f.toUpperCase();
                });
                $log.debug("canonical category is " + tag);
            }
            return tag;
        };

        $scope.addCategory = function () {
            var tag = convertToCanonicalCategory($scope.groupInput.category);
            if (tag &&
                $scope.groupInput.groupInfo.categories.indexOf(tag) < 0) {
                $scope.groupInput.groupInfo.
                categories.push(tag);
                $scope.groupInput.category = "";
                $cordovaToast.showShortCenter("Added category '" + tag + "'");
            } else {
                $cordovaToast.showShortCenter("Failed to add category '" + tag + "'");
            }
        };
        $scope.addInvitee = function (invitee) {
            var member = invitee;
            if (member.number &&
                $scope.groupInput.members.map(function (mem) {
                    return mem.number;
                }).indexOf(member.number) < 0) {
                $scope.groupInput.
                members.push({
                    addressBookId: member.addressBookId,
                    name: member.name,
                    number: member.number,
                    type: ConstsService.MEMBER_ROLE
                });
            }
        };


        $scope.createNewGroup = function () {
            $log.debug("From createGroup - " + JSON.stringify($scope.groupInput));
            //Error Processing
            var errorMsgs = createErrorProcessing();
            if (errorMsgs.length) {
                $scope.data = {};
                $scope.data.errorMsgs = errorMsgs;
                $ionicPopup.show({
                    scope: $scope,
                    title: "Error in Create New Group",
                    template: '<div class="row" ng-repeat="msg in data.errorMsgs">{{msg}}</div>',
                    buttons: [{
                        text: 'OK',
                        type: 'button-positive',
                        onTap: function (e) {
                            // Returning a value will cause the promise to resolve with the given value.
                            return $scope.data.errorMsgs;
                        }
                }]
                });
                return;
            }
            $ionicLoading.show();
            var group = {};
            angular.copy($scope.groupInput.groupInfo, group);
            //For now we don't store rating info with group, but with handles
            delete group.ratingsAndReviews;
            delete group.ratingCount;
            delete group.meanRating;
            group.country = LocationService.getCountry(group.country);
            group.state = (group.state === 'Select a state') ? '' : group.state;
            group.thumbnailData = group.thumbnailData === "//:0" ? "" : group.thumbnailData;
            $log.debug("Country is " + group.country);

            group.categories = group.categories.map(function (cat) {
                return cat.text;
            });
            return GroupService.createGroup(group, $scope.groupInput.ownerHandle)
                .then(function (groupResp) {
                    //Create local database for Group
                    var groupDoc = group;
                    angular.extend(groupDoc, {
                        ownerHandle: $scope.groupInput.ownerHandle,
                        //_id: group.gid,
                        handleName: $scope.groupInput.ownerHandle
                    });
                    GroupService.extendGroupFromServer(groupDoc, groupResp);

                    return GroupService.saveGroup(groupDoc).then(function (group) {
                            //$log.debug('After saveGroup, group is ' + JSON.stringify(group));
                            //subscribe to the right topic
                            $log.debug('222');
                            try {
                                MqttBrokerService.subscribeTo(group.topic);
                                MqttBrokerService.subscribeToStatus(group.topic);
                                MqttBrokerService.subscribeTo(group.topic + ConstsService.PVT_SUB_TOPIC);
                                $log.debug('333');
                            } catch (err) {
                                console.error("Error subscribing to mqtt topics - Err is " + JSON.stringify(err));
                            }
                            MqttBrokerService.addToUserGroupHandle(group._id, group.handleName);
                            //Send invite messages
                            $log.debug('444');

                            $ionicLoading.hide();
                            askForInvitingMembers(group.name).then(function (res) {
                                if (res) {
                                    NavigationService.goNativeToView('app.groups.inviteContacts', {
                                        'group': group,
                                        'inviterIsOwnerOrAdmin': true
                                    });
                                } else {
                                    return $q.reject("invite canceled");
                                }
                                $log.debug(res + 'File upload confirm exited');
                            });
                            $cordovaToast.showLongCenter('New group created !');
                            var chatId = ConstsService.CHAT_PREFIX + group._id;
                            $log.debug('888');

                            return ChatService.saveGroupChat(group, chatId);
                            //return result;
                        }, function (err) {
                            console.error("saveGroup error " + JSON.stringify(err));
                            //return $q.reject(err);
                            throw err;
                        })
                        .then($scope.clearNewGroup);
                })
                .then(function (status) {
                        $log.debug("createGroup success " + status);
                        return status;
                    },
                    function (err) {
                        $log.debug('eee');
                        $ionicLoading.hide();
                        $ionicPopup.alert({
                                title: 'Group creation',
                                template: 'Server Error !'
                            })
                            .then($scope.clearNewGroup);
                        console.error("createNewGroup err " + err);
                        throw err;
                    });
        };

        var createErrorProcessing = function () {
            var errorMsgs = [];
            if (!$scope.groupInput.nameAvailable && $scope.groupInput.groupInfo.name) {
                errorMsgs.push("There is already a group with Id " + $scope.groupInput.groupInfo.name + ".");
            }
            if (!$scope.groupInput.groupInfo.name) {
                errorMsgs.push("Group Id is a required field and should not have invalid characters.");
            }
            if (!$scope.groupInput.ownerHandle) {
                errorMsgs.push("My Handle is a required field and should not have invalid characters.");
            }
            if (!$scope.groupInput.groupInfo.categories.length) {
                errorMsgs.push("At least one Category is required.");
            } else if ($scope.groupInput.groupInfo.categories.length > 3) {
                errorMsgs.push("At most three Categories are allowed.");
            }
            return errorMsgs;
        };

        var askForInvitingMembers = function (groupName) {
            $scope.inviteData = {
                groupName: groupName
            };
            return $ionicPopup.show({
                cssClass: 'customPopup',
                templateUrl: 'templates/rubyonic/groupCreateComfirmPopup.html',
                title: '',
                subTitle: '',
                scope: $scope,
                buttons: [
                    {
                        text: '<b>Invite members</b>',
                        type: 'button button-energized button-block',
                        onTap: function (e) {
                            // e.preventDefault() will stop the popup from closing when tapped.
                            //e.preventDefault();
                            return true;
                        }
                    },
                    {
                        text: 'Cancel',
                        type: 'button button-light button-block',
                        onTap: function (e) {
                            // e.preventDefault() will stop the popup from closing when tapped.
                            //e.preventDefault();
                            return false;
                        }
                    }
                ]
            });
        };

        $scope.getCountries = function () {
            return LocationService.getCountries();
        };

        $scope.getStates = function (countryCode) {
            return LocationService.getStates(countryCode);
        };

        var checkAvailability = Tring.debounce(function () {
            if (!$scope.groupInput.groupInfo.name) {
                $scope.groupInput.nameAvailable = false;
                return;
            }
            var url = ConstsService.GROUP_IS_AVAILABLE_URL.replace('@0', $scope.groupInput.groupInfo.name);
            $http.get(ConstsService.MEMBERSHIP_SERVER_URL + url).then(function (result) {
                if (result && result.data && result.data.success) {
                    $scope.groupInput.nameAvailable = true;
                } else {
                    $scope.groupInput.nameAvailable = false;
                }
            });
        }, 2000).callback;

        $scope.resetCheckAvailability = function () {
            $scope.groupInput.nameAvailable = null;
        };

        $scope.clearNewGroup = function (result) {
            $log.debug("cleargroup - arg is " + JSON.stringify(result));
            var info = $scope.groupInput.groupInfo;
            info.name = info.rules = info.description =
                info.state = info.contentId = "";
            info.country = '0';
            info.city = "";
            info.thumbnailData = "//:0";
            info.o2m = true;
            info.closed = false;
            info.categories = [];
            $scope.groupInput.members = [];
            $scope.groupInput.ownerHandle = "";
            $scope.groupInput.category = "";
            $scope.groupInput.invitee = {};
            $scope.groupInput.nameAvailable = null;
            $log.debug('clearNewGroup done');
            return true;
        };

        $scope.clearEditGroup = function () {
            var info = $scope.groupInput.groupInfo;
            info.rules = info.description =
                info.state = info.contentId = "";
            info.country = '0';
            info.thumbnailData = "";
            info.closed /*= info.subgroupsVisibleToOwner*/ = false;
            info.categories = [];
            $log.debug('clearEditGroup done');
            return true;
        };

        $scope.onTagAddingHandler = function (tagBeingAdded) {
            if ($scope.groupInput.groupInfo.categories && $scope.groupInput.groupInfo.categories.length === 3) {
                return false;
            }
            return true;
        };

        $scope.onTagAddedHandler = function (tagBeingAdded) {
            tagBeingAdded = convertToCanonicalCategory(tagBeingAdded.text);
            $scope.groupInput.groupInfo.categories[$scope.groupInput.groupInfo.categories.length - 1].text = tagBeingAdded;
        };

        $scope.clearReadGroupRatings = function () {
            var info = $scope.groupEdited.groupInfo;
            info.rating = null;
            info.review = "";
        };
        $scope.handleClickCategories = function () {
            focus("categoriesInputId");
        }
        $scope.addReviewToExisting = function (ratingAndReview) {
            var rating = ratingAndReview.rating;
            var review = ratingAndReview.review;
            var revMap = $scope.groupInput.groupInfo.ratingsAndReviews;
            var mean = $scope.groupInput.groupInfo.meanRating;
            var ratingCount = $scope.groupInput.groupInfo.ratingCount;
            var newRatingCount = ratingCount;
            var overWritingExisting = angular.isDefined(revMap[$scope.group.handleName]);
            var ratingGiven = angular.isDefined(rating);

            var oldRatingExists = overWritingExisting && angular.isDefined(revMap[$scope.group.handleName].rating);

            var oldRating = oldRatingExists ? revMap[$scope.group.handleName].rating : 0;
            var newRating = ratingGiven ? rating : 0;


            revMap[$scope.group.handleName] = {
                rating: rating,
                review: review
            };
            if (!mean) {
                mean = 0.0;
            }
            if (overWritingExisting) {
                if (oldRatingExists) {
                    if (!ratingGiven) {
                        newRatingCount = ratingCount - 1;
                    }
                } else {
                    if (ratingGiven) {
                        newRatingCount = ratingCount + 1;
                    }
                }
            } else {
                if (ratingGiven) {
                    newRatingCount = ratingCount + 1;
                }
            }
            if (newRatingCount > 0) {
                $scope.groupInput.groupInfo.meanRating = (mean * ratingCount + newRating - oldRating) / newRatingCount;
            } else {
                $scope.groupInput.groupInfo.meanRating = rating;
            }
            $scope.groupInput.groupInfo.ratingCount = newRatingCount;
        };

        $scope.pushRatingAndReview = function () {
            var rating = $scope.groupInput.rating;
            var review = $scope.groupInput.review;
            $log.debug("rating and review " + $scope.groupInput.rating + " " + $scope.groupInput.review);

            if (!rating && !review) {
                $log.debug("Empty rating and review");
                return;
            }
            var ratingAndReview = {
                rating: rating,
                review: review
            };

            GroupService.pushRatingAndReview(ratingAndReview, $scope.group)
                .then(function () {
                    return ratingAndReview;
                })
                .then($scope.addReviewToExisting).then(null, function (err) {
                    throw err;
                });
        };

            }]);