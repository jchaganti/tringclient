angular.module('tring.group.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'ionic.rating'])
    .controller('GroupController', ['$rootScope' ,'$scope', 'GroupService', '$q', '$ionicLoading', 'Tring', 'NavigationService', 'ConstsService', 'ChatService', '$ionicPopup', '$cordovaToast', 'MqttBrokerService', '$log',
    function ($rootScope, $scope, GrpService, $q, $ionicLoading, Tring, NavigationService, ConstsService, ChatService, $ionicPopup, $cordovaToast, MqttBrokerService, $log) {
            //$scope.groups=[{name:"abc"},{name: "ert"}];
            $scope.groups = [];
            $scope.ownedGroups = [];
            $scope.joinedGroups = [];

            var PAGE_SIZE = 10;
            $scope.firstGroupIndex = 0;
            $scope.lastGroupIndex = PAGE_SIZE;
            $scope.Tring = Tring;

            $scope.goTo = function (state, params) {
                $log.debug("Going to state " + state + " with params " + JSON.stringify(params));
                NavigationService.goNativeToView(state, params);
            };


            $scope.$on('$ionicView.enter', function () {
              $rootScope.hideTabs = 'tabs-item-hide';

            });


            var initGroups = function () {
                $log.debug("GroupController beforeEnter");
                //$scope.showBurger();
                $scope.groups = [];
                $scope.ownedGroups = [];
                $scope.joinedGroups = [];
                $scope.firstGroupIndex = 0;
                $scope.lastGroupIndex = PAGE_SIZE;
                $scope.allRowsLoaded = false;
                $scope.noMemberShips = false;
                $scope.requestTimedOut = false;
                $ionicLoading.show();
                var timeoutErrorFunc = function (err) {
                    if (err && err.type === "timedOut") {
                        $cordovaToast.showLongCenter("Request to server timed out.");
                        $scope.requestTimedOut = true;
                        $ionicLoading.hide();
                    } else {
                        $scope.noOwnedMemberShips = GrpService.noOwnedGroups();
                        $scope.noJoinedMemberShips = GrpService.noJoinedGroups();
                    }
                };
                GrpService.getGroups()
                    .then(unsubscribeNonActiveGroups)
                    .then(filterInOnlyActiveGroups)
                    .then(GrpService.partitionGroups)
                    .then(GrpService.setGroups)
                    .then(function () {
                        //Add the first page of records
                        //$scope.addOwnedGroups();
                        $scope.ownedGroups = GrpService.getOwnedGroups();
                        $ionicLoading.hide();
                        $scope.noOwnedMemberShips = GrpService.noOwnedGroups();
                    })
                    .then(function () {
                        //Add the first page of records
                        //$scope.addJoinedGroups();
                        $scope.joinedGroups = GrpService.getJoinedGroups();
                        $ionicLoading.hide();
                        $scope.noJoinedMemberShips = GrpService.noJoinedGroups();
                    })
                    .then(null, timeoutErrorFunc);
            };

            var unsubscribeNonActiveGroups = function (groups) {
                angular.forEach(groups, function (grp) {
                    if (grp.userGroupStatus !== ConstsService.USER_GROUP_STATUS_ACTIVE) {
                        try {
                            MqttBrokerService.unsubscribeTo(grp.topic);
                        } catch (err) {
                            console.error(JSON.stringify(err));
                        }
                    }
                });
                return groups;
            };
            var filterInOnlyActiveGroups = function (groups) {
                return groups.filter(function (grp) {
                    return grp.userGroupStatus === ConstsService.USER_GROUP_STATUS_ACTIVE;
                });
            };
            $scope.$on('$ionicView.beforeEnter', initGroups);

            $scope.addGroups = function (noInfiniteScrollBroadcast) {
                var doInfiniteScrollBroadcast = !noInfiniteScrollBroadcast;

                $log.debug('addGroups() called. $scope.firstGroupIndex is ' + $scope.firstGroupIndex);
                $log.debug('$scope.lastGroupIndex is ' + $scope.lastGroupIndex);
                if (!$scope.allRowsLoaded) {
                    $q.when(GrpService.getGroupsPaged($scope.firstGroupIndex,
                            $scope.lastGroupIndex))
                        .then(function (grps) {
                            $log.debug('then part of getGroupsPaged');
                            $log.debug(JSON.stringify(grps));
                            if (grps.length === PAGE_SIZE) {
                                $scope.lastGroupIndex =
                                    $scope.lastGroupIndex + PAGE_SIZE;
                            }
                            $scope.firstGroupIndex =
                                $scope.firstGroupIndex + grps.length;
                            if (grps.length <= 0) {
                                $scope.allRowsLoaded = true;
                            }
                            Array.prototype.push.apply($scope.groups, grps);
                            if (doInfiniteScrollBroadcast) {
                                $scope.$broadcast('scroll.infiniteScrollComplete');
                            }
                        }, function () {
                            console.warn('All groups shown. Cant scroll more');
                            if (doInfiniteScrollBroadcast) {
                                $scope.$broadcast('scroll.infiniteScrollComplete');
                            }
                            $scope.allRowsLoaded = true;
                        });
                } else {
                    if (doInfiniteScrollBroadcast) {
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                    }
                }
            };

            $scope.jumpToInviteMembers = function (grp) {
                var isOwnerOrAdmin = grp.membershipType === ConstsService.OWNER_ROLE ||
                    grp.membershipType === ConstsService.ADMIN_ROLE;
                $scope.goTo('app.groups.inviteNonMemberContactsForMembership', {
                    'group': grp,
                    inviterIsOwnerOrAdmin: isOwnerOrAdmin
                });
            };

            angular.extend($scope, function () {
                var groupShownMap = {};
                var groupRatingsAndReviewsMap = {};
                return {
                    toggleGroup: function (grp) {
                        if (!groupShownMap[grp._id]) {
                            groupShownMap[grp._id] = true;
                        } else {
                            groupShownMap[grp._id] = false;
                        }
                        if (!groupRatingsAndReviewsMap[grp._id]) {
                            groupRatingsAndReviewsMap[grp._id] = {
                                groupRating: null,
                                groupReview: ""
                            };
                        }
                    },
                    isGroupShown: function (grp) {
                        return groupShownMap[grp._id];
                    },
                    groupRatingsAndReviewsMap: groupRatingsAndReviewsMap
                };
            }());

            $scope.pushRatingAndReview = function (group) {
                var rating = $scope.groupRatingsAndReviewsMap[group._id].groupRating;
                var review = $scope.groupRatingsAndReviewsMap[group._id].groupReview;
                $log.debug("rating and review " + $scope.groupRatingsAndReviewsMap[group._id].groupRating + " " + $scope.groupRatingsAndReviewsMap[group._id].groupReview);

                if (!rating && !review) {
                    $log.debug("Empty rating and review");
                    return;
                }
                var ratingAndReview = {
                    rating: rating,
                    review: review
                };

                GrpService.pushRatingAndReview(ratingAndReview, group)
                    .then(function () {
                        return ratingAndReview;
                    })
                    .then(null, function (err) {
                        throw err;
                    });
            };

            $scope.isOwnerOrAdmin = function (grp) {
                return grp.membershipType === ConstsService.OWNER_ROLE || grp.membershipType === ConstsService.ADMIN_ROLE;
            };

            $scope.manageGroup = function(grp) {
                $scope.goTo("app.members", {group: grp});
            };

            $scope.leave = function (group) {
                //Disallow if owner or moderator or admin of the group
                $log.debug('entering leave()');
                return $q.when(GrpService.getMyGroupAndSubgroupsFor(group)).then(function (groupArray) {
                    var roleValues = groupArray.map(function (grp) {
                        return grp.membershipType;
                    });
                    if (roleValues.indexOf(ConstsService.OWNER_ROLE) >= 0 || roleValues.indexOf(ConstsService.MODERATOR_ROLE) >= 0 ||
                        roleValues.indexOf(ConstsService.ADMIN_ROLE) >= 0) {
                        $cordovaToast.showLongCenter("Error: Admins, Owners and Moderators can't leave a Group");
                        $log.debug("Error: Admins, Owners and Moderators can't leave a Group");
                        return $q.reject(false);
                    } else {
                        //confirm first
                        return $ionicPopup.confirm({
                            template: 'Are you sure you want to leave the group ' + group.name + ' ?'
                        }).then(function (res) {
                            if (res) {
                                $ionicLoading.show();
                                return GrpService.leaveGroup(group).then(function (success) {
                                    $ionicLoading.hide();
                                    if (success) {
                                        //finally delete from local db
                                        return GrpService.setStatusForGroupsFromLocalDb(groupArray, ConstsService.USER_GROUP_STATUS_LEFT)
                                            .then(function () {
                                                $cordovaToast.showShortCenter("Success leaving Group " + group.name);
                                                return groupArray;
                                            }).then(function (groupArray) {
                                                angular.forEach(groupArray, function (grp) {
                                                    ChatService.removeSenderIdDataInCache(grp._id);
                                                    MqttBrokerService.unsubscribeTo(grp.topic);
                                                });
                                                return true;
                                            })
                                            .then(function () {
                                                var data = "User with handle '" + group.handleName + "' left group";
                                                MqttBrokerService.sendMessage(ConstsService.MESSAGE_TYPE_APP, ConstsService.APP_MSG_TYPE_LEAVE_GROUP, null, group._id,
                                                    function () {
                                                        return group.topic + ConstsService.PVT_SUB_TOPIC;
                                                    }, data, null, group.handleName, null, {
                                                        handleName: group.handleName
                                                    });
                                            });
                                            //.then(initGroups);
                                        //.then(ChatService.deleteGroupChats);
                                    } else {
                                        $cordovaToast.showLongCenter("Error: server error leaving Group");
                                        return $q.reject(false);
                                    }
                                }).then(null, function (err) {
                                    $ionicLoading.hide();
                                    $cordovaToast.showLongCenter("Error: server error leaving a Group ");
                                    console.error("Error: server error leaving a Group - err is " + JSON.stringify(err));
                                    return $q.reject(false);
                                });
                            } else {
                                $log.debug("Cancelled leave group");
                                return $q.reject(false);
                            }
                        });
                    }
                });
            };

                }]);
