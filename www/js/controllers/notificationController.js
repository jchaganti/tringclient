angular.module('tring.notification.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.directives', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller('NotificationController', ['$scope', '$ionicHistory', '$q', 'ConstsService', '$cordovaCapture', '$cordovaCamera', '$log', 'NetworkService', '$window', 'DBService', function ($scope, $ionicHistory, $q, SharedDataService, ConstsService, $cordovaCapture, ThumbnailService, $ionicPopup, FileBrowseService, $cordovaCamera, $log, NetworkService, $window, DBService) {
     		
     	$scope.showNotification = false;
      	$scope.fromSelfProfileScreen = $scope.inProfileScreen && !$scope.readOnly;
      	if ($scope.fromSelfProfileScreen) {
        	$scope.showNotification = true;
      	}
     	   
     	$scope.notification = {
     		radio: {
     			individual: true,
     			group: false,
     			admin: true
     		}
     	};

     	$scope.notification.change = function() {
     		console.log('notification radio changed: ' + $scope.notification.radio);
     	}
    }]);