angular.module('tring.profile.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller('ProfileController', ['$scope', '$cordovaImagePicker', 'AuthService', '$q', '$cordovaActionSheet', '$cordovaCapture', 'UserService', '$ionicLoading', '$http', 'Tring', '$ionicPopup', 'ThumbnailService', '$jrCrop', 'FileBrowseService', 'ConstsService', 'NavigationService', '$rootScope', '$stateParams', '$cordovaToast', '$timeout', '$log', 'NetworkService', '$state', 'DBService', function ($scope, $cordovaImagePicker, AuthService, $q, $cordovaActionSheet, $cordovaCapture, UserService, $ionicLoading, $http, Tring, $ionicPopup, ThumbnailService, $jrCrop, FileBrowseService, ConstsService, NavigationService, $rootScope, $stateParams, $cordovaToast, $timeout, $log, NetworkService, $state, DBService) {
        //This controller is used either for defining a profile picture (readOnly=falsey, group={}, contact=falsey) for any general purpose
        //or for defining the device user's contact profile picture and status info(readOnly=falsey, group=falsey, contact=falsey) and seeing other Profile and Settings for the device user
        //or for defining the profile picture (only) for a group (readOnly=falsey, group=<some group>, contact=falsey)
        //or for viewing the profile of a contact (readOnly=true, group=falsey, contact=<some contact>)
        //or for viewing the profile of a group (readOnly=true, group=<some group>, contact=falsey)

        $scope.inProfileScreen = true;
        $scope.profile = {};
        $scope.newProfile = {};

        $scope.readOnly = $stateParams.readOnly;
        $scope.contact = $stateParams.contact;
        $scope.group = $stateParams.group;

        $scope.contactTring = {
            yourMessage: ""
        };

        $scope.contentMessages = {
            cancelDownloadArray: {},
            fileTransferProgress: {}
        };

        var initProfileInfo = function (profile) {
            //profile is the profile of this device user
            $scope.newProfile = {};
            $scope.profile = {};
            if (!profile) {
                $log.debug("This user's profile is null or empty");
            }
            if (!$scope.readOnly) {
                //This is the Profile edit case.
                //It applies only to editing of the device User's own profile
                //Editing Group profiles is largely handled by NewGroupController
                //(which should be renamed GroupCRUDController). Only the Profile
                //picture selection and cropping features of this controller
                //are used there.
                if (!$scope.group && profile) {
                    $scope.profile = angular.copy(profile);
                    if (!('paypalId' in profile)) {
                      angular.extend($scope.profile,{paypalId:""})
                    }
                    $scope.newProfile = angular.copy(profile);
                }
                var clearPendingCaptureResults = function () {
                    $state.params.pendingCaptureResults = null;
                };
                if ($state.params.pendingCaptureResults) {
                    processProfilePicture($state.params.pendingCaptureResults[0].fullPath)
                        .then(clearPendingCaptureResults, clearPendingCaptureResults)
                        .then($scope.doneHandler);
                }
            } else {
                //This is the readOnly case for either a Contact or a Group.
                //Just ignore the passed in profile, which is that of the device user
                //Here there is no need for $scope.newProfile since you can't change anything
                var container = $scope.contact ? $scope.contact : $scope.group;
                var tmpProfile = {
                    profileUri: container.localURL,
                    profileThumbnailData: container.thumbnailData,
                    name: container.profileName,
                    statusMessage: container.customMessage
                };
                angular.extend($scope.profile, tmpProfile);
            }
        };

        var onEnter = function () {
            UserService.userProfile().then(initProfileInfo).then(null, function (err) {
                initProfileInfo(null);
            });
        };

        $scope.$on('$ionicView.enter', onEnter);

        //When this controller is used as nested in say a NewGroupController, then
        // when the $scope.group there changes because of info brought from the server
        //, this is not automatically available to this controller's scope, since it still
        //has the initial copy of $scope.group. Hence this event is thrown in the parent controller
        //and handled here to update $scope.group.
        $scope.$on("updateGroup", function (evt, grp) {
            if ($scope.group) {
                angular.extend($scope.group, grp);
            }
        });

        $scope.downloadAndOpenProfileImage = function () {
            var cntnr = {};
            var container = $scope.contact ? $scope.contact : $scope.group;
            angular.copy(container, cntnr);
            cntnr.contentType = container.contentType ? container.contentType : 'image/jpeg';
            $scope.$broadcast("downloadAndOpenProfileContent", cntnr);
        };

        $scope.getPhotoUri = function () {
            var options = {
                title: 'Get Profile Picture',
                buttonLabels: ['Camera', 'Pick Image'],
                addCancelButtonWithLabel: 'Cancel',
                androidEnableCancelButton: true,
                winphoneEnableCancelButton: true
            };
            return $cordovaActionSheet.show(options)
                .then(function (btnIndex) {
                    var index = btnIndex;
                    var result;
                    switch (index) {
                    case 1:
                        result = capturePicture();
                        break;
                    case 2:
                        result = selectPicture();
                        break;
                    default:
                        result = $q.reject("profile pict error");
                        break;
                    }
                    return result;
                });
        };

        var editPicture = function (imageURI) {
            $log.debug("imageURI is " + imageURI);
            if (!imageURI) {
                return $q.reject("No image selected or captured");
            }
            return $jrCrop.crop({
                url: imageURI,
                width: 200,
                height: 200,
                title: "Move and Scale",
                circle: true
            }).then(function (canvas) {
                return canvas.toDataURL("image/jpeg");
            }, function () {
                return $q.reject("");
            });
        };

        var processProfilePicture = function (uri) {
            var origUri = uri;
            return $q.when(uri)
                .then(editPicture)
                .then(function (uriData) {
                    var uri = uriData;
                    if (!uriData) {
                        uri = origUri;
                    }
                    var type = Tring.mimeType(origUri);
                    var thumbnailFunction = ThumbnailService.getThumbnailFunction(type);
                    return thumbnailFunction(uri, 96).then(function (dataurl) {
                        $scope.profile.profileUri = uri;
                        $scope.profile.profileThumbnailData = dataurl;
                        return true;
                    });
                });
        };

        $scope.setProfilePicture = function () {
            return $scope.getPhotoUri()
                .then(processProfilePicture);
        };

        //Do a diff to find what was edited
        var keepOnlyChangedFields = function () {
            var x;
            //We dont cater to arrays here since we dont allow editing array like categories yet
            for (x in $scope.profile) {
                if (angular.isDefined($scope.newProfile[x]) && $scope.newProfile[x] === $scope.profile[x]) {
                    delete $scope.newProfile[x];
                } else {
                    $scope.newProfile[x] = angular.copy($scope.profile[x]);
                }
            }
        };

        $scope.doneHandler = function () {
            keepOnlyChangedFields();
            if (Object.keys($scope.newProfile).length > 0) {

                if ($scope.group) {
                    //For groups, only picture info is uploaded and contentId
                    //sent back to parent controller in a broadcast event (should be emitted)
                    saveAndupdateGroupProfilePicture().then(function () {
                        if (!$scope.inPlaceProfileDef) {
                            $cordovaToast.showLongCenter("Saved Changes");
                            $scope.newProfile = angular.copy($scope.profile);
                            return NavigationService.goNativeBack();
                        }
                    });
                } else {
                    //For this device user, both picture info and text info is
                    //uploaded
                    saveAndUpdateContactProfilePictureAndInfo().then(function () {
                        $scope.newProfile = angular.copy($scope.profile);
                    });
                }

            } else if (!$scope.group) {
                $scope.newProfile = angular.copy($scope.profile);
            }

        };

        $scope.contactTringTeam = function () {
            var msg;
            var yourMessage = $scope.contactTring.yourMessage;
            if (!yourMessage) {
                msg = "Please type a message";
                $cordovaToast.showLongCenter(msg);
            } else {
                var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.CONTACT_TRING_URL + encodeURIComponent(yourMessage);
                $http.get(url).then(function (resp) {
                    if (resp && resp.data && resp.data.success) {
                        $log.debug("Success storing feedback");
                        msg = "Message sent to Tring Team - Thanks !";
                    } else {
                        msg = "Unable to send message to Tring team - Please try again or check network !";
                    }
                    $cordovaToast.showLongCenter(msg);

                }).then(null, function (err) {
                    msg = "Unable to send message to Tring team - Please try again or check network !";
                    $log.error("contactTringTeam http error is " + JSON.stringify(err));
                    $cordovaToast.showLongCenter(msg);
                });
                $scope.contactTring.yourMessage = "";
            }
        };

        $scope.$on("processCapturedResult", function (evt, results) {
            $scope.readOnly = $scope.contact = null;
            if (!$scope.group) {
                $scope.group = {}; //set a dummy group object
            }
            onEnter();
            $scope.profile = {};
            $scope.newProfile = {};
            $scope.inPlaceProfileDef = true;
        });

        var storeAppStateInDb = function (appState) {
            var mbrDb = DBService.mbrDb();
            var key = ConstsService.PICKER_STATE_STORAGE_KEY;
            return mbrDb.get(key).then(function (obj) {
                return angular.extend(obj, {
                    appState: appState.data,
                    appRouteState: appState.routeState
                });
            }).then(null, function (err) {
                return {
                    "_id": key,
                    appState: appState.data,
                    appRouteState: appState.routeState
                };
            }).then(function (obj) {
                $log.debug("storing app state before capture - state is " + JSON.stringify(obj));
                return mbrDb.put(obj);
            }).then(function () {
                $log.debug("after capture onPause");
                NetworkService.setOnResumeFunction(key, appState);
                return true;
            });
        };

        var createCreateEditGroupAppStateBeforePause = function () {
            var appState = {};
            appState.capturing = true;
            appState.data = {
                isCreateNew: $scope.$parent.isCreateNew,
                readOnly: $scope.$parent.readOnly,
                contact: $scope.$parent.contact,
                group: $scope.$parent.group
            };
            appState.routeState = $scope.$parent.isCreateNew ? $state.current.name : 'app.groups.manage';
            return storeAppStateInDb(appState);
        };

        $scope.setGroupProfilePictureInPlace = function () {
            $scope.readOnly = $scope.contact = null;
            if (!$scope.group) {
                $scope.group = {}; //set a dummy group object
            }
            onEnter();
            $scope.profile = {};
            $scope.newProfile = {};
            $scope.inPlaceProfileDef = true;

            $scope.setProfilePicture().then($scope.doneHandler);
        };

        var saveAndupdateGroupProfilePicture = function () {
            $ionicLoading.show();
            return uploadProfilePictureContent($scope.profile, 'group.jpg')
                .then(function (contentId) {
                    $log.debug("Uploaded profile image, content id is " + contentId);
                    //$scope.newProfile = {};
                    $ionicLoading.hide();
                    return $rootScope.$broadcast("groupProfilePictureSet", angular.extend($scope.profile, {
                        contentId: contentId
                    }));
                }).then(null, function (err) {
                    console.error("error storing profile info - err is " + JSON.stringify(err));
                    $ionicLoading.hide();
                    throw err;
                });
        };

        var saveAndUpdateContactProfilePictureAndInfo = function () {
            var userProfile;
            $ionicLoading.show();
            return UserService.saveProfile($scope.profile)
                .then(function (profile) {
                    //below event handled by NavigationCtrl to change profile pict in burger menu
                    $rootScope.$broadcast("selfProfileChange", profile);
                    userProfile = profile;
                    return profile;
                })
                .then(function (profile) {
                    if ("profileUri" in $scope.newProfile) {
                        return uploadProfilePictureContent(profile, AuthService.getDevice().p2p.split('/').pop() + '.jpg');
                    } else {
                        return null;
                    }
                })
                .then(function (contentId) {
                    $log.debug("Uploaded profile image, content id is " + contentId);
                    var data = {
                        customMessage: ("statusMessage" in $scope.newProfile) ? userProfile.statusMessage : undefined,
                        name: ("name" in $scope.newProfile) ? userProfile.name : undefined,
                        thumbnailData: ("profileThumbnailData" in $scope.newProfile) ? userProfile.profileThumbnailData : undefined,
                        notifications: ("notifications" in $scope.newProfile) ? userProfile.notifications : undefined,
                        paypalId: 'paypalId' in $scope.newProfile ? userProfile.paypalId : undefined
                    };
                    //Ensure we pass on only changed values
                    for (var field in data) {
                        if (!angular.isDefined(data[field])) {
                            delete data[field];
                        }
                    }
                    $log.debug("Profile info (except for contentId) that is changed and being sent to server is :- ");
                    $log.debug(JSON.stringify(data));
                    if (contentId) {
                        data.contentId = contentId;
                    }
                    var uploadProfileUrl = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.UPDATE_PROFILE_URL;
                    return $http.post(uploadProfileUrl, data, {
                        timeout: ConstsService.HTTP_TIMEOUT
                    });

                })
                .then(function () {
                    //$scope.newProfile = {};
                    $ionicLoading.hide();
                    $cordovaToast.showLongCenter("Successfully changed profile details");
                    return; //NavigationService.goNativeBack();
                }).then(null, function (err) {
                    console.error("error storing profile info - err is " + JSON.stringify(err));
                    $cordovaToast.showLongCenter("Error changing profile details");
                    $ionicLoading.hide();
                    throw err;
                });
        };

        var uploadProfilePictureContent = function (profile, fileName) {
            var fs = new FileBrowseService();
            var result = fs.uploadB64Data(profile.profileUri, {
                fileName: fileName,
                mimeType: "image/jpeg"
            });
            $timeout(function () {
                result.cancel();
            }, ConstsService.HTTP_CONTENT_UPLOAD_TIMEOUT);
            return result.promise.then(function (innerResult) {
                return innerResult.promise;
            }).then(null, function (err) {
                console.error("Error " + JSON.stringify(err) + " uploading profile image file ");
                $cordovaToast.showLongCenter("Failed to upload content - please check network connection");
                return $q.reject(err);
                //return err;
            }).then(function (result) {
                var contentId = JSON.parse(result.response).contentId;
                $log.debug('profilestatus leave - result.response is ' + result.response);
                return contentId;
            });
        };

        var selectPicture = function () {

            var appState = $scope.group ? createCreateEditGroupAppStateBeforePause() : createAppStateBeforePause();
            //NetworkService.setOnPauseFunction(ConstsService.PICKER_STATE_STORAGE_KEY, appState);
            //NetworkService.setOnResumeFunction(ConstsService.PICKER_STATE_STORAGE_KEY, appState);

            var setOptions = function (srcType) {
                var options = {
                    // Some common settings are 20, 50, and 100
                    quality: 50,
                    destinationType: Camera.DestinationType.FILE_URI,
                    // In this app, dynamically set the picture source, Camera or photo gallery
                    sourceType: srcType,
                    encodingType: Camera.EncodingType.JPEG,
                    mediaType: Camera.MediaType.PICTURE,
                    allowEdit: false,
                    correctOrientation: true //Corrects Android orientation quirks
                };
                return options;
            };
            var srcType = Camera.PictureSourceType.SAVEDPHOTOALBUM;
            var options = setOptions(srcType);
            var deferred = $q.defer();

            appState.then(function () {
                navigator.camera.getPicture(function cameraSuccess(imageUri) {
                    deferred.resolve(imageUri);
                }, function cameraError(error) {
                    console.debug("Unable to obtain picture: " + error, "app");
                    deferred.reject(error);
                }, options);
            });
            return deferred.promise;
        };

        var createAppStateBeforePause = function () {
            var appState = {};
            appState.capturing = true;
            appState.data = {
                readOnly: $scope.readOnly,
                contact: $scope.contact,
                group: $scope.group
            };
            appState.routeState = $state.current.name;
            return storeAppStateInDb(appState);
            //return appState;
        };

        var capturePicture = function () {
            var options = {
                limit: 1
            };
            $log.debug("Calling capturePicture");

            var appState = $scope.group ? createCreateEditGroupAppStateBeforePause() : createAppStateBeforePause();
            //NetworkService.setOnPauseFunction(ConstsService.APP_STORAGE_KEY, appState);
            //NetworkService.setOnResumeFunction(ConstsService.APP_STORAGE_KEY, appState);

            return appState.then(function () {
                    return $cordovaCapture.captureImage(options);
                })
                .then(function (data) {
                    $log.debug("Image data ! " + JSON.stringify(data));
                    if (data[0].fullPath.indexOf('file:///') < 0) {
                        return data[0].fullPath.replace('file:/', 'file:///');
                    } else {
                        return data[0].fullPath;
                    }
                });
        };

        $scope.loadPasswordPop = function () {
            setPassword();
        };

        var setPassword = function () {

            $scope.data = {
                password: null,
                repassword: null,
                oldpassword: null
            };
            return $ionicPopup.show({
                cssClass: 'customPopup',
                templateUrl: 'templates/rubyonic/setPassword.html',
                title: '',
                scope: $scope,
                buttons: [{
                    text: 'Set Password',
                    type: 'button button-energized button-block',
                    onTap: function (e) {
                        if (!$scope.data.password || !$scope.data.repassword ||
                            $scope.data.password !== $scope.data.repassword) {
                            $cordovaToast.showLongCenter("Please give identical inputs for the new password fields");
                            e.preventDefault();
                        } else {
                            $cordovaToast.showLongCenter("Password changed/stored");
                            return $scope.data.password;
                        }
                    }
        }, {
                    text: 'Cancel',
                    type: 'button button-assertive button-block',
                    onTap: function () {
                        $scope.data.password = "";
                        return $scope.data.password;
                    }
        }]
            });
        };
            }]);
