angular.module('tring.groupInvite.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
  .controller('GroupInviteController', ['$scope', 'GroupService', '$q', '$ionicPopup', 'MqttBrokerService', 'MessagesService', 'Tring', 'ConstsService', 'ChatService', '$http', '$cordovaToast', '$stateParams', '$ionicLoading', 'ContactsService', '$cordovaSms', '$log', 'NavigationService', '$timeout', '$filter', 'UserService','$rootScope',function ($scope, GroupService, $q, $ionicPopup, MqttBrokerService, MessagesService, Tring, ConstsService, ChatService, $http, $cordovaToast, $stateParams, $ionicLoading, ContactsService, $cordovaSms, $log, NavigationService, $timeout, $filter,UserService, $rootScope) {
    if ($stateParams.group) {
      $scope.group = $stateParams.group;
      $scope.data = {
        userMessageText: "",
        userHandle: "",
        requestSent: false,
        nameAvailable: null,
        checkAvailability: Tring.debounce(function () {
          return GroupService.checkHandleAvailabilityForGroupName($scope.data.userHandle, $scope.group.groupName).then(function (result) {
            $scope.data.nameAvailable = result;
            return result;
          });
        }, 1500).callback,
        resetCheckAvailability: function () {
          $scope.data.nameAvailable = null;
        }
      };

      $scope.$watch('data.userHandle', function (val) {
        $scope.data.checkAvailability();
        //$scope.data.userHandle = $filter('lowercase')(val);
      }, true);
    }
    if ($stateParams.inviterIsOwnerOrAdmin) {
      $scope.inviterIsOwnerOrAdmin = JSON.parse($stateParams.inviterIsOwnerOrAdmin);
    }

    // (1) Used on the groupDiscover.html page when you JOIN a discovered group
    //Does not require any of the stateParams
    var getErrorMsgForHandleStatus = function (status, handle) {
      var str = "";
      switch (status) {
        case ConstsService.USER_GROUP_STATUS_EVICTED:
          str = "ERROR- You were previously evicted from this group and can't join again";
          break;
        case ConstsService.USER_GROUP_STATUS_REJECTED:
          str = "ERROR- You were previously rejected from this group and can't join again";
          break;
        case ConstsService.USER_GROUP_STATUS_ACTIVE:
          //str = "You are already a member of this group with handle '" + handle + "' !";
          str = "You have successfully joined the group. You can chat by going to the chat list screen.";
          break;
        default:
          str = "You already have a handle named '" + handle + "' for this group which will be retained";
          break;
      }
      return str;
    };

    // (1) Used on the groupDiscover.html page when you JOIN a discovered group
    //Does not require any of the stateParams
    var sendMembershipRequest = function (groupOrMessage) {
      var serverResponse;
      var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.CREATE_GROUP_TEMP_MEMBER_URL;
      url = url.replace('@0', groupOrMessage.groupName);

      serverResponse = $http.post(url, {
        handleName: $scope.data.userHandle,
        data: $scope.data.userMessageText
      }).then(function (response) {
        if (response.data && response.data.success) {
          $scope.data.requestSent = true;
        } else {
          if (response.data && response.data.error && response.data.error.code === 409) {
            var status = response.data.membershipStatus;
            var existingHandle = response.data.handleName;
            var errorMsg = getErrorMsgForHandleStatus(status, existingHandle);
            if (status === ConstsService.USER_GROUP_STATUS_EVICTED || status === ConstsService.USER_GROUP_STATUS_REJECTED) {
              $cordovaToast.showLongCenter(errorMsg);
              $scope.data.requestSent = false;
            } else if (status === ConstsService.USER_GROUP_STATUS_ACTIVE) {
              $cordovaToast.showLongCenter(errorMsg);
            } else {
              $cordovaToast.showLongCenter(errorMsg);
            }
            $scope.data.requestSent = true;
          } else {
            console.error("sendMembershipRequest- Error in server request ");
            throw "sendMembershipRequest- Error in server request ";
          }
        }
        return GroupService.formGroupMemberObjectFromServerResponse(response.data, groupOrMessage.groupName);
      }, function (err) {
        $log.debug("err is " + JSON.stringify(err));
        throw err;
      });
      return serverResponse || false;
    };

    $scope.cancelSendRequest = function () {
      $scope.data.requestSent = false;
      NavigationService.goNativeBack();
      return false;
    };

    var checkUserName = function () {
      return UserService.saveProfile({}).then(function (profile) {
        return $q(function (success, failure) {
          if (profile && profile.name) {
            success(profile.name);
          } else {
            $ionicPopup.show({
              cssClass: 'customPopup',
              templateUrl: 'templates/rubyonic/setName.html',
              title: '',
              scope: $scope,
              buttons: [{
                text: 'Set Name',
                type: 'button button-outline button-balanced button-block',
                onTap: function (e) {
                  if (!$scope.data.userHandle ) {
                    $cordovaToast.showLongCenter("Please enter name");
                    e.preventDefault();
                  } else {
                    $cordovaToast.showLongCenter("Name stored");
                    return $scope.data.userHandle;
                  }
                }
              }, {
                text: 'Cancel',
                type: 'button button-outline button-assertive button-block',
                onTap: function () {
                  $scope.data.userHandle = "";
                  return $scope.data.userHandle;
                }
              }]
            }).then(function(name) {
              if (name) {
                return name;
              }else {
                throw "";
              }
            }).then(function(name){
              return UserService.saveProfile({name:name}).then(function(){
                return name;
              })
            }).then(function(name){
              var postdata = {
                name: name
              };
              $ionicLoading.show();
              var uploadProfileUrl = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.UPDATE_PROFILE_URL;
              $http.post(uploadProfileUrl, postdata, {
                timeout: ConstsService.HTTP_TIMEOUT
              }).then(function () {
                $ionicLoading.hide();
                $cordovaToast.showLongCenter("Successfully added name");
                success(postdata.name);
              }).then(null, function (err) {
                console.error("error storing name - err is " + JSON.stringify(err));
                $cordovaToast.showLongCenter("Error adding name");
                $ionicLoading.hide();
                failure(err);
              });
            },failure);


          }
        });
      });
    };
    var checkHandleName=function(name){
      return GroupService.checkHandleAvailabilityForGroupName(name, $scope.group.groupName || $scope.group.name).then(function (result) {
        $scope.data.nameAvailable = result;
        if (!result) {
          //$scope.handleName = "";
          // return $ionicPopup.show(handleNamePopup('Enter a different user name for this group, to avoid name conflict with another user','OK','Cancel')).then(function(){
          //   return true;
          // });
          //$cordovaToast.showLongCenter("There is a user name conflict for this group. Please change the user name in Settings and try again");
          //return $q.reject("user name conflict");
        }
        $scope.data.userHandle=name;
        return result;
      });
    };
    $scope.tryJoinGroup = function(grp) {
      return GroupService.getLocalGroup(ConstsService.GROUPS_START_KEY + (grp.gid||grp._id)).then(function (group) {
        if (angular.isDefined(group.membershipType) && angular.isDefined(group.userGroupStatus) &&
          group.userGroupStatus != ConstsService.USER_GROUP_STATUS_LEFT &&
          group.userGroupStatus != ConstsService.USER_GROUP_STATUS_PENDING) {
          var errormsg = GroupService.getGrouMembershipError(group.userGroupStatus);
          if (errormsg) {
            $cordovaToast.showLongCenter(errormsg);
          }
        } else {
          $scope.requestMembership(grp);
        }
      }).then(null, function () {
        $scope.requestMembership(grp);
      });
    };

    $scope.requestMembership = function (groupOrMessage) {
      var membershipNeedsApproval = groupOrMessage.groupMembershipNeedsApproval;
      return checkUserName().then(checkHandleName).then(function() {
        return groupOrMessage;
      }).then(sendMembershipRequest).then(function (grp) {
          if ($scope.data.requestSent) {
            //$cordovaToast.showLongTop("Group Membership request sent");
          } else {
            $cordovaToast.showLongTop("ERROR: Group Membership request not sent or failed");
          }
          if (grp) { //the request returns a response with the right status of the user in the group
            if (grp.userGroupStatus === ConstsService.USER_GROUP_STATUS_ACTIVE) {

              $ionicLoading.show();
              return GroupService.createLocalGroupMemberAndChat(grp).then(function () {
                try {
                  MqttBrokerService.subscribeTo(grp.groupTopic);
                  MqttBrokerService.subscribeToStatus(grp.groupTopic);
                  if (grp.membershipType === ConstsService.OWNER_ROLE ||
                    grp.membershipType === ConstsService.ADMIN_ROLE) {
                    MqttBrokerService.subscribeTo(grp.groupTopic + ConstsService.PVT_SUB_TOPIC);
                  }
                  $log.debug("Before calling addToUserGroupHandle");
                  MqttBrokerService.addToUserGroupHandle(grp.gid, grp.handleName);
                  if (!membershipNeedsApproval) {
                    MqttBrokerService.sendNewMemberJoinedGroupMessage(grp,
                      function () {
                        $log.debug("Sent New member created message");
                        $cordovaToast.showLongCenter("You have successfully joined this group with handle " + grp.handleName);
                      });
                    $cordovaToast.showLongCenter("You have successfully joined this group with handle " + grp.handleName);
                  }
                } catch (ex) {
                  console.error(JSON.stringify(ex));
                }
                return true;
              }).then(function () {
                $ionicLoading.hide();
                NavigationService.goNativeBack();
                return true;
              }).then(null, function () {
                $ionicLoading.hide();
                $log.debug("Problem in createLocalGroupMemberAndChat or downstream");
                throw "Problem in createLocalGroupMemberAndChat or downstream";
              });
            } else {
              NavigationService.goNativeBack();
              return true;
            }
          } else {
            console.error("grp is falsey !");
            throw "In requestMembership- grp is falsey !";
          }

        }).then(null, function (err) {
          $log.debug("err is " + JSON.stringify(err));
          $cordovaToast.showLongCenter("Problem sending membership request. Please try again")
          NavigationService.goNativeBack();
          return false;
        });
      };

      var handleNamePopup = function(title,okButtonText, cancelButtonText) {
        return {
          cssClass: 'customPopup',
          templateUrl: 'templates/rubyonic/groupInviteAcceptRejectPopup.html',
          title: title,
          scope: $scope,
          buttons: [{
            text: okButtonText, //'Accept Membership',
            type: 'button button-outline button-balanced button-block',
            onTap: function (e) {
              if (!$scope.data.userHandle || $scope.handleName && $scope.handleName.$error) {
                $cordovaToast.showLongCenter("Please give a valid 'My username'");
                e.preventDefault();
              } else {
                message.processed = true;
                watchDestroy();
                return $scope.data.userHandle;
              }
            }
          }, {
            text: cancelButtonText,//'Reject Membership',
            type: 'button button-outline button-assertive button-block',
            onTap: function () {
              message.processed = true;
              $scope.data.userHandle = "";
              watchDestroy();
              return $scope.data.userHandle;
            }
          }]
        };
      }

    // (2) Used on the chat-ui.html template when you click the invite message
    // state params not required
    $scope.$on('groupOrdinaryInviteClicked', function (evt, message) {
      //return $scope.requestMembership(message);
      NavigationService.goNativeToView('app.groups.requestForMembership', {
        group: message
      });
    });

    // (2) Used on the chat-ui.html template when you click the invite message to
    //accept/reject group membership invitation
    $scope.$on('groupInviteClicked',
      function (evt, message) {
        if (message.processed) {
          $ionicPopup.alert({
            title: "User Error",
            template: 'Invitation already processed'
          });
          return;
        }
        $scope.handleName = "";
        var p1 = function (hdlName) {
          var deleteMsg = function () {
            return MessagesService.deleteMessage(message._id);
          };
          $log.debug('popup response is ' + JSON.stringify(hdlName));
          if (hdlName) {
            $scope.handleName = hdlName;
            var result = GroupService.createGroupMember(message.gid, hdlName)
              .then(null, function (err) {
                $log.debug("createGroupMember error is - " + JSON.stringify(err));
                if (err.status !== 409) {
                  return $q.reject(err);
                } else {
                  return {
                    data: {
                      success: true
                    }
                  };
                }
              });
            result.then(deleteMsg);
            return result;
          } else {
            return deleteMsg().then(function () {
              return $q.reject("rejected the invitation");
            });
          }
        };

        var p1_err = function (err) {
          $log.debug(JSON.stringify(err));
          $cordovaToast.showLongCenter("Rejected invitation");
          return $q.reject("rejected");
        };

        //var hid;
        var p2 = function (response) {
          var data = response.data;
          $log.debug("After createGroupMember, response data from server is " + JSON.stringify(data));
          if (data.success ||
            (data.error && data.error.code === 409 &&
            data.membershipStatus !== ConstsService.USER_GROUP_STATUS_REJECTED &&
            data.membershipStatus !== ConstsService.USER_GROUP_STATUS_EVICTED)) {
            delete data.error;
            delete data.success;
            angular.extend(message, {
              handleName: $scope.handleName
            }, GroupService.formGroupMemberObjectFromServerResponse(data, message.groupName));
            $log.debug("groupInviteClicked processing p2- message is " + JSON.stringify(message));
            return GroupService.createLocalGroupMemberAndChat(message);
          } else if (data.error && data.error.code === 402) {
            $cordovaToast.showLongCenter("Handle name is already in use. Please choose a different name.");
            return $q.reject("http request to create group member failed");
          } else {
            return $q.reject("http request to create group member failed");
          }
        };
        var p3 = function () {
          try {
            MqttBrokerService.subscribeTo(message.groupTopic);
            MqttBrokerService.subscribeToStatus(message.groupTopic);
          } catch (err) {
            console.error(JSON.stringify(err));
          }
          $log.debug("Before calling addToUserGroupHandle");
          MqttBrokerService.addToUserGroupHandle(message.gid, message.handleName);
          return $q.resolve();
        };
        var p4 = function () {
          $log.debug("Sending group invite accept message");
          MqttBrokerService.sendNewMemberJoinedGroupMessage(message,
            function () {
              $log.debug("Sent group invite accept message");
              $cordovaToast.showLongCenter("Successfully created group member with handle " + message.handleName);
            });
          return $q.resolve();
        };
        var p_err = function (err) {
          if (err !== "rejected") {
            console.error("Error during group invite popup processing is " + err);
            $cordovaToast.showLongCenter("Error creating group member");
            return $q.reject(err);
          } else {
            return true;
          }
        };
        //We assign a promise purely for use in unit tests
        $scope.resultPromise = $scope.groupInviteDeciderPopup(message).then(p1).then(p2, p1_err).then(p3).then(p4).then(null, p_err);
      });

    // (2) Also part of the work flow of accepting/rejecting an invitation to group membership. Invoked from
    //chats-ui.html
    $scope.groupInviteDeciderPopup = function (message) {
      $scope.data = {};
      $scope.data.groupName = message.groupName;
      $scope.data.groupO2M = message.groupO2M;
      $scope.data.groupClosed = message.groupClosed;
      $scope.data.groupRules = message.groupRules;
      $scope.data.groupDescription = message.groupDescription;
      $scope.data.gid = message.gid;
      $scope.data.nameAvailable = null;

      $scope.data.resetCheckAvailability = function () {
        $scope.data.nameAvailable = null;
      };
      var checkAvailability = Tring.debounce(function () {
        return GroupService.checkHandleAvailabilityForGroup($scope.data.userHandle, $scope.data.gid)
          .then(function (result) {
            $scope.data.nameAvailable = result;
            return result;
          });
      }, 1500).callback;

      var watchDestroy = $scope.$watch('data.userHandle', function (val) {
        checkAvailability();
        //$scope.data.userHandle = $filter('lowercase')(val);
      }, true);

      return $ionicPopup.show(handleNamePopup('','Accept Membership','Reject Membership'));
    };

    // (3) The following functions ar called when this controller is used for state
    // app.groups.inviteNonMemberContactsForMembership or app.groups.inviteContacts
    //to send invites with the correct stateParams for group and inviterIsOwnerOrAdmin
    //The invite members page is created using the inviteContacts.html template

    $scope.filterInRegisteredMembers = function () {
      $scope.showTringMembers = true;
      $scope.showNonTringMembers = false;
      $scope.$broadcast('callSelectWithAutoSuggestScopeFunction', 'refreshElementsWrapper');
    };

    $scope.filterInNonRegisteredMembers = function () {
      $scope.showTringMembers = false;
      $scope.showNonTringMembers = true;
      $scope.$broadcast('callSelectWithAutoSuggestScopeFunction', 'refreshElementsWrapper');
    };

    $scope.refreshContacts = function () {
      return $scope.initializeContacts().then(function (cts) {
        if ($scope.showTringMembers) {
          return cts.filter(function (ct) {
            return ct.registered;
          });
        } else {
          return cts.filter(function (ct) {
            return !ct.registered;
          });
        }
      });
    };

    var inviteMsg = 'Introducing Tring, the mobile chat app which opens you to a world of unlimited open discoverable groups. Download from Android or iOS app store and register today! Please visit http://tringchat.com for more';

    var sendSMS = function (contact) {
      var data = MessagesService.getGroupInviteMessage($scope.group);
      var msg = inviteMsg + '\n' + data;
      $ionicLoading.show();
      $cordovaSms.send(contact.number, msg, {})
        .then(function () {
          $ionicLoading.hide();
          // Success! SMS was sent
          $log.debug("Success! SMS was sent");
          $cordovaToast.showLongCenter('Success! SMS was sent').then(function () {
            // success
          }, function () {
            // error
          });
        }, function (error) {
          $timeout(function () {
            $ionicLoading.hide();
            $cordovaToast.showLongCenter('Failure sending SMS !');
          }, 10);
          // An error occurred
          console.error("Error occurred sending SMS. error is " + error);
          throw "error";
        });
    };

    var sendGroupMembershipInvite = function (contact) {
      return inviteForGroupMembership($scope.group, [contact]);
    };

    $scope.sendInvite = function (contact) {
      if (contact.registered) {
        sendGroupMembershipInvite(contact).then(function () {
          $cordovaToast.showLongCenter('Sent invitation for group membership to ' + contact.name + ' !');
        });
      } else {
        sendSMS(contact);
      }
    };

    var inviteForGroupMembership = function (group, members) {
      $log.debug('aaa');
      $ionicLoading.show();
      return ContactsService.getDbContacts(members).then(function (result) {
        $log.debug('999');
        var rows = result.rows;
        if (rows && rows.length) {
          angular.forEach(rows, function (row) {
            var contact = row.doc;
            var data = MessagesService.getGroupInviteMessage(group);
            var hid = null;
            var f = function () {
              $log.debug("inviteForGroupMembership:mMessage successfully sent to topic: " + contact.topic);
            };
            var getTopic = function () {
              return contact.topic;
            };
            MqttBrokerService.sendMessage(ConstsService.MESSAGE_TYPE_APP, ConstsService.APP_MSG_TYPE_GROUP_INVITE, null, Tring.getSenderId(contact.topic),
              getTopic, data, f, hid, null, {
                groupName: group.name,
                gid: group._id,
                groupTopic: group.topic,
                groupO2M: group.o2m,
                groupClosed: group.closed,
                groupDescription: group.description,
                groupRules: group.rules,
                dataForSelf: MessagesService.getGroupInviteMessage(group, true),
                ordinaryInvite: (!$scope.inviterIsOwnerOrAdmin)
              });
          });
        }
        $ionicLoading.hide();
        return true;
      }, function (err) {
        $ionicLoading.hide();
        $log.debug("Error in inviteForGroupMembership - err is " + JSON.stringify(err));
        return false;
      });
    };

    $scope.initializeContacts = function () {
      $scope.sendMode = 'Tring';
      $ionicLoading.show();
      $scope.showBackButton();
      return ContactsService.getNonMemberContactsFromServer($scope.group._id, $scope.group.handleName).then(function (cntcts) {
        $ionicLoading.hide();
        $scope.members = cntcts;
        return cntcts;
      }, function (err) {
        $ionicLoading.hide();
        throw err;
      });
    };

    $scope.getContactsPaged = function (f, l, cnts) {
      return $q.when(cnts.slice(f, l));

    };

    $scope.invokeWhatsApp = function (group) {
      var msg = inviteMsg;
      var data = MessagesService.getGroupInviteMessage(group);
      msg = msg + '\n' + data;
      window.open('whatsapp://send?text=' + encodeURI(msg), '_system', 'location=yes');
    };
  }
  ]);
