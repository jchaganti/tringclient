angular.module('tring.groupMembershipRequest.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller('GroupMembershipRequestController', ['$scope', 'GroupService', '$q', '$ionicPopup', 'MqttBrokerService', 'MessagesService', 'Tring', 'ConstsService', 'ChatService', '$http', '$stateParams', 'NavigationService', '$log', function ($scope, GroupService, $q, $ionicPopup, MqttBrokerService, MessagesService, Tring, ConstsService, ChatService, $http, $stateParams, NavigationService, $log) {

        $scope.processingGroupMembershipRequest = true;

        if ($stateParams.message) {
            $scope.message = $stateParams.message;
            $scope.data = {
                gid: $scope.message.gid,
                handleName: $scope.message.handleName
            };
        } else if ($stateParams.groupName) {
            $scope.groupName = $stateParams.groupName;
        }


        $scope.goBack = function () {
            NavigationService.goNativeBack();
        };

        $scope.showAlert = function (title, message) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: message
            });

            alertPopup.then(function () {
                $log.debug('Alert processed');
            });
        };
        $scope.processAcceptReject = function (accept) {
            var message = $scope.message;
            var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.ACCEPT_GROUP_MEMBER_URL;
            var response;
            var title;
            var deleteMsg = function () {
                return MessagesService.deleteMessage(message._id);
            };
            if (accept) {
                title = 'Accept Membership';
                url = url.replace('@0', message.gid).replace('@1', message.handleName);
                response = $http.post(url);
            } else {
                url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.REJECT_GROUP_MEMBER_URL;
                url = url.replace('@0', message.gid).replace('@1', message.handleName);
                title = 'Reject Membership';
                response = $http.post(url);
            }
            return response.then(function () {
                $scope.showAlert(title, 'Success !');
                deleteMsg();
                $scope.goBack();
            }).catch(function (err) {
                console.error(JSON.stringify(err));
                $scope.showAlert(title, 'Server error ');
            });
        };

        $scope.$on("groupMembershipRequestClicked", function (evt, message) {
            groupMembershipRequestDeciderPopup(message).then(function (result) {
                var url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.ACCEPT_GROUP_MEMBER_URL;
                var response;
                var title;
                var deleteMsg = function () {
                    return MessagesService.deleteMessage(message._id);
                };
                if (result) {
                    title = 'Accept Membership';
                    url = url.replace('@0', message.gid).replace('@1', message.handleName);
                    response = $http.post(url);
                } else if (!message.processed) {
                    $log.debug("group membership request decision deferred");
                } else {
                    url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.REJECT_GROUP_MEMBER_URL;
                    url = url.replace('@0', message.gid).replace('@1', message.handleName);
                    title = 'Reject Membership';
                    response = $http.post(url);
                }
                return response.then(function () {
                    $scope.showAlert(title, 'Success !');
                    deleteMsg();
                }).catch(function (err) {
                    $scope.showAlert(title, 'Server error ' + JSON.stringify(err));
                });
            });
        });

        var groupMembershipRequestDeciderPopup = function (message) {
            $scope.data = {};
            $scope.data.groupName = message.groupName;
            $scope.data.groupId = message.groupId;
            $scope.data.handleName = message.handleName;
            $scope.processingGroupMembershipRequest = true;

            return $ionicPopup.show({
                templateUrl: 'templates/rubyonic/userReputationReport.html',
                title: 'Process Membership Request',
                scope: $scope,
                buttons: [{
                        text: 'Accept',
                        type: 'button-default',
                        onTap: function () {
                            message.processed = true;
                            return true;
                        }
                                },
                    {
                        text: 'Reject',
                        type: 'button-default',
                        onTap: function () {
                            message.processed = true;
                            return false;
                        }
                                }
                    /*,
                                        {
                                            text: 'Defer',
                                            type: 'button-default',
                                            onTap: function () {
                                                message.processed = false;
                                                return false;
                                            }
                                              }*/
                         ]
            });
        };
            }]);
