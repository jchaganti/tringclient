angular.module('tring.user.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
  .controller('UserCtrl', ['$scope', '$state', '$ionicHistory', 'UserService', 'ChatService', '$ionicModal', '$ionicLoading', '$window', 'AuthService', 'NavigationService', '$rootScope', '$timeout', '$cordovaToast','$cordovaSplashscreen', '$log',
    function ($scope, $state, $ionicHistory, UserService, ChatService, $ionicModal, $ionicLoading, $window, AuthService, NavigationService, $rootScope, $timeout, $cordovaToast, $cordovaSplashscreen, $log) {
      var inActiveDevice = false;
      var currentDevice = AuthService.getDevice();
      if (currentDevice) {
        if (currentDevice.active) {
          $log.debug("Somethng is wrong. Should not show login page if the device is active");
        } else {
          inActiveDevice = true;
        }
      }
      var device = {
        mobileNo: '',
        pin: ''
      };
      var reSendPinEnabledTid;
      $timeout(function() {
        $cordovaSplashscreen.hide()
      }, 50);
      function openCountrySelectorModal() {
        $log.debug("openCountrySelectorModal");
        if ($scope.modal) {
          $scope.modal.show();
          return;
        }
        $ionicModal.fromTemplateUrl('templates/rubyonic/login-country-selector.html', {
          scope: $scope,
          animation: 'none'
        }).then(function (modal) {
          $scope.modal = modal;
          var countries = UserService.getCountries();
          var countriesArr = [];
          for (var code in countries) {
            countriesArr.push(countries[code]);
          }

          $scope.countries = countriesArr;
          $scope.modal.show();
        });
        $scope.$on('$destroy', function () {
          if ($scope.modal) {
            $scope.modal.remove();
          }
        });
      }

      function updateSelectedCountry(country) {
        $log.debug("updateSelectedCountry: " + JSON.stringify(country));
        $scope.country = country;
        UserService.setCountry(country);
      }

      function printMobileNum(mobilenumber) {
        $log.debug("Parsing " + mobilenumber);
        var parsedMobileNum = $window.parsePhone(mobilenumber);
        $log.debug("Stringified: " + JSON.stringify(parsedMobileNum));
        var countryCode = parsedMobileNum === null ? "IN" : parsedMobileNum.countryISOCode;
        $log.debug("Formatting in E164: " + $window.phoneUtils.formatE164(mobilenumber, countryCode));
      }

      function setReSendPinFlag() {
        $scope.reSendPinEnabled = false;
        reSendPinEnabledTid = $timeout(function () {
          reSendPinEnabledTid = null;
          $scope.reSendPinEnabled = true;
        }, 180000);
      }

      $scope.$on('$ionicView.beforeEnter', function () {
        //printMobileNum ("919890128847")
        $scope.mobilenumber = UserService.getMobilenumber();
        $scope.country = UserService.getCountry();
        setReSendPinFlag();
      });

      $scope.$on('$ionicView.enter', function () {
        //printMobileNum ("919890128847")
        $timeout(function() {
          var elem = document.querySelector('#phonenumber');
          if(elem) {
            cordova.plugins.Focus.focus(elem);
          }

        });
      });

      $scope.reEnterMobileNo = function () {
        if (reSendPinEnabledTid) {
          $timeout.cancel(reSendPinEnabledTid);
        }
        NavigationService.goNativeBack();
      }

      $scope.reSendPin = function () {
        $scope.registerDevice(true);
      }

      //UTILS
      function showLoading(tpl) {
        $ionicLoading.show({
          template: tpl
        });
      }

      function hideLoading() {
        $ionicLoading.hide();
      }

      $scope.openCountrySelectorModal = openCountrySelectorModal;
      $scope.getFormattedNumber = UserService.getFormattedNumber;
      $scope.updateSelectedCountry = updateSelectedCountry;
      $scope.device = device;
      $scope.inActiveDevice = inActiveDevice;
      $scope.registerDevice = function (reSendPinFlag) {
        $ionicLoading.show();
        if(!reSendPinFlag) {
          UserService.setMobilenumber($scope.device.mobileNo);
        }

        var formattedMobileNo = UserService.getFormattedNumber();
        $log.debug("Formatted Mobile No: " + formattedMobileNo);
        $log.debug("ionic.Platform.platform: " + ionic.Platform.platform());
        UserService.registerDevice(formattedMobileNo, ionic.Platform.platform()).then(function (res) {
          $ionicLoading.hide();
          $log.debug("Device Registration response: " + JSON.stringify(res));
          //$state.go('pin');
          if (!reSendPinFlag) {
            NavigationService.goNativeToView('pin', null, 'right');
          }
          else {
            $cordovaToast.showLongCenter("A new PIN has been sent to the registered mobile");
            $scope.$evalAsync(function () {
              setReSendPinFlag();
            });
          }
        }, function (err) {
          $ionicLoading.hide();
          console.error("Error occured during Device Registration: " + JSON.stringify(err));
        });
      };

      $scope.verifyPin = function () {
        $log.debug("PIN: " + $scope.device.pin);
        $ionicLoading.show();
        UserService.verifyPin($scope.device.pin).then(function (res) {
          $ionicLoading.hide();
          $log.debug("Pin Verification response: " + JSON.stringify(res));
          if(res) {
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            UserService.getGroupsRestored().then(function (groupsRestoredRecord) {
              var groupsRestored = groupsRestoredRecord && groupsRestoredRecord.done;
              $log.debug(" groupsRestored: " + JSON.stringify(groupsRestored));
              if (!groupsRestored) {
                ChatService.startRestoringGroups().then(function (result) {
                  $log.debug("startRestoringGroups result: " + JSON.stringify(result));
                  if (result.wasGroupRestoringInProgress) {
                    $log.debug("Ignoring startRestoringGroups since it is already in progress");
                  }
                  else {
                    UserService.setGroupsRestored().then(function (_res) {
                      $rootScope.$broadcast("groupsRestored", {
                        result: result
                      });
                    }).catch(function (err) {
                      $log.debug("Error occurred during UserService.setGroupsRestored. Hence re-trying");
                      UserService.setGroupsRestored().then(function (_res) {
                        $rootScope.$broadcast("groupsRestored", {
                          result: result
                        });
                      }).catch(function (err) {
                        result.groupsRestoredSuccess = false;
                        $rootScope.$broadcast("groupsRestored", {
                          result: result
                        });
                      });
                    });
                  }
                });
              }
              else {
                $rootScope.$broadcast("groupsRestored", {
                  result: {
                    groupsRestoredSuccess: true,
                    wasGroupRestoringInProgress: false
                  }
                });
              }
            })
            //$state.go('app.chats');
            NavigationService.goNativeToView('app.newIntro', {justStarting: true}, 'right');
          }
          else {
            $cordovaToast.showLongCenter("You have entered an invalid PIN.");
          }
        }, function (err) {
          $ionicLoading.hide();
          console.error("Error occured during pin verification: " + JSON.stringify(err));
        });
      };
      //Function to go back a step using $ionicHistory
      $scope.goBackAStep = function () {
        $log.debug('clicked');
        $ionicHistory.goBack();
        NavigationService.goNativeBack();
      };

    }]);
