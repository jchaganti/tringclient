angular.module('tring.chat.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
  .controller('ChatCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', '$cordovaSplashscreen','$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopup, $stateParams, $cordovaSplashscreen,$log) {
      $log.debug("ChatCtrl");
      $rootScope.activeScreen = {
        chats: 'chats',
        groups: 'groups',
        profile: 'profile',
        about: 'about',
        contacts: 'contacts',
        settings: 'settings'
      };
      var device = AuthService.getDevice();
      $log.debug("Device found in Chat " + JSON.stringify(device));
      $timeout(function() {
        $cordovaSplashscreen.hide()
      }, 50);
      if (device) {
        $scope.chats = [];

        $rootScope.networkStatus = NetworkService.isOnline();
        var onlineCB = function () {
          $log.debug("The onlineCB called");
          $rootScope.$evalAsync(function () {
            $rootScope.networkStatus = true;
          });
        };
        NetworkService.addOnlineCB(onlineCB);

        var offlineCB = function () {
          $log.debug("The offlineCB called");
          $rootScope.$evalAsync(function () {
            $rootScope.networkStatus = false;
          });
          MqttBrokerService.disconnect();
        };

        NetworkService.addOfflineCB(offlineCB);

        NetworkService.startService();



        $scope.isGroup = Tring.isGroupSid;

        var updateRecentMessage = function (chats, fwdedMsgId) {
          var recentMsgs = [];
          chats.map(function (chat) {
            $log.debug("Chat: " + JSON.stringify(chat));
            var p1 = function () {
              var recentMsgData = SharedDataService.getValue(ConstsService.RECENT_MESSAGE_PREFIX + chat.sid);
              if (recentMsgData) {
                $log.debug("Got Recent Message Data: " + JSON.stringify(recentMsgData));
                var msgs = [];
                msgs.push(recentMsgData);
                return $q.when(msgs);
              } else {
                return MessagesService.getRecentMessage(chat.sid);
              }

            };

            var p2 = function (msgs) {
              //$log.debug("Latest Message: " + JSON.stringify(msgs[0]))
              if (msgs && msgs.length > 0) {
                chat.message = {
                  data: msgs[0].data,
                  dir: msgs[0].dir,
                  handleName: msgs[0].handleName,
                  ts: parseInt(msgs[0].ts)
                };
              } else {
                chat.message = {
                  data: '',
                  ts: 0
                };
              }
              $log.debug("chat.message: " + JSON.stringify(chat.message))
              // Get from the ChatService cache since sometimes the chat record may not have got updated.
              // If not available then get from the chat record
              var lastReadMsgId = ChatService.getLastReadMessageIdForChat(chat.sid);
              if (!lastReadMsgId) {
                lastReadMsgId = chat.lastReadMsgId;
              }
              $log.debug("chat.lastReadMsgId: " + JSON.stringify(lastReadMsgId));
              return MessagesService.getUnReadMessageCount(chat.sid, lastReadMsgId);
            };

            var p3 = function (count) {
              chat.unReadCount = count;
              $log.debug("chat.unReadCount: " + chat.unReadCount);
              return ChatService.getSenderIdData(chat.localSid, chat.sendertype);
            };

            var p4 = function (senderData) {
              if(senderData.pgName) {
                chat.name = senderData.pgName + "/" + senderData.name;
              }
              else {
                chat.name = senderData.name ? senderData.name : (senderData.phoneNumber ? senderData.phoneNumber : "Unknown");
              }

              chat.thumbnailData = senderData.thumbnailData ? senderData.thumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
              chat.isGroup = Tring.isGroupSid(chat.sid);
              $log.debug("updateRecentMessage-In p4 chat thumbnailData is " + chat.thumbnailData);
              return $q.when();

            };
            if (!fwdedMsgId) {
              recentMsgs.push($q.serial([p1, p2, p3, p4]));
            }
            else {
              var q1 = function () {
                return $q.when(0);
              }
              recentMsgs.push($q.serial([q1, p3, p4]));
            }
          });

          if (recentMsgs.length > 0) {
            return $q.all(recentMsgs);
          } else {
            return $q.when();
          }

        };

        var dbChangesCallBack = function (change) {
          var newDoc = change.doc;

          var deleteSyncRecord = function(syncId) {
            $log.debug("Sending the delete sync record to the server with syncId: " + syncId);
            if (syncId) {
              $http.delete(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL + "/" + syncId).then(function (response) {
                var success = response.data.success;
                $log.debug("The delete sync record repsonse from server: " + response.data.success);
              });
            }
          }

          var updateChatList = function (chat) {
            var chats = [];
            chats.push(chat);
            updateRecentMessage(chats).then(function () {
              $scope.chats = $scope.chats.concat(chats);
            }).catch(function (err) {
              $log.debug("Error while updateRecentMessage: " + JSON.stringify(err));
            });
          };

          if (Tring.isMessage(newDoc)) {
            var sid = newDoc.sid;
            var index = Tring.findIndexBasedOnFieldValue($scope.chats, "sid", sid);
            $log.debug("ChatCtrl Message DB change: Modifying Chat index = " + index);
            if (Tring.isEvictionMessage(newDoc)) {
              $log.debug("Updating evicted status of the handle");
              ChatService.updateDocument(sid, "userGroupStatus", ConstsService.USER_GROUP_STATUS_EVICTED).then(function (group) {
                if (group) {
                  $log.debug("Unsubscribing to group topic: " + group.topic);
                  if (group.topic) {
                    MqttBrokerService.unsubscribeToStatus(group.topic);
                  }
                  deleteSyncRecord(newDoc.syncId);
                }
              }).catch(function (err) {
                $log.debug("Updating of group failed for eviction message: " + JSON.stringify(err));
              });
            }
            else if (Tring.isWarningMessage(newDoc)) {
              deleteSyncRecord(newDoc.syncId)
            }
            if (index >= 0) {
              $scope.$evalAsync(function () {
                var chat = $scope.chats[index];
                chat.message = {
                  data: newDoc.data,
                  ts: parseInt(newDoc.ts),
                  handleName: newDoc.handleName,
                  dir: newDoc.dir
                };
                $log.debug("New message arrived in ChatCtrl: " + JSON.stringify(newDoc));
                var activeMsgId = SharedDataService.getValue(ConstsService.ACTIVE_MESSAGING_SID);
                if (activeMsgId !== sid && newDoc.dir === 0) {
                  chat.unReadCount = chat.unReadCount + 1;
                }
                //                            if(activeMsgId == null) {
                //                                $ionicScrollDelegate.scrollTop(true);
                //                            }
              });
            } else {
              if (Tring.startsWith(sid, ConstsService.USERS_START_KEY)) {
                var chatId = ConstsService.CHAT_PREFIX + sid;
                ChatService.findChat(chatId).then(function (doc) {
                  $log.debug("The chat found: " + JSON.stringify(doc));
                  if (!doc) {
                    // Get the user details from the server
                    var p1 = function () {
                      var userId = sid.substring(ConstsService.USERS_START_KEY.length, sid.length);
                      return $http.get(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_BY_ID_URL + "/" + userId);
                    };

                    var p2 = function (response) {
                      var user = response.data;
                      $log.debug("Server side user data: " + JSON.stringify(user));

                      var q1 = function () {
                        return UserService.getAllUsers();
                      };

                      var q2 = function (users) {
                        var existingUsers = users.filter(function (_user) {
                          return _user.phoneNumber === user.mobileNo;
                        });

                        return $q.when(existingUsers);

                      };
                      var q3 = function (existingUsers) {
                        var userData = {};
                        if (existingUsers.length > 0) {
                          $log.debug("Existing user found and snapping it to first: " + JSON.stringify(existingUsers[0]));
                          userData.user = existingUsers[0];
                          userData.existing = true;
                          return $q.when(userData);
                        } else {
                          var _q1 = function () {
                            return UserService.createServerUser(user);
                          };

                          var _q2 = function () {
                            userData.user = user;
                            userData.existing = false;
                            return $q.when(userData);
                          };
                          return $q.serial([_q1, _q2]).catch(function (err) {
                            if (err.status === 409) {
                              userData.user = user;
                              userData.existing = false;
                              return $q.when(userData);
                            } else {
                              $log.debug("Should not have reached here!: " + JSON.stringify(err));
                            }
                          });
                        }
                      };
                      return $q.serial([q1, q2, q3]);
                    };

                    var p3 = function (userData) {
                      var _user = userData.user;
                      MqttBrokerService.subscribeToStatus(_user.topic);
                      if (userData.existing) {
                        return ChatService.saveSingleUserChat(chatId, sid, _user._id);
                      } else {
                        $log.debug("Saving single user chat");
                        return ChatService.saveSingleUserChat(chatId, sid, ConstsService.USERS_START_KEY + _user.mobileNo);
                      }
                    };

                    $q.serial([p1, p2, p3]).then(function (chat) {
                      $log.debug("Chat record created for sid: " + sid);
                      updateChatList(chat);
                    }).catch(function (err) {
                      $log.debug("Error during chat record creation: " + JSON.stringify(err));
                      if (err.status === 409) {
                        ChatService.findChat(chatId).then(function (_chat) {
                          updateChatList(_chat);
                        });
                      } else {
                        $log.debug("Should not have reached here!: " + JSON.stringify(err));
                      }
                    });
                  } else {
                    $log.debug("This is an existing chat");
                  }
                });
              }
              else {
                var chatId = ConstsService.CHAT_PREFIX + sid;
                ChatService.findChat(chatId).then(function (_chat) {
                  updateChatList(_chat);
                })
              }
            }
          }
        };

        MessagesService.registerMessageDBChange(dbChangesCallBack);

        var connectionSuccessCB = function () {
          ChatService.initDbs(false, false).then(function () {

            $log.debug("Inited dbs...");
            var p1 = function () {
              return UserService.getAllRegisteredUsers();
            };

            var p2 = function (users) {
              users.map(function (user) {
                if (user.topic !== device.p2p) {
                  var statusTopic = MqttBrokerService.getStatusTopic(user.topic);
                  var profileTopic = MqttBrokerService.getProfileStatusTopic(user.topic);
                  $log.debug("statusTopic: " + statusTopic);
                  MqttBrokerService.addToSubscription(statusTopic);
                  MqttBrokerService.addToSubscription(profileTopic);
                }

              });
              return $q.when();
            };

            var p3 = function () {
              return GroupService.getLocalGroups();
            };

            var p4 = function (groups) {
              // TODO - Revisit handling of user handles
              var handles = [];
              groups.map(function (group) {
                $log.debug("Group found: " + JSON.stringify(group));
                var userGroupStatus = Tring.getUserGroupStatus(group);
                if (group.topic && userGroupStatus !== ConstsService.USER_GROUP_STATUS_EVICTED &&
                   userGroupStatus !== ConstsService.USER_GROUP_STATUS_LEFT &&
                   userGroupStatus !== ConstsService.USER_GROUP_STATUS_REJECTED
                   ) {
                  var pgid = Tring.getParentGroupId(group);
                  var userRole = Tring.getGroupMembershipRole(group);
                  if(userRole != ConstsService.NON_MEMBER_ROLE) {
                    MqttBrokerService.addToSubscription(group.topic);
                    var statusTopic = MqttBrokerService.getStatusTopic(group.topic);
                    MqttBrokerService.addToSubscription(statusTopic);
                    if(!pgid && userRole < ConstsService.MODERATOR_ROLE) {
                      // Owners and Admins have to subscribe to pvt topic
                      MqttBrokerService.addToSubscription(group.topic + ConstsService.PVT_SUB_TOPIC);
                    }
                    else if(pgid && userRole <= ConstsService.MODERATOR_ROLE) {
                      // Subgroup moderators have to subscribe to pvt topic
                      MqttBrokerService.addToSubscription(group.topic + ConstsService.PVT_SUB_TOPIC);
                    }
                    $log.debug("Group handleName: " + group.handleName);
                    if (group.handleName) {
                      handles[group._id] = group.handleName;
                    }
                  }
                }
              });
              MqttBrokerService.setUserGroupHandles(handles);
              return $q.when();
            };

            var p5 = function () {
              MqttBrokerService.onConnect();
            };

            $q.serial([p1, p2, p3, p4]).then(p5).catch(function (err) {
              $log.debug("Error during connectionSuccessCB: " + JSON.stringify(err));
            });
          });
        };

        var connectionFailCB = function () {
          $log.debug("ConnectionFailCB..");
        };

        MqttBrokerService.registerCallBacks(connectionSuccessCB, connectionFailCB);

        var connectToBroker = function () {
          $timeout(function () {
            MqttBrokerService.connect(true);
          }, 10);
        };

//Commenting for affiliate marketing version
/*        if (!device.bootstrapState || device.bootstrapState < 2) {
          //kick in bootstrapping of contacts
          $log.debug("kick in bootstrapping of contacts");
          UserService.bootstrapContacts().finally(function () {
            connectToBroker();
          });
        } else if (device.bootstrapState < 3) {
          //kick in bootstrapping of profile info
          $log.debug("kick in bootstrapping of profile info");
          UserService.bootstrapProfileInfo().finally(function () {
            connectToBroker();
          });
        } else {
          connectToBroker();
        }*/
        connectToBroker();
        var checkDeviceValidity = function () {
          return $http.get(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.DEVICE_VALIDATE_URL);
        };

        $timeout(function () {
          checkDeviceValidity().then(function (res) {
            $log.debug("Device validity: " + JSON.stringify(res));
          });
        }, 10);

        $rootScope.$on("inValidDeviceFound", function () {
          $log.debug("Deactivating device");
          UserService.deactivateDevice().then(function () {
            MqttBrokerService.disconnect();
            $ionicHistory.clearHistory();
            $ionicHistory.clearCache();
            $ionicHistory.nextViewOptions({
              historyRoot: true
            });
            //$state.go('login');
            NavigationService.goNativeToView('login', null, 'right');
          });
        });
        var mqttDisconnectPromise;
        var mqttReconnectCB = function () {
          if(mqttDisconnectPromise) {
            var cancelled = $timeout.cancel(mqttDisconnectPromise);
            if(cancelled) {
              $log.debug("Successfully cancelled mqttDisconnectPromise");
            }
          }
          MqttBrokerService.connect();
          $log.debug("OnResumeCB: Sending online message!");
          MqttBrokerService.sendOnlineMessage();
        }

        NetworkService.addOnlineCB(mqttReconnectCB);

        var onPauseCB = function () {
          $log.debug("OnPauseCB: Sending offline message!");
          MqttBrokerService.sendOfflineMessage(function () {
            mqttDisconnectPromise = $timeout(MqttBrokerService.disconnect,
                                              ConstsService.MQTT_EXPLICIT_DISCONNECT_WAITING_PERIOD);
            mqttDisconnectPromise.then(function (){
              $log.debug("MqttBrokerService.disconnect called");
            }).catch(function(err) {
              $log.debug("Error due disconnection: " + JSON.stringify(err.message));
            }).finally(function (){
              $log.debug("mqttDisconnectPromise set to null");
              mqttDisconnectPromise = null;
            });
          });
        };

        var onPauseDebounce = Tring.debounce(onPauseCB, 5000);
        var onResumeDebounce = Tring.debounce(mqttReconnectCB, 5000);
        var onPauseWrapper = function () {
          onResumeDebounce.cancel();
          onPauseDebounce.callback();
        }

        var onResumeWrapper = function () {
          onPauseDebounce.cancel();
          onResumeDebounce.callback();
        }

        NetworkService.addOnPauseCB(onPauseWrapper);
        NetworkService.addOnResumeCB(onResumeWrapper);

        $scope.$on('$ionicView.enter', function () {
          $rootScope.hideTabs = '';
          $log.debug("$stateParams: " + JSON.stringify($stateParams))
          var fwdedMsgId = $stateParams.messageId;
          $scope.noChatsToShow = false;
          $log.debug("fwdedMsgId: " + fwdedMsgId);
          $scope.fwdedMsgId = fwdedMsgId;
          $rootScope.activeScreen.current = $rootScope.activeScreen.chats;
          ChatService.resetChatOptions();
          ChatService.getNextChatPage().then(function (chats) {
            if(chats && chats.length > 0) {
              //var startMoment = moment();
              updateRecentMessage(chats, fwdedMsgId).then(function () {
                //$log.debug("The diff time: " + moment().diff(startMoment));
                $scope.chats = chats;
                if ($state.current.name === 'app.chats') {
                   $ionicScrollDelegate.scrollTop(true);
                }
              }).catch(function (err) {
                $log.debug("Error while updateRecentMessage during initial chat load: " + JSON.stringify(err));
              });
              ;
            }
            else {
              $scope.$evalAsync(function () {
                $scope.noChatsToShow = true;
              })
            }
          });
        });
        $scope.$on("contactProfileChanged", function (evt, contact) {
          angular.forEach($scope.chats.filter(function (chat) {
            return chat.localSid === contact._id;
          }), function (chat) {
            chat.thumbnailData = contact.thumbnailData ? contact.thumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
          });

        });
        $scope.go = function (path) {
          //$state.go(path);
          NavigationService.goNativeToView(path, null, 'right');
          $log.debug($ionicHistory.viewHistory());
        };

        function _goToMessage(chat) {
          chat.unReadCount = 0;
          $log.debug("Clicked Chat Info: " + JSON.stringify(chat));
          var data = {
            "senderId": chat.sid,
            //This is required for getSenderIdData(), since we do a local db query on the Contacts/Group data there,
            //and the Contacts record will have a local id that is not based on the server id for the Contact. In
            //the case of Groups where there is no concept of a 'local' Group (unlike Contacts), the localSid and
            //sid are the same.
            "localSenderId": chat.localSid,
            "fwdedMsgId": $scope.fwdedMsgId
          };
          NavigationService.goNativeToView('app.chat-ui', data, 'right');
        }


        $scope.goToMessage = function (chat) {
          var fwdedMsgId = $scope.fwdedMsgId;
          if (fwdedMsgId) {
            $ionicPopup.confirm({
              template: 'Forwarding message to  ' + chat.name
            }).then(function (res) {
              if (res) {
                _goToMessage(chat);
              } else {
                $log.debug("Cancelled forwarding message")
              }
            });
          }
          else {
            _goToMessage(chat)
          }
        };
        //Function to go back a step using $ionicHistory
        $scope.goBackAStep = function () {
          $log.debug('clicked');
          //$ionicHistory.goBack();
          NavigationService.goNativeBack();
        };

      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }]);
