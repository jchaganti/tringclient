angular.module('tring.navigation.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
  .controller('NavigationCtrl', ['$scope', '$state', '$ionicHistory', 'AuthService', '$rootScope', 'NavigationService', '$location', 'ConstsService', 'UserService', 'ChatService', '$cordovaToast', '$timeout', '$interval', '$log',function ($scope, $state, $ionicHistory, AuthService, $rootScope, NavigationService, $location, ConstsService, UserService, ChatService, $cordovaToast, $timeout, $interval, $log) {
    // Function to go states. We're using ng-click="go('app.state')" in all our anchor tags
    var bootstrapState = AuthService.getDevice().bootstrapState;
    $scope.activeScreen = $rootScope.activeScreen;
    //var device = AuthService.getDevice();
//    var selfInfo;
//    UserService.getDeviceFromDb().then(function (info) {
//      selfInfo = info;
//      $scope.myName = selfInfo.profile && selfInfo.profile.name ? selfInfo.profile.name : "";
//      $scope.myThumbnailData = selfInfo.profile && selfInfo.profile.profileThumbnailData ? selfInfo.profile.profileThumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
//    });

    UserService.userProfile().then(function(profile){
      $scope.myName = profile && profile.name ? profile.name : "";
      $scope.myThumbnailData = profile && profile.profileThumbnailData ? profile.profileThumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
    });

    $log.debug("$scope.activeScreen is " + JSON.stringify($scope.activeScreen));
    $log.debug("NavigationCtrl- bootstrapState is " + bootstrapState);
    var deviceActiveTid = null;
    $scope.$on("selfProfileChange", function (evt, profile) {
      $scope.myName = profile.name;
      $scope.myThumbnailData = profile.profileThumbnailData ? profile.profileThumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
    });

    $scope.burgerMenu = true;
    $scope.backButton = false;

    $scope.showBurger = function() {
      $scope.$evalAsync(function() {
        $scope.burgerMenu = true;
        $scope.backButton = false;
      });

    };

    $scope.showBackButton = function() {
      $scope.$evalAsync(function() {
        $scope.burgerMenu = false;
        $scope.backButton = true;
      });

    };

    //select active state
    $scope.getClass = function (path) {
      if ($location.path() === path) {
        return 'active';
      } else {
        if ($location.path().substr(0, path.length) === path) {
          return 'active';
        } else {
          return '';
        }
      }
    };
    // For the affiliate marketing version. Changing to true from 'bootstrapState >= 2';
    $scope.bootstrapComplete = true;
    $scope.go = function (path) {
      //$state.go(path);
      NavigationService.goNativeToView(path, null, 'right');
      $log.debug($ionicHistory.viewHistory());
    };
    //Function to go back a step using $ionicHistory
    $scope.goBackAStep = function () {
      $log.debug('clicked');
      //$ionicHistory.goBack();
      NavigationService.goNativeBack();
    };


    UserService.getGroupsRestored().then(function (groupsRestoredRecord) {
      var groupsRestored = groupsRestoredRecord && groupsRestoredRecord.done;
      $log.debug(" groupsRestored: " + groupsRestored);
      $scope.groupsRestored = groupsRestored;
      if(!groupsRestored) {
        var device = AuthService.getDevice();
        if (device && device.status == 1 && (!device.hasOwnProperty('active') || device.active)) {
          ChatService.startRestoringGroups().then(function (result) {
            $log.debug("startRestoringGroups in NavigationCtrl result: " + JSON.stringify(result));
            if(result.wasGroupRestoringInProgress) {
              $log.debug("Ignoring startRestoringGroups in NavigationCtrl since it is already in progress");
            }
            else {
              UserService.setGroupsRestored().then(function (_res) {
                $rootScope.$broadcast("groupsRestored", {
                  result: result
                });
              }).catch(function (err) {
                $log.debug("Error occurred during UserService.setGroupsRestored. Hence re-trying");
                UserService.setGroupsRestored().then(function (_res) {
                  $rootScope.$broadcast("groupsRestored", {
                    result: result
                  });
                }).catch(function (err) {
                  result.groupsRestoredSuccess = false;
                  $rootScope.$broadcast("groupsRestored", {
                    result: result
                  });
                });
              });
            }
          });
        }
        else {
          $log.debug("The groups will restored after verifyPIN");
        }
      }
      else {
        $scope.$evalAsync(function () {
          $scope.groupsRestored = true;
        });
      }
    })

    var push = PushNotification.init({
      "android": {
        senderID: "947222147905",
        icon: "ic_noti",
        iconColor: "#03A9F4"
      },
      "ios": {
        alert: "true",
        badge: "true",
        sound: "true"
      },
      "windows": {}
    });

    push.on('registration', function(data) {
      // data.registrationId
      var registrationId = data.registrationId
      $log.debug("Registration ID: " + data.registrationId);
      var pnId = AuthService.getDevice().pnId;
      if(registrationId !== pnId) {
        var postTrackCB = function () {
          UserService.handlePushNotificationRegistration(registrationId).then(function (res) {
            if(res) {
              $log.debug("Device registration for push notification successful");
            }
            else {
              $log.debug("Device registration for push notification failed");
            }
          });
        }
        var trackingCondition = function () {
          var _device = AuthService.getDevice();
          return  _device.status === 1 && _device.active === true;
        }
        Tring.trackAndCall(trackingCondition, null, postTrackCB, 100, $interval);
      }
      else {
        $log.debug("Registration ID is same as existing one");
      }
    });

    push.on('notification', function(data) {
      // data.message,
      // data.title,
      // data.count,
      // data.sound,
      // data.image,
      // data.additionalData
      $log.debug("data: " + JSON.stringify(data))
    });

    push.on('error', function(e) {
      $log.debug("Error from PushNotification: " + e);
    });


    $scope.$on("groupsRestored", function (event, args) {
      var result = args.result.groupsRestoredSuccess;
      if(args.result.groupsRestoredSuccess) {
        $scope.$evalAsync(function () {
          $scope.groupsRestored = true;
        });
      }
      else {
        $log.debug("Something went wrong. The groups could not be restored!");
      }
      $log.debug("groupsRestored event handled");
    });

    $scope.$on("bootstrapComplete", function () {
      $scope.bootstrapComplete = true;
      $log.debug("bootstrapComplete (refresh contacts initially) event handled");
    });

    $scope.onManageGroupsSelected = function () {
      $timeout(function () {
        NavigationService.goNativeToView("app.groups.manage", null, 'right');
      }, 10)

    }
  }]);
