angular.module('tring.fileBrowse.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller("FileBrowseController", ['$scope', '$ionicPlatform', 'FileBrowseService', '$ionicPopup', '$ionicHistory', '$q', 'SharedDataService', 'ConstsService', 'Tring', 'NavigationService', 'ThumbnailService', '$ionicScrollDelegate', '$rootScope', '$timeout', '$log', function ($scope, $ionicPlatform, FileBrowseService, $ionicPopup, $ionicHistory, $q, SharedDataService, ConstsService, Tring, NavigationService, ThumbnailService, $ionicScrollDelegate, $rootScope, $timeout, $log) {
        var SIXTEEN_MB = 16 * 1024 * 1024;
        $log.debug("SIXTEEN_MB is " + SIXTEEN_MB);
        $log.debug("Entered FileBrowseController");
        var fs = new FileBrowseService();
        SharedDataService.deleteValues(ConstsService.CONTENT_ID, ConstsService.CONTENT_NAME);

        $scope.startIndex = 0;

        var PAGE_SIZE = 20;

        $scope.thumbnailUpdateTasks = [];
        $scope.thumbnails = {};

        var resetThumbnails = function () {
            $scope.thumbnails = {};
            angular.forEach($scope.thumbnailUpdateTasks, function (taskPromise) {
                $timeout.cancel(taskPromise);
            });
            $scope.thumbnailUpdateTasks = [];
        };

        $scope.thumbnailForFile = function (file) {
            if (file.isDirectory) {
                return $q.resolve();
            }
            return $scope.getMimeType(file.nativeURL).then(function (type) {
                var func = ThumbnailService.getThumbnailFunction(type);
                return func(file.nativeURL, 50).then(function (dataUrl) {
                    $scope.thumbnails[file.name] = dataUrl;
                    return true;
                }, function (err) {
                    $log.debug('thumbnailForData Bad or no thumbnail data- err is ' + JSON.stringify(err));
                    return $q.reject(err);
                });
            }).then(null, function (err) {
                $log.debug('thumbnailForData Bad or no mime type- err is ' + JSON.stringify(err));
                return $q.reject(err);
            });
        };

        $scope.getMimeType = function (path) {
            var deferred = $q.defer();
            window.resolveLocalFileSystemURL(path, function (fileEntry) {
                fileEntry.file(function (file) {
                    $log.debug("file name is " + file.name);
                    $log.debug("file type is " + file.type);
                    var contentType = file.type ? file.type : Tring.mimeType(file.name);
                    deferred.resolve(contentType);
                }, function (err) {
                    deferred.reject(err);
                });
            }, function (err) {
                deferred.reject(err);
            });
            return deferred.promise;
        };

        var createThumbnailsForFileInPage = function () {
            angular.forEach($scope.files, function (file) {
                if (file.name === "[root]" || file.name === "[up]") {
                    return;
                }
                $scope.thumbnailUpdateTasks.push($timeout(function () {
                    return $scope.thumbnailForFile(file);
                }, 10));
            });
        };

        $scope.addFilesPaged = function (noInfiniteScrollBroadcast) {
            var len;
            var tmpStart, tmpEnd;

            if (noInfiniteScrollBroadcast) {
                tmpStart = $scope.startIndex;
                $scope.noMorePreviousFilesAvailable = true;
            } else {
                tmpStart = $scope.endIndex;
            }
            len = (tmpStart + PAGE_SIZE);
            tmpEnd = (len > $scope.filesAll.length) ? $scope.filesAll.length : len;
            if (tmpStart < tmpEnd) {

                resetThumbnails();
                $scope.noMorePreviousFilesAvailable = false;
                $scope.noMoreFilesAvailable = false;

                $scope.files = $scope.filesAll.slice(tmpStart, tmpEnd);
                $scope.startIndex = tmpStart;
                $scope.endIndex = tmpEnd;
                $log.debug("somewhere in the middle of file listing - " + $scope.startIndex + ' ' + $scope.endIndex);
                $ionicScrollDelegate.scrollTop(false);
                createThumbnailsForFileInPage();
            } else {
                $scope.noMoreFilesAvailable = true;
                $log.debug("reached end of file listing - " + $scope.startIndex + ' ' + $scope.endIndex);
            }
            if (!noInfiniteScrollBroadcast) {
                $timeout(function() {
                  $scope.$broadcast('scroll.infiniteScrollComplete');
                },2000);
            }
        };

        $scope.loadPreviousFiles = function () {
            var tmpStart, tmpEnd;
            tmpEnd = $scope.startIndex;
            if (tmpEnd === 0) {
                $scope.noMorePreviousFilesAvailable = true;
                $log.debug("reached start of file listing - " + $scope.startIndex + ' ' + $scope.endIndex);
                return;
            }
            tmpStart = tmpEnd - PAGE_SIZE;

            if (tmpStart < 0) {
                tmpStart = 0;
            }
            $scope.noMorePreviousFilesAvailable = false;
            $scope.noMoreFilesAvailable = false;

            resetThumbnails();

            $scope.files = $scope.filesAll.slice(tmpStart, tmpEnd);
            $scope.startIndex = tmpStart;
            $scope.endIndex = tmpEnd;
            $log.debug("somewhere in the middle of file listing - " + $scope.startIndex + ' ' + $scope.endIndex);
            $scope.$broadcast('scroll.refreshComplete');
            createThumbnailsForFileInPage();
            $ionicScrollDelegate.scrollTop(false);

        };

        var getRootEntries = function () {
            return fs.getEntriesAtRoot().then(function (result) {
                $scope.filesAll = result;
                $log.debug("getEntriesAtRoot : result :-");
                $log.debug(JSON.stringify(result));
                return result;
            }, function (error) {
                console.error(JSON.stringify(error));
                return $scope.filesAll ? $scope.filesAll : [];
            });
        };

        getRootEntries().then(function () {
            $scope.addFilesPaged(true);
        });

        $scope.getContents = function (path) {
            return fs.getEntries(path).then(function (result) {
                $scope.filesAll = result;
                $log.debug("File Entries are :- " + JSON.stringify(result));
                $scope.filesAll.unshift({
                    name: "[root]"
                });
                return fs.getParentDirectory(path).then(function (result) {
                    result.name = "[up]";
                    $scope.filesAll[0] = result;
                    return $scope.filesAll;
                }).then(function (filesAll) {
                    $ionicScrollDelegate.scrollTop(false);
                    return filesAll;
                }).then(null, function (err) {
                    return getRootEntries();
                }).then(function () {
                    $scope.addFilesPaged(true);
                });
            });
        };

        $scope.uploadFile = function (path) {
            $log.debug("uploadFile: path is " + JSON.stringify(path));
            window.resolveLocalFileSystemURL(path, function (fileEntry) {
                var deferred = $q.defer();
                $log.debug(JSON.stringify("fileEntry is :- " + fileEntry));
                var contentName, contentType;
                var thumbnailDataPromise;
                //Get file info, do some size checks and and ask confirmation from user showing thumbnail if possible
                fileEntry.file(function (file) {
                    $log.debug("file name is " + file.name);
                    $log.debug("file size is " + file.size);
                    $log.debug("file type is " + file.type);

                    contentName = path.split('/').pop();
                    contentType = file.type ? file.type : Tring.mimeType(contentName);
                    var thumbnailFunction = ThumbnailService.getThumbnailFunction(contentType);
                    if (!file.size || file.size > SIXTEEN_MB) {
                        $ionicPopup.alert({
                            title: 'File Upload Error!',
                            template: (!file.size) ?
                                'Can\'t determine file size or size is 0 bytes' : 'File size is ' + file.size + ' bytes and exceeds the upload limit of 16 MB'
                        }).then(function () {
                            $log.debug('File upload error alert exited');
                        });
                        deferred.reject("exceeds upload file size limit");
                    } else {
                        thumbnailDataPromise = thumbnailFunction(fileEntry.nativeURL, 100);

                        var confirmUpload = function (thumbnailDataUrl) {
                            var confirmUploadOptions = fs.confirmContentUploadOptions($scope, file.name, file.type, file.size, thumbnailDataUrl);
                            return $ionicPopup.show(confirmUploadOptions).
                            then(function (caption) {
                                if (!caption.error) {
                                    deferred.resolve({
                                        size: file.size,
                                        type: file.type,
                                        caption: caption
                                    });
                                } else {
                                    deferred.reject("upload canceled");
                                }
                                $log.debug('File upload confirm exited');
                            });
                        };
                        thumbnailDataPromise.then(confirmUpload, function () {
                            $log.debug('error creating thumbnail in uploadFile');
                            confirmUpload("");
                        });
                    }
                });
                //Once confirmed by user, send content message
                deferred.promise.then(function (fileMetadata) {
                    var contentName = path.split('/').pop();
                    var onprogress = function (progressEvent) {
                        $rootScope.$broadcast('fileTransferProgress', progressEvent, fileEntry.name || contentName);
                    };
                    $log.debug("fileMetadata.type " + fileMetadata.type);
                    $log.debug("fileMetadata.caption is " + fileMetadata.caption);

                    $log.debug(JSON.stringify(contentType));
                    $log.debug(JSON.stringify(contentName));
                    var retVal =
                        fs.upload(path, {
                            fileName: fileEntry.name || contentName,
                            mimeType: contentType
                        }, onprogress);

                    var returnPromise = retVal.promise.then(function (result) {
                        $log.debug("Upload Success");
                        var doc = JSON.parse(result.response);
                        var contentId = doc.contentId;


                        //result has contentId, which can be used in the message
                        var callingView = $ionicHistory.backView();
                        $log.debug("callingView obj is :-");
                        $log.debug(JSON.stringify(callingView));
                        return contentId;
                    }, function (err) {
                        console.error("Upload Error");
                        console.error(JSON.stringify(err));
                        throw err;
                    });

                    function contentMessageSend(thumbnailData) {
                        SharedDataService.setValue(ConstsService.CONTENT_ID, returnPromise);
                        SharedDataService.setValue(ConstsService.CONTENT_NAME, contentName);
                        SharedDataService.setValue(ConstsService.CONTENT_CAPTION, fileMetadata.caption);
                        SharedDataService.setValue(ConstsService.CONTENT_TYPE, contentType);
                        SharedDataService.setValue(ConstsService.CONTENT_UPLOAD_CANCEL, function () {
                            retVal.cancel();
                        });
                        SharedDataService.setValue(ConstsService.CONTENT_LOCAL_URL, fileEntry.nativeURL);
                        SharedDataService.setValue(ConstsService.CONTENT_FULL_PATH, fileEntry.nativeURL);
                        SharedDataService.setValue(ConstsService.CONTENT_THUMBNAIL_DATA, thumbnailData);
                        //$ionicHistory.goBack();
                        NavigationService.goNativeBack();
                    }
                    thumbnailDataPromise.then(contentMessageSend).catch(function () {
                        contentMessageSend("");
                    });
                });
            });
        };
                    }]);
