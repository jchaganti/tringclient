var removeAdminAction = {
  name: "Remove Admin",
  cb: "removeAsAdmin",
  img: '<i class="icon icon-user-img text-dark-grey"></i>'
};

var addAdminAction = {
  name: "Add Admin",
  cb: "addAsAdmin",
  img: '<i class="icon icon-user-img text-orange"></i>'
};

var moderateAction = {
  name: "Moderate",
  cb: "moderate",
  img: '<i class="icon icon-modrater text-dark-grey"></i>'
};

var privateChatAction = {
  name: "Private Chat",
  cb: "sendPvtMessage",
  img: '<i class="icon ion-ios-email-outline"></i>'
}

var ownerAddAdminModerate = null;
var ownerRemoveAdminModerate = null;
var nonOwnerModerate = null;
var nonOwnerPvtChat = null;
var moderatorActions = null;
var nonModeratorActions = null;

function handleGroupMemberships( $scope, data, $stateParams, Tring, ConstsService, ManageGroupService, $log, $rootScope) {
  $scope.noMoreMembersAvailable = false;
  var pageSize, pageNum;
  $scope.$on('$ionicView.enter', function () {
    if($rootScope) {
      $rootScope.hideTabs = 'tabs-item-hide';
    }

    $log.debug("$stateParams Enter: " + JSON.stringify($stateParams))
    data.group = $stateParams.group;
    $scope.group = data.group;
    $scope.isOwner = Tring.isGroupOwner(data.group);
    var gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
    //TO-DO Use pagination
    pageSize = -1;
    pageNum = 1;
    ManageGroupService.getAllMembers(gid, data.group.handleName, pageNum, pageSize).then(function (membersData) {
      if (membersData && membersData.success) {
        $scope.$evalAsync(function () {
          $scope.admins = membersData["OWNER"].concat(membersData["ADMIN"]);
          $scope.members = membersData["MEMBER"];
        });
      }
      else {
        $log.debug("No members found for  this group: " + data.group.name);
      }
    }, function (err) {
      $log.debug("Error while getting first page of members: " + JSON.stringify(err));
    });
  });

  $scope.getNextPage = function () {
    var gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
    ManageGroupService.getAllMembers(gid, data.group.handleName, pageNum++, pageSize).then(function (membersData) {
      if (membersData && membersData.admin.length === 0 && membersData.member.length === 0) {
        if (membersData.admin.length > 0) {
          $scope.admins.concat(membersData.admin);
        }
        if (membersData.member.length > 0) {
          $scope.members.concat(membersData.member);
        }
      }
      else {
        $scope.noMoreMembersAvailable = true;
      }
      $scope.$broadcast('scroll.refreshComplete');
    }, function (err) {
      $log.debug("Error while getting next page of members: " + JSON.stringify(err));
    });
  }
}

function showSubGroups(memberValue, $rootScope) {
  var template = '<span class="holderName">Sub-Groups</span>';
  template += '<br/>';
  var asModerator = memberValue.asModerator;
  if (asModerator.length > 0) {
    template += '<span class="holderName">As Moderator:</span>';
    template += '<br/>';
    for (var k = 0; k < asModerator.length; k++) {
      var moderator = asModerator[k];
      var keys = $rootScope.Utils.keys(moderator);
      template += '<span class="holderName2">' + moderator[keys[0]] + '</span>';
      template += '<br/>';
    }
  }
  template += '<br/>'
  var asMember = memberValue.asMember;
  if (asMember.length > 0) {
    template += '<span class="holderName">As Member</span>';
    template += '<br/>'
    for (var k = 0; k < asMember.length; k++) {
      var member = asMember[k];
      var keys = $rootScope.Utils.keys(member);
      template += '<span class="holderName2">' + member[keys[0]] + '</span>';
      template += '<br/>';
    }
  }
  return template;
}

function showActionsPopOver(Tring, actions, varName, memberValue, $rootScope, $ionicPopover, $scope, $event) {
  var template = '<ion-popover-view class="sub-group-details"><ion-content>';
  template += 'Member Name: ' + memberValue.name;
  template += Tring.getPopOverActionsTemplate(actions, varName, 3);

  template += showSubGroups(memberValue, $rootScope);
  template += "</ion-content></ion-popover-view>";

  popOver = $ionicPopover.fromTemplate(template, {
    scope: $scope
  });
  $scope.currentPopover = popOver;
  popOver.show($event);
}

function handleSubGroupCreation(ConstsService, subGroupId, data, gid, Tring, resp, ChatService, $q) {
  var localSubGroup = {
    _id: ConstsService.GROUPS_START_KEY + subGroupId,
    name: subGroupName,
    handleName: data.group.handleName,
    parentGroupId: gid,
    membershipType: ConstsService.MODERATOR_ROLE,
    topic: Tring.getGroupMQTTtopic(resp.topic)
  }

  var mbrDb = DBService.mbrDb();
  var _p1 = function () {
    return mbrDb.put(localSubGroup);
  }

  var _p2 = function (res) {
    var chatId = ConstsService.CHAT_PREFIX + subGroupId;
    return ChatService.saveGroupChat(localSubGroup, chatId);
  }

  var _p3 = function (chat) {
    subscribeTo(localSubGroup.topic);
    return $q.when(localSubGroup);
  }

  return $q.serial([_p1, _p2, _p3]);
}



function handleModerate($scope, memberData, Tring, ConstsService, groupId, $ionicPopup, $log) {
  $scope.currentPopover.hide();
  $log.debug("moderate memberData: " + JSON.stringify(memberData));
  $scope.adminContext = true;
  $scope.moderateHandleName = memberData.memberValue.name;
  $scope.processingGroupMembershipRequest = true;
  $scope.data = {};
  $scope.data.gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, groupId);
  $scope.data.handleName = memberData.memberValue.name
  return $ionicPopup.show({
    cssClass: 'modratePopup',
    templateUrl: 'templates/rubyonic/groupModratorPopup.html',
    //title: '<a class="button icon-left ion-close button-clear" style="font-size: smaller;" href="#;" ng-click="closePopup()"></a>',
    title: '',
    scope: $scope
  });


}
angular.module('tring.manageGroup.controller', ['tring.authService', 'tring.manageGroupService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'ionic.closePopup'])
  .controller('ManageGroupCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ManageGroupService', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopup', '$stateParams', '$cordovaToast', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ManageGroupService, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopup, $stateParams, $cordovaToast, $log) {
      $log.debug("ManageGroupCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group " + device);
      if (device) {
        $scope.groups = null;
        $scope.noGroupsToManage = false;
        $scope.$on('$ionicView.enter', function () {
          //$scope.showBurger();
          if ($state.params.pendingCaptureResults) {
              NavigationService.goNativeToView("app.groups.networkDetails", $state.params, 'right');
          } else {
              ManageGroupService.getAllParentGroups().then(function (groups) {
                $log.debug("All parent groups: " + JSON.stringify(groups));
                if(groups && groups.length > 0) {
                  $scope.$evalAsync(function () {
                    $scope.groups = groups;
                    $scope.noGroupsToManage = false;
                  });
                }
                else {
                  $scope.$evalAsync(function () {
                    $scope.noGroupsToManage = true;
                  });
                }

              });
          }
        }, function (err) {
          $log.debug("Error while getting all parent groups: " + JSON.stringify(err));
        });

        var dbChangesCallBack = function (change) {
          var newGroup = change.doc;
          $log.debug("dbChangesCallBack called for parent groups with newGroup: " + JSON.stringify(newGroup));
          if(newGroup.membershipType === ConstsService.ADMIN_ROLE) {
            var index = Tring.findIndexBasedOnFieldValue($scope.groups, "_id", newGroup._id);
            $log.debug("The add index found: " + index);
            if(index < 0) {
              $log.debug("This is a newGroup. Hence adding it");
              $cordovaToast.showLongCenter("You have been added as admin of a group. Hence updating the list of groups you are managing.");
              $scope.$evalAsync(function () {
                $scope.groups.push(newGroup);
              });
            }
            else {
              $log.debug("This is an existing newGroup. Hence ignoring it");
            }
          }
          else if(newGroup.membershipType === ConstsService.MEMBER_ROLE || newGroup.membershipType === ConstsService.NON_MEMBER_ROLE) {
            var index = Tring.findIndexBasedOnFieldValue($scope.groups, "_id", newGroup._id);
            $log.debug("The remove index found: " + index);
            if(index >= 0 && index < $scope.groups.length) {
              $log.debug("You have been removed as admin");
              $cordovaToast.showLongCenter("You have been removed as admin of a group. Hence updating the list of groups you are managing.");
              $scope.$evalAsync(function () {
                $scope.groups.splice(index, 1);
              });
            }
          }
        }

        ChatService.registerMbrDBChange(dbChangesCallBack);

        $scope.handleGroup = function (group) {
          $log.debug("Clicked Group Info: " + JSON.stringify(group));
          var data = {
            group: group
          };
          NavigationService.goNativeToView('app.manageGroup.home', data, 'right');
        }

        $scope.isGroupShown = function (group) {
          return $scope.shownGroup && $scope.shownGroup._id === group._id;
        };

        $scope.handleMemberships = function (group) {
          $log.debug("Clicked Group handleMemberships of group: " + JSON.stringify(group));
          NavigationService.goNativeToView('app.groups.members', {group: group}, 'right');
        };

        $scope.handleInviteMembers = function (group) {
         $log.debug("Clicked Group handleInviteMembers of group: " + JSON.stringify(group));
         NavigationService.goNativeToView('app.groups.inviteNonMemberContactsFromManageGroups', {
             group: group,
             inviterIsOwnerOrAdmin: true
         }, 'right');
        };

        $scope.handleListSubgroups = function (group) {
          $log.debug("Clicked Group handleListSubgroups of group: " + JSON.stringify(group));
          NavigationService.goNativeToView('app.groups.listSubNetworks', {group: group}, 'right');
        };

        $scope.handleGroupDetails = function (group) {
          $log.debug("Clicked Group handleGroupDetails of group: " + JSON.stringify(group));
          NavigationService.goNativeToView('app.groups.networkDetails', {group: group, readOnly: false}, 'right');
        };

        $scope.toggleGroup = function (group) {
          if ($scope.isGroupShown(group)) {
            $scope.shownGroup = null;
          } else {
            $scope.shownGroup = group;
          }
        };
      } else {
        NavigationService.goNativeToView('login', null, 'left');
      }

    }])
  .controller('ManageGroupMembershipsCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', 'ManageGroupService', '$cordovaToast', '$ionicPopup', 'IonicClosePopupService', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, ManageGroupService, $cordovaToast, $ionicPopup, IonicClosePopupService, $log) {
      $log.debug("ManageGroupMembershipsCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Memberships " + device);
      var data = {};
      if (device) {
        //$scope.showBackButton();
        $log.debug("$stateParams: " + JSON.stringify($stateParams))
        $scope.group = $stateParams.group;

        $scope.$on('$ionicView.beforeLeave', function () {
          $rootScope.hideTabs = '';
        });
        handleGroupMemberships( $scope, data, $stateParams, Tring, ConstsService, ManageGroupService, $log, $rootScope);
      } else {
        NavigationService.goNativeToView('login', null, 'left');
      }

      function handleAdmin(memberData, action, check) {
        var adminHid = memberData.memberKey;
        if ((!angular.isFunction(check) || check()) && adminHid) {
          //gid, handleName, action, adminHid
          var gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
          var handleName = data.group.handleName;

          ManageGroupService.handleAdmin(gid, handleName, action, adminHid).then(function (data) {
            if (data.success) {
              $log.debug("Admin action: " + action + " is successful");
              var srcArr = action === 'add' ? $scope.members : $scope.admins;
              var dstArr = action === 'add' ? $scope.admins : $scope.members;

              $scope.$evalAsync(function () {
                srcArr.splice(memberData.index, 1);
                var _mbr = {};
                _mbr[adminHid] = memberData.memberValue;
                dstArr.push(_mbr);
              });

            }
            else {
              $log.debug("Admin action: " + action + " has failed");
            }
          }, function (err) {
            $log.debug("Error while performing Admin action" + action);
          });
        }
        else {
          if (!adminHid) {
            $log.debug("Admin Hid is not found!");
          }
          else {
            $cordovaToast.showLongTop("Adding as admin is not allowed for members who are already member of a sub-group!");
            $log.debug("Adding as admin is not allowed for members who are already member of a sub group");
          }
        }
      }

      $scope.addAsAdmin = function (memberData) {
        $scope.currentPopover.hide();
        var check = function () {
          return memberData.memberValue.asMember.length === 0;
        }
        $log.debug("addAsAdmin memberData: " + JSON.stringify(memberData));
        handleAdmin(memberData, 'add', check);
      }

      $scope.removeAsAdmin = function (memberData, index) {
        $scope.currentPopover.hide();
        $log.debug("removeAsAdmin memberData: " + JSON.stringify(memberData));
        handleAdmin(memberData, 'remove', null);
      }

      $scope.moderate = function (memberData) {
        var popUp = handleModerate($scope, memberData, Tring, ConstsService, $scope.group._id, $ionicPopup, $log);
        IonicClosePopupService.register(popUp);
      }

      $scope.sendPvtMessage = function (memberData) {
        $scope.currentPopover.hide();
        $log.debug("sendPvtMessage memberData: " + JSON.stringify(memberData));
        var _data = {
          senderId: data.group._id,
          localSenderId: data.group._id,
          toHandleName: memberData.memberValue.name
        }
        NavigationService.goNativeToView('app.chat-ui', _data, 'right');
      }

      $scope.getRandomColor = function(){
          var color =  Math.random().toString(16).slice(2, 8);
          return "background-color: #"+color+";";
      }

      $scope.showActions = function (isMemberAdmin, memberKey, memberValue, $event, index) {
        $scope.currentMemberData = {
          memberKey: memberKey,
          memberValue: memberValue,
          index: index
        };
        var actions = null;

        if ($scope.isOwner) {
          if (isMemberAdmin) {
            if(index > 0 && data.group.handleName !== memberValue.name) {
              if(ownerRemoveAdminModerate == null) {
                ownerRemoveAdminModerate = [];
                ownerRemoveAdminModerate.push(removeAdminAction);
                //ownerRemoveAdminModerate.push(moderateAction);
                ownerRemoveAdminModerate.push(privateChatAction);
              }
              actions = ownerRemoveAdminModerate;
            }
            else {
              $log.debug("Owner is clicking self. Hence ignoring!")
              return;
            }
          }
          else {
            if(ownerAddAdminModerate == null) {
              ownerAddAdminModerate = [];
              ownerAddAdminModerate.push(addAdminAction);
              ownerAddAdminModerate.push(moderateAction);
              ownerAddAdminModerate.push(privateChatAction);
            }
            actions = ownerAddAdminModerate;
          }
        }
        else {
          if(isMemberAdmin) {
            if(nonOwnerPvtChat == null) {
              nonOwnerPvtChat = [];
              nonOwnerPvtChat.push(privateChatAction);
            }
            actions = nonOwnerPvtChat;
          }
          else {
            if(nonOwnerModerate == null) {
              nonOwnerModerate = [];
              nonOwnerModerate.push(moderateAction);
              nonOwnerModerate.push(privateChatAction);
            }
            actions = nonOwnerModerate;
          }
        }
        var varName = 'currentMemberData';
        showActionsPopOver(Tring, actions, varName, memberValue, $rootScope, $ionicPopover, $scope, $event);
      }
    }])
  .controller('ManageSubGroupCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', 'ManageGroupService', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, ManageGroupService, $log) {
      $log.debug("ManageSubGroupCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Sub groups " + device);
      if (device) {
        $scope.subgroups = null;

        $scope.$on('$ionicView.enter', function () {
          $scope.showBackButton();
          var group = $stateParams.group;
          $log.debug("Parent group in sub groups listing: " + JSON.stringify(group));
          var pgid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, group._id);
          ManageGroupService.getAllSubgroups(ConstsService.GROUPS_START_KEY + pgid).then(function (subgroups) {
            $scope.$evalAsync(function () {
              $scope.group = group;
              $log.debug("Subgroups found: " + JSON.stringify(subgroups));
              $scope.subgroups = subgroups;
            });
          });
        }, function (err) {
          $log.debug("Error while getting all  subgroups: " + JSON.stringify(err));
        });

        var dbChangesCallBack = function (change) {
          var newSubGroup = change.doc;
          var pgid = Tring.getParentGroupId(newSubGroup);
          if(pgid && pgid === $scope.group._id) {
            var index = Tring.findIndex($scope.subgroups, newSubGroup._id);
            if(index < 0) {
              $log.debug("This is a new subgroup. Hence adding it");
              $scope.$evalAsync(function () {
                $scope.subgroups.concat(newSubGroup);
              });
            }
            else {
              $log.debug("This is an existing subgroup. Hence ignoring it");
            }
          }
        }

        ChatService.registerMbrDBChange(dbChangesCallBack);

        $scope.handleSubGroup = function (subgroup) {
          $log.debug("Clicked subGroup Info: " + JSON.stringify(subgroup));
          var data = {
            group: $scope.group,
            subgroup: subgroup
          };
          NavigationService.goNativeToView('app.groups.subGroupMemberships', data, 'right');
        }

        $scope.handleCreateSubGroup = function () {
          var data = {
            group: $scope.group
          };
          NavigationService.goNativeToView('app.groups.createSubNetwork', data, 'right');
        }
      } else {
        NavigationService.goNativeToView('login', null, 'left');
      }
    }])
  .controller('ManageSubGroupMembersCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', 'ManageGroupService', '$ionicPopup', 'IonicClosePopupService',
   '$log', function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, ManageGroupService, $ionicPopup, IonicClosePopupService, $log) {
      $log.debug("ManageSubGroupMembersCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Sub group Members " + device);
      var data = {};
      if (device) {
        $scope.$on('$ionicView.enter', function () {
          data.group = $stateParams.group;
          data.subgroup = $stateParams.subgroup;
          $scope.group = data.group;
          $scope.subgroup = data.subgroup;
          $scope.members = [];
          $scope.moderators = [];
          var gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
          var sgid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.subgroup._id);
          ManageGroupService.getAllSubGroupMembers(gid, data.group.handleName, sgid, 1, 50, false).then(
            function (membersData) {
              if (membersData && membersData.success) {
                $scope.$evalAsync(function () {
                  $scope.moderators = membersData["adminsAsModerator"].concat(membersData["membersAsModerator"]);
                  $scope.members = membersData["members"];
                });
              }
              else {
                $log.debug("No members found for  this subgroup: " + data.group + "/" + data.subgroup.name);
              }
            }, function (err) {
              $log.debug("Error while getting subgroup members: " + JSON.stringify(err));
            });
        });

        $scope.moderate = function (memberData) {
          var popUp = handleModerate($scope, memberData, Tring, ConstsService, $scope.group._id, $ionicPopup, $log);
          IonicClosePopupService.register(popUp);
        }

        $scope.updateSubNetwork = function () {
          NavigationService.goNativeToView('app.groups.updateSubGroupMemberships', data, 'right');
        }

        $scope.sendPvtMessage = function (memberData) {
          $scope.currentPopover.hide();
          $log.debug("sendPvtMessage memberData: " + JSON.stringify(memberData));
          var _data = {
            senderId: data.group._id,
            localSenderId: data.group._id,
            toHandleName: memberData.memberValue.name
          }
          NavigationService.goNativeToView('app.chat-ui', _data, 'right');
        }

        $scope.showActions = function (isModerator, memberKey, memberValue, $event, index) {
          $scope.currentMemberData = {
            memberKey: memberKey,
            memberValue: memberValue,
            index: index
          };

          var actions = null;
          if (!isModerator) {
            if(nonModeratorActions == null) {
              nonModeratorActions = [];
              nonModeratorActions.push(moderateAction);
              nonModeratorActions.push(privateChatAction);
            }
            actions = nonModeratorActions;
          }
          else {
            if(moderatorActions == null) {
              moderatorActions = [];
              moderatorActions.push(privateChatAction);
            }
            actions = moderatorActions;
          }
          showActionsPopOver(Tring, actions, 'currentMemberData', memberValue, $rootScope, $ionicPopover, $scope, $event);
        }
      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }])
  .controller('ManageSubGroupUpdateMembersCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', 'ManageGroupService', '$cordovaToast', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, ManageGroupService, $cordovaToast, $log) {
      $log.debug("ManageSubGroupMembersCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Sub group Members " + device);
      var data = {};
      if (device) {
        var members = [];
        var admins = [];
        var existingAdminModerators;
        var existingMemberModerators;
        var gid;
        var sgid;
        $scope.$on('$ionicView.enter', function () {
          data.group = $stateParams.group;
          data.subgroup = $stateParams.subgroup;
          $scope.group = data.group;
          $scope.subgroup = data.subgroup;


          gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
          sgid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.subgroup._id);
          ManageGroupService.getAllSubGroupMembers(gid, data.group.handleName, sgid, 1, 50, true).then(
            function (membersData) {
              if (membersData && membersData.success) {
                existingAdminModerators = [];
                existingMemberModerators = [];
                $scope.$evalAsync(function () {
                  var adminsAsModerators = membersData["adminsAsModerator"];
                  var existingAdminModeratorsKeys = $rootScope.Utils.keys(adminsAsModerators);
                  angular.forEach(existingAdminModeratorsKeys, function (existingAdminModeratorsKey) {
                    existingAdminModerators.push($rootScope.Utils.keys(adminsAsModerators[existingAdminModeratorsKey])[0]);
                  });
                  $scope.admins = adminsAsModerators.concat(membersData["adminsAsNonMembers"]);

                  var membersAsModerators = membersData["membersAsModerator"];
                  var existingMemberModeratorsKeys = $rootScope.Utils.keys(membersAsModerators);
                  angular.forEach(existingMemberModeratorsKeys, function (existingMemberModeratorsKey) {
                    existingMemberModerators.push($rootScope.Utils.keys(membersAsModerators[existingMemberModeratorsKey])[0]);
                  });
                  $scope.members = membersAsModerators.concat(membersData["members"]).concat(membersData["nonMembers"]);
                });
              }
              else {
                $log.debug("No members found for  this subgroup: " + data.group + "/" + data.subgroup.name);
              }
            }, function (err) {
              $log.debug("Error while getting subgroup members: " + JSON.stringify(err));
            });
        });
        $scope.ConstsService = ConstsService;

        $scope.getRole = function (value) {
          if(angular.isDefined(value.newRole)) {
            return value.newRole;
          }
          else {
            return value.role;
          }
        }

        $scope.isNonMember = function (value) {
          var role = $scope.getRole(value);
          return role ===  ConstsService.NON_MEMBER_ROLE || !(angular.isDefined(value.role) );
        }

        $scope.addMember = function (memberAs, index, key, value, $event) {
          if(memberAs === ConstsService.NON_MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.newRole = ConstsService.NON_MEMBER_ROLE;
            });
          }
          else if(memberAs === ConstsService.MODERATOR_ROLE) {
            $scope.$evalAsync(function () {
              value.newRole = ConstsService.MODERATOR_ROLE;
            });
          }
          else if(memberAs === ConstsService.MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.newRole = ConstsService.MEMBER_ROLE;
            });
          }
          $log.debug("memberAs: " + memberAs + " index:" + index + " key: " + key);
          members[key] = {
            memberAs: memberAs,
            index: index
          };
          $log.debug("@addMember members: " + JSON.stringify(members[key]));
          $event.stopPropagation();
        }

        $scope.addAdmin = function (adminAs, index, key, value, $event) {
          if(adminAs === ConstsService.NON_MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.newRole = ConstsService.NON_MEMBER_ROLE;

            });
          }
          else if(adminAs === ConstsService.MODERATOR_ROLE) {
            $scope.$evalAsync(function () {
              value.newRole = ConstsService.MODERATOR_ROLE;
            });
          }
          $log.debug("adminAs: " + adminAs + " index:" + index);
          admins[key] = {
            adminAs: adminAs,
            index: index
          };
          $log.debug("@addAdmin admins: " + JSON.stringify(admins[key]));
          $event.stopPropagation();
        }

        var isNotSameAsExistingRole = function (arr, key, newRole) {
          var elem = arr.filter(function (_elem) {
            $log.debug("Elem: " + JSON.stringify(_elem));
            if(_elem) {
              var val = _elem[key];
              $log.debug("val: " + JSON.stringify(val));
              return val && val['role'] === newRole;
            }
            else {
              return false;
            }
          });

          return elem.length === 0;
        }

        $scope.updateSubNetwork = function () {
          var adminAsModerators = [];
          var adminsAsNonMembers = [];
          var asModerators = [];
          var asMembers = [];
          var nonMembers = [];
          var keys = $rootScope.Utils.keys(admins);
          angular.forEach(keys, function (key) {
            var value = admins[key];
            if (value.adminAs === ConstsService.MODERATOR_ROLE && isNotSameAsExistingRole($scope.admins, key, ConstsService.MODERATOR_ROLE)) {
              adminAsModerators.push(key);
            }
            else if (value.adminAs === ConstsService.NON_MEMBER_ROLE && isNotSameAsExistingRole($scope.admins, key, ConstsService.NON_MEMBER_ROLE)) {
              adminsAsNonMembers.push(key);
            }
          });

          keys = $rootScope.Utils.keys(members);
          angular.forEach(keys, function (key) {
            var value = members[key];
            if (value.memberAs === ConstsService.MODERATOR_ROLE && isNotSameAsExistingRole($scope.members, key, ConstsService.MODERATOR_ROLE)) {
              asModerators.push(key);
            }
            else if (value.memberAs === ConstsService.MEMBER_ROLE && isNotSameAsExistingRole($scope.members, key, ConstsService.MEMBER_ROLE)) {
              asMembers.push(key);
            }
            else if (value.memberAs === ConstsService.NON_MEMBER_ROLE && isNotSameAsExistingRole($scope.members, key, ConstsService.NON_MEMBER_ROLE)) {
              nonMembers.push(key);
            }
          });

          var ensureOneModerator = function () {
            $log.debug("existingAdminModerators: " + JSON.stringify(existingAdminModerators));
            $log.debug("adminsAsNonMembers: " + JSON.stringify(adminsAsNonMembers));
            $log.debug("existingMemberModerators: " + JSON.stringify(existingMemberModerators));
            $log.debug("nonMembers: " + JSON.stringify(nonMembers));

            var areAllNonMembers = existingAdminModerators.every(function (existingAdmin) {
              return adminsAsNonMembers.indexOf(existingAdmin) === -1 ? false : true;
            });

            $log.debug("Stage 1 areAllNonMembers: " + areAllNonMembers);

            if (areAllNonMembers) {
              areAllNonMembers = existingMemberModerators.every(function (existingMemberModerator) {
                return (nonMembers.indexOf(existingMemberModerator) === -1 && asMembers.indexOf(existingMemberModerator) === -1) ? false : true;
              });
              $log.debug("Stage 2 areAllNonMembers: " + areAllNonMembers);
              return areAllNonMembers ? false: true;
            }
            else {
              return true;
            }
          }

          $log.debug("@Edit adminAsModerators: " + JSON.stringify(adminAsModerators));
          $log.debug("@Edit adminsAsNonMembers: " + JSON.stringify(adminsAsNonMembers));
          $log.debug("@Edit asModerators: " + JSON.stringify(asModerators));
          $log.debug("@Edit asMembers: " + JSON.stringify(asMembers));
          $log.debug("@Edit nonMembers: " + JSON.stringify(nonMembers));

          if (adminAsModerators.length > 0 || asModerators.length > 0 || ensureOneModerator()) {
            $log.debug("data.group.handleName: " + data.group.handleName + " data.subgroup.name: " + data.subgroup.name);
            if(adminAsModerators.length > 0 || asModerators.length > 0 || asMembers.length > 0 || adminsAsNonMembers.length > 0 || nonMembers.length > 0) {
              ManageGroupService.handleGroup(gid, sgid, data.group.handleName, data.subgroup.name, "EDIT", adminAsModerators, asModerators, asMembers, adminsAsNonMembers, nonMembers).then(function (resp) {
                if (resp.success ) {
                  $log.debug("Update subgroup is successful with response: " + JSON.stringify(resp));
                  $log.debug("Update subgroup is successful with response: " + JSON.stringify(resp));
                  NavigationService.goNativeToView('app.groups.listSubNetworks', {group: $scope.group}, 'right');
                }
                else {
                  $log.debug("Edit subgroup is failed");
                  $cordovaToast.showLongCenter("Update Sub-Group is failed");
                }
              });
            }
            else {
              $cordovaToast.showLongCenter("No updates to memberships found!");
            }

          }
          else {
            $log.debug("At least one admin/member should be added as moderator of the subgroup");
            $cordovaToast.showLongCenter("At least one admin/member should be added as moderator of the Sub-Group");
          }
        }

        $scope.showSubGroups = function (memberValue, $event) {
          var template = '<ion-popover-view><ion-content>';
          template += showSubGroups(memberValue, $rootScope);
          template += "</ion-content></ion-popover-view>";
          $ionicPopover.fromTemplate(template, {
            scope: $scope
          }).show($event);
        }
      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }])
  .controller('ManageSubGroupCreateCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', 'ManageGroupService', 'DBService', '$cordovaToast', '$filter','$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, ManageGroupService, DBService, $cordovaToast, $filter, $log) {
      $log.debug("ManageSubGroupCreateCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Create SubGroup" + device);
      var data = {};
      if (device) {
        var members = [];
        var admins = [];
        $scope.subGroup = {
          name: ""
        };
        $scope.isSubGroupCreatorModerator = false;
        $scope.ConstsService = ConstsService;

        $scope.isNonMember = function (value) {
          return value.role ===  ConstsService.NON_MEMBER_ROLE || !(angular.isDefined(value.role) );
        }

        handleGroupMemberships($scope, data, $stateParams, Tring, ConstsService, ManageGroupService, $log);

        $scope.addMember = function (memberAs, index, key, value, $event) {
          if(memberAs === ConstsService.NON_MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.role = ConstsService.NON_MEMBER_ROLE;
            });
          }
          else if(memberAs === ConstsService.MODERATOR_ROLE) {
            $scope.$evalAsync(function () {
              value.role = ConstsService.MODERATOR_ROLE;
            });
          }
          else if(memberAs === ConstsService.MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.role = ConstsService.MEMBER_ROLE;
            });
          }
          $log.debug("memberAs: " + memberAs + " index:" + index + " key: " + key);
          members[key] = {
            memberAs: memberAs,
            index: index
          };
          $log.debug("@addMember members: " + JSON.stringify(members[key]));
          $event.stopPropagation();
        }

        $scope.addAdmin = function (adminAs, index, key, value, $event, isSubGroupCreatorModerator) {
          if(adminAs === ConstsService.NON_MEMBER_ROLE) {
            $scope.$evalAsync(function () {
              value.role = ConstsService.NON_MEMBER_ROLE;

            });
          }
          else if(adminAs === ConstsService.MODERATOR_ROLE) {
            $scope.$evalAsync(function () {
              value.role = ConstsService.MODERATOR_ROLE;
            });
          }
          $log.debug("adminAs: " + adminAs + " index:" + index + " key: " + key);
          admins[key] = {
            adminAs: adminAs,
            index: index
          };
          $scope.isSubGroupCreatorModerator = isSubGroupCreatorModerator;
          $log.debug("@addAdmin admins: " + JSON.stringify(admins[key]));
          $event.stopPropagation();
        }

        $scope.$watch('subGroup.name', function (val) {
          $scope.subGroup.name = $filter('lowercase')(val);
        }, false);

        $scope.createSubNetwork = function () {
          $log.debug("$scope.subGroupName: " + $scope.subGroup.name);
          var subGroupName = $scope.subGroup.name;
          var validName = /^[0-9a-zA-Z_\.]+$/.test(subGroupName);
          if (subGroupName && subGroupName.length > 0 &&  subGroupName.length <= 20 && validName) {
            subGroupName = subGroupName.trim();
            var adminAsModerators = [];
            var asModerators = [];
            var asMembers = [];

            var keys = $rootScope.Utils.keys(admins);
            angular.forEach(keys, function (key) {
              var value = admins[key];
              $log.debug("Key: " + key + "  Value: " + JSON.stringify(value));
              if (value.adminAs === ConstsService.MODERATOR_ROLE) {
                adminAsModerators.push(key);
              }
            });
            keys = $rootScope.Utils.keys(members);
            angular.forEach(keys, function (key) {
              var value = members[key];
              if (value.memberAs === ConstsService.MODERATOR_ROLE) {
                asModerators.push(key);
              }
              else if (value.memberAs === ConstsService.MEMBER_ROLE) {
                asMembers.push(key);
              }
            });

            $log.debug("@Create adminAsModerators: " + JSON.stringify(adminAsModerators));
            $log.debug("@Create asModerators: " + JSON.stringify(asModerators));
            $log.debug("@Create asMembers: " + JSON.stringify(asMembers));

            if (adminAsModerators.length > 0 || asModerators.length > 0) {
              var gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, data.group._id);
              ManageGroupService.handleGroup(gid, null, data.group.handleName, subGroupName, "CREATE", adminAsModerators, asModerators, asMembers, null, null).then(function (resp) {
                if (resp.success) {
                  $log.debug("Create subgroup is successful with response: " + JSON.stringify(resp));
                  if(!$scope.isSubGroupCreatorModerator) {
                    var subGroup = {
                      _id: ConstsService.GROUPS_START_KEY + resp.subGroupId,
                      name: subGroupName,
                      handleName: data.group.handleName,
                      parentGroupId: data.group._id,
                      membershipType: ConstsService.NON_MEMBER_ROLE,
                      topic: Tring.getGroupMQTTtopic(resp.topic)
                    }

                    $log.debug("Sub group is: " + JSON.stringify(subGroup));
                    var mbrDb = DBService.mbrDb();
                    mbrDb.put(subGroup).then(function (res) {
                      $log.debug("subGroup created");
                      $cordovaToast.showLongCenter("Your device will be shortly synced with the new sub group");
                      NavigationService.goNativeToView('app.groups.listSubNetworks', {group: $scope.group}, 'right');
                    });
                  }
                  else {
                    $cordovaToast.showLongCenter("Your device will be shortly synced with the new sub group");
                    NavigationService.goNativeToView('app.groups.listSubNetworks', {group: $scope.group}, 'right');
                  }
                }
                else {
                  $log.debug("Create subgroup is failed");
                  $cordovaToast.showLongCenter("Create Sub-Group is failed");
                }
              });

            }
            else {
              $cordovaToast.showLongCenter("At least one admin/member should be added as moderator of the Sub-Group");
              $log.debug("At least one admin/member should be added as moderator of the subgroup");
            }
          }
          else {
            $cordovaToast.showLongCenter("Invalid Sub-Group ID. It should be unique string not more than 20 characters in length.Only alphanumeric, '.' and '_' are allowed.");
            $log.debug("Invalid sub group name!")
          }

        }
        $scope.showSubGroups = function (memberValue, $event) {
          var template = '<ion-popover-view><ion-content>';
          template += showSubGroups(memberValue, $rootScope);
          template += "</ion-content></ion-popover-view>";
          $ionicPopover.fromTemplate(template, {
            scope: $scope
          }).show($event);
        }
      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }])
  .controller('ManageGroupInviteMembersCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, $log) {
      $log.debug("ManageGroupInviteMembersCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Manage Group > Invite Members  " + device);
      if (device) {


      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }])
  .controller('ManageGroupDetailsCtrl', ['$rootScope', '$scope', '$state', '$ionicHistory', 'ChatService', 'MqttBrokerService', 'AuthService', 'MessagesService', 'Tring', '$ionicLoading', '$timeout', 'UserService', 'GroupService', '$q', 'NetworkService', '$http', 'ConstsService', 'SharedDataService', 'NavigationService', '$ionicScrollDelegate', '$ionicPopover', '$stateParams', '$log',
    function ($rootScope, $scope, $state, $ionicHistory, ChatService, MqttBrokerService, AuthService, MessagesService, Tring, $ionicLoading, $timeout, UserService, GroupService, $q, NetworkService, $http, ConstsService, SharedDataService, NavigationService, $ionicScrollDelegate, $ionicPopover, $stateParams, $log) {
      $log.debug("ManageGroupDetailsCtrl");
      var device = AuthService.getDevice();
      $log.debug("Device found in Group " + device);
      if (device) {


      } else {
        //  $state.go('login');
        NavigationService.goNativeToView('login', null, 'left');
      }
    }]).run(function ($rootScope) {
  $rootScope.Utils = {
    keys: Object.keys
  }
});
