angular.module('tring.message.controller', ['tring.authService','monospaced.elastic', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'ngSanitize', 'ionic.closePopup'])
  .controller('MessagesCtrl', ['$scope', '$state', '$stateParams', '$ionicHistory', 'MessagesService', 'ChatService', '$ionicScrollDelegate', 'SharedDataService', 'ConstsService', 'MqttBrokerService', 'AuthService', 'Tring', '$ionicLoading', 'FileBrowseService', '$q', '$rootScope', 'NetworkService', 'NavigationService', '$cordovaClipboard', '$cordovaToast', '$interval', '$http', '$ionicPopup', '$ionicPopover', 'IonicClosePopupService', 'ContactsService', '$log',
    function ($scope, $state, $stateParams, $ionicHistory, MessagesService, ChatService, $ionicScrollDelegate, SharedDataService, ConstsService, MqttBrokerService, AuthService, Tring, $ionicLoading, FileBrowseService, $q, $rootScope, NetworkService, NavigationService, $cordovaClipboard, $cordovaToast, $interval, $http, $ionicPopup, $ionicPopover, IonicClosePopupService, ContactsService, $log) {
      var device = AuthService.getDevice();
      $scope.Tring = Tring;
      $log.debug("Device found in Messages " + device);
      if (device) {
        $ionicLoading.show();
        $scope.messages = [];
        $scope.inputMessage = "";
        $scope.chat = {};
        $scope.sendMessageType = ConstsService.SEND_MESSAGE_TYPE_REGULAR;
        $scope.isOwnerOrAdmin = false;
        var userTypingStatus = false;
        var senderId = $stateParams.senderId;
        var localSenderId = $stateParams.localSenderId;
        var fs = new FileBrowseService();


        $scope.contentMessages = {
                    cancelDownloadArray: {},
                    fileTransferProgress: {}
        };

        // Below modelOptions will control the model update to indicate the correct typing status

        var unSetTypingStatus = Tring.debounce(function () {
          var userStatus = MqttBrokerService.getUserStatus(senderId);
          if (userStatus && $rootScope.networkStatus && $scope.chat.sendertype === ConstsService.P2P && userStatus.clientStatus === 1) {
            ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
              MqttBrokerService.sendTypingMessage(ConstsService.TYPING_STATUS_OFF, senderData.topic);
              userTypingStatus = false;
            });
          }
        }, 1000).callback;

        $scope.$watch('inputMessage', function (newVal, oldVal) {
          unSetTypingStatus();
        });

        $scope.chat.typing = false;
        $scope.focusManager = {
          focusInputOnBlur: true
        };

        $scope.scrollContentToBottom = function () {
          $ionicScrollDelegate.$getByHandle("chatMessagesScreen").scrollBottom(true);
        };

        window.addEventListener('native.keyboardshow', function () {
          $scope.scrollContentToBottom();
        });

        window.addEventListener('native.keyboardhide', function () {
          $scope.scrollContentToBottom();
        });

        $scope.shouldNotFocusOnBlur = function () {
          $scope.focusManager.focusOnBlur = false;
        };

        $scope.sendTypingStatus = function (event) {
          var userStatus = MqttBrokerService.getUserStatus(senderId);
          if (userStatus && $rootScope.networkStatus && !userTypingStatus && $scope.chat.sendertype === ConstsService.P2P && userStatus.clientStatus === 1) {
            var code = event.which || event.keyCode;
            if ($scope.inputMessage.length === 0 && event.which <= 46) {
              return;
            }
            userTypingStatus = true;
            ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
              MqttBrokerService.sendTypingMessage(ConstsService.TYPING_STATUS_ON, senderData.topic);
            });
          }
        };
        var typingStatusCB = function (msgData) {
          $log.debug("The typing message status obtained is: " + JSON.stringify(msgData));
          if (msgData && msgData.sid === senderId) {
            var userStatus = MqttBrokerService.getUserStatus(msgData.sid);
            var typingStatus = false;
            if (userStatus.clientStatus === 1 && msgData && msgData.status === ConstsService.TYPING_STATUS_ON) {
              typingStatus = true;
            }
            $scope.$evalAsync(function () {
              $scope.chat.typing = typingStatus;
            });
          }
        };

        MqttBrokerService.addTypingStatusCB(typingStatusCB);

        var statusCB = function (msgData) {
          $log.debug("The other client's msgData: " + JSON.stringify(msgData) + "   senderId: " + senderId);
          if (msgData && msgData.sid === senderId) {
            var _sidStatus = msgData.clientStatus;
            if($scope.sidStatus !== msgData.clientStatus) {
              $scope.$evalAsync(function () {
                $log.debug("SenderId: " + senderId + "  oldSidStatus: " + $scope.sidStatus + "  newSidStatus: " + msgData.clientStatus);
                $scope.sidStatus = _sidStatus;
                if (!$scope.sidStatus) {
                  $scope.chat.typing = false;
                }
              });
            }

          }
        };

        MqttBrokerService.addUserStatusCB(statusCB);

        $scope.$on('$ionicView.beforeEnter', function () {
          $log.debug("MessagesCtrl beforeEnter");
          $log.debug("Before Enter State params: " + JSON.stringify($state.params));
          SharedDataService.setValue(ConstsService.ACTIVE_MESSAGING_SID, senderId);
          var chatId = ConstsService.CHAT_PREFIX + senderId;
          $scope.isGroup = Tring.isGroupSid(localSenderId);

          $scope.senderId = senderId;
          $scope.localSenderId = localSenderId;
          ChatService.findChat(chatId).then(function (doc) {
            $log.debug("The chat found: " + JSON.stringify(doc));
            $scope.chat = doc;
            ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
              $scope.$evalAsync(function () {
                if(senderData.pgName) {
                  $scope.chat.senderName = senderData.pgName + "/" + senderData.name;
                }
                else {
                  $scope.chat.senderName = senderData.name ? senderData.name : (senderData.phoneNumber ? senderData.phoneNumber : "Unknown");
                }
                $scope.isOwnerOrAdmin = senderData.membershipType === ConstsService.OWNER_ROLE || senderData.membershipType === ConstsService.ADMIN_ROLE;
                $scope.chat.message = senderData.message;
                if ($scope.chat.sendertype === ConstsService.GROUP) {
                  /*if (senderData.membershipType === 'OWNER') {
                   $scope.chat.handleName = senderData.ownerHandle;
                   } else {*/
                  $scope.chat.handleName = senderData.handleName;
                  //}
                }
              });
              var fwdedMsgId = $state.params.fwdedMsgId;
              if (fwdedMsgId) {
                MessagesService.getMessageById(fwdedMsgId).then(function (message) {
                  $log.debug("Forwarding message: " + JSON.stringify(message));
                  if (message.contentId) {
                    $scope.sendContentMessage(message.data, message.contentName, $q.when(message.contentId), message.contentType, message.localURL, message.fullPath, {
                      thumbnailData: message.thumbnailData
                    });
                  } else {
                    sendTextMessage(message.data);
                  }
                });
              } else if ($state.params.toHandleName) {
                setSendMessageType(ConstsService.SEND_MESSAGE_TYPE_PVT_MEMBER);
                $scope.toHandleName = $state.params.toHandleName;
                $scope.showCloseIcon = true;
                $scope.showBorder = "border:1px solid #F00";
              } else if ($state.params.pendingCaptureResults) {
                  $scope.$broadcast("processCapturedResult", $state.params.pendingCaptureResults);
              }
            });
          });
        });

        var dbChangesCallBack = function (change) {
          var newDoc = change.doc;
          //$log.debug("New change doc  = " + JSON.stringify(newDoc));
          if (Tring.isMessage(newDoc) && newDoc.sid === senderId) {
            // Assuming this change is already in our $scope.messages
            var index = Tring.findMessageForSenderId($scope.messages, change.id);
            $log.debug("MessagesCtrl Message DB Changed: Message Index found while DB update = " + index);
            var message = $scope.messages[index];
            //$log.debug("The messag doc  = " + JSON.stringify(message));
            if (message && message._id === change.id) {
              // Update content message or if the message is not transit message
              var validForUpdate = (newDoc.dir === 1 && newDoc.status !== 0) || (Tring.isContentMessage(newDoc));
              $log.debug("Updating existing. validForUpdate: " + validForUpdate);
              if (validForUpdate) {
                $scope.$evalAsync(function () {
                  // Re-compute index since it might have changed due to new insertions meanwhile
                  var _index = Tring.findMessageForSenderId($scope.messages, change.id);
                  var msg = $scope.messages[_index];
                  $scope.messages[_index] = angular.extend(msg, change.doc); // update
                  if (Tring.isContentMessage(msg)) {
                    msg.uploadInProgress = false;
                  }
                });
              }
            } else {
              $log.debug("Inserting new");
              //If a content message was sent and user moved away from this messages screen to a different one
              //while upload is happening,
              //then the message after writing to database is treated as new. Hence, enter the if block
              //for content messages even if outbound
              if (newDoc.dir === 0 || Tring.isContentMessage(newDoc)) { // 0 is inbound
                $scope.$evalAsync(function () {
                  $scope.messages.splice(0, 0, newDoc);
                  var activeMsgId = SharedDataService.getValue(ConstsService.ACTIVE_MESSAGING_SID);
                  if (activeMsgId) {
                    $ionicScrollDelegate.scrollBottom(true);
                  }
                });
              }
            }
          }
        };
        $scope.$on("contactProfileChanged", function (evt, contact) {
          if (localSenderId == contact._id) {
            $log.debug("Existing thumbnail data for MessagesCtrl is " + $scope.chat.thumbnailData);
            $log.debug("MessagesCtrl on contactProfileChanged contact is " + JSON.stringify(contact));
            $scope.chat.thumbnailData = contact.thumbnailData ? contact.thumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
          }
        });
        $scope.$on('$ionicView.enter', function () {
          //var startMoment = moment();
          var contentId = SharedDataService.consumeValue(ConstsService.CONTENT_ID);
          var contentName = SharedDataService.consumeValue(ConstsService.CONTENT_NAME);
          var contentType = SharedDataService.consumeValue(ConstsService.CONTENT_TYPE);
          var caption = SharedDataService.consumeValue(ConstsService.CONTENT_CAPTION);
          if (!$scope.contentMessages.cancelUploadArray) {
            $scope.contentMessages.cancelUploadArray = {};
          }
          ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
            $scope.chat.thumbnailData = senderData.thumbnailData ? senderData.thumbnailData : ConstsService.DEFAULT_PROFILE_IMAGE;
          });
          var contentURL = SharedDataService.consumeValue(ConstsService.CONTENT_LOCAL_URL);
          var contentFullPath = SharedDataService.consumeValue(ConstsService.CONTENT_FULL_PATH);
          var contentThumbnailData = SharedDataService.consumeValue(ConstsService.CONTENT_THUMBNAIL_DATA);
          $scope.contentMessages.cancelUploadArray[contentName] = SharedDataService.consumeValue(ConstsService.CONTENT_UPLOAD_CANCEL);
          $log.debug('Content Id initial is ' + JSON.stringify(contentId) + " contentName: " + contentName + " contentType: " + contentType);
          MessagesService.getFirstMessagePage(senderId).then(function (messages) {
            $log.debug("First page messages loaded ");
            //$log.debug("The diff time1: " + moment().diff(startMoment));
            $scope.$evalAsync(function () {
              $scope.messages = messages;
              MessagesService.registerMessageDBChange(dbChangesCallBack);
              //If contentId is defined it means that this view has been
              //entered after Uploading content. Add another message for the
              //Uploaded message at the end
              if (contentId) {
                var data = caption;
                $scope.sendContentMessage(data, contentName, contentId, contentType, contentURL, contentFullPath, {
                  thumbnailData: contentThumbnailData
                });
              }
              $ionicScrollDelegate.scrollBottom(true);
            });
            var msgData = MqttBrokerService.getUserStatus(senderId);
            statusCB(msgData, false);
            $scope.chat.typing = false;
            $ionicLoading.hide();
          });
        });
        var lastMessageReadId = null;
        var beforeLeaveCB = function () {
          var activeMsgSid = SharedDataService.getValue(ConstsService.ACTIVE_MESSAGING_SID);
          if (senderId === activeMsgSid) {
            var lastMessage = $scope.messages[0];
            $log.debug("Before leaving the message view lastMessage: " + JSON.stringify(lastMessage));
            if (lastMessage && lastMessage._id !== lastMessageReadId) {
              //We don't want the thumbnailData to get into the chat record, so delete it
              delete $scope.chat.thumbnailData;
              ChatService.setLastReadMessageIdForChat($scope.chat, lastMessage._id).then(function (res) {
                $scope.chat._rev = res.rev;
                lastMessageReadId = lastMessage._id;
                $log.debug("Successfully added last read message!");
              }).catch(function (err) {
                if (err.status === 409) {
                  $log.debug("409 error occured while setting the lastMessageRead. Hence retrying");
                  ChatService.findChat($scope.chat._id).then(function (_chat) {
                    ChatService.setLastReadMessageIdForChat(_chat, $scope.messages[0]._id).then(function (_res) {
                      $scope.chat._rev = _res.rev;
                      lastMessageReadId = lastMessage._id;
                      $log.debug("Successfully added last read message after retrying!");
                    });
                  });
                }
              });
            } else {
              if (!lastMessage) {
                $log.debug("No message is available to set as the lastMessageReadId");
              } else if (!lastMessageReadId) {
                $log.debug("The lastMessageReadId is not yet set");
              } else {
                $log.debug("The lastMessageReadId and the current last message's id are same. Hence no need to set lastMessageReadId.");
              }
            }
          } else {
            $log.debug("The activeMsgSid and current senderId do not match. Hence no need to set lastMessageReadId.");
          }
        };

        NetworkService.addOnPauseCB(beforeLeaveCB);

        $scope.$on('$ionicView.beforeLeave', function () {
          beforeLeaveCB();
          SharedDataService.setValue(ConstsService.ACTIVE_MESSAGING_SID, null);
        });

        var onResumeCB = function () {
          $scope.$evalAsync(function () {
            $scope.chat.typing = false;
          });
        };

        NetworkService.addOnResumeCB(onResumeCB);

        NetworkService.addOnlineCB(onResumeCB);

        NetworkService.addOfflineCB(onResumeCB);

        var GETTING_PREVIOUS_PAGE = false;
        var _loadMessages = function () {
          var lastMessage = $scope.messages[$scope.messages.length - 1];
          $log.debug("lastMessage: " + JSON.stringify(lastMessage));
          if (lastMessage) {
            MessagesService.getPrevMessagePage(senderId, lastMessage._id).then(function (messages) {
              if (messages.length > 0) {
                $scope.messages = $scope.messages.concat(messages);
                $scope.noMoreOldMessagesAvailable = false;
              } else {
                $scope.noMoreOldMessagesAvailable = true;
              }
              $scope.$broadcast('scroll.refreshComplete');
              GETTING_PREVIOUS_PAGE = false;
            }).catch(function (err) {
              $log.debug("Error while loading earlier messages: " + JSON.stringify(err));
            });
          } else {
            $scope.$broadcast('scroll.refreshComplete');
            GETTING_PREVIOUS_PAGE = false;
          }
        };

        $scope.loadOldMessages = function () {
          //$log.debug("Loading previous set of messages. The current messages length: " + $scope.messages.length);
          if (!GETTING_PREVIOUS_PAGE) {
            GETTING_PREVIOUS_PAGE = true;
            _loadMessages();
          } else {
            $log.debug("Still busy getting previous page. Hence ignoring");
          }
        };

          $scope.goToDetailsAndProfile = function() {
              ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
              if ($scope.isGroup) {
                var group = senderData;
                NavigationService.goNativeToView('app.seeGroupDetails', {group: group, readOnly: true}, 'right');
              } else {
                  var ctct = senderData;
                  NavigationService.goNativeToView('app.profile-ro', {contact: senderData, group:null, readOnly: true}, 'right');
              }
            });
          };

        $scope.go = function (path) {
          //$state.go(path);
          NavigationService.goNativeToView(path, null, 'left');
          $log.debug($ionicHistory.viewHistory());
        };
        //Function to go back a step using $ionicHistory
        $scope.goBackAStep = function () {
          $log.debug('clicked');
          //$ionicHistory.goBack();
          //NavigationService.goNativeBack();
          NavigationService.goNativeToView('app.chats', null, 'right');
        };


        var _sendMessage = function (type, data, handleName, contentIdPromise, options) {
          //$log.debug("Message data: " + data + "...chat = " + JSON.stringify($scope.chat));
          var isGroup = senderId.substring(0, ConstsService.GROUPS_START_KEY.length) === ConstsService.GROUPS_START_KEY;
          if (isGroup) {
            type = type | ConstsService.MESSAGE_TYPE_GROUP;
          } else {
            type = type | ConstsService.MESSAGE_TYPE_P2P;
          }

          $log.debug("localSenderId: " + localSenderId);

          ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
            var otherMsgTypeSetter = function (msg) {
              if (isGroup) {
                var userRole = Tring.getGroupMembershipRole(senderData);
                if (sendMessageType) {
                  if ((m2m || userRole !== ConstsService.MEMBER_ROLE) && sendMessageType === ConstsService.SEND_MESSAGE_TYPE_PVT_MEMBER) {
                    Tring.addPrivateMessageMemberToMessage(msg);
                  } else if (sendMessageType === ConstsService.SEND_MESSAGE_TYPE_PVT_MODERATOR) {
                    Tring.addPrivateMessageModeratorToMessage(msg);
                  } else if (sendMessageType === ConstsService.SEND_MESSAGE_TYPE_ABUSE) {
                    Tring.addAbuseMessage(msg);
                  }
                }
              }
            }

            var __sendMessage = function (getTopicCB, postMessageCB, _data) {
              var sendMsgPromise = MqttBrokerService.sendMessage(type, null, otherMsgTypeSetter, senderId, getTopicCB, _data, postMessageCB, handleName, contentIdPromise, options);
              sendMsgPromise.then(function (msg) {
                $scope.$evalAsync(function () {
                  $scope.messages.splice(0, 0, msg);
                  var activeMsgId = SharedDataService.getValue(ConstsService.ACTIVE_MESSAGING_SID);
                  if (activeMsgId) {
                    $ionicScrollDelegate.scrollBottom(true);
                  }
                });
              }).catch(function (err) {
                $log.debug("Could not send message due to error : " + JSON.stringify(err));
              });
            }

            var sendPrivateMessageToMember = function (_data) {
              // Get the handleData and then send the message
              var toHandleName = $scope.toHandleName;
              $log.debug("Getting $scope.toHandleName: " + $scope.toHandleName);
              ChatService.getHandleData(toHandleName, localSenderId, Tring.isO2MGroup(senderData)).then(function (handleData) {
                var getTopicCB = function () {
                  return handleData.p2p;
                }
                var postMessageCB = function () {
                  $scope.pvtMessageToMemberSent = true;
                  $log.debug("Private message to member successfully sent to topic: " + getTopicCB());
                };
                __sendMessage(getTopicCB, postMessageCB, _data);
              }).catch(function (err) {
                $log.debug("Error while sending private message to member: " + JSON.stringify(err));
              });
            }

            if (isGroup) {
              if(handleName) {
                MqttBrokerService.addToUserGroupHandle(senderId, handleName);
              }
              var userGroupStatus = Tring.getUserGroupStatus(senderData);
              $log.debug("The user group status is: " + Tring.getUserGroupStatus(senderData));
              if (userGroupStatus !== ConstsService.USER_GROUP_STATUS_EVICTED &&
                 userGroupStatus !== ConstsService.USER_GROUP_STATUS_LEFT &&
                 userGroupStatus !== ConstsService.USER_GROUP_STATUS_REJECTED
                 ) {
                var pgid = Tring.getParentGroupId(senderData);
                var m2m = Tring.isM2MGroup(senderData);
                var sendMessageType = $scope.sendMessageType;
                var userRole = Tring.getGroupMembershipRole(senderData);
                $log.debug("Is M2M: " + m2m + " userRole: " + userRole);
                if((pgid && userRole ===  ConstsService.NON_MEMBER_ROLE) || (!pgid && userRole ===  ConstsService.NON_MEMBER_ROLE && sendMessageType !== ConstsService.SEND_MESSAGE_TYPE_PVT_MODERATOR)) {
                  $cordovaToast.showLongCenter("You are not a member of this group. Hence you cannot send message");
                }
                else if ((m2m || userRole <= ConstsService.MODERATOR_ROLE) && sendMessageType === ConstsService.SEND_MESSAGE_TYPE_PVT_MEMBER) {
                  sendPrivateMessageToMember(data);
                } else {
                  var _data = data;
                  var getTopicCB = function () {
                    var topic = senderData.topic;
                    if (sendMessageType || (!m2m && userRole === ConstsService.MEMBER_ROLE)) {
                      topic = topic + "/pvt"
                    }
                    return topic;
                  }
                  var postMessageCB = function () {
                    if (m2m && sendMessageType === ConstsService.SEND_MESSAGE_TYPE_ABUSE && $scope.reportAbuseMessage) {
                      $scope.pvtMsgSentToModerator = true;
                      $log.debug("Private message to moderator/report abuse successfully sent to topic: " + getTopicCB() + " Now updating" +
                        " abuse count and sending message to abuser.");
                      var toHandleName = $scope.toHandleName;
                      $log.debug("Getting $scope.toHandleName: " + $scope.toHandleName);
                      ChatService.getHandleData(toHandleName, localSenderId, Tring.isO2MGroup(senderData)).then(function (handleData) {
                        MqttBrokerService.getMessageId(localSenderId).then(function (msgId) {
                          var msg = {
                            type: type,
                            _id: msgId,
                            ts: moment().format('x'),
                            sid: localSenderId,
                            data: "You have been reported for abuse.",
                            status: 0, // Transit - 0, Unsent - -1, Sent - 1, Delivered - 2
                            dir: 1,
                            handleName: null,
                            toAbuser: true
                          };
                          Tring.addAbuseMessage(msg);
                          var cb = function () {
                            $scope.pvtMessageToMemberSent = true;
                            $log.debug("Private message to member successfully sent to topic: " + getTopicCB());
                          };
                          MqttBrokerService.sendDataMessage(msg, handleData.p2p, false, cb);
                        });

                      }).catch(function (err) {
                        $log.debug("Error while sending private message to member: " + JSON.stringify(err));
                      });

                      // Update the abuse count on the server
                      var reputationData = {
                        "moderatorHandleName": handleName,
                        "handleName": $scope.reportAbuseMessage.handleName,
                        "reputationKey": "abuses",
                        "reputationValue": 1,
                        "gid": Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, senderId)
                      }
                      $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL, reputationData).then(function (response) {
                        $scope.reportAbuseDone = true;
                        $log.debug("Updated the abuse count result: " + response.data.success);
                      });
                    } else {
                      $log.debug("Private message sent to topic: " + getTopicCB());
                      $scope.pvtMsgSentToModerator = true;
                    }
                  };
                  if (m2m && sendMessageType === ConstsService.SEND_MESSAGE_TYPE_ABUSE && $scope.reportAbuseMessage) {
                    var reportAbuseOptions = {
                      toModerator: true
                    };
                    options = !options? reportAbuseOptions: angular.extend(options, reportAbuseOptions);
                    _data = "You have reported for abuse against the handle " + $scope.reportAbuseMessage.handleName + ": " + _data;
                  }
                  __sendMessage(getTopicCB, postMessageCB, _data);
                }
              }
              else {
                  if (userGroupStatus === ConstsService.USER_GROUP_STATUS_EVICTED) {
                $cordovaToast.showLongCenter("You have been evicted from the group. Hence you cannot send the message");
                  } else if (userGroupStatus === ConstsService.USER_GROUP_STATUS_LEFT){
                      $cordovaToast.showLongCenter("You have left the group. Hence you cannot send the message");
                  } else if (userGroupStatus === ConstsService.USER_GROUP_STATUS_REJECTED){
                      $cordovaToast.showLongCenter("Your membership to the group has been rejected. Hence you cannot send the message");
                  }
              }
            } else {
              if(!senderData.topic) {
                $log.debug("P2P chat topic not found");
                ContactsService.synchronizerGeneric.run(function () {
                  ContactsService.pingMbrSrvrForContact(senderData).then(function (cntct) {
                      // Subscribe to the special topic of this user
                      MqttBrokerService.subscribeToStatus(cntct.topic);
                      var getTopicCB = function () {
                        return cntct.topic;
                      }
                      var postMessageCB = function () {
                        $log.debug("P2P message successfully sent to topic after ContactsService.pingMbrSrvrForContact: " + getTopicCB());
                      };
                      ChatService.removeSenderIdDataInCache(localSenderId);
                      __sendMessage(getTopicCB, postMessageCB, data);
                    })
                    .catch(function (err) {
                      $log.debug("Error while getting contact from DB/server: " + JSON.stringify(err));
                    });
                });
              }
              else {
                var getTopicCB = function () {
                  return senderData.topic;
                }
                var postMessageCB = function () {
                  $log.debug("P2P message successfully sent to topic: " + getTopicCB());
                };
                __sendMessage(getTopicCB, postMessageCB, data);
              }

            }
          });
        };


        var setSendMessageType = function (type) {
          $log.debug("setting message type: " + type);
          $scope.sendMessageType = type;
        }

        $scope.showCloseIcon = false;
        $scope.setPrivateMessageToMember = function (message) {
          $scope.currentPopover.hide();
          setSendMessageType(ConstsService.SEND_MESSAGE_TYPE_PVT_MEMBER);
          $scope.toHandleName = message.handleName;
          $scope.showCloseIcon = true;
          $scope.showBorder = "border:1px solid #F00";
          $cordovaToast.showShortCenter("You are now in private chat mode");
        }

        $scope.exitPrivateMode = function () {
          $scope.$evalAsync(function () {
            $scope.showCloseIcon = false;
            $scope.showBorder = "border:0px solid #f35c20";
            $scope.sendMessageType = ConstsService.SEND_MESSAGE_TYPE_REGULAR;
          });
        }

        $scope.setPrivateMessageToModerator = function () {
          setSendMessageType(ConstsService.SEND_MESSAGE_TYPE_PVT_MODERATOR);
          $scope.showCloseIcon = true;
          $scope.showBorder = "border:1px solid #f35c20";
          $cordovaToast.showShortCenter("You are now in contact admin mode");
        }

        $scope.setReportAbuseMessage = function (message) {
          $scope.currentPopover.hide();
          setSendMessageType(ConstsService.SEND_MESSAGE_TYPE_ABUSE);
          $scope.reportAbuseMessage = message;
          $scope.toHandleName = message.handleName;
          $scope.showCloseIcon = true;
          $scope.showBorder = "border:1px solid #F00";
          $cordovaToast.showShortCenter("You are now in report abuse mode and the message will be send to the group moderators");
        }

        $scope.profileImageClickHandler = function () {
          ChatService.getSenderIdData($scope.chat.localSid, $scope.chat.sendertype).then(function (sender) {
            var ext = fs.getExtension(sender);
              if (!ext) {
                  ext='.jpg';
              }
            sender.contentName = sender._id + ext;
            $scope.$broadcast("downloadProfileContent", sender);
          });
        };

        $scope.moderateHandle = function(message) {
          $scope.currentPopover.hide();
          $scope.adminContext = false;
          $scope.moderateHandleName = message.handleName;
          $scope.processingGroupMembershipRequest = true;
          $scope.data = {};
          $scope.data.gid = Tring.getIdFromSid(ConstsService.GROUPS_START_KEY, senderId);
          $scope.data.handleName = message.handleName;
          var moderatePopUp = $ionicPopup.show({
              cssClass:'modratePopup',
            templateUrl: 'templates/rubyonic/groupModratorPopup.html',
            title: '',
            scope: $scope
          });
          IonicClosePopupService.register(moderatePopUp);
        }



        $scope.blockHandle = function (message) {
          $scope.currentPopover.hide();
          ChatService.blockUnblockHandle(message.handleName, localSenderId, true).then(function (res) {
            $log.debug("Blocked handle: " + message.handleName + " senderId: " + localSenderId);
            $cordovaToast.showLongCenter("The handle " + message.handleName + " has been blocked. You will not receive private messages from this handle.")
          });
        }

        $scope.unblockHandle = function (message) {
          $scope.currentPopover.hide();
          ChatService.blockUnblockHandle(message.handleName, localSenderId, false).then(function (res) {
            $log.debug("UnBlocked handle: " + message.handleName + " senderId: " + localSenderId);
            $cordovaToast.showLongCenter("The handle " + message.handleName + " has been unblocked. You will start receiving private messages from this handle.")
          });
        }

        var sendTextMessage = function (data, cb) {
          $log.debug("$rootScope.networkStatus: " + $rootScope.networkStatus);
          if (!$rootScope.networkStatus) {
            return;
          }
          if (data && data.length > 0) {
            $scope.inputMessage = "";
            var type = ConstsService.MESSAGE_TYPE_USER | ConstsService.MESSAGE_TYPE_TEXT;
            //$scope.chat.handleName will be defined only for Group chats
            _sendMessage(type, data, $scope.chat.handleName, null, {
              handleName: $scope.chat.handleName
            });
          } else {
            $log.debug("The data is null");
          }
        }

        $scope.sendMessage = function () {
          $log.debug("Input text is " + $scope.inputMessage);
          sendTextMessage($scope.inputMessage);
        };

        $scope.sendContentMessage = function (caption, contentName, contentIdPromise, contentType, localURL, fullPath, options) {
          $log.debug("$rootScope.networkStatus: " + $rootScope.networkStatus);
          if (!$rootScope.networkStatus) {
            return;
          }
          if (!options) {
            options = {};
          }
          angular.extend(options, {
            contentName: contentName,
            contentType: contentType,
            localURL: localURL,
            fullPath: fullPath,
            //$scope.chat.handleName will be defined only for Group chats
            handleName: $scope.chat.handleName
          });
          var type = ConstsService.MESSAGE_TYPE_USER | ConstsService.MESSAGE_TYPE_CONTENT;
          _sendMessage(type, caption, $scope.chat.handleName, contentIdPromise, options);
        };

        $scope.handleMessageClick = function (message) {
          if (message.contentId && message.dir === 0) {
            //DownloadController.download(message);
            $log.debug("handleMessageClick message is " + JSON.stringify(message));
            if (message.localURL) {
              $scope.$broadcast('openContent', message);
            } else if (!message.downloadInProgress) {
              $scope.$broadcast('downloadContent', message);
            }
          } else if (message.dir === 1 && message.localURL) {
            $log.debug("handleMessageClick message is " + JSON.stringify(message));
            $scope.$broadcast('openContent', message);
          } else if (Tring.isGroupInviteMessage(message)) {
            $log.debug("handleMessageClick message is " + JSON.stringify(message));
            if (message.dir === 0) {
              if (message.ordinaryInvite) {
                $scope.$broadcast('groupOrdinaryInviteClicked', message);

              } else {
                $scope.$broadcast('groupInviteClicked', message);
              }
            }
          } else if (Tring.isGroupMembershipRequestMessage(message)) {
            $log.debug("handleMessageClick message is " + JSON.stringify(message));
            //$scope.$broadcast('groupMembershipRequestClicked', message);
            NavigationService.goNativeToView('app.chat-ui.processMembershipRequest', {message: message});
          } else {
          }
        };

        $scope.deleteMessage = function (message) {
          $log.debug("Deleting message: " + JSON.stringify(message));
          $scope.currentPopover.hide();
          MessagesService.deleteMessage(message._id).then(function (result) {
            // Update UI accordingly
            $log.debug("deleteMessage result: " + JSON.stringify(result));
            var _index = Tring.findMessageForSenderId($scope.messages, message._id);
            $log.debug("The index obtained for delete message: " + _index);
            if (_index >= 0) {
              $scope.$evalAsync(function () {
                $scope.messages.splice(_index, 1);
              })

            }
          });
        };

        $scope.copyMessage = function (message) {
          $scope.currentPopover.hide();
          $cordovaClipboard
            .copy(message.data)
            .then(function () {
              $log.debug("Message data: " + message.data + " copied");
              $cordovaToast.showLongTop("Message copied successfully!");
            }, function () {
              $log.debug("Message data: " + message.data + " could not be copied");
            });
        };

        $scope.forwardMessage = function (message) {
          $log.debug("Forwarding message id: " + message._id);
          $scope.currentPopover.hide();
          var data = {
            "messageId": message._id
          }
          NavigationService.goNativeToView('app.chats', data, 'right');
        };

      $scope.needsPlayButton = function(message){
          return Tring.isContentMessage(message) && message.contentType.indexOf('image') !== 0 && (message.contentType.indexOf('video') !==0 || Tring.endsWith(message.contentType,'3gpp'));
      };

    $scope.getOpenerIcon = function (message) {
          var image;
          if (message.contentType) {
            if (message.contentType.indexOf('audio') === 0 || Tring.endsWith(message.contentType,'3gpp')) {
              image = 'ion-ios-play';
            } else if (message.contentType.indexOf('image') === 0) {
              image = '';
            } else if (message.contentType.indexOf('video') === 0 ) {
              image = '';
            } else {
              image = 'ion-android-open';
            }
          } else if (Tring.isGroupInviteMessage(message)) {
            image = '';
          }
          return image;
        };
        $scope.getIcon = function (message) {
          var image;
          if (message.contentType) {
            if (message.contentType.indexOf('audio') === 0) {
              image = 'ion-music-note';
            } else if (message.contentType.indexOf('image') === 0) {
              image = 'ion-image';
            } else if (message.contentType.indexOf('video') === 0) {
              image = 'ion-ios-film-outline';
            } else {
              image = 'ion-document';
            }
          } else if (Tring.isGroupInviteMessage(message)) {
            image = '';
          }
          return image;
        };

        var actions = null;
        var moderatorActions = null;
        var blockedMemberActions = null;
        var unblockedMemberActions = null;
        var moderatorHandleActions = null;
        var pvtMsgAction = {
          name: "Private Message",
          cb: "setPrivateMessageToMember",
          img: '<i class="icon ion-ios-email-outline"></i>'
        };
        var reportAbuseAction = {
          name: "Report Abuse",
          cb: "setReportAbuseMessage",
          img: '<i class="icon icon-notification"></i>'
        };
        $scope.popOvers = {};

        $scope.handleHandleNameClick = function ($event, message) {
          $log.debug("handleHandleNameClick message: " + JSON.stringify(message));
          $scope.currentCtxMessage = message;

          if (actions == null) {
            actions = [];
            actions.push({
              name: "Copy",
              cb: "copyMessage",
              img: '<i class="icon icon-copy-message"></i>'
            });

            actions.push({
              name: "Forward",
              cb: "forwardMessage",
              img: '<i class="icon icon-double-arrow"></i>'
            });

            actions.push({
              name: "Delete",
              cb: "deleteMessage",
              img: '<i class="icon icon-trash"></i>'
            });
          }

          function showPopOver(popoverType, actions) {
            var popOver = $scope.popOvers[popoverType];
            if (!popOver) {
              $log.debug("Popover of type not found: " + popoverType);
              var template = Tring.getPopOverTemplate(actions, "currentCtxMessage", 3);
              popOver = $ionicPopover.fromTemplate(template, {
                scope: $scope
              });
              $scope.popOvers[popoverType] = popOver;
            }
            else {
              $log.debug("Popover of type found: " + popoverType);
            }
            $scope.currentPopover = popOver;
            popOver.show($event);
          }

          if (Tring.isGroupSid(localSenderId) && message.dir !== 1) {
            ChatService.getSenderIdData(localSenderId, $scope.chat.sendertype).then(function (senderData) {
              var userRole = Tring.getGroupMembershipRole(senderData);
              if ((Tring.isO2MGroup(senderData) && userRole > ConstsService.MODERATOR_ROLE) || !message.handleName) {
                showPopOver("p2p", actions);
              } else {
                if (userRole <= ConstsService.MODERATOR_ROLE) {
                  if (moderatorActions == null) {
                    moderatorActions = [];
                    moderatorActions = moderatorActions.concat(actions);
                    moderatorActions.push(pvtMsgAction);
                  }
                  showPopOver("moderator", moderatorActions);
                } else {
                  ChatService.getHandleData(message.handleName, localSenderId, false).then(function (handleData) {
                    $log.debug("handleData: " + JSON.stringify(handleData));
                    if(!handleData.isModerator) {
                      if (handleData.blocked) {
                        if (blockedMemberActions == null) {
                          blockedMemberActions = [];
                          blockedMemberActions = blockedMemberActions.concat(actions);
                          blockedMemberActions.push(pvtMsgAction);
                          blockedMemberActions.push(reportAbuseAction);
                          blockedMemberActions.push({
                            name: "UnBlock",
                            cb: "unblockHandle",
                            img: '<i class="icon ion-ios-plus-outline"></i>'
                          });
                        }
                        showPopOver("blockedMember", blockedMemberActions);
                      } else {
                        if (unblockedMemberActions == null) {
                          unblockedMemberActions = [];
                          unblockedMemberActions = unblockedMemberActions.concat(actions);
                          unblockedMemberActions.push(pvtMsgAction);
                          unblockedMemberActions.push(reportAbuseAction);
                          unblockedMemberActions.push({
                            name: "Block",
                            cb: "blockHandle",
                            img: '<i class="icon ion-ios-minus-outline"></i>'
                          });
                        }
                        showPopOver("unblockedMember", unblockedMemberActions);
                      }
                    }
                    else {
                      if(moderatorHandleActions == null) {
                        moderatorHandleActions = [];
                        moderatorHandleActions = moderatorHandleActions.concat(actions);
                        moderatorHandleActions.push(pvtMsgAction);
                      }
                      showPopOver("moderatorHandle", moderatorHandleActions);
                    }
                  });
                }
              }
            });
          } else {
            showPopOver("p2p", actions);
          }
        }

        $scope.doUpload = function (data) {
          //var fs = new FileBrowseService();

          var onprogress = function (progressEvent) {
             //$scope.apply(function () {
             onProgressHandler(null, progressEvent, data.name);
             //});
         };
          $log.debug("Upload data captured: " + JSON.stringify(data));
          //$ionicLoading.show();
          var retVal = fs.upload(data.fullPath, {
            fileName: data.name,
            mimeType: data.type
          }, onprogress);
          var returnPromise = retVal.promise.then(function (result) {
            var contentId = JSON.parse(result.response).contentId;
            $log.debug('doUpload - result.response is ' + result.response);
            if ($scope.contentMessages.cancelUploadArray[data.name]) {
              $scope.contentMessages.cancelUploadArray[data.name] = undefined;
            }
            return contentId;
          }, function (err) {
            console.error("Error " + JSON.stringify(err) + " uploading captured file " + data.name);
            throw err;
            //return err;
          });
          if (!$scope.contentMessages.cancelUploadArray) {
            $scope.contentMessages.cancelUploadArray = {};
          }
          $scope.contentMessages.cancelUploadArray[data.name] = function () {
            retVal.cancel();
          };
          return returnPromise;
        };

        $scope.cancelUpload = function (contentName, messageId, event) {
          event.stopPropagation();
          if ($scope.contentMessages.cancelUploadArray && $scope.contentMessages.cancelUploadArray[contentName]) {
            $scope.contentMessages.cancelUploadArray[contentName]();
          } else {
            MessagesService.updateMessageInLocalDatabase(messageId, {
              uploadAborted: true
            });
          }
        };

        $scope.getClasses = function(message) {
          var classes = message.dir == 0? 'chat-message is-inbound ': 'chat-message is-outbound';

          if(Tring.isSystemMessage(message)) {
            classes += ' sys-msg';
          }
          else if(Tring.isPrivateMessageMember(message)) {
            classes += ' prvt-msg'
          }
          else if(Tring.isPrivateMessageModerator(message)) {
            classes += ' admin-msg';
          }
          return classes;
        }

        $scope.cancelDownload = function (messageId, event) {
          event.stopPropagation();
          if ($scope.contentMessages.cancelDownloadArray && $scope.contentMessages.cancelDownloadArray[messageId]) {
            $scope.contentMessages.cancelDownloadArray[messageId]();
          }
        };
        var onProgressHandler = function (evt, progressEvent, messageId) {
            var perc = "";
            if (progressEvent.lengthComputable) {
                perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
            }
            $scope.$evalAsync(function(){
               $scope.contentMessages.fileTransferProgress[messageId] = perc + " %";
            });
        };
        $scope.$on('fileTransferProgress', onProgressHandler);

      } else {
        //$state.go('login');
        NavigationService.goNativeToView('login', null, 'right');
      }
    }])
  .controller('ModerateHandleCtrl', ['$scope', '$state', '$stateParams', '$ionicHistory', 'MessagesService', 'ChatService', '$ionicScrollDelegate', 'SharedDataService', 'ConstsService', 'MqttBrokerService', 'AuthService', 'Tring', '$ionicLoading', 'FileBrowseService', '$q', '$rootScope', 'NetworkService', 'NavigationService', '$cordovaClipboard', '$cordovaToast', '$interval', '$http', '$ionicPopup', '$ionicPopover', '$log',
    function ($scope, $state, $stateParams, $ionicHistory, MessagesService, ChatService, $ionicScrollDelegate, SharedDataService, ConstsService, MqttBrokerService, AuthService, Tring, $ionicLoading, FileBrowseService, $q, $rootScope, NetworkService, NavigationService, $cordovaClipboard, $cordovaToast, $interval, $http, $ionicPopup, $ionicPopover, $log) {
      var device = AuthService.getDevice();
      if(device) {
        $log.debug("Device found in ModerateHandleCtrl: " + JSON.stringify(device));

        var handleModeratorMessage = function (moderatorMessage, reputationKey, handleName) {
          ChatService.getSenderIdData(ConstsService.GROUPS_START_KEY + $scope.data.gid, ConstsService.GROUP).then(function (group) {
            var userRole = Tring.getGroupMembershipRole(group);
            if (userRole !== ConstsService.MEMBER_ROLE) {
              var reputationData = {
                "moderatorMessage": moderatorMessage,
                "moderatorHandleName": group.handleName,
                "handleName": handleName,
                "reputationKey": reputationKey,
                "reputationValue": 1,
                "gid": $scope.data.gid
              }
              $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL, reputationData).then(function (response) {
                $log.debug("Updated the " + reputationKey + " count result: " + response.data.success);
                if(response.data.success) {
                  if(reputationKey === "warnings") {
                    $scope.$evalAsync(function () {
                      $scope.warningMessage = "";
                    });
                    $scope.$broadcast("incrementWarnings");
                    $cordovaToast.showLongCenter("The warning message has been sent to the handle: " + handleName);
                  }
                  else {
                    $scope.$evalAsync(function () {
                      $scope.evictionMessage = "";
                    });
                    $scope.$broadcast("incrementEvictions");
                    $cordovaToast.showLongCenter("The eviction message has been sent to the handle: " + handleName);
                    var pgTopic = group.pgTopic;
                    var pgid = group.pgid;
                    var sid = angular.isDefined(pgid) && pgid? pgid: ConstsService.GROUPS_START_KEY + $scope.data.gid;
                    var tgtTopic = angular.isDefined(pgTopic) && pgTopic? pgTopic: group.topic;
                    MqttBrokerService.getMessageId(sid).then(function (msgId) {
                      var msg = {
                        type: ConstsService.MESSAGE_TYPE_USER_REPUTATION,
                        userReputationMsgType: ConstsService.USER_REPUTATION_EVICTION_NOTIFICATION,
                        _id: msgId,
                        ts: moment().format('x'),
                        sid: sid,
                        data: "An eviction message has been sent to handle " + handleName + ": " + moderatorMessage,
                        status: 0, // Transit - 0, Unsent - -1, Sent - 1, Delivered - 2
                        dir: 1,
                        handleName: group.handleName
                      };
                      var cb = function () {
                        $scope.evictionMessageToModeratorsSent = true;
                        $log.debug("Private message to member successfully sent to topic: " + group.topic + "/pvt");
                      };
                      MqttBrokerService.sendDataMessage(msg, tgtTopic + "/pvt", false, cb);
                    });
                  }
                }
                else {
                  if(reputationKey === "warnings") {
                    $cordovaToast.showLongCenter("The warning message could not be sent to the handle: " + handleName);
                  }
                  else {
                    $cordovaToast.showLongCenter("The eviction failed since the handle: " + handleName + " could be already evicted.");
                  }
                }

              });
            }
            else {
              $cordovaToast.showLongCenter("This user should have the member role to be evicted")
            }
          });
        }

        $scope.evictionMessage = "";
        $scope.sendEvictionMessage = function () {
          var evictionMessage = $scope.evictionMessage;
          if(evictionMessage) {
            evictionMessage = evictionMessage.trim();
            if(evictionMessage.length > 0) {
              handleModeratorMessage(evictionMessage, "evictions", $scope.data.handleName);
            }
            else {
              $cordovaToast.showLongCenter("Please enter valid eviction message.");
            }
          }
          else {
            $cordovaToast.showLongCenter("Please enter valid eviction message.");
          }

        }

        $scope.warningMessage = "";
        $scope.sendWarningMessage = function () {
          var warningMessage = $scope.warningMessage;
          if(warningMessage) {
            warningMessage = warningMessage.trim();
            if(warningMessage.length > 0) {
              handleModeratorMessage(warningMessage, "warnings", $scope.data.handleName);
            }
            else {
              $cordovaToast.showLongCenter("Please enter valid warning message.");
            }
          }
          else {
            $cordovaToast.showLongCenter("Please enter valid warning message.");
          }

        }

        $scope.userNote = "";
        $scope.notes = [];
        $scope.sendUserNote = function () {
          var notesData = {
            "notes": $scope.userNote,
            "handleName": $scope.data.handleName,
            "gid": $scope.data.gid
          }

          $http.post(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_NOTES_URL, notesData).then(function (response) {
            $log.debug("Updated the notes.");

            $scope.$evalAsync(function () {
              $scope.notes.splice(0, 0, {
                notes: $scope.userNote
              });
              $scope.userNote = "";
            });
            $cordovaToast.showLongCenter("Notes have been updated");

          });
        }

        var getNotes = function() {
          $http.get(ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_NOTES_URL + "/" + $scope.data.handleName + "/" + $scope.data.gid + "?adminContext="+$scope.adminContext).then(function (response) {
            $log.debug("Got the notes as ." + JSON.stringify(response.data));
            var success = response.data.success;
            if(success) {
              $scope.$evalAsync(function () {
                $scope.notes = response.data.userNotesInfo;
              });
            }
            else {
              $scope.$evalAsync(function () {
                $scope.moderationEnabled = false;
              });
            }
          });
        };

        $scope.stopClickPropagation =function(evt) {
            console.log(JSON.stringify(evt));
        };

        $scope.moderationEnabled = true;

        getNotes();
      }
    }]);
