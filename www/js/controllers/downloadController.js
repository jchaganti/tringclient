angular.module('tring.download.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop', 'Tring', 'tring.appServices'])
    .controller('DownloadController', ['$scope', '$cordovaFile', '$ionicLoading', 'ConstsService', 'FileBrowseService', '$cordovaFileOpener2', '$ionicPopover', 'MessagesService', '$cordovaToast', '$q', 'ContactsService', 'NavigationService', 'Tring', 'GroupService', '$rootScope', '$timeout', '$log', function ($scope, $cordovaFile, $ionicLoading, ConstsService, FileBrowseService, $cordovaFileOpener2, $ionicPopover, MessagesService, $cordovaToast, $q, ContactsService, NavigationService, Tring, GroupService, $rootScope, $timeout, $log) {

        var download = function (message, forProfile) {
            var storageDirectory = cordova.file.documentsDirectory;
            if (ionic.Platform.isAndroid()) {
                //storageDirectory = cordova.file.externalRootDirectory;
                storageDirectory = cordova.file.externalApplicationStorageDirectory;
                if (!storageDirectory) {
                    storageDirectory = cordova.file.externalRootDirectory;
                    if (!storageDirectory) {
                        storageDirectory = cordova.file.dataDirectory;
                        if (!storageDirectory) {
                            console.error("storageDirectory error");
                            $q.reject("storageDirectory error");
                        }
                    }
                }
            }
            $log.debug("Download message: " + JSON.stringify(message));
            var fs = new FileBrowseService();
            var name = message.contentId ? message.contentId : message._id;
            var ext = fs.getExtension(message);
            name = name + ext;
            var result = $q.when(message);

            var onprogress = function (progressEvent) {
                $rootScope.$broadcast('fileTransferProgress', progressEvent, name);
            };
            if (message.contentId) {
                var localPath = storageDirectory + name;
                $log.debug('DownloadController: externalRootDirectory ' + localPath);
                var retVal = fs.download(message.contentId, localPath, {}, null);

                if ($scope.contentMessages.cancelDownloadArray) {
                    $scope.contentMessages.cancelDownloadArray[name] = function () {
                        retVal.cancel();
                    };
                }
                result = retVal.promise.then(function (response) {
                    $log.debug('Success downloading ' + JSON.stringify(response));
                    $log.debug('response.toURL ' + JSON.stringify(response.toURL()));
                    message.downloadInProgress = false;
                    message.localURL = localPath;
                    return message;
                }, function (err) {
                    message.downloadInProgress = false;
                    console.error('Failure downloading - error is ' + err);
                    $cordovaToast.showLongTop('Failed to download - please check network connection!');
                    result = $q.reject(err);
                });
                //Only when download is being done for profile content download,
                //we want to time out the download after some time
                if (forProfile) {
                    $timeout(function () {
                        retVal.cancel();
                    }, ConstsService.HTTP_CONTENT_DOWNLOAD_TIMEOUT);
                }
                message.downloadInProgress = true;
            }
            return result;
        };

        var open = function (message) {
            var contentType = message.contentType ? message.contentType : 'text/plain'; //for now
            var fullpath = message.fullPath;
            window.resolveLocalFileSystemURL(message.localURL, function (entry) {
                $cordovaFileOpener2.open(entry.nativeURL, contentType).then(function () {
                    $log.debug("Success opening content");
                }, function (err) {
                    $cordovaFileOpener2.open(fullpath,contentType).then(function () {
                      $log.debug("Success opening content");
                    },function(err1) {
                      console.error("Failed to open content - err1 is " + JSON.stringify(err1));
                      $cordovaToast.showLongTop('Failed to open content !');
                    });
                });
            }, function (e) {
                console.log('File Not Found');
            });

        };

        $scope.$on('openContent', function (evt, message) {
            $log.debug("Message received - " + message.data + " and " + message.contentType);
            open(message);
        });

        $scope.$on('downloadContent', function (evt, contentContainer) {
            var forProfile = false;
            $log.debug("Message received - " + contentContainer.data + " and " + contentContainer.contentId);
            download(contentContainer, forProfile).then(function (message) {
                    //Update the message data with localPath so that next open is quicker
                    //Note this is ASYNCHRONOUS
                    MessagesService.updateMessageInLocalDatabase(message._id, message);
                    return message;
                })
                .then(open);
        });

        $scope.go = function (path, params) {
            NavigationService.goNativeToView(path, params, 'left');
            //$log.debug($ionicHistory.viewHistory());
        };

        $scope.$on('downloadAndOpenProfileContent', function (evt, contentContainer) {
            var downloadResult = $q.when(contentContainer);
            var forProfile = true;
            var fs = new FileBrowseService();
            var downloadFunction = function () {
                return download(contentContainer, forProfile).then(function (contentBag) {
                    $log.debug("After profile image download, contentBag is " + JSON.stringify(contentBag));
                    $ionicLoading.hide();
                    var result =
                        Tring.isContactObject(contentContainer) ? ContactsService.saveContact(contentBag) : GroupService.storeGroup(contentBag);
                    return result;
                });
            };
            if (!contentContainer.localURL) {
                $cordovaToast.showLongTop("Please wait while content is being downloaded");
                $ionicLoading.show();
                downloadResult = downloadFunction();
            } else {
                downloadResult = fs.getFileStats(contentContainer.localURL).then(function (contentInfo) {
                    if (!contentInfo.contentSize || contentInfo.contentSize <= 0) {
                        return $q.reject("size 0");
                    } else {
                        return contentContainer;
                    }
                }).then(null, function (err) {
                    return downloadFunction();
                });
            }
            downloadResult.then(function (contentBag) {
                $log.debug("Contact content localURL received - " + contentBag.localURL);
                //var isGroup = Tring.isGroupObject(contentBag);
                if (!contentBag.contentType) {
                    contentBag.contentType = 'image/jpg';
                }
                $scope.$broadcast("openContent", contentBag);
                //                $scope.go('app.profile-ro', {
                //                    readOnly: true,
                //                    contact: isGroup ? null : contentBag,
                //                    group: isGroup ? contentBag : null
                //                });
            });
        });
  }]);
