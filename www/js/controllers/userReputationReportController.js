angular.module('tring.userReputationReport.controller', ['tring.dbService', 'tring.authService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
  .controller('UserReputationReportController', ['$scope', '$state', '$stateParams', 'MessagesService', 'ChatService', '$ionicScrollDelegate', 'SharedDataService', 'ConstsService', 'MqttBrokerService', 'AuthService', 'Tring', '$ionicLoading', 'FileBrowseService', '$q', '$rootScope', 'NetworkService', 'NavigationService', '$cordovaClipboard', '$cordovaToast', '$http', '$log',
    function ($scope, $state, $stateParams, MessagesService, ChatService, $ionicScrollDelegate, SharedDataService, ConstsService, MqttBrokerService, AuthService, Tring, $ionicLoading, FileBrowseService, $q, $rootScope, NetworkService, NavigationService, $cordovaClipboard, $cordovaToast, $http, $log) {

      $scope.showReputation = false;
      $scope.fromSelfProfileScreen = $scope.inProfileScreen && !$scope.readOnly;
      if ($scope.fromSelfProfileScreen || $scope.processingGroupMembershipRequest) {
        $scope.showReputation = true;
      }

      var getReputationFromServer = function () {
        var url;
        $log.debug("0");
        if ($scope.fromSelfProfileScreen) {
          //var myUid = Tring.getIdFromSid(ConstsService.USERS_START_KEY, Tring.getSenderId(AuthService.getDevice().p2p));
          url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.USER_REPUTATION_URL;
        } else if ($scope.processingGroupMembershipRequest) {
          url = ConstsService.MEMBERSHIP_SERVER_URL + ConstsService.HANDLE_REPUTATION_URL;
          url = url.replace('@0', $scope.data.gid).replace('@1', $scope.data.handleName);
        }

        return $http.get(url).then(function (response) {
          $log.debug("1");
          if (response && response.data) {
            $log.debug(JSON.stringify(response));
            var data = response.data;
            $scope.warnings = data.warnings;
            $scope.evictions = data.evictions;
            $scope.abusesReported = data.abuses;

          }
        }).catch(function (err) {
          $log.debug("Error: getting reputation info from server - err is " + JSON.stringify(err));
        });
      };

      getReputationFromServer();
      $scope.$on('incrementWarnings', function () {
        $scope.$evalAsync(function () {
          $scope.warnings = $scope.warnings + 1;
        })
      });

      $scope.$on('incrementEvictions', function () {
        $scope.$evalAsync(function () {
          $scope.evictions = $scope.evictions + 1;
        })
      });
    }]);
