angular.module('tring.intro.controller', ['tring.navigationService'])
    .controller('IntroController', ['$scope', '$ionicSlideBoxDelegate', '$stateParams', 'NavigationService', '$timeout', 'AuthService', 'UserService', '$log',
    function ($scope, $ionicSlideBoxDelegate, $stateParams, NavigationService, $timeout, AuthService, UserService, $log) {

            //Since this is now the first page seen by the user after registration the following is required here
            //also in addition to ChatController.
            var device = AuthService.getDevice();
            $log.debug("Device found in searchViewCtr " + JSON.stringify(device));
            //Commenting for affiliate marketing version
            /*if (device) {
                if (!device.bootstrapState || device.bootstrapState < 2) {
                    //kick in bootstrapping of contacts
                    $log.debug("kick in bootstrapping of contacts");
                    UserService.bootstrapContacts();
                } else if (device.bootstrapState < 3) {
                    //kick in bootstrapping of profile info
                    $log.debug("kick in bootstrapping of profile info");
                    UserService.bootstrapProfileInfo();
                }
            }*/
            //$scope.groups=[{name:"abc"},{name: "ert"}];
            $scope.justStarting = $stateParams.justStarting;
            $log.debug($scope.justStarting + " start Con");
            $scope.startApp = function () {
                NavigationService.goNativeToView('app.discover');
            };
            $scope.slideChanged = function (index) {
                if (index === 5) {
                    $timeout(function () {
                        $scope.startApp();
                    }, 2500);
                }
            };
}]);
