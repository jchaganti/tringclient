angular.module('tring.capture.controller', ['tring.authService', 'tring.dbService', 'tring.appServices', 'Tring', 'tring.constService', 'tring.userService', 'tring.chatService', 'tring.messagesService', 'tring.mqttBrokerService', 'tring.fileServices', 'tring.sharedDataService', 'tring.directives', 'tring.thumbnailService', 'tring.networkService', 'tring.navigationService', 'jrCrop'])
    .controller('CaptureController', ['$scope', '$ionicHistory', '$q', 'SharedDataService', 'ConstsService', '$cordovaCapture', 'ThumbnailService', '$ionicPopup', 'FileBrowseService', '$cordovaCamera', '$log', 'NetworkService', '$window', 'DBService', '$cordovaToast', 'Tring','$timeout', function ($scope, $ionicHistory, $q, SharedDataService, ConstsService, $cordovaCapture, ThumbnailService, $ionicPopup, FileBrowseService, $cordovaCamera, $log, NetworkService, $window, DBService, $cordovaToast, Tring, $timeout) {
        $scope.captureToggle = false;
        $scope.captureState = false;
        $scope.toggle = function (input) {
            if (!input) {
                if(!this.captureToggle) {
                  this.captureToggle = true;
                }
                this.captureState = !this.captureState;
            } else {
                $scope.captureToggle = false;
            }
          var elem = angular.element(document.querySelector('#multiMediaBtns'));
          if(elem) {
            var that = this;
            $timeout(function() {
              if(that.captureState) {
                if(!elem.hasClass('fadeIn')) {
                  elem.addClass('fadeIn');
                }
                if(elem.hasClass('fadeOut')) {
                  elem.removeClass('fadeOut');
                }

              }
              else {
                if(!elem.hasClass('fadeOut')) {
                  elem.addClass('fadeOut');
                }
                if(elem.hasClass('fadeIn')) {
                  elem.removeClass('fadeIn');
                }
              }
            }, 50);

          }
        };

        $scope.getCaptureClass = function() {
         return $scope.captureToggle? 'on': 'fadeOut';
        }

        var fs = new FileBrowseService();

        var createAppStateBeforePause = function (key) {
            var mbrDb = DBService.mbrDb();
            if (!key) {
                key = ConstsService.APP_STORAGE_KEY;
            }
            var appState = {};
            appState.capturing = true;
            appState.data = {
                senderId: $scope.$parent.senderId,
                localSenderId: $scope.$parent.localSenderId
            };
            appState.routeState = 'app.chat-ui';
            return mbrDb.get(key).then(function (obj) {
                return angular.extend(obj, {
                    appState: appState.data,
                    appRouteState: appState.routeState
                });
            }).then(null, function (err) {
                return {
                    "_id": key,
                    appState: appState.data,
                    appRouteState: appState.routeState
                };
            }).then(function (obj) {
                $log.debug("storing app state before capture - state is " + JSON.stringify(obj));
                return mbrDb.put(obj);
            }).then(function () {
                $log.debug("after capture onPause");
                NetworkService.setOnResumeFunction(key, appState);
                return true;
            });
        };

        $scope.$on("processCapturedResult", function (evt, results) {
            uploadAndSendMessage(results);
        });

        var getFileDataFromUri = function (uri) {
            $log.debug("getFileDataFromUri output uri - " + uri);

            return fs.getFileStats(uri).then(function (contentInfo) {
                if (!contentInfo.contentSize || contentInfo.contentSize <= 0) {
                    return $q.reject("size 0");
                } else {
                    var data = {};
                    data.localURL = contentInfo.localURL;
                    data.fullPath = contentInfo.fullPath;
                    data.size = contentInfo.contentSize;
                    data.type = contentInfo.contentType;
                    data.name = contentInfo.contentName;
                    return $q.resolve([data]);
                }
            }).then(null, function (err) {
                return $q.reject(err);
            });
        };

        //        var isMediaUrl = function (data) {
        //            return data.fullPath.indexOf('content://media') === 0 ||
        //                !('size' in data);
        //        };

        var uploadAndSendMessage = function (data_) {
            //appState.capturing = false;
            $scope.captureToggle = false;
            var data = data_[0];
            var dataWithFullPath;
            //For whatever reason if 'data.fullPath' is  a media url then it needs to be
            //converted to a native FS path. This happens  after selecting the media file
            //during File choosing.
            if (data.fromFileChooser) {
                dataWithFullPath = getFileDataFromUri(data.fullPath).then(function (dataArr) {
                    if (dataArr) {
                        return dataArr[0];
                    } else {
                        return $q.reject("error");
                    }
                }, function (err) {
                    $cordovaToast.showLongCenter("File Chooser does not support this path format");
                    return $q.reject(err);
                });
            } else if (data.fullPath.indexOf('file:///') < 0) {
                data.fullPath = data.fullPath.replace('file:/', 'file:///');
                dataWithFullPath = $q.when(data);
            } else {
                dataWithFullPath = $q.when(data);
            }
            dataWithFullPath.then(function (data) {
                var thumbnailFunction = ThumbnailService.getThumbnailFunction(data.type || data.contentType);

                var confirmUpload = function (thumbnailDataUrl) {
                    var confirmUploadOptions = fs.confirmContentUploadOptions($scope, data.name, data.type, data.size, thumbnailDataUrl);
                    return $ionicPopup.show(confirmUploadOptions).
                    then(function (caption) {
                        if (caption.error) {
                            throw -1;
                        }
                        $log.debug('File upload confirm exited');
                        return $q.resolve({
                            size: data.size,
                            type: data.type,
                            caption: caption,
                            thumbnailData: thumbnailDataUrl
                        });
                    }).then(null, function (err) {
                        return $q.reject(err);
                    });

                };

                function contentMessageSend(contentPromise, caption, thumbnailData) {

                    $scope.sendContentMessage(caption, data.name, contentPromise, data.type, data.localURL, data.fullPath, {
                        thumbnailData: thumbnailData
                    });
                }
                return thumbnailFunction(data.fullPath, 100).
                then(confirmUpload, function (err) {
                    $log.debug("Error in thumnail generation - err " + JSON.stringify(err));
                    return confirmUpload("");
                }).
                then(function (contentMetaData) {
                    var contentPromise = $scope.doUpload(data);
                    var caption = contentMetaData.caption;
                    var thumbnailData = contentMetaData.thumbnailData;
                    return contentMessageSend(contentPromise, caption, thumbnailData);
                });
            });
        };

        $scope.captureAudio = function () {
            $scope.captureToggle = false;
            var options = {
                limit: 1,
                duration: 300
            };

            var appState = createAppStateBeforePause();
            //NetworkService.setOnPauseFunction(ConstsService.APP_STORAGE_KEY, appState);
            //NetworkService.setOnResumeFunction(ConstsService.APP_STORAGE_KEY, appState);

            appState.then(function () {
                return $cordovaCapture.captureAudio(options);
            }).then(ensureTypeIsDefined).then(uploadAndSendMessage,
                function (err) {
                    // An error occurred. Show a message to the user
                    console.error("Audio error ! " + err);
                    //appState.capturing = false;
                });

        };
        $scope.captureVideo = function () {
            $scope.captureToggle = false;
            var options = {
                limit: 1,
                duration: 30
            };
            $log.debug("Calling captureVideo");

            var appState = createAppStateBeforePause();
            //NetworkService.setOnPauseFunction(ConstsService.APP_STORAGE_KEY, appState);
            //NetworkService.setOnResumeFunction(ConstsService.APP_STORAGE_KEY, appState);

            appState.then(function () {
                return $cordovaCapture.captureVideo(options);
            }).then(ensureTypeIsDefined).then(uploadAndSendMessage, function (err) {
                // An error occurred. Show a message to the user
                console.error("Video error ! " + err);
                //appState.capturing = false;
            });
        };
        var ensureTypeIsDefined = function (_data) {
            if (_data && _data[0] && angular.isUndefined(_data[0].type)) {
                _data[0].type = Tring.mimeType(_data[0].fullPath);
            }
            return _data;
        };

        $scope.capturePicture = function () {
            $scope.captureToggle = false;
            var options = {
                limit: 1
            };
            $log.debug("Calling captureImage");

            var appState = createAppStateBeforePause();
            //NetworkService.setOnPauseFunction(ConstsService.APP_STORAGE_KEY, appState);
            //NetworkService.setOnResumeFunction(ConstsService.APP_STORAGE_KEY, appState);

            appState.then(function () {
                return $cordovaCapture.captureImage(options);
            }).then(ensureTypeIsDefined).then(uploadAndSendMessage, function (err) {
                // An error occurred. Show a message to the user
                console.error("Image error ! " + JSON.stringify(err));
                //appState.capturing = false;
            });
        };
        $scope.chooseAndUploadFile = function () {
            var appState = createAppStateBeforePause(ConstsService.CONTENT_BROWSER_STORAGE_KEY);
            appState.then(function () {
                //Here I am using a slightly different pattern which is more useful
                //to convert callback based APIs into promises.
                return fs.fileChooser();
            }).then(uploadAndSendMessage, function (err) {
                // An error occurred. Show a message to the user
                console.error("File error ! " + JSON.stringify(err));
            });

        };
                    }]);
