/**
 * Created by jchaganti on 01/02/16.
 */

if (typeof Paho === "undefined") {
  Paho = {};
}

Paho.MQTT = (function (global) {
  var Client = function (host, port, path, clientId) {
    this._host = host;
    this._port = port;
    this._path = path;
    this._clientId = clientId;
    this._onConnectionLost = null;
    this._onMessageArrived = null;
    this._onMessageDelivered = null;
    this._trace = null;

    this.connect = function (connectOptions) {
      var successCB = connectOptions.onSuccess;

      if(angular.isFunction(successCB)) {
        successCB();
      }

      console.log("Mocking connect");
    }

    this.subscribe = function (filter, subscribeOptions) {
      console.log("Mocking subscribe");
    }

    this.unsubscribe = function (filter, unsubscribeOptions) {
      console.log("Mocking unsubscribe");
    }

    this.send = function (topic, payload, qos, retained) {
      console.log("Mocking send");
    }

    this.disconnect = function () {
      console.log("Mocking disconnect");
    }

    this.isConnected = function () {
      return true;
    }
  }

  var Message = function (newPayload) {
    var payload;
    if (typeof newPayload === "string" || newPayload instanceof ArrayBuffer || newPayload instanceof Int8Array || newPayload instanceof Uint8Array || newPayload instanceof Int16Array || newPayload instanceof Uint16Array || newPayload instanceof Int32Array || newPayload instanceof Uint32Array || newPayload instanceof Float32Array || newPayload instanceof Float64Array) {
      payload = newPayload;
    } else {

    }

    this._getPayloadString = function () {
      return payload;
    };

    this._getPayloadBytes = function () {
      return payload;
    };

    var destinationName = undefined;
    this._getDestinationName = function () {
      return destinationName;
    };
    this._setDestinationName = function (newDestinationName) {
      if (typeof newDestinationName === "string")
        destinationName = newDestinationName;
      else {
      }
    };

    var qos = 0;
    this._getQos = function () {
      return qos;
    };
    this._setQos = function (newQos) {
      if (newQos === 0 || newQos === 1 || newQos === 2)
        qos = newQos;
      else {
      }
    };

    var retained = false;
    this._getRetained = function () {
      return retained;
    };
    this._setRetained = function (newRetained) {
      if (typeof newRetained === "boolean")
        retained = newRetained;
      else {
      }
    };

    var duplicate = false;
    this._getDuplicate = function () {
      return duplicate;
    };
    this._setDuplicate = function (newDuplicate) {
      duplicate = newDuplicate;
    };
  };

  Message.prototype = {
    get payloadString() {
      return this._getPayloadString();
    },
    get payloadBytes() {
      return this._getPayloadBytes();
    },

    get destinationName() {
      return this._getDestinationName();
    },
    set destinationName(newDestinationName) {
      this._setDestinationName(newDestinationName);
    },

    get qos() {
      return this._getQos();
    },
    set qos(newQos) {
      this._setQos(newQos);
    },

    get retained() {
      return this._getRetained();
    },
    set retained(newRetained) {
      this._setRetained(newRetained);
    },

    get duplicate() {
      return this._getDuplicate();
    },
    set duplicate(newDuplicate) {
      this._setDuplicate(newDuplicate);
    }
  };


  Client.prototype = {
    get host() {
      return this._host;
    },
    set host(newHost) {
      this._host = newHost;
    },

    get port() {
      return this._port;
    },
    set port(newPort) {
      this._port = newPort;
    },

    get path() {
      return this._path;
    },
    set path(newPath) {
      this._path = newPath;
    },

    get clientId() {
      return this._clientId;
    },
    set clientId(newClientId) {
      this._clientId = newClientId;
    },

    get onConnectionLost() {
      return this._onConnectionLost;
    },
    set onConnectionLost(newOnConnectionLost) {
      this._onConnectionLost = newOnConnectionLost;
    },

    get onMessageDelivered() {
      return this._onMessageDelivered;
    },
    set onMessageDelivered(newOnMessageDelivered) {
      this._onMessageDelivered = newOnMessageDelivered;
    },

    get onMessageArrived() {
      return this._onMessageArrived;
    },
    set onMessageArrived(newOnMessageArrived) {
      this._onMessageArrived = newOnMessageArrived;
    },

    get trace() {
      return this._trace;
    },
    set trace(newTraceFunction) {
      this._trace = newTraceFunction;
    }

  };

  return {
    Client: Client,
    Message: Message
  };
})(window);
