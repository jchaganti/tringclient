Important build notes

1. PLease ensure the following lines are copied to Android manifest file, before building the apk

<uses-permission android:name="android.permission.CAMERA" />
    <uses-feature android:name="android.hardware.camera" />
    <uses-feature android:name="android.hardware.camera.autofocus" />

If we don’t add these lines, the permissions are not being asked and since we don’t submit this file, i am asking you to add. Please do that without fail.
the permissions are not being asked for CAMERA

2. Please note that the following is the plugin list on Sachin's setup as of 2-Jun-2016

com.cordova.plugins.sms 0.1.6 "Cordova SMS Plugin"
com.dbaq.cordova.contactsPhoneNumbers 0.0.5 "Contacts Phone Numbers"
com.verso.cordova.clipboard 0.1.0 "Clipboard"
cordova-plugin-actionsheet 2.2.0 "ActionSheet"
cordova-plugin-camera 2.2.0 "Camera"
cordova-plugin-compat 1.0.0 "Compat"
cordova-plugin-console 1.0.2 "Console"
cordova-plugin-crosswalk-webview 1.7.0 "Crosswalk WebView Engine"
cordova-plugin-device 1.1.0 "Device"
cordova-plugin-dialogs 1.2.0 "Notification"
cordova-plugin-file 4.2.0 "File"
cordova-plugin-file-opener2 2.0.2 "File Opener2"
cordova-plugin-file-transfer 1.5.0 "File Transfer"
cordova-plugin-image-picker 1.1.1 "ImagePicker"
cordova-plugin-inappbrowser 1.3.0 "InAppBrowser"
cordova-plugin-media 2.3.0 "Media"
cordova-plugin-media-capture 1.3.1-dev "Capture"
cordova-plugin-network-information 1.2.0 "Network Information"
cordova-plugin-photokandy-video-thumbnail 2.1.0 "Video Thumbnail"
cordova-plugin-splashscreen 3.2.2 "Splashscreen"
cordova-plugin-whitelist 1.2.2 "Whitelist"
cordova-plugin-x-toast 2.3.1 "Toast"
io.litehelpers.cordova.sqlite 0.7.7 "Cordova sqlite storage plugin"
ionic-plugin-keyboard 1.0.8 "Keyboard"
phonegap-plugin-push 1.6.2 "PushPlugin"

3. Ensure that you have this lintOption in your android build.gradle file, to avoid translation errors while building
non-debug apk
android {
  lintOptions {
    disable 'MissingTranslation'
   }
}

Protractor and Appium install notes:

To install the Protractor Appium setup please install following softwares.
1) Node
2) Jasmin
3) Protractor
4) Appium
5) wd
5) wd-bridge

Build your apk file

Start Appium server

Change the apk file path and deviceName in protractor-config.js file.
Run Protractor protractor-config.js command
